<<effects>>

You call out from your hiding place. No one responds at first. You hear music playing through the walls, but it's faint, and you're not sure how well your voice carries here.
<br><br>

<<generate1>><<person1>>
You're about to give up when a <<person>> dressed in servant's garb rounds the corner. <<He>> stops when <<he>> sees you peeking from the pantry.
<br><br>

<<if $submissive gte 1150>>
	"P-please help," you say. "I'm indecent from the waist down."
<<elseif $submissive lte 850>>
	"You got a towel or anything?" you ask.
<<else>>
	"Could you give me a towel or something please?" you ask.
<</if>>
<br><br>

<<if $rng gte 51>>
	<<He>> looks confused for a moment, then blushes as realisation dawns. <<He>> leaves the way <<he>> came, and a moment later returns carrying a towel. <<He>> hands it to you, and leaves without a word.
	<br><br>
	<<towelup>>
	<<endevent>>
	<<link [[Next|Avery Party End Alone]]>><</link>>
	<br>
<<else>>
	<<He>> smiles at you. "Sure thing," <<he>> says, walking closer and pulling a towel from <<his>> pocket. "I've got a towel right here." <<He>> darts forward as <<he>> reaches the pantry door, <span class="red">shoving you in.</span> <<He>> closes the door behind <<himstop>>
	<br><br>

	"Fucking <<bitch>>," <<he>> snarls. "Wearing those nice clothes. You think you're better than us. Well, no one can hear you now. I'm gonna put you in your place."
	<br><br>

	<<link [[Next|Avery Party Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<</if>>