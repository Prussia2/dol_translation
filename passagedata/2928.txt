<<effects>>

You lean back in your chair and hope you can endure the experience. Doing anything productive right now is out of the question. It's a struggle to even think straight, but you do your best to distract yourself by counting things. It's going well until you glance at the clock and realise only a couple of minutes have passed.
<br><br>

By the time the drug's effects begin to fade your muscles ache from the tension. You realise that your teeth have been clenched too. You're still aroused and frustrated. At least you can think straight again.
<br><br>

<<link [[Next|School Library]]>><</link>>
<br>