<<widget "eventsdrain">><<nobr>>
	<<set $rng to random(1, 100)>>
	<<if $rng gte 80 and $hallucinations gte 2 and $tentacledisable is "f">>
		Several tentacles rise from the water, blocking your path and trapping you on the walkway.
		<br><br>
		<<link [[Next|Drain Tentacles]]>><<set $molestationstart to 1>><</link>>
		<br>
	<<elseif $rng gte 30>>
		A torrent of water rushes through a pipe to your right, colliding with you and knocking you into the canal!
		<br><br>
		<<set $worn.upper.integrity -= 10>><<set $worn.lower.integrity -= 10>><<set $worn.under_lower.integrity -= 10>><<set $pain += 5>>
		<<link [[Next|Drain Water]]>><</link>>
	<<else>>
		A torrent of water rushes through a pipe to your right, colliding with you and knocking you into the canal!
		<br><br>
		<<set $worn.upper.integrity -= 10>><<set $worn.lower.integrity -= 10>><<set $worn.under_lower.integrity -= 10>><<set $pain += 5>>
		You are sucked to the drain floor, and through a gap in the concrete. You are pulled deeper through the dark until you emerge in a dim light. The water leaves you sprawled on the stone floor.
		<br><br>
		<<sewersstart>>
		<<if $sewersintro is 1>>
			<<if $bus is "residentialdrain">>
				<<link [[Climb down|Sewers Residential]]>><</link>>
				<br><br>
			<<elseif $bus is "commercialdrain">>
				<<link [[Climb down|Sewers Commercial]]>><</link>>
				<br><br>
			<<else>>
				<<link [[Climb down|Sewers Industrial]]>><</link>>
				<br><br>
			<</if>>
		<<else>>
			<<link [[Next|Sewers Intro]]>><</link>>
			<br>
		<</if>>
	<</if>>
<</nobr>><</widget>>