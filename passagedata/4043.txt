<<set $outside to 0>><<set $location to "town">><<effects>>

<<npc Robin>>You continue between houses, gathering more and more treats. The sun hangs low in the sky. "We should head back," Robin says. "I think the orphans are getting tired."
<br><br>
You return to the orphanage, bags full of sweets.
<<ghope>><<hope 1>>

<br><br>
<<link [[Next|Robin Trick 7]]>><<pass 60>><</link>>
<br>