You stroll across the sand and splash through waves. The sun feels warm and gentle against your bare skin.
<br><br>
<<generate1>><<person1>>
On a slightly calmer part of the beach, your path takes you near a <<person>> scurrying to get <<his>> expensive looking camera ready.

<br><br>
<<if $uncomfortable.nude is true>>
	<<link [[Take a Detour|Beach Strip End]]>><</link>>
	<br>
<</if>>
<<link [[Walk past|Beach Strip Camera Snap]]>><<set $phase to 0>><</link>>
<br>
<<if $uncomfortable.nude is false>>
	<<link [[Stop and Pose|Beach Strip Camera Snap]]>><<set $phase to 1>><</link>><<exhibitionist5>>
	<br>
	<<if $arousal gte 5000>>
		<<link [[Stop and Pose Lewdly|Beach Strip Camera Snap]]>><<set $phase to 2>><</link>><<exhibitionist5>><<promiscuous1>>
		<br>
	<</if>>
<</if>>