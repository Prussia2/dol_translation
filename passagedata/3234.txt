<<effects>>

<<if $submissive gte 1150>>
	"I-I just came out to give your <<wallet>> back," you say. "I don't want any trouble."
<<elseif $submissive lte 850>>
	"It's the middle of the night," you say. "Keep the noise down."
<<else>>
	"Sorry," you say. "I need to get to bed."
<</if>>
<br><br>

"Cute voice," says the <<if $pronoun is "m">>man<<else>>woman<</if>>.
<<if $rng gte 51>>
	A <<person1>><<person>> appears at the top of the stairwell, looking down at you. "And a cuter body. Guys, come look." The others stick their heads over the railing, looking down at you.
	<<covered>><<fameexhibitionism 4>>
	<br><br>

	You walk up the stairs. At least you'll feel less trapped up there. The <<person>> grasps your arm and pulls you in front of <<himcomma>> exposing you to <<his>> friends.
	<br><br>

	"So you're some kind of pervert, exposing yourself for the thrill?" <<he>> whispers. "Don't worry. We'll help get you off."
	<br><br>

	<<link [[Next|Street Ex Gang Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<<else>>
	"You live here? I guess it's late. See ya around, helpful stranger." You hear them walk away, and breathe a sigh of relief.
	<br><br>
	<<endevent>>
	<<destinationeventend>>
<</if>>