<<set $outside to 0>><<set $location to "underground">><<effects>>

<<if $submissive gte 1150>>
You stare meekly at your feet, but refuse their demand.
<<elseif $submissive lte 850>>
You stare out at the crowd in defiance, and refuse their demand.
<<else>>
Though afraid, you refuse their demand.
<</if>>

The crowd starts booing. A <<generate1>><<person1>><<person>> runs on stage. "Please accept our apologies," <<he>> says, picking up your leash.

<<if $rng gte 51 and $bestialitydisable is "f">>

"Instead of our scheduled show, we have a special treat for you." A metal shutter opens behind you, revealing a gigantic pig.
<br><br>
<<endevent>>
<<link [[Next|Underground Stage Pig]]>><<set $molestationstart to 1>><</link>>
<br>

<<else>>

"Instead of our scheduled show, we invite you to watch our punishment of this insolent <<bitchstop>>"
<br><br>

<<link [[Next|Underground Stage Molestation]]>><<set $molestationstart to 1>><<set $phase to 1>><</link>>
<br>

<</if>>