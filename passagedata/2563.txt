<<effects>>

<<earnFeat "Wrong Size">>

The locks prove no obstacle. You dash between the changing rooms, carrying armfuls of clothes, making sure the coast is clear each time. You've soon replaced every student's clothing.
<br><br>

<<if $schoolrep.crossdress gte 2>>
	<span class="green">The rumours of your crossdressing won't seem so interesting now.</span>
<<else>>
	<span class="green">Word of your crossdressing exploits are sure to be drowned out.</span>
<</if>>
<br><br>

The students continue their lesson, unaware of what awaits.
<br><br>

<<link [[Wait to witness the results|School Changing Wait]]>><<control 25>><<trauma -12>><<stress -12>><</link>><<ggcontrol>><<lltrauma>><<llstress>>
<br>
<<link [[Leave|School Pool Entrance]]>><</link>>
<br>