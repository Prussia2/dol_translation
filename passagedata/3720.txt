<<set $outside to 0>><<set $location to "temple">><<effects>>
<<npc River>><<person1>><<generatey2>><<generate3>>
<br><br>
You tell River about the loft at the orphanage. <<Hes>> quiet. You're not sure if <<he>> even listened, until <<he>> at last speaks. "It's not my place to discuss this," <<he>> says.
<br><br>
"Are we allowed seconds?" interrupts a <<person2>><<person>> you recognise from the orphanage. <<He>> places an empty bowl on the counter. River hesitates for a moment, then fills it. The <<person>> scurries away.
<br><br>
<<person1>>
"However," River continues. "We could set up another kitchen. We're crowded here as it is. It will cost <span class="gold">£20000.</span> We'll funnel food over without further charge."
<br><br>
<i>A kitchen will let the orphans prepare meals away from Bailey's gaze, which will make it easier to increase hope.</i>
<br><br>
<<if $money gte 2000000>>
	<<link [[Have kitchen installed (£20000 3:00)|River Loft 2]]>><<set $loft_river to 1>><<set $loft_upgrade += 1>><<set $money -= 2000000>><</link>>
	<br>
<</if>>
<<link [[Leave|Soup Kitchen]]>><<endevent>><<endevent>><</link>>
<br>