<<effects>>

Alex stops at the porch. "There's a lot more to cover, but you can learn as you go. So how about it?" <<he>> smiles. "Fancy helping? It's okay if you need time to think about it."
<br><br>

<<link [[Accept|Farm Intro Accept]]>><<set $farm_stage to 2>><</link>>
<br>
<<link [[Refuse|Farm Intro Refuse]]>><</link>>
<br>