<<effects>>

<<if $milk_amount gte 1 and $lactating gte 1>>
	<<set $livestock_milk to $milk_amount>><<set $milk_produced_stat += $milk_amount>>
	Your milk is sucked up the tube, reappearing in a glass canister attached to the milking machine outside your cell. You try not to think about how your milk output is visible to anyone who cares to look.
	<br><br>
<<else>>
<</if>>

You try to focus on something, anything other than the sucking on your breasts. You can't stop them milking you, but you won't let them force an orgasm. You won't. The machine becomes more intense, as if sensing your defiance. The suction is maddening.
<br><br>

<<if $willpower gte random(1, 1000)>>
	You remember the gel Remy rubbed into you, and how unnatural that warmth felt. You feel it again now, as the gel works its devilry on your chest, spreading its yearning into your core, trying to twist your mind. You won't let it. <span class="green">You resist.</span> <<gwillpower>><<willpower 2>>
	<br><br>

	You remain as still as you can, afraid that even a slight movement would send you over the edge. The urge to cum is almost painful in its maddening intensity.
	<br><br>

	You're not sure how long passes, stuck there on the edge. At last, the machine falls quiet. The suction stops.
	<br><br>
	<<set $milk_amount to 0>>
	<<link [[Next|Livestock Milking End]]>><</link>>
	<br>

<<else>>
	You can't take it. <span class="red">Your body gives in.</span><<gggwillpower>><<willpower 24>>

	<br><br>
	<<orgasmpassage>>

	You remember the gel Remy rubbed into you, and how unnatural that warmth felt. You feel it again as the gel works its devilry on your chest, keeping you primed and milkable.
	<br><br>

	<<link [[Next|Livestock Breasts Give 2]]>><</link>>
	<br>
<</if>>