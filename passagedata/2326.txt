<<set $outside to 0>><<set $location to "museum">><<effects>>

Winter nods. <<He>> peeks around the door. "Everyone's in. It's time. Wait for your cue." <<He>> returns to the museum proper.
<br><br>

"Ladies and gentlemen," Winter begins. <<He>> stands on a raised platform beside the horse, addressing an audience of around fifty people. "Our town has a rich history. Of great deeds and heroism yes, but also of tragedy and brutality. Take this <<girlcomma>>" <<He>> gestures at the door, and you <<nervously>> emerge into the museum. A <<person2>><<person>> stands closest to you. <<He>> licks <<his>> lips.
<br><br>

<<person1>>"Caught stealing bread for <<pher>> starving sibling," Winter continues. "<<pShe>> must now face the lord's justice." You climb atop the horse and ease yourself down. You feel it press into your crotch like before.<<masopain 5>><<arousal 1200>>
<br><br>

Winter continues <<his>> speech, talking about the specifics of the horse. And how it was used. <<He>> steps behind you while doing so, and begins to tie your arms. <<He>> works fast, and before long your arms are bound and immobile.
<br><br>

The audience is enraptured by the spectacle. You feel their eyes on your barely-covered <<genitalsstop>> "Of course, humiliation was part of the punishment." <<He>> throws open a chest at <<his>> feet, and reaches in. <<He>> pulls out a metal weight. You see straps dangling from it before <<he>> kneels beside you. <<He>> ties one to your ankle, and gently releases it. It was uncomfortable before, but now it hurts. <<He>> walks around the horse and repeats the process. Your eyes water.
<<masopain 20>>
<br><br>

<<He>> steps away from the horse, and glances at you. <<He>> looks unsure.
<br><br>

<<link [[Try to reassure|Museum Horse Extreme 2]]>><<set $phase to 0>><</link>>
<br>
<<link [[Safe word|Museum Horse Extreme 2]]>><<set $phase to 1>><</link>>
<br>