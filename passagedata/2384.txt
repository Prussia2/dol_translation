<<effects>>

<<if $physique gte random(1, $physiquemax)>>

	<span class="green">You struggle free from Harper's grip,</span> letting out a terrified moo as you tumble to the ground. You stagger to your feet and run.
	<br><br>
	You stop near a tree and look around. Nobody followed.
	<br><br>

	<<link [[Next|Park]]>><<set $eventskip to 1>><<endevent>><</link>>
	<br>

<<else>>

	You knock Harper's hand away, <span class="red">but <<he>> presses it against your mouth again with more force.</span>
	<Br><br>

	<<link [[Plead|Park Run Harper Plead]]>><</link>>
	<br>
	<<link [[Give in|Park Run Harper Give]]>><</link>>
	<br>

<</if>>