<<set $outside to 1>><<set $location to "park">><<effects>><<set $bus to "park">>

<<if $submissive gte 1150>>
"Oh, it feels so good to be looked at," you say. "Can I take a selfie with you?"
<<elseif $submissive lte 850>>
"Amateur," you tut. "Give it here. I'll capture my good side."
<<else>>
"Give me the camera," you say. "I have a naughty idea."
<</if>>
You hold your hand out.

<<skulduggerycheck>>
	<<if $skulduggerysuccess is 1>><<set $blackmoney += 20>>
	<span class="green"><<He>> passes you the camera.</span> You give it a thoughtful look, stick your tongue out, and run. "Oi!" <<he>> shouts after you, but you're gone by the time <<hes>> stood. Looks like it's worth £20.
	<br><br>

		<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
		<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
		<</if>>

	<<else>>
	<span class="red">"Nice try," <<he>> says.</span> <<He>> takes a picture of your <<bottom>> as you walk away.
	<br><br>
	<<fameexhibitionism 10>>

		<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
		<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
		<</if>>

	<</if>>

<<endevent>>
<<link [[Next|Park]]>><<set $eventskip to 1>><</link>>
<br>