<<set $location to "sea">><<set $outside to 1>><<effects>>

You're on the thin coastal path east of town, above a cliff facing the sea.

<<cliff_top_desc>>

White rose bushes flower here, unbothered by the wind.
<br><br>

<<if !$plants_known.includes("white_rose")>>
	<<link [[Search for seeds (0:30)|Coast Path White Rose]]>><<pain 6>><<tending 6>><<set $plants_known.push("white_rose")>><</link>><<gpain>><<gtending>>
	<br>
<</if>>
<<if $historytrait gte 3>>
	<<link [[Walk east along the coast (2:00)|Coast Path East]]>><<pass 120>><</link>>
	<br>
<</if>>
<br>
<<link [[Enter town (0:30)|Mer Street]]>><<pass 30>><</link>>
<br>