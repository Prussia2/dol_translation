<<set $outside to 0>><<set $location to "school">><<effects>>

"This is wrong," you say to the vicar. "How can you ignore my predicament?"
<br><br>

Kylar glares at you. "D-don't cause a fuss dear," <<person1>><<he>> says.
<br><br>

The vicar shrugs. "You'd be surprised what people are into," <<person2>><<he>> says. "Still, if you aren't into it I can't perform the rites." <<He>> hiccups. "Keeping the money."
<br><br>

<<He>> turns to leave, until Kylar thuds the axe into the staircase beside <<himstop>> "Finish the ceremony," The vicar sighs and nods.
<br><br>

<<link [[Next|Kylar Basement Police]]>><</link>>
<br>