<<set $outside to 0>><<set $location to "shopping_centre">><<effects>><<set $lock to 200>>
<<if $skulduggery gte $lock>>
	<span class="green">The lock looks easy to pick.</span>
	<br><br>
	<<link [[Pick it (0:10)|Pet Shop]]>><<pass 10>><<crimeup 20>><</link>><<crime>>
	<br>
<<else>>
	<span class="red">The lock looks beyond your ability to pick.</span>
	<<skulduggeryrequired>>
	<br><br>
<</if>>
<<link [[Leave|Shopping Centre]]>><</link>>
<br>