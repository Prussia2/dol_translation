<<effects>>

<<if $whitneymaths is "seat">>

	<<npc Whitney>><<person1>>
	Once outside, Whitney rests <<his>> forearm on the wall above you. "All alone with me," <<he>> says, leaning in to kiss your neck. "What a shame."
	<br><br>

	<<if $submissive lte 500>>
		<<link [[Take charge|Maths Event4 Take]]>><<npcincr Whitney love 1>><<npcincr Whitney dom -1>><<def 1>><</link>><<defianttext>><<promiscuous1>><<glove>><<ldom>>
		<br>
	<</if>>
	<<link [[Allow|Maths Event4 Allow]]>><<npcincr Whitney love 1>><<npcincr Whitney dom 1>><<sub 1>><<stress 6>><<arousal 6>><</link>><<glove>><<gdom>><<gstress>><<garousal>>
	<br>
	<<link [[Push away|Maths Event4 Push]]>><<npcincr Whitney love -1>><<npcincr Whitney dom -1>><<def 1>><</link>><<llove>><<ldom>>
	<br>

<<else>>

	<<generatey1>><<generatey2>>
	<<if $rng gte 81>>
	As you wait in the corridor, a <<person1>><<person>> and <<person2>><<person>> walk up to you. They stand either side of you, preventing you from moving away.
	<br><br>

	<<link [[Next|Maths Event4 Molestation]]>><<set $molestationstart to 1>><</link>>
	<<else>>
	As you wait in the corridor, a <<person1>><<person>> and <<person2>><<person>> eye you up as they pass by. The <<person1>><<person>> grins at you. <<catcall>><<gtrauma>><<gstress>><<trauma 1>><<stress 1>>
	<br><br>

	A few minutes later River emerges from the class. "It seems I made a mistake. Come back in."
	<br><br>

	<<link [[Next|Maths Lesson]]>><<endevent>><</link>>
	<br>
	<</if>>
<</if>>