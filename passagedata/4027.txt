<<set $outside to 0>><<set $location to "town">><<effects>>
<<if $halloween_robin_costume is undefined>><<set $halloween_robin_costume to "ghost">><</if>>
<<if $robinromance is 1>><<set $dateCount.Total++>><<set $dateCount.Robin++>><</if>>
Together you, Robin and the orphans leave the orphanage, carrying empty bags. The orphans wear a mix of homemade costumes. Some just wear bedsheets with holes cut for eyes.

<<if $robin_halloween_costume is "witch" and $NPCName[$NPCNameList.indexOf("Robin")].pronoun is "m">>
	<<if $player.gender is "f" and $player.gender_appearance is "m">>
		"Thanks for dressing up as a boy," Robin says as you pass through the door. "It feels better not being the only one dressed as the opposite sex."
	<<elseif $player.gender isnot "f" and $player.gender_appearance is "f">>
		"Thanks for dressing up as a girl too," Robin says as you pass through the door. "It's made me feel more confident."
	<</if>>
<<elseif $robin_halloween_costume is "vampire" and $NPCName[$NPCNameList.indexOf("Robin")].pronoun is "f">>
	<<if $player.gender isnot "m" and $player.gender_appearance is "m">>
		"Thanks for dressing up as a boy too," Robin says as you pass through the door. "It's made me feel more confident."
	<<elseif $player.gender is "m" and $player.gender_appearance is "f">>
		"Thanks for dressing up as a girl," Robin says as you pass through the door. "It feels better not being the only one dressed as the opposite sex."
	<</if>>
<</if>>
<br><br>
The group stops at the edge of the road. You see other trick-or-treaters up and down the street. "Stay close everyone," Robin announces.
<br><br>

<<link [[Next|Robin Trick 2]]>><</link>>
<br>