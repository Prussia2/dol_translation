<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<npc Kylar>><<person1>>
You kiss Kylar. <<He>> almost tackles you off your seat.
<<gglove>><<llsuspicion>>
<br><br>
<<endevent>><<npc Robin>><<person1>>
<<if $robinromance is 1>>
Robin stares at you for a moment, then down at <<his>> tray. <<He>> stands and leaves without a word.
<<lllove>><<npcincr Robin love -5>>
<br><br>
<<else>>
Robin glances at you, but blushes and looks away when <<he>> sees what you're up to.
<br><br>
<</if>>

<<endevent>>
<<link [[Next|Canteen]]>><</link>>
<br>