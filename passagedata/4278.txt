<<set $outside to 1>><<set $location to "school">><<schooleffects>><<effects>>

<<if $halloween is 1 and !$halloween_whitney>><<set $halloween_whitney to 1>>
	<<npc Whitney>><<person1>>
	An arm wraps around your neck. It's Whitney. <<He>> keeps walking, pushing you along. <<if $monthday is 31>>"Happy Halloween,"<<else>>"Halloween soon,"<</if>> <<he>> says, squeezing. <<if $monthday is 31>>"We're<<else>>"When it arrives we're<</if>> going trick-or-treating on <span class="gold">Domus Street after <<if $timestyle is "military">>19:00<<else>>7:00 pm<</if>></span>. You better dress nice." <<He>> doesn't wait for a response before turning and walking back to <<his>> friends.
	<br><br>

	<<link [[Next|Oxford Street]]>><<endevent>><</link>>

<<elseif $whitneyromance is 1>>

	<<if $rng gte 81>>
<<npc Whitney>><<person1>>Whitney's busy arguing with one of <<his>> friends and doesn't spot you. Another of <<his>> friends, a <<generatey2>><<person2>><<person>> does though, and walks over to you. "You're Whitney's slut," <<he>> says. "Think I fancy a piece myself." You withdraw into a defensive posture as <<he>> tries to grope your <<genitalsstop>>
<br><br>

The <<person2>><<person>> turns just in time to get smacked in the face. Whitney grasps <<his>> collar and pulls <<him>> close. "Keep your <<if $pronoun is "m">>pervert hands<<else>>skanky ass<</if>> away from my slut." Whitney shoves <<him>> aside. <<He>> turns away, red in the face as Whitney grabs your arm and tugs.
<br><br>

<<endevent>><<npc Whitney>><<person1>><<He>> pulls you away from <<his>> friends, behind the bike shed. "The nerve of that cunt," <<he>> says. "I need to vent," <<he>> shoves you to the ground and drops on top of you.
<br><br>

<<link [[Next|School Leave Whitney Rape]]>><<set $molestationstart to 1>><</link>>
<br>

	<<elseif $rng gte 61>>
		<<npc Whitney>><<person1>>Whitney grins when <<he>> spots you. <<He>> stomps out <<his>> cigarette and marches over.
		<<if $worn.face.type.includes("mask")>>
			"Why hide your pretty face?" <<he>> asks. <<He>> reaches for your $worn.face.name.
			<br><br>

			<<link [[Allow it|School Leave Whitney Mask]]>><<npcincr Whitney love 1>><</link>><<glove>>
			<br>
			<<link [[Push Whitney's arm away|School Leave Whitney Mask Push]]>><<npcincr Whitney love -1>><<npcincr Whitney dom -1>><</link>><<llove>><<ldom>>
			<br>
		<<elseif $skin.left_cheek.special is "whitney">>
			<<He>> leans in and licks your left cheek, right on the <<tattoo left_cheek>>. "Just wanted to make sure you were still labelled." <<He>> gives your <<bottom>> a smack before returning to <<his>> friends.
			<<gstress>><<garousal>><<glust>><<stress 6>><<arousal 600>><<npcincr Whitney lust 5>>
			<br><br>
			<<link [[Next|Oxford Street]]>><<endevent>><</link>>
			<br>
		<<elseif $skin.right_cheek.special is "whitney">>
			<<He>> leans in and licks your right cheek, right on the <<tattoo right_cheek>>. "Just wanted to make sure you were still labelled." <<He>> gives your <<bottom>> a smack before returning to <<his>> friends.
			<<gstress>><<garousal>><<glust>><<stress 6>><<arousal 600>><<npcincr Whitney lust 5>>
			<br><br>
			<<link [[Next|Oxford Street]]>><<endevent>><</link>>
			<br>
		<<elseif $skin.left_cheek.pen is "pen">>
			<<bodywriting_clear left_cheek>>
			<<He>> leans in and licks your left cheek, right on the <<tattoo left_cheek>>. <<He>> rubs it with <<his>> sleeve, then pulls a pen from <<his>> pocket. "Time for a proper label. Hold Still."
			<br><br>

			<<link [[Allow it|School Leave Whitney Writing]]>><<set $phase to 1>><<npcincr Whitney love 1>><</link>><<glove>>
			<br>
			<<link [[Push Whitney's arm away|School Leave Whitney Writing Push]]>><<npcincr Whitney love -1>><<npcincr Whitney dom -1>><</link>><<llove>><<ldom>>
			<br>
		<<elseif $skin.right_cheek.pen is "pen">>
			<<bodywriting_clear right_cheek>>
			<<He>> leans in and licks your right cheek, right on the <<tattoo right_cheek>>. <<He>> rubs it with <<his>> sleeve, then pulls a pen from <<his>> pocket. "Time for a proper label. Hold still."
			<br><br>

			<<link [[Allow it|School Leave Whitney Writing]]>><<set $phase to 2>><<npcincr Whitney love 1>><</link>><<glove>>
			<br>
			<<link [[Push Whitney's arm away|School Leave Whitney Writing Push]]>><<npcincr Whitney love -1>><<npcincr Whitney dom -1>><</link>><<llove>><<ldom>>
			<br>
		<<elseif !$skin.left_cheek.pen>>
			<<He>> pulls a pen from <<his>> pocket. "Gonna let everyone know who you belong to. Hold still."
			<br><br>

			<<link [[Allow it|School Leave Whitney Writing]]>><<set $phase to 2>><<npcincr Whitney love 1>><</link>><<glove>>
			<br>
			<<link [[Push Whitney's arm away|School Leave Whitney Writing Push]]>><<npcincr Whitney love -1>><<npcincr Whitney dom -1>><</link>><<llove>><<ldom>>
			<br>
		<<elseif !$skin.right_cheek.pen>>
			<<He>> pulls a pen from <<his>> pocket. "Gonna let everyone know who you belong to. Hold still."
			<br><br>

			<<link [[Allow it|School Leave Whitney Writing]]>><<set $phase to 2>><<npcincr Whitney love 1>><</link>><<glove>>
			<br>
			<<link [[Push Whitney's arm away|School Leave Whitney Writing Push]]>><<npcincr Whitney love -1>><<npcincr Whitney dom -1>><</link>><<llove>><<ldom>>
			<br>
		<<else>>
			<<if random(1, 2) is 2>>
				<<He>> leans in and licks your left cheek, right on the <<tattoo left_cheek>> "Didn't think it would come off," <<he>> says as <<he>> pulls away. "Oh well. Later slut." <<He>> smacks your <<bottom>> as <<he>> returns to <<his>> friends.
				<<gstress>><<garousal>><<glust>><<stress 6>><<arousal 600>><<npcincr Whitney lust 5>>
				<br><br>
			<<else>>
				<<He>> leans in and licks your right cheek, right on the <<tattoo right_cheek>> "Didn't think it would come off," <<he>> says as <<he>> pulls away. "Oh well. Later slut." <<He>> smacks your <<bottom>> as <<he>> returns to <<his>> friends.
				<<gstress>><<garousal>><<glust>><<stress 6>><<arousal 600>><<npcincr Whitney lust 5>>
			<</if>>
			<br><br>

			<<link [[Next|Oxford Street]]>><<endevent>><</link>>
			<br>
		<</if>>
	<<else>>
<<npc Whitney>><<person1>>Whitney spots you approach the gate. <<He>> shoves one of <<his>> friends out of the way and marches towards you. <<He>> pushes you against a wall and holds your hands above your head in <<his>> own. "Almost missed you," <<he>> says. <<He>> kisses you, still pressing your arms against the wall. "You wouldn't do that to me, would you?" <<He>> kisses once more before releasing you. <<He>> slaps your <<bottom>> as you leave.
<<gstress>><<garousal>><<glust>><<stress 6>><<arousal 600>><<npcincr Whitney lust 5>>
<br><br>

<<link [[Next|Oxford Street]]>><<endevent>><</link>>
<br>

	<</if>>
<<else>>
	<<if $rng gte 81>>
<<npc Whitney>><<person1>>Whitney spots you approach the gate. "Where do you think you're going," <<he>> says. "You haven't paid the toll." <<He>> grasps <<his>> crotch and <<his>> friends laugh.
<<gtrauma>><<gstress>><<trauma 6>><<stress 6>><<glust>><<npcincr Whitney lust 5>>
<br><br>

	<<link [[Next|Oxford Street]]>><<endevent>><</link>>
	<br>

	<<elseif $rng gte 61>>
		<<if $physique gte 15000>>
			<<npc Whitney>><<person1>>You hear footsteps behind you. Before you can turn, Whitney leaps on your back and wraps <<his>> arms around your neck. "Giddy up, <<bitch>>," <<he>> says. 
			<span class="green">You resist being knocked to your knees.</span> "You're a good horse. I should ride you more often."
			<br><br>
			
			<<link [[Go along with it (0:10)|School Leave Whitney Ride]]>><<pass 10>><<npcincr Whitney dom 1>><<npcincr Whitney love 1>><</link>><<gdom>><<glove>>
			<br>
			<<link [[Take control (0:10)|School Leave Whitney Control]]>><<pass 10>><<npcincr Whitney dom -1>><</link>><<ldom>>
			<br>
			<<link [[Throw Whitney off|School Leave Whitney Throw]]>><<npcincr Whitney love -1>><</link>><<llove>>
		
		<<else>>
			<<npc Whitney>><<person1>>You hear footsteps behind you. Before you can turn, Whitney leaps on your back and wraps <<his>> arms around your neck. "Giddy up, <<bitch>>," <<he>> says. <span class="red">The weight forces you to your knees.</span> "You're a shit horse. Maybe you need to be ridden more." <<He>> turns to <<his>> friends. "Any takers?" They laugh as you climb to your feet.
			<<gtrauma>><<gstress>><<trauma 6>><<stress 6>><<glust>><<npcincr Whitney lust 5>>
			<br><br>

			<<link [[Next|Oxford Street]]>><<endevent>><</link>>
			<br>
		<</if>>

	<<elseif $rng gte 41>>
	<<npc Whitney>><<person1>>Whitney spots you approach the gate. "Look who it is," <<he>> shouts. <<His>> friends turn to watch you. "Look at the way <<pshe>> walks. I bet <<pshes>> sore after fucking all the teachers." They laugh.
	<<gtrauma>><<gstress>><<trauma 6>><<stress 6>><<glust>><<npcincr Whitney lust 5>>
	<br><br>

	<<link [[Next|Oxford Street]]>><<endevent>><</link>>
	<br>

	<<elseif $rng gte 21>>

	<<npc Whitney>><<person1>>Whitney spots you approach the gate. <<He>> blocks your path. "Where do you think you're going?" <<he>> says. "There's a toll for passing through." <<He>> holds out a hand. "Twenty quid. Or I'll take something else as payment."
	<br><br>

		<<if $money gte 2000>>
	<<link [[Pay up|School Leave Whitney Pay]]>><<set $money -= 2000>><<set $submissive += 1>><</link>><<gdom>>
	<br>
		<</if>>
		<<link [[Say you can't afford it|School Leave Whitney Strip]]>><<set $phase to 0>><<set $molestationstart to 1>><</link>>
		<br>
		<<link [[Shove|School Leave Whitney Strip]]>><<set $submissive -= 1>><<npcincr Whitney dom -1>><<set $phase to 1>><<set $molestationstart to 1>><</link>><<ldom>>
		<br>
	<<else>>
	<<npc Whitney>><<person1>>You walk past Whitney. You feel a sting on your arm and turn to see Whitney putting <<his>> cigarette out on it. <<He>> gives you an innocent smile.
	<<gpain>><<set $pain += 20>><<gtrauma>><<gstress>><<trauma 6>><<stress 6>><<glust>><<npcincr Whitney lust 5>>
	<br><br>

	<<endevent>>

	<<link [[Next|Oxford Street]]>><<endevent>><</link>>
	<br>

	<</if>>
<</if>>