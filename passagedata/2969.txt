<<set $outside to 0>><<set $location to "town">><<effects>>

<<if $submissive gte 1150>>
"P-please, no," you say. "I worked so hard on it."
<<elseif $submissive lte 850>>
"Don't you dare," you say. "I worked really hard on that."
<<else>>
"Please don't," you say. "I worked really hard on that."
<</if>>
<br><br>

Whitney laughs. "Poor you," <<he>> says. <<He>> looks thoughtfully at the paper, then hands it to you. "You'll make it up to me later."
<br><br>

You continue to the front row and find an empty seat. You glance around, worried that Whitney followed you. You see <<him>> sitting with <<his>> friends on the other side of the hall, in the second row.
<br><br>
<<endevent>>
<<link [[Next|Maths Competition Intro]]>><</link>>
<br>