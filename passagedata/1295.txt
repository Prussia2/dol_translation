<<effects>>

You search among the wild grass for suitable material. You find a number of firm, long fronds, and build a skirt around your waist. It's not ideal, and you doubt you could remove it without destroying it, but it keeps you covered at least.
<br><br>
<<lowerwear 8>>
You mount your <<steed_text>>, and spur it toward the thicket. You ride up the hill, until you find the dirt road. The procession is gone, so you trot until you catch up to them.
<br><br>

<<link [[Next|Riding School Lesson]]>><</link>>
<br>