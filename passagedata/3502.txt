<<set $outside to 0>><<set $location to "strip_club">><<effects>>

You wrest your arm free from the <<personstop>> "Fucking slut," <<he>> says, hefting <<himself>> on and over the counter. <<He>> grasps your shoulders and tries to push you towards the exit.
<br><br>

<<if $rng gte 51>>
<<generate2>><<generate3>>
Two members of security, a <<person2>><<person>> and <<person3>><<personcomma>> intervene, pulling the <<person1>><<person>> away from you. <<He>> struggles against them, but is no match for both. <<He>> curses them as they drag <<him>> outside.
<br><br>

You return to the bar. A few moments later the <<person2>><<person>> approaches you. "You go careful when you head home," <<he>> warns before returning to <<his>> post. <<tipreceive>>
<br><br>

<<if !$per_npc.bartend and random(1, 2) is 2>>
	<<saveNPC 0 bartend>>
<</if>>

<<link [[Next|Strip Club]]>><<endevent>><<pass 30>><</link>>
<br>

<<else>>

<span class="red">Security pretend not to see.</span>
<br><br>

<<link [[Struggle|Bartending VIP Patron Struggle]]>><</link>><<physiquedifficulty>>
<br>
<<link [[Don't Struggle|Bartending VIP Patron Dragged]]>><</link>>
<br>

<</if>>