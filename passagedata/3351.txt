<<set $outside to 0>><<effects>>

<<if $street_slime is 1>>
You turn away from the <<personcomma>> and feel a jolt of pain as the slime punishes you for defiance. However, <span class="teal">it's still worn out from overpowering you earlier.</span> You feel it try, <span class="green">but fail,</span> to force your steps in the opposite direction.
<br><br>
<<endevent>>

<<destinationeventend>>

<<elseif $corruption_slime gte ($willpower / 10)>><<set $street_slime to 1>>

You turn away from the <<personcomma>> and feel a jolt of pain as the slime punishes you for defiance. It tries to force you to stay still, <span class="red">and you fail</span> to resist its influence.
<<ggwillpower>><<willpower 3>>
<br><br>

Forced by the slime's urging, you approach the <<personstop>>
<br><br>

"T-take me!" you say.
<br><br>

The <<person>> is taken aback, but recovers quickly. <<He>> glances around, then grabs your arm. <<He>> pulls you into an alley.
<br><br>

<<link [[Next|Street Slime Rape]]>><<set $molestationstart to 1>><</link>>
<br>

<<else>>

You turn away from the <<personcomma>> and feel a jolt of pain as the slime punishes you for defiance. You feel it try, <span class="green">but fail,</span> to force your steps in the opposite direction.
<<gwillpower>><<willpower 1>>
<br><br>

<<endevent>>

<<destinationeventend>>

<</if>>