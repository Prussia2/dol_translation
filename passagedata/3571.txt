<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
<<generate2>><<pass 60>>
<<bind>><<facewear 6>>
You awaken in darkness. <span class="red">Your arms are bound and mouth gagged.</span> You feel something around your head. A blindfold. You're sat down.
<br><br>
"<<pShes>> awake," says a familiar voice. It's the <<monk>> from the garden.
<br><br>

"Lovely," says an unfamiliar <<person2>><<if $pronoun is "m">>man<<else>>woman<</if>>. Their voice echoes. "Nice and nubile. Just my type." You hear a clatter to your left. It sounded far away.
<br><br>

"Now listen close," the <<person1>><<monk>> says. You think you hear a distant howl to your right, but you might be imagining it. "You're gonna be a good <<girl>> and do as we say. Understood?" Hands grope your <<bottom>> and <<breastsstop>>
<br><br>

<<link [[Be obedient|Temple Garden Obedient]]>><<set $submissive += 1>><<grace 3 monk>><</link>><<gggrace monk>>
<br>
<<link [[Escape|Temple Garden Defiant]]>><<set $submissive -= 1>><<grace -1 monk>><</link>><<lgrace monk>>
<br>