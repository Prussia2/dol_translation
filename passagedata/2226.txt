<<set $location to "landfill">><<set $outside to 1>><<effects>>

You turn and run. Pain jolts through your body, and you feel something try to wrest control away from you. For a moment your steps falter, as an outside force steals your strength.
<br><br>
<<tearful>> you manage to stumble away, where you crouch and hide behind a rundown car.
<br><br>

<i>Obeying the slime will provide benefits, but increase your corruption. The slime will become more powerful, and harder to resist, as corruption increases.</i>
<br><br>

<<endevent>>

<<link [[Next|Trash]]>><<set $eventskip to 1>><</link>>
<br>