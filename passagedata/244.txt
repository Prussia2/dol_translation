<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
Refusing to play along, you stand in the middle of the office. The Doctor shakes <<his>> head. "We can't help you if you won't let us," <<he>> says. "You can go."
<br><br>
<<endevent>>
<<link [[Next|Asylum]]>><</link>>
<br>