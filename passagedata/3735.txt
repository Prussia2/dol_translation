<<set $outside to 0>><<set $location to "temple">><<effects>>
You return to the kitchen and approach River.
<<if $submissive gte 1150>>
	"S-someone's being threatened," you say. "They sound scary."
<<elseif $submissive lte 850>>
	"A couple of thugs are harassing someone," you say.
<<else>>
	"Someone's being threatened," you say.
<</if>>
<br><br>
River tenses, but nods and walks outside.
<br><br>
A few moments later a <<person3>><<person>> staggers through the door, looking dishevelled and alarmed. <<He>> sinks to the ground beside the ovens a wraps <<his>> arms around <<his>> legs. A <<monk>> crouches in front of <<himstop>> <<He>> manages to coax the <<person>> to <<his>> feet and lead <<him>> to the back rooms.
<br><br>
<<endevent>>
<<npc River>><<person1>>
Another minute passes, and River returns as well. <<His>> hair net is gone, <<his>> shirt torn and <span class="red">blood trickles down <<his>> temple.</span> <<He>> washes <<his>> face in the sink. "Thank you for telling me," <<he>> says to while rubbing <<himself>> dry with a cloth. "Some people have no decency."
<br><br>
<<endevent>>
<<link [[Next|Soup Kitchen]]>><</link>>
<br>