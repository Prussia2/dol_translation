<<effects>>

You hope the water's deep enough. You place your arms above your head, and leap from the edge.
<<if $swimmingskill gte random(1, 1000)>>
	<span class="green">You break the water with elegance,</span> and arc back to the surface. The water isn't too cold. You wonder how deep it is.<<water>>
	<br><br>
	<<link [[Next|Meadow Cave]]>><</link>>
	<br>
<<else>>
	You lose your balance, <span class="red">and flail as you collide with the water.</span><<water>><<gtrauma>><<gstress>><<gpain>><<trauma 6>><<stress 6>><<pain 4>>
	<br><br>

	The water isn't too cold, but the current is strong. It snatches you away from the surface, pulling you down and along.
	<br><br>

	You surface in a river, running alongside the meadow. You pull yourself ashore. You try to find the hole again, but it's nowhere to be seen.
	<br><br>
	<<link [[Next|Meadow]]>><<clothesruined>><<unset $fox>><</link>>
	<br>
<</if>>