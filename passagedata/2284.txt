<<effects>>

<<if $submissive gte 1150>>
	"You did okay," you say, taking the <<print $stall_plant.replace(/_/g," ")>>. "But you could be better."
<<elseif $submissive lte 850>>
	"You fucked up," you say, snatching the <<print $stall_plant.replace(/_/g," ")>>. "I should never have noticed you."
<<else>>
	"You botched it," you say, snatching the <<print $stall_plant.replace(/_/g," ")>>. "I should never have noticed you."
<</if>>
<br><br>

You give the <<person>> some skulduggerous pointers. <<Hes>> shocked that you would share this with such candour. "Th-thank you," <<he>> says, when you finish, a little intimidated. "I-I'm not a thief though. I don't normally do this."
<br><br>

At that, you laugh. You return to your stall.
<br><br>

<<stall_actions>>