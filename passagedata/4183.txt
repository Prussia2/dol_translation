<<set $outside to 0>><<set $location to "town">><<effects>>

Robin's face turns white. "A scary film?" <<he>> asks. "I'm not good with scary."
<br><br>

You buy the tickets and enter the theatre. Robin is hesitant to follow. <<He>> sits beside you, but when the person sat behind <<him>> nudges <<his>> seat <<he>> leaps up with a sharp scream.
<br><br>

<<link [[Comfort|Robin Cinema Comfort]]>><<pass 45>><<npcincr Robin love 1>><<stress 6>><</link>><<gstress>><<glove>>
<br>
<<link [[Embolden|Robin Cinema Embolden]]>><<pass 45>><</link>>
<br>