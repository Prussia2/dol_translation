<<effects>>
Your heart flutters as you stick out your tongue, and the tentacle wiggles closer. It flicks the tip before weaving around until
it nudges your lips. It's kissing you.
<<deviancy1>>
A pink liquid drips from the tip.
<br><br>
<<if $deviancy gte 15>>
	<<link [[Lick|Eerie Mirror 4]]>><</link>><<set $drugged += 60>><<deviant2>>
	<br>
<</if>>
<<link [[Step away|Eerie Mirror Stop]]>><</link>>
<br>