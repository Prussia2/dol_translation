<<effects>>


The knot deflates, and the <<farm_text dog>> wiggles free. The <<person2>><<person>> walks away while examining the footage.
<<clotheson>>

<<if $exposed gte 1 and $farm_naked isnot 1>>
	"L-Let me get you some towels," Alex says at last.
	<br><br>
	
	<<He>> returns with the towels a few moments later, <<his>> pallor given way to a blush.<<towelup>><<npcincr Alex lust 1>><<glust>>
	<br><br>
	
	"Might get some good dosh from that," <<he>> says, avoiding eye contact. <<He>> opens <<his>> mouth again, but hesitates before speaking. "I'm gonna get back to work." <<He>> marches away.
	<br><br>
<<else>>
	Alex's pallor gives way to a blush. "Might get some good dosh from that," <<he>> says, avoiding eye contact. <<He>> opens <<his>> mouth again, but hesitates before speaking. "I'm gonna get back to work." <<He>> marches away.
<</if>>
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>