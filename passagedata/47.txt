<<effects>><<set $outside to 0>><<set $location to "beach">>
<<set $wardrobe_location to "wardrobe">>
You are in a small wooden changing room.
<br><br>
<<wardrobewear>>
<<if $exposed lte 1>>
<<link[[Leave|Beach]]>><<unset $saveColor>><<unset $wardrobeRepeat>><<unset $tempDisable>><</link>>
<br><br>
<<else>>
You can't go out like this!
<br><br>
<</if>>
<<wardrobe>>