<<effects>>

<<if $submissive gte 1150>>
	"Please don't look," you say. "You're scaring me."
<<elseif $submissive lte 850>>
	"I'm not the one leering," you say. "Fuck off."
<<else>>
	"Please don't look," you say.
<</if>>
<br><br>

The students look thoughtful. The <<person1>><<person>> speaks up first.

<<if $leftarm is "bound" and $rightarm is "bound">>
	"Let's let <<phim>> go," <<he>> says. The others look surprised. "Running around with <<pher>> arms bound like that. We've seen everything anyway."
	<br><br>

	The others grudgingly accept the <<persons>> logic, and the students turn and walk back to Connudatus Street.
	<br><br>

	You lean against the wall, your whole body shaking.
	<br><br>

	<<link [[Next|Commercial alleyways]]>><<endevent>><</link>>
	<br>
<<else>>
	"Tell you what," <<he>> says. "You move your arms so we can see you properly, and we'll let you go."
	<br><br>

	<<link [[Accept|Road Ex Accept]]>><<trauma 6>><<stress 6>><<arousal 600>><</link>><<gtrauma>><<gstress>><<garousal>>
	<br>
	<<link [[Refuse|Road Ex Refuse]]>><</link>>
	<br>
<</if>>