<<effects>>

<<if $leftarm is "bound" and $rightarm is "bound">>You shuffle across the floor on your knees, trying your hardest to keep up with Whitney and <<his>> friends without falling on your face.<<else>>You crawl across the floor on your hands and knees while Whitney walks along beside you.<</if>>
<br><br>

You glance up at the whispering crowd of onlookers and gawkers. Several faces turn red and look away while others peer between their hands.
<br><br>

You're pulled around a corner and come face to face with a crowd of <<peopley>>. <<covered>> A nearby <<persony>> looks you up and down, leering as others gasp in horror.
<br><br>

"Here <<girlcomma>>" Whitney shouts, throwing your clothes down the hallway. "Fetch!"
<br><br>

You scramble after before anyone in the crowd steals them. You make it in time. You flee somewhere secluded to dress.
<<npcincr Whitney lust 5>><<glust>>
<br><br>

<<fameexhibitionism 70>>

<<endevent>>

<<link [[Next|Hallways]]>><<clotheson>><<set $eventskip to 1>><</link>>
<br>