<<effects>>

Alex turns to run, but hesitates when <<he>> sees you plant your feet. Instead, <<he>> stands alongside you and braces <<himself>>.
<br><br>
<<if $rng gte 76>>
	The boar runs straight at you, <span class="red">and knocks you off your feet.</span> The world spins, and you hear Alex shout.<<gpain>><<gtrauma>><<gstress>><<pain 4>><<stress 6>><<trauma 6>>
	<br><br>
	
	You gear your bearings, and see the boar disappear into the forest. "Are you hurt?" Alex asks, crouching beside you. You shake your head.
	<br><br>
	
	"You idiot," Alex says. "You're lucky you're uninjured."
	<br><br>
	
	<<link [[Apologise|Farm Race Sorry]]>><<npcincr Alex love 3>><<npcincr Alex dom 1>><</link>><<gdom>><<gglove>>
	<br>
	<<link [[Brag|Farm Race Brag]]>><<npcincr Alex dom -1>><</link>><<ldom>>
	<br>
<<elseif $rng gte 51>>
	The boar runs straight for Alex. The beast veers at the last moment, <span class="red">but <<hes>> still bashed by its shoulder,</span> and knocked to the ground.
	<br><br>
	
	You crouch beside Alex as the boar runs back into the forest. "I'm fine," <<he>> says. "But what were you thinking? I could've been hurt."<<llove>><<npcincr Alex love -1>>
	<br><br>
	
	<<link [[Apologise|Farm Race Sorry 2]]>><<npcincr Alex love 1>><<npcincr Alex dom 1>><</link>><<gdom>><<glove>>
	<br>
	<<link [[Defend your decision|Farm Race Defend]]>><<npcincr Alex love -1>><<npcincr Alex dom -1>><</link>><<llove>><<ldom>>
	<br>
<<else>>
	The boar lurches from left to right as it approaches, as if unsure who to aim for, then <span class="green">veers and runs into the forest.</span>
	<br><br>
	
	Alex leans forward, resting <<his>> hands on <<his>> thighs. "Fuck," <<he>> laughs. "I thought you were mad when you didn't run. Come on, let's get back to work."
	<br><br>
	
	<<link [[Next|Farm Work]]>><<endevent>><</link>>
	<br>
<</if>>