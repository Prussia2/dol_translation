<<effects>>

<<if $submissive gte 1150>>
	"P-please don't take any pictures," you say. "I need help."
<<elseif $submissive lte 850>>
	"Don't you dare," you warn. "Give me something to cover with."
<<else>>
	"Please," you say. "I need help."
<</if>>
<br><br>

<<if random(1, 2) is 2>>
	"I got a towel for ya," <<he>> says as <<his>> camera clicks. "Just want some souvenirs. Not everyday a cute <<girl>> drops into my lap."
	<<fameexhibitionism 7>>
	<br><br>

	<<He>> takes a few more pictures before throwing you a towel, and showing you to the gate.
	<br><br>

<<else>>
	<<He>> pauses a moment, then lowers the camera.
	<<lstress>><<stress -6>>
	<br><br>

	<<He>> hands you a towel before showing you to the gate.
	<br><br>
<</if>>
<<towelup>>
<<endevent>>
<<destinationeventend>>