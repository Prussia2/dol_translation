<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<endevent>>
<<npc Robin>><<person1>>You check Robin for injury before falling into <<his>> arms. <<He>> doesn't speak for a few minutes, until you pull away. "I need to get ready for my next lesson," <<he>> says. <<He>> still looks concerned, and waves at you as <<he>> leaves the canteen.
<br><br>
<<pass 3>><<endevent>>
<<link [[Next|Canteen]]>><<endevent>><</link>>