<<set $outside to 1>><<set $location to "town">><<temple_effects>><<effects>>

You wait for the <<person1>><<monk>> to make <<his>> pitch. The <<person5>><<person>> isn't interested. The <<person2>><<person>> stares at <<his>> feet.
<br><br>

The procession returns to the temple. You don't think you made much, but the <<person1>><<monk>> seems happy.
<br><br>

<<endevent>>
<<link [[Next|Temple]]>><</link>>
<br>