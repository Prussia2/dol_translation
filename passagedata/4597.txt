<<effects>>

You smack the <<person1>><<persons>> hand away.

<<if $rng gte 21>>

	<<He>> takes the hint.
	<br><br>

	<<endevent>>
	<<cream_walk>>

<<else>>

	<span class="red">Enraged,</span> <<he>> lunges at you from the crowd, tackling you to the ground.
	<br><br>

	<<link [[Call for help|Cream Ex Naked Walk Call]]>><</link>>
	<br>
	<<link [[Endure|Cream Ex Naked Walk Endure 2]]>><<arousal 2000>><<trauma 6>><<stress 6>><</link>><<gtrauma>><<gstress>><<gggarousal>>
	<br>

<</if>>