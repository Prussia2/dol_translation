<<set $outside to 0>><<set $location to "museum">><<effects>>

<<npc Winter>><<person1>>"I must admit," Winter says. "I have no idea what it is. There's a carving. Looks like writing, but I don't recognise it." <<He>> frowns. "A corporation is looking to buy the thing. They must know something I don't."
<br><br>

<<link [[Next|Museum]]>><<endevent>><</link>>