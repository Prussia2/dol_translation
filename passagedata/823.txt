<<effects>>

<<if $tending gte random(1, 1000)>>
	You making soothing noises, <span class="green">and the <<farm_text_many horse>> calm down.</span> They're still on edge, but they won't be breaking anything.<<grespect>><<farm_horses 1>><<ggtending>><<tending 9>><<set $farm_work.horses_panic to 0>><<gfarm>><<farm_yield 1>>
	<br><br>
	
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<<else>>
	You try to soothe <<farm_text_many horse>>, <span class="red">but you're not sure they even hear you.</span><<gtending>><<tending 2>>
	<br><br>
	
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<</if>>