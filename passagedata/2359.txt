<<set $outside to 0>>
<<museumAntiqueText>>
<<npc Winter>><<person1>>
You look around the museum. The pedestals are grooved, as if awaiting specific objects.
<br><br>
<<if $museumAntiques.museumCount is 0>>
	The museum is empty.
<<elseif $museumAntiques.museumCount lt $museumAntiques.maxCount / 3>>
	The museum is almost empty
<<elseif $museumAntiques.museumCount lt $museumAntiques.maxCount / 2>>
	The museum looks better than it did.
<<elseif $museumAntiques.museumCount lt $museumAntiques.maxCount / 1.5>>
	The museum has a reasonable collection.
<<elseif $museumAntiques.museumCount lt $museumAntiques.maxCount / 1.25>>
	The museum has a good collection.
<<elseif $museumAntiques.museumCount lt $museumAntiques.maxCount>>
	The museum has an amazing collection.
<<elseif $museumAntiques.museumCount is $museumAntiques.maxCount>>
	Every spot is full.
<</if>>
<br><br>

<<set _possibleHints to []>>
<ul>
<<for _labelM, _valueM range $museumAntiques.antiques>>
	<<switch _valueM>>
		<<case "museum">><li><<print _museumAntiqueText[_labelM].museum>></li>
		<<case "found" "talk">><li>Maybe you should show the <<print _museumAntiqueText[_labelM].name>> to Winter.</li>
		<<case "notFound">><li>Empty. <<run _possibleHints.push(_labelM)>></li>
	<</switch>>
<</for>>
</ul>

<<if _possibleHints.length gt 0>>
	<<if $winterHint is "notGiven">>
		<<set $winterHint to _possibleHints[random(0,_possibleHints.length - 1)]>>
		As you look around, Winter approaches.
		<br><br>
		<<print _museumAntiqueText[$winterHint].hint>>
		<br>
	<</if>>
<</if>>

<br>
<<link [["Go back to the entrance"|Museum]]>><<endevent>><</link>>