<<set $location to "landfill">><<set $outside to 1>><<effects>>

You search the piles of rubbish for anything of value.

<br><br>

<<if $trash_key isnot 1 and $trash_unlocked isnot 1>>
You find a large, iron key. You wonder if it opens anything nearby.
<<set $trash_key to 1>>
<br><br>

<<elseif $trash gte 100>>
	<<if $rng gte 80>>
	You find an ancient incense burner. It's clearly an antique. You're not sure why anyone would throw it out.
	<<set $antiquemoney += 100>><<museumAntiqueStatus "antiquetrashburner" "found">><<crimeup 100>>

	<<elseif $rng gte 60>>
		<<set $rng to random(1, 100)>>
		<<if $rng gte 81>>
		You find a discarded purse. There's <span class="gold">£100</span> still inside.
		<<crimeup 100>><<set $money += 10000>>
		<<elseif $rng gte 61>>
		You find a discarded purse. There's <span class="gold">£80</span> still inside.
		<<crimeup 80>><<set $money += 8000>>
		<<elseif $rng gte 41>>
		You find a discarded purse. There's <span class="gold">£60</span> still inside.
		<<crimeup 60>><<set $money += 6000>>
		<<elseif $rng gte 21>>
		You find a discarded purse. There's <span class="gold">£40</span> still inside.
		<<crimeup 40>><<set $money += 4000>>
		<<else>>
		You find a discarded purse. There's <span class="gold">£20</span> still inside.
		<<crimeup 20>><<set $money += 2000>>
		<</if>>
	<<else>>
	You don't find anything.
	<</if>>
<<elseif $trash gte 50>>
	<<if $rng gte 80>>
		You find a stuffed envelope, full of someone's personal information. Bank details, social security number, the works. Someone should be willing to pay for it.
		<<crimeup 50>><<set $blackmoney += 50>>

		<br><br>
	<<elseif $rng gte 60>>
		<<set $rng to random(1, 100)>>
		<<if $rng gte 81>>
		You find a discarded purse. There's <span class="gold">£50</span> still inside.
		<<crimeup 50>><<set $money += 5000>>
		<<elseif $rng gte 61>>
		You find a discarded purse. There's <span class="gold">£40</span> still inside.
		<<crimeup 40>><<set $money += 4000>>
		<<elseif $rng gte 41>>
		You find a discarded purse. There's <span class="gold">£30</span> still inside.
		<<crimeup 30>><<set $money += 3000>>
		<<elseif $rng gte 21>>
		You find a discarded purse. There's <span class="gold">£20</span> still inside.
		<<crimeup 20>><<set $money += 2000>>
		<<else>>
		You find a discarded purse. There's <span class="gold">£10</span> still inside.
		<<crimeup 10>><<set $money += 1000>>
		<</if>>
	<<else>>
	You don't find anything.
	<</if>>
<<elseif $trash gte 30>>
	<<if $rng gte 80>>
		You find a bank card. Someone made a half-hearted effort to rendering it unusable before throwing it out, but you bend it back into shape. Should be worth something to the right people.
		<<crimeup 30>><<set $blackmoney += 30>>

		<br><br>
	<<elseif $rng gte 60>>
		<<set $rng to random(1, 100)>>
		<<if $rng gte 81>>
		You find a discarded purse. There's <span class="gold">£40</span> still inside.
		<<crimeup 40>><<set $money += 4000>>
		<<elseif $rng gte 61>>
		You find a discarded purse. There's <span class="gold">£30</span> still inside.
		<<crimeup 30>><<set $money += 3000>>
		<<elseif $rng gte 41>>
		You find a discarded purse. There's <span class="gold">£20</span> still inside.
		<<crimeup 20>><<set $money += 2000>>
		<<elseif $rng gte 21>>
		You find a discarded purse. There's <span class="gold">£15</span> still inside.
		<<crimeup 15>><<set $money += 1500>>
		<<else>>
		You find a discarded purse. There's <span class="gold">£10</span> still inside.
		<<crimeup 10>><<set $money += 1000>>
		<</if>>
	<<else>>
	You don't find anything.
	<</if>>
<<elseif $trash gte 10>>
	<<if $rng gte 80>>
	You find a stained metal cup. You wipe the grime off, and reveal a silver shine beneath. It looks like it belongs in a museum.
	<<set $antiquemoney += 50>><<museumAntiqueStatus "antiquetrashcup" "found">><<crimeup 20>>
	<<elseif $rng gte 60>>
		<<set $rng to random(1, 100)>>
		<<if $rng gte 81>>
		You find a discarded purse. There's <span class="gold">£30</span> still inside.
		<<crimeup 30>><<set $money += 3000>>
		<<elseif $rng gte 61>>
		You find a discarded purse. There's <span class="gold">£25</span> still inside.
		<<crimeup 25>><<set $money += 2500>>
		<<elseif $rng gte 41>>
		You find a discarded purse. There's <span class="gold">£20</span> still inside.
		<<crimeup 20>><<set $money += 2000>>
		<<elseif $rng gte 21>>
		You find a discarded purse. There's <span class="gold">£15</span> still inside.
		<<crimeup 15>><<set $money += 1500>>
		<<else>>
		You find a discarded purse. There's <span class="gold">£10</span> still inside.
		<<crimeup 10>><<set $money += 1000>>
		<</if>>
	<<else>>
	You don't find anything.
	<</if>>
<<else>>
	<<if $rng gte 80>>
	You find an old, but working phone. It should be worth something at least.
	<<crimeup 30>><<set $blackmoney += 30>>
	<i>Perhaps greater valuables can be found deeper in the landfill.</i>
	<<elseif $rng gte 60>>
		<<set $rng to random(1, 100)>>
		<<if $rng gte 81>>
		You find a discarded purse. There's <span class="gold">£25</span> still inside.
		<<crimeup 25>><<set $money += 2500>>
		<<elseif $rng gte 61>>
		You find a discarded purse. There's <span class="gold">£20</span> still inside.
		<<crimeup 20>><<set $money += 2000>>
		<<elseif $rng gte 41>>
		You find a discarded purse. There's <span class="gold">£15</span> still inside.
		<<crimeup 15>><<set $money += 1500>>
		<<elseif $rng gte 21>>
		You find a discarded purse. There's <span class="gold">£10</span> still inside.
		<<crimeup 10>><<set $money += 1000>>
		<<else>>
		You find a discarded purse. There's <span class="gold">£5</span> still inside.
		<<crimeup 5>><<set $money += 500>>
		<</if>>
	<<else>>
	You don't find anything.
	<</if>>
<</if>>
<br><br>

<<if $rng gte 80 and $milkshake gte 1 and $exposed is 0 and $malechance gte 1>>
	You hear the sound of boys in the distance.
	<br><br>
	<<link [[Investigate|Trash Boys]]>><</link>>
	<br>
	<<link [[Ignore|Trash]]>><</link>>
	<br>
<<else>>
	<<link [[Next|Trash]]>><</link>>
	<br>
<</if>>

/* SugarCube.State.display("Trash") */