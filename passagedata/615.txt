<<set $outside to 0>><<set $location to "tentworld">><<effects>>
<<if $tentacleadmire is undefined>>
	<<set $tentacleadmire to 0>>
	As your eyes scan the unending horizon, everything seems to slow down. The smooth sway of the tentacles becomes almost hypnotic to you. Before long you're swaying in place, too. The tentacles at your feet tickle you.
	<<ltrauma>><<lstress>><<garousal>>
	<br><br>
	<<trauma -6>><<stress -6>><<arousal 600>>
	<<link [[Next|Tentacle Plains]]>><</link>>
	<br>
<<elseif $tentacleadmire is 0>>
	<<set $tentacleadmire to 1>>
	Everything begins to melt away as you focus on the landscape. Nothing else seems to matter. The world outside holds no meaning. Your only thoughts are of the immense pleasure this realm could offer you. Your tail moves in a phantom breeze, and a tentacle gently creeps up your leg.
	<<ltrauma>><<lstress>><<garousal>>
	<br><br>
	<<trauma -6>><<stress -6>><<arousal 600>>
	<<link [[Next|Tentacle Plains]]>><</link>>
	<br>
<<elseif $tentacleadmire is 1>>
	<<set $tentacleadmire to 2>>
	You feel no guilt or self-loathing as the heat builds within you. Every tentacle seems to provocatively motion for you to just come one step closer. You can't help yourself any longer. You fall to your knees and present yourself to the countless tentacles surrounding you. They respond to your offering and snake up your arms, legs, and tail.
	<<gcontrol>><<control 25>><<ltrauma>><<lstress>><<ggarousal>>
	<br><br>
	<<trauma -6>><<stress -6>><<arousal 1200>>
	<<if $leftarm is "bound" or $rightarm is "bound" or $feetuse is "bound">>
		A pair of tentacles reach your <<if $leftarm is "bound" or $rightarm is "bound">>arms<</if>><<if ($leftarm is "bound" or $rightarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>> and squirt a viscous liquid onto your bindings, slowly dissolving them.<<unbind>>
		<br><br>
	<</if>>
	<<link [[Next|Tentacle Plains Admire Sex]]>><<set $sexstart to 1>><</link>>
<<else>>
	Just thinking about what's to come is driving you mad with lust. You could leave at any time, and yet here you remain. You crouch low to the ground, taking a tentacle in each hand. They respond to your touch eagerly, and an entire swarm of tentacles is soon upon you.
	<<gcontrol>><<control 25>><<ltrauma>><<lstress>><<ggarousal>>
	<br><br>
	<<trauma -6>><<stress -6>><<arousal 1200>>
	<<link [[Next|Tentacle Plains Admire Sex]]>><<set $sexstart to 1>><</link>>
<</if>>