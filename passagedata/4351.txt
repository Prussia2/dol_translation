<<set $outside to 0>><<set $location to "town">><<effects>>

<<if $molestationstart is 1>>
	<<set $molestationstart to 0>>
	<<molested>>
	<<controlloss>>
	<<set $combat to 1>>
	<<swarminit "slimes" "slime masses" "moving towards you" "encircle you" "fend off" 1 10>>
	<<if $bus is "residentialdrain">>
		<<set $timer to 40>>
	<<elseif $bus is "commercialdrain">>
		<<set $timer to 30>>
	<<elseif $bus is "industrialdrain">>
		<<set $timer to 20>>
	<<else>>
		<<set $timer to 10>>
	<</if>>
	<<set $water to 1>>
	<<set $swimdistance to 10>>
<</if>>
<<if $bus is "drainexit">>
	You see the exit of the drains up ahead. If you don't reach safety soon, you'll be carried into the ocean!
	<br><br>
<</if>>
<<swarmeffects>>
<<set $swimdistance += 1>>
<<if $swimdistance gte 20>><<set $swimdistance to 20>><</if>>
The water pulls you further along the tunnel.
<<if $swimdistancecheck is $swimdistance>>
	You fight against the current, but it is too strong to make headway. You need to use both arms.
<<elseif $swimdistancecheck gt $swimdistance>>
	You fight against the current and move closer to safety.
<<elseif $swimdistancecheck lt $swimdistance>>
	You are dragged further away from safety.
<</if>>
<<if $swimdistance gte 15>>
	You've been pulled away from the walkway.
<<elseif $swimdistance gte 10>>
	The walkway is some distance away from you.
<<elseif $swimdistance gte 5>>
	The walkway is close.
<<elseif $swimdistance gte 2>>
	The walkway is almost within reach.
<<else>>
	<span class="teal">The walkway is within reach!</span>
<</if>>
<<set $swimdistancecheck to $swimdistance>>
<br><br>
<<swarm>>
<<swarmactions>>
<<if $stress gte 10000>>
	<span id="next"><<link [[Next|Drain Water Slime Finish]]>><</link>></span><<nexttext>>
<<elseif $timer lte 0>>
	<span id="next"><<link [[Next|Drain Water Slime Finish]]>><</link>></span><<nexttext>>
<<elseif $swimdistance lte 1>>
	<span id="next"><<link [[Next|Drain Water Slime Finish]]>><</link>></span><<nexttext>>
<<else>>
	<span id="next"><<link "Next">><<script>>state.display(state.active.title, null)<</script>><</link>></span><<nexttext>>
<</if>>