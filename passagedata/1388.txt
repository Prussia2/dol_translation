<<set $outside to 1>><<set $location to "beach">><<effects>>

<<if $phase is 0>>

You walk over to the <<person>> who pretends not to notice you until you stop in front of <<himstop>> <<He>> looks at you but doesn't speak.
<br><br>

<<link [[Friendly chat (0:10)|Beach Day Encounter]]>><<set $phase to 1>><<pass 10>><<stress -2>><<trauma -1>><</link>><<lstress>><<ltrauma>>
<br>

<<link [[Flirt (0:05)|Beach Day Encounter]]>><<pass 5>><<set $phase to 2>><</link>><<promiscuous1>>
<br>

<<link [[Leave|Beach]]>><<endevent>><<set $eventskip to 1>><</link>>

<<elseif $phase is 1>>
You try to bring <<him>> out of <<his>> shell by talking about innocuous things. <<He>> responds politely but you do most of the talking. You catch <<him>> eyeing you up when <<he>> thinks you're not looking.
<br><br>

<<link [[Say Goodbye|Beach]]>><<endevent>><<set $eventskip to 1>><</link>>
<br>

<<link [[Flirt (0:05)|Beach Day Encounter]]>><<pass 5>><<set $phase to 2>><</link>><<promiscuous1>>
<br>

<<elseif $phase is 2>>
You sit on the towel beside <<him>> and lean close. You tell <<him>> <<he>> is a terrific parent but that <<he>> must be in need of stress relief every now and then.
	<<if $NPCList[0].penis is "clothed">>
	<<He>> shifts <<his>> legs as if to conceal something.
	<<else>>
	<<He>> blushes with increasing intensity as you continue.
	<</if>>
<<promiscuity1>>
<br><br>

<<link [[Stop|Beach Day Encounter]]>><<set $phase to 3>><</link>>
<br>
<<if $promiscuity gte 15>>
<<link [[Seduce|Beach Day Encounter]]>><<set $phase to 4>><</link>><<seductiondifficulty>><<promiscuous2>>
<</if>>

<<elseif $phase is 3>>
Satisfied with the effect you stand and take your leave.
<br><br>
<<link [[Next|Beach]]>><<endevent>><<set $eventskip to 1>><</link>>

<<elseif $phase is 4>><<if $seductionskill lt 1000>><span class="gold">You feel more confident in your powers of seduction.</span><</if>><<seductionskilluse>><<promiscuity2>>
	<<if 1000 - ($rng * 10) - $seductionskill - ($attractiveness / 10) lte -100>>
Gazing into <<his>> eyes, you press your body against <<hers>> and make it absolutely clear what your intentions are. <<He>> glances at <<his>> kids. Satisfied that they're distracted, <<he>> turns and embraces you, <<his>> heart beating furiously.
<br><br>
<<link [[Next|Beach Day Encounter Sex]]>><<set $sexstart to 1>><</link>>
	<<else>>
Gazing into <<his>> eyes, you press your body against <<hers>> and make it absolutely clear what your intentions are. Stammering excuses, <<he>> recoils from your advance and leaves to check on <<his>> children.
<br><br>

<<link [[Nevermind|Beach]]>><<endevent>><<set $eventskip to 1>><</link>>
	<</if>>
<</if>>