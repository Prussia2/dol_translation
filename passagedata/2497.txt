<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
You are in the school canteen.
<<if $schoolday is 1>>
	<<if $schoolstate is "early">>
	The room is empty.
	<br><br>
	<<elseif $schoolstate is "late">>
	The room is empty.
	<br><br>
	<<elseif $schoolstate is "lunch">>
	The room is packed with students chatting and eating their lunch.
	<br><br>
	<<else>>
	The room is empty, aside from a small group of students chatting around one of the tables.
	<br><br>
	<</if>>
<<elseif $schoolday isnot 1>>
The room is empty.
<br><br>
<</if>>

<<if $exposed gte 1>>
	<<if $schoolstate is "lunch">>
	You hide beneath a counter to conceal your <<nuditystop>> You hear the bustle of students mere feet away.
	<br><br>
	<<elseif $schoolday is 1 and $schoolstate isnot "early" and $schoolstate isnot "late">>
	You hide beneath a counter to conceal your <<nuditystop>> You should be able to sneak out if you're quick.
	<br><br>
	<<else>>
	<<exhibitionclassroom>>
	<</if>>
<</if>>

<<if $stress gte 10000>>
<<link [[Everything fades to black...|School Passout]]>><</link>>
<<else>>
	<<if $schoolday is 1>>
		<<if $schoolstate is "early">>
		<<elseif $schoolstate is "late">>
		<<elseif $schoolstate is "lunch">>
			<<if $robinmissing isnot 1 and $NPCName[$NPCNameList.indexOf("Robin")].init is 1 and $luncheaten isnot 1 and $exposed lte 0>>
				<<set $rng to random(1, 100)>>
				<<if $rng gte 96>>
				<<npc Robin>><<person1>>You see Robin trying to eat <<his>> lunch while a group of delinquents harass <<himstop>><<endevent>>
				<br>
				<<link [[Intervene (0:20)|Canteen Lunch Intervene]]>><<set $luncheaten to 1>><<npcincr Robin love 5>><</link>>
				<br><br>
				<<elseif $rng gte 91 and $NPCName[$NPCNameList.indexOf("Whitney")].dom gte 16 and $NPCName[$NPCNameList.indexOf("Whitney")].state isnot "dungeon">>
				You see Robin eating lunch.
				<br><br>
				<<link [[Eat with Robin (0:20)|Robin Kiyoura Start]]>><<stress -6>><<trauma -2>><<pass 20>><<set $luncheaten to 1>><<npcincr Robin love 1>><</link>><<ltrauma>><<lstress>>
				<br>
				<<else>>
				You see Robin eating lunch.
				<br>
				<<link [[Eat with Robin (0:20)|Canteen Lunch Robin]]>><<stress -6>><<trauma -2>><<pass 20>><<set $luncheaten to 1>><<npcincr Robin love 1>><</link>><<ltrauma>><<lstress>>
				<br>
				<</if>>
			<</if>>
			<<if $NPCName[$NPCNameList.indexOf("Kylar")].state is "active" and $luncheaten isnot 1 and $exposed lte 0>>
			Kylar sits alone, stabbing food with a fork.
			<br>
			<<link [[Eat with Kylar (0:20)|Canteen Lunch Kylar]]>><<pass 20>><<set $luncheaten to 1>><<npcincr Kylar love 1>><<npcincr Kylar rage -1>><</link>><<glove>><<lksuspicion>>
			<br><br>
			<</if>>
			<<if $luncheaten isnot 1 and $exposed lte 0>>
<<link [[Eat Lunch (0:10)|Canteen Lunch]]>><<pass 10>><<stress -6>><<set $luncheaten to 1>><</link>><<lstress>>
<br>
			<</if>>

		<<elseif $canteenapproach isnot 1 and $exposed lte 0>>
<<link [[Approach the students|Canteen Students]]>><</link>>
<br>
		<</if>>
	<<if def $milkshake and $milkshake gt 0>>
		<<link [[Drink your milkshake (0:10)|Canteen Milkshake]]>><<pass 10>><<set $milkshake -= 1>><<stress -5>><</link>><<lstress>>
		<br>
	<</if>>
	<<elseif $schoolday isnot 1>>

	<</if>>

	<<if $exposed gte 1 and $schoolstate is "lunch">>
		<<if $leftarm isnot "bound" and $rightarm isnot "bound">>
	<<link [[Cover yourself with a tray and try to leave|Canteen Tray]]>><</link>>
	<br>
		<</if>>
	<<link [[Wait until the coast is clear|Canteen Wait]]>><</link>>
	<br>
	<<else>>
<<link [[Leave the canteen (0:01)|Hallways]]>><<pass 1>><</link>>
<br>
	<</if>>

<</if>>