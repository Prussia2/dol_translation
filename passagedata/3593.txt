<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<beastejaculation>>
	The wolf returns to the forest. <<tearful>> you climb to your feet
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Temple Garden]]>><</link>>
	<br>
<<elseif $enemyhealth lte 0>>
	The wolf whimpers and limps back to the forest. <<tearful>> you climb to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Temple Garden]]>><</link>>
	<br>
<<else>>
	<<generate1>><<person1>>
	The wolf's ears prick. A moment later you hear it yourself. Footsteps.
	<br><br>
	<<if $current_sex is 1>>
		The wolf tries to flee, but can't pull away from you. The footsteps get closer. You struggle to pull away, but it's no use. A <<monk>> rounds a corner while you're still stuck together.
		<br><br>
		<<if $exposed gte 1>>
			<<He>> stops and stares at you. <<covered>> <<He>> tries to speak, but can't find the words. <<He>> leans again a sundial to steady <<himself>> as the wolf finally pulls free and runs for the forest.
			<br><br>
			<<clotheson>>
			"Ab-" <<he>> manages, stopping and steadying <<himself>> again. "Absolutely unacceptable. The bishop will hear of this." <<He>> pulls some towels from <<his>> habit, drops them on top of you, and marches away.
			<<towelup>>
			<br><br>
		<<else>>
			<<He>> stops and stares at you. <<He>> tries to speak, but can't find the words. <<He>> leans again a sundial to steady <<himself>> as the wolf finally pulls free and runs for the forest.
			<<clotheson>>
			<br><br>
			"Ab-" <<he>> manages, stopping and steadying <<himself>> again. "Absolutely unacceptable. The bishop will hear of this." <<He>> turns and marches away.
			<br><br>
		<</if>>
		<<tearful>> you hide behind a hedge.
		<br><br>
		<<endcombat>>
		<<link [[Next|Temple Garden]]>><</link>>
	<<else>>
		<<if $exposed gte 1>>
			The wolf runs towards the forest, just before a <<monk>> rounds a hedge.
			<br><br>
			"Well I never," <<he>> says, regarding you. <<covered>> <<clotheson>>
			<<He>> averts <<his>> eyes and rummages beneath <<his>> habit. <<He>> pulls out some towels and throws them atop you. <<He>> walks away, tutting.
			<<llgrace>><<gtrauma>><<gstress>><<grace -5>><<trauma 6>><<stress 6>>
			<<towelup>>
			<br><br>
		<<else>>
			The wolf runs towards the forest, just before a <<monk>> rounds a hedge. <<He>> glances at you, but doesn't realise anything is wrong.
			<br><br>
			<<clotheson>>
		<</if>>
		<<tearful>> you climb to your feet.
		<br><br>
		<<endcombat>>
		<<link [[Next|Temple Garden]]>><</link>>
	<</if>>
<</if>>