<<temple_effects>><<effects>>
You drop to your knees and begin gathering the scrolls. This shakes the initiate out of <<his>> lethargy, and <<he>> joins in. Some of the scrolls travelled a long way, and many need to be fished out from beneath pews. Despite this, you manage to gather them all.
<br><br>
"Thank you," the initiate says, smiling. "I'm so clumsy." <<He>> pauses for a moment. Then, in a burst of daring, leans in and plants a kiss on your cheek. <<He>> scurries away before you can respond.
<br><br>
<<endevent>>
<<link [[Next|Temple]]>><</link>>
<br>