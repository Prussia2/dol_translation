<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>

You describe the problems you've had, and how they started when a slime entered your ear. Jordan listens with a blank look.
<br><br>

<<Hes>> silent for a moment. "There are stories," <<he>> begins. "Of old creatures like you describe. One of our artifacts details how to remove them. There was an altar at the old temple. <i>The afflicted would lie on it during a blood moon.</i>"
<br><br>

"It doesn't matter though," <<he>> adds. "The old temple <i>sank beneath the lake</i> centuries ago. Students sometimes go out there after school. Entering the forest alone is a bad idea."
<br><br>

<<endevent>>

<<link [[Next|Temple]]>><</link>>
<br>