<<effects>>

<<if $submissive gte 1150>>
	"Y-You don't need to pretend you're not looking," you say.
<<elseif $submissive lte 850>>
	"Don't be a wimp," you say. "If you wanna leer, own it."
<<else>>
	"Don't be shy about checking me out," you say.
<</if>>
<<promiscuity3>>

"I-I wasn't!" <<he>> protests, but <<his>> blush tells a different story. You catch <<him>> looking again a short while later.
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>