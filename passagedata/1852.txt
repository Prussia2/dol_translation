<<set $outside to 0>><<set $location to "docks">><<dockeffects>><<effects>>

<<if $phase is 0>>
You look more closely,
	<<if $history gte random(1, 1000)>>
	<span class="green">and recognise the shape.</span> You <<generate1>><<person1>>inform a manager, who calls the captain in time. The ship steers around it.
	<br><br>

	"Damned reef," the manager say. "I swear it just pops up where it pleases. Good job."
	<<dockstatus 1>><<gcool>>
	<br><br>

	<<else>>
	<span class="red">but the shape vanishes.</span>
	<br><br>

	With a groan, the ship collides with something beneath the surface. Dockers rush to the moorings to assess the damage.
	<br><br>

	<</if>>
<<else>>
You're sure it's fine. You finish cleaning.
<br><br>

A metallic tearing sound shudders the pier. Something tears a giant gash down the side of the approaching ship, though it still makes it to the dock.
<br><br>

<</if>>
<<dockoptions>>