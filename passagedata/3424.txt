<<set _stun_chance to 25>>
<<set _like_chance to 50>>
<<set _hate_chance to 25>>

<<if $flaunt_type is "words">>
	<<if $phase is 1>>
		You look the <<person1>> in the eye, and respond "Thanks! <<sayexhibproud>>"
	<<elseif $phase is 2>>
		You wink at the <<person1>><<person>>.
	<<elseif $phase is 3>>
		You smile and say "Yes, that's me! <<sayexhibproud>>"
	<<elseif $phase is 4>>
		You smile, <<famesalute>> in the direction of the group.
	<<elseif $phase is 5>>
		You smile at the <<person>>, <<famesalute>>.
	<<else>>
		You <<famesalute>>.
	<</if>>
	<<set _stun_chance to 5>>
	<<set _like_chance to 80>>
	<<set _hate_chance to 15>>
<<else>>
	<<silently>>
	<<set _revealed_thing to "ERROR">>
	<<if $flaunt_type is "skirt">>
		<<if $flaunt_part is "genitals">>
			<<genitals>>
			<<set _revealed_thing to _text_output>>
		<<elseif $flaunt_part is "chastity">>
			<<set _revealed_thing to $worn.genitals.name>>
		<<elseif $flaunt_part is "lowerunderwear">>
			<<set _revealed_thing to $worn.under_lower.name>>
		<<else>>
			<<script>>
				throw "Error flaunt_part $flaunt_type $flaunt_part";
			<</script>>
		<</if>>
	<<elseif $flaunt_type is "moon">>
		<<if $flaunt_part is "bottom">>
			<<bottom>>
			<<set _revealed_thing to _text_output>>
		<<elseif $flaunt_part is "chastity">>
			<<set _revealed_thing to $worn.genitals.name>>
		<<elseif $flaunt_part is "lowerunderwear">>
			<<set _revealed_thing to $worn.under_lower.name>>
		<<else>>
			<<script>>
				throw "Error flaunt_part $flaunt_type $flaunt_part";
			<</script>>
		<</if>>
	<<elseif $flaunt_type is "top">>
		<<if $flaunt_part is "breasts">>
			<<breasts>>
			<<set _revealed_thing to _text_output>>
		<<elseif $flaunt_part is "upperunderwear">>
			<<set _revealed_thing to $worn.under_upper.name>>
		<<else>>
			<<script>>
				throw "Error flaunt_part $flaunt_type $flaunt_part";
			<</script>>
		<</if>>
	<<else>>
		<<script>>
			throw "Error flaunt_type $flaunt_type $flaunt_part";
		<</script>>
	<</if>>

	<<set _flaunt to "ERROR">>
	<<set _exposing to "exposing">>

	<<if $flaunt_type is "skirt">>
		<<set _flaunt to "lift up your skirt">>

		<<if $flaunt_part is "genitals">>
			<<fameexhibitionism 50>><<exhibitionism5>><<arousal 300>>
		<<elseif $flaunt_part is "chastity">>
			<<fameexhibitionism 5>> <<exhibitionism2>><<arousal 150>>
		<<elseif $flaunt_part is "lowerunderwear">>
			<<fameexhibitionism 5>> <<exhibitionism2>><<arousal 150>>
		<</if>>
	<<elseif $flaunt_type is "moon">>
		<<set _flaunt to "bend over and pull down your $worn.lower.name">>

		<<if $flaunt_part is "bottom">>
			<<fameexhibitionism 30>><<exhibitionism5>><<arousal 300>>
		<<elseif $flaunt_part is "chastity">>
			<<fameexhibitionism 5>> <<exhibitionism2>><<arousal 50>>
		<<elseif $flaunt_part is "lowerunderwear">>
			<<fameexhibitionism 5>> <<exhibitionism2>><<arousal 50>>
		<</if>>
	<<elseif $flaunt_type is "top">>
		<<set _flaunt to "pull aside your $worn.upper.name">>
		<<set _exposing to "flashing">>

		<<if $flaunt_part is "breasts">>
			<<if $player.gender_appearance is "m" and $breastsize lte 1>>
			<<fameexhibitionism 15>><<exhibitionism2>><<arousal 100>>
			<<else>>
			<<fameexhibitionism 40>><<exhibitionism4>><<arousal 300>>
			<</if>>
		<<elseif $flaunt_part is "upperunderwear">>
			<<fameexhibitionism 5>> <<exhibitionism2>><<arousal 100>>
		<<else>>
			<<fameexhibitionism 5>> <<exhibitionism2>><<arousal 100>>
		<</if>>
	<<else>>
		<<script>>
			throw "Error flaunt flaunt_part $flaunt_type $flaunt_part";
		<</script>>
	<</if>>
	<</silently>>

	<<if $phase is 1>>
		You _flaunt, giving them a brief look at your _revealed_thing.
	<<elseif $phase is 2>>
		You _flaunt, giving the <<person1>><<person>> and everyone else in the street a brief glimpse of your _revealed_thing.
	<<elseif $phase is 3>>
		You smile and answer "Yes, that's me... look!", and _flaunt, giving <<him>> a good look at your _revealed_thing.
	<<elseif $phase is 4>>
		You smile, briefly _flaunt in the direction of the group, _exposing your _revealed_thing.
	<<elseif $phase is 5>>
		You smile at the <<person>>, <<famesalute>>, and _flaunt giving the <<person1>><<person>> and everyone else in the street an eyeful of your _revealed_thing.
	<<else>>
		You briefly _flaunt, _exposing your _revealed_thing.
	<</if>>
	<<garousal>>
<</if>>
<br><br>

<<set $rng to random(1, 100)>>
<<if $rng gte _like_chance + _hate_chance>>
	<<if $phase is 1>>
		The <<person1>><<person>> and <<person2>><<person>> stare at you with open mouths.
		After a few seconds, the <<person1>><<person>> snaps out of it, blushing, <<he>> prods <<his>> friend and urges "Come on, we're late already!" They hurry off together.
	<<elseif $phase is 2>>
		The <<person1>><<person>> seems too shocked to react. You beam with pride as you walk on.
	<<elseif $phase is 3>>
		The <<person1>><<person>> stares speechlessly at your <<bottom>> as you walk on.
	<<elseif $phase is 4>>
		The wispering stops, and you walk away, enjoying the sudden stunned silence.
	<<elseif $phase is 5>>
		The <<person>> is dumbfounded. Proudly, you walk on.
	<<else>>
		You walk on, beaming with pride.
	<</if>>
	<<lstress>><<stress -6>>
	<<set _punish_chance to 10>>
<<elseif $rng gte _hate_chance>>
	<<if $phase is 1>>
		The <<person2>><<person>> stares at you with open mouth.
		<<if $flaunt_type is "words">>
			The <<person1>><<person>> grins and points at <<his>> friend, "Seems <<he>> wants to see you naked too!".
		<<else>>
			The <<person1>><<person>> grins at <<his>> friend, "Told you! <<pShes>> gorgeous!"
		<</if>>
		He prods <<his>> friend and says "Come on, we're late already!" They hurry off together.
	<<elseif $phase is 2>>
		<<if $flaunt_type is "words">>
			The <<person1>><<person>> shouts "I knew it! You look stunning!" You beam with pride as you walk on.
		<<else>>
			The <<person1>><<person>> shouts "Hah! Thanks for the reminder <<girl>>! You look stunning!" You beam with pride as you walk on.
		<</if>>
	<<elseif $phase is 3>>
		The <<person1>><<person>> grins and shamelessly stares at your body. You feel <<him>> leer at your bottom as you strut on.
	<<elseif $phase is 4>>
		An excited chatter bursts from the group. They keep glancing at you. As you walk on, you make out "Did you see that?!", "What a slut!" and "So sexy!"
	<<elseif $phase is 5>>
		The <<person>> grins at <<his>> friend and exclaims "Did you see that? <<pShes>> shameless!". <<His>> friend replies "Wouldn't you be with such a body?" Proudly, you walk on.
	<<else>>
		You walk on, beaming with pride.
	<</if>>
	<<lstress>><<stress -6>>
	<<ltrauma>><<trauma -6>>
	<<set _punish_chance to 0>>
<<else>>
	<<if $phase is 1>>
		The <<person2>><<person>> glares at you, and then bellows "Shame on you! Slut! Slut!"
		<<His>> friend drags <<him>> along, "Come on, we're late already!"
		<<He>> continues glaring at you and barks one more "Slut!" before leaving.
	<<elseif $phase is 2>>
		The <<person1>><<person>> shouts "Filthy slut! Have you no shame?" You hurry on.
	<<elseif $phase is 3>>
		The <<person1>><<person>> snarls at you, "You should be ashamed of yourself!" You feel him glaring at you as you hurry on.
	<<elseif $phase is 4>>
		An angry chatter bursts from the group. They're all looking your way, shouting abuse. "Slut!" "Shame on you!" "Pervert!" You hurry along.
	<<elseif $phase is 5>>
		The <<person>> grins at <<his>> friend and exclaims "<<pShes>> shameless!". <<His>> friend glares at you and shouts "You're a pervert!". You quickly walk away.
	<<else>>
		Someone starts shouting "Pervert!". You hurry on.
	<</if>>
	<<gstress>><<stress 12>>
	<<gtrauma>><<trauma 12>>
	<<set _punish_chance to 40>>
<</if>>

<<set $rng to random(1, 100)>>
<<if $rng lte _punish_chance>>
   <br><br>
   <<if $phase is 1>>
      <<set $firstNPC to clone($NPCList[0])>>
      <<set $secNPC to clone($NPCList[1])>>
      <<endevent>><<generate1>><<set $NPCList[0] to clone($secNPC)>>
      A short while later, you suddenly spot the <<person1>><<person>> right next to you. 
      Taking advantage of your confusion, <<he>> grabs you, and shoves you into a nearby alley. 
      <<He>> spits "Shameless slut! I'll show you!".
   <<elseif $phase is 2>>
      You only notice that the <<person1>><<person>> followed you, when <<he>> grabs you, and drags you into a nearby alley. 
      <<He>> bellows "Filthy slut! I'll teach you a lesson!"
   <<elseif $phase is 3>>
      Too late, you notice that the <<person1>><<person>> is following you. 
      Before you can react, <<he>> grabs you, and drags you into a nearby alley. 
      <<He>> grabs you close, and hisses "I'll give you what you deserve!" in your ear.
   <<elseif $phase is 4>>
      Two streets further, you suddenly get the feeling of someone sneaking up on you.
      You turn around, and look straight into the angry face of a <<person1>><<person>>. You recognize <<him>> as part of the group.<br>
      Before you can react, <<he>> pushes you into a nearby alley and throws <<himself>> against you, shrieking "You need a lesson!".
   <<elseif $phase is 5>>
      You walk a short while, when all of a sudden, you're pulled into an alley by the <<person1>><<person>>. 
      <<He>> presses you against a wall, and barks "I'll show you what we do with perverts here!"
   <<else>>
      Suddenly you're pushed into an alley by the <<person1>><<person>>. <<He>> presses you against a wall, and barks "I'll show you what we do with perverts here!"
   <</if>>
   <<gstress>><<stress 6>>
   <br><br>
   <<link [[Next|Street Exhibitionism Fame Flaunt Punish]]>><</link>>
<<else>>
   <br><br>
   <<endevent>>
   <<destinationeventend>>
<</if>>