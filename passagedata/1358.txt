<<set $outside to 1>><<set $location to "town">><<effects>>

You drop to the ground and assume a mating posture. The cat steps toward you, until you push a hand against its nose and give it a teasing wiggle.
<<deviancy1>>
The cat growls, annoyed but amused.
<br><br>
<<if $bestialitydisable is "f" and $deviancy gte 15>>
	<<link [[Seduce|Residential Beast]]>><<set $sexstart to 1>><</link>><<deviant2>>
	<br>
<</if>>
<<link [[Shoo|Residential Shoo]]>><</link>>
<br>