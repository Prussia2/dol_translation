<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "danube">>
<<skulduggerycheck>>
<br><br>
You sneak up to the <<person>> as quietly as you can. Once close enough, you reach out for the loot and
<<if $skulduggerysuccess is 1>>
	quickly ease it free. Like taking candy from a baby. A snoring, dribbling baby with more money than sense.
	<<crime 80>>
	<br><br>
	<<set $blackmoney += 80>>
	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>
	<<link [[Leave|Danube Street]]>><<endevent>><</link>>
	<br>
<<else>>
	try to ease it free. Unfortunately, <<he>> rolls over at just that moment. As you try to adjust your grip, you hear a snort and look down to find <<him>> blearily looking up at you.
	<br>
	<<His>> face turns to anger and you leap back just before <<he>> can grab you.
	<br><br>
	You have no choice but to flee empty handed.
	<<crime 80>>
	<br><br>
	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>
	<<link [[Run|Danube Street]]>><<endevent>><</link>>
	<br>
<</if>>