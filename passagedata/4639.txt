<<set $outside to 1>><<effects>>
<<generate1>><<person1>>
<<flaunting>> you approach the service station. You take a deep breath, and step through the automatic doors.
<<exhibitionism3>>

<<if $rng gte 51>>
	<<generate2>>
	A <<person>> reads a magazine at the counter. <<He>> doesn't look up as you enter.
	<br><br>

	You explore the aisles, looking for the sweets. You hear the doors open again as you find them. Someone else is in the shop.
	<<garousal>><<arousal 6>>
	<br><br>

	You walk to the end of the aisle, and peek around the corner, unsure if you'll find the newcomer. You hear a thud behind you. A <<person2>><<person>> stands there, <<his>> shopping basket on the floor beside <<him>> and <<his>> mouth agape. <<covered>>
	<br><br>

	A minute later you're back outside, a lollipop in your mouth, paid for by the <<personstop>>
	<<lstress>><<stress -6>>
	<br><br>

	<<link [[Next|High Street]]>><<endevent>><<set $eventskip to 1>><</link>>
	<br>

<<else>>
	<<fameexhibitionism 1>>
	A <<person>> reads a magazine at the counter. <<He>> glances up as you enter, then looks again, a shocked expression on <<his>> face. <<covered>>

	<<if $submissive gte 1150>>
		"Sorry to bother you," you say, approaching the <<personstop>> You try to remain nonchalant, but you feel your knees shake. "Do you sell sweets?"
	<<elseif $submissive lte 850>>
		"Yo," you say, approaching the <<personstop>> You try to remain nonchalant, but you feel your knees shake. "You sell sweets?"
	<<else>>
		"Excuse me," you say, approaching the <<personstop>> You try to remain nonchalant, but you feel your knees shake. "Do you sell sweets?"
	<</if>>
	<br><br>

	"I-I-", the <<person>> stutters, trying to remain focused on your face. "S-sure. I mean, y-yes. Just there." <<He>> points at one of the aisles.
	<br><br>

	You find a selection of penny sweets. You bend over to examine the labels, sticking your <<bottom>> in the <<persons>> direction. You return to the <<person>> once your bag is filled. Sweat rolls down the side of <<his>> face.
	<br><br>

	<<He>> lets you have the sweets without charge. You thank <<him>> before leaving the service station, a boiled sweet in your mouth.
	<<lstress>><<stress 6>>
	<br><br>

	<<link [[Next|High Street]]>><<endevent>><<set $eventskip to 1>><</link>>
	<br>

<</if>>