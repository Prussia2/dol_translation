<<run Save.autosave.save(null, {"saveId":$saveId, "saveName":$saveName})>>

<<sleep>><<effects>>
<<if $nightmares gte 1 and $controlled is 0>>
	You dream you are being hunted by something dark and terrible.
<<else>>
	<<if $specialClothesEffects.bimboTotal gte random(100,1000) or $arousal gte 9000>>
		Your sleep is plagued by dirty dreams.
		<<arousal `$specialClothesEffects.bimboTotal / 10 * $sleepHoursTotal`>>
		<<arousal `100 * $sleepHoursTotal`>>
		<<if $arousal gte $arousalmax>>
			They push you over the edge.
			<<orgasmpassage>>
		<<else>>
			<br><br>
		<</if>>
	<<else>>
		You sleep soundly.
	<</if>>
<</if>>
<<sleepeffects>>
<<if $schoolwake is 1>>
	<<set $schoolwake to 0>>
	You wake up to something mounting you.
	<br><br>
	<<link [[Let it happen|Wolf Cave Sleep Sex]]>><<set $sexstart to 1>><</link>><<deviant1>>
	<br>
	<<link [[Push away|Wolf Cave Sleep Push]]>><</link>>
	<br>
<<else>>
	<br><br>
	<<link [[Get up|Wolf Cave]]>><</link>>
	<br><br>
<</if>>