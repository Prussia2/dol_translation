<<set $outside to 0>><<set $location to "home">><<effects>>
<<if $submissive gte 1150>>
	"Would you like to go for a walk?" you ask.
<<elseif $submissive lte 850>>
	"Time for some exercise," you say.
<<else>>
	"Let's go for a walk," you say.
<</if>>
<br><br>
<<if $NPCName[$NPCNameList.indexOf("Robin")].trauma gte 40>>
	Robin smiles and nods.
	<br><br>
<<else>>
	"Good idea," Robin says. "Where do you want to go?"
	<br><br>
<</if>>
<<foresticon>><<link [[Forest (0:30)|Robin Forest]]>><<pass 30>><<set $robinwalk to 1>><</link>>
<br>
<<if $NPCName[$NPCNameList.indexOf("Robin")].love gte 20 and $money gte 1500>>
	<<link [[Cinema (£15 2:00)|Robin Walk Cinema]]>><<pass 15>><</link>>
	<br>
<</if>>
<<if $halloween_robin is 1 and $halloween_robin_costume is "ghost">>
<<link [[Go shopping for Halloween costumes (0:30)|Robin Forest Shop]]>><<pass 30>><</link>>
<br>
<</if>>
<br>
<<robinoptions>>