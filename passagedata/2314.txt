<<effects>>
<<wash>>
Winter pulls a lever, and you descend toward the water below. It splashes agaisnt your feet, then your legs, then up to your abdomen and chest. It's cold. You wonder how long the demonstration will last.
<br><br>

"Such a punishment was not usually fatal," Winter says, pulling the lever in the other direction. You begin to rise. "Public humiliation was the primary goal." The audience murmur amongst themselves. With a flash of panic, you look down to make sure the rags have not soaked to transparency. Before you can get a good look, Winter plunges you into the river once more, this time up to your neck.
<br><br>

With a final push of the lever, the chair lowers the rest of the way, submerging you.
<br><br>

<<link [[Next|Museum Duck Extreme 3]]>><</link>>
<br>