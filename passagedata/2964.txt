<<effects>>

You rifle through the <<person1>><<persons>> pockets. <<He>> mumbles something about tentacles and tries to squirm away, but <<hes>> in no state to stop you. You find a bag of bright-coloured pills just as a librarian rounds the corner. <<generate2>><<person2>><<He>> rushes to the <<person1>><<persons>> side.
<br><br>

<<person2>><<He>> eyes you with suspicion as you leave, but doesn't say anything.
<br><br>

<<endevent>>

<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
<br>