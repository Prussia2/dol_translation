<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You duck under Leighton's arm and flee the room, chased by laughter.
<br><br>
<<endevent>><<npc Sirris>><<person1>>
You run down the hallways, angry and frustrated at Leighton and the class. You run round a corner and collide with Sirris. You both tumble to the ground.
<<set $pain += 40>>
<br><br>

"No running," Sirris says, rubbing <<his>> back. "Aren't you supposed to-" <<he>> sees something in your appearance that changes <<his>> demeanour.
"What's wrong?" <<He>> helps you up. You try to speak, but struggle to form the words. "It's okay," Sirris says. "I'm on my way to the staff room.
Going to wait for the head to finish. You're welcome to join me if you like."
<br><br>

You follow Sirris to the staff room. It's small, with several chairs around a table in the middle. "Take a seat," <<he>> says as <<he>> sits.
<br><br>

You spend the next few minutes sat in silence while Sirris reads an old newspaper. You feel a need to stand up and run, as if you're still in the
classroom. Slowly though, your pulse slows and you start to feel more at ease. Sirris puts the paper back on the table. "I feel guilty sat here when
I should be working," <<he>> says. "Let's go check on them."
<br><br>

You don't like the idea of returning to class, but you follow <<him>> back to the classroom and arrive in time to see Leighton leaving.
<<endevent>><<npc Leighton>><<person1>>"Good timing," Leighton says to Sirris. "You're doing a good job." <<He>> looks at you. "I'll be seeing you in detention."
<br><br>

You follow Sirris into the classroom, prepared to face your peers. You're surprised when people barely look in your direction. The girls chat,
still excited, while the boys sit quiet and avoid looking at each other. Your flight was just a footnote in the entire experience.
<br><br>

<<endevent>>
<<link [[Next|Science Lesson]]>><</link>>