<<set $outside to 1>><<effects>><<lakeeffects>>
You sprint away from the boar. You hear it's paws breaking the dirt behind you.
<<if $athletics gte random(1, 1000)>>
	<span class="green">You run fast enough that the boar gives up.</span> You turn and see it returning to the litter by the tree.
	<br><br>
	<<set $eventskip to 1>>
	<<destinationlake>>
<<else>>
	<span class="red">You're not fast enough.</span> The boar knocks you to the ground and leaps on top of you.
	<br><br>
	<<link [[Next|Lake Boar Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<</if>>