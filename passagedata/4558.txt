<<effects>>

You pinch yourself.

<<if $masochism_level gte 1>>
	The pain takes your mind off the unnatural thoughts forced on you by the fluid, but your masochism spurs lewd thoughts of your own. You need to be careful. You'll be caught just as readily should you start crying.
<<else>>
	The pain takes your mind off your arousal, but you need to be careful. You'll be caught just as readily should you start crying.
<</if>>
<br><br>

<<link [[Next|Industrial Ex Aphrodisiac]]>><</link>>
<br>