<<effects>>

<<generate1>><<generate2>><<generate3>><<generate4>><<person1>>

"We got ourselves another would-be escapee," says the hand's owner as <<he>> releases you.
<br>
"This one's pretty hot though," says a <<person2>><<person>> beside <<himstop>> Your eyes adjust to the dark at last. It looks like a basement with a hole built into the floor, giving access to the river below. "For a <<if $player.gender_appearance is "m">>bull<<else>>cow<</if>>, I mean."
<br>
You feel someone pinch your ass, and jump. "Remy won't mind if we, ah, take the punishment into our own hands," says the culprit, a <<person3>><<personstop>> "Saves the trouble."
<br>
"I like it," says a fourth voice. You hear wood scrape against the floor, and a <<person4>><<person>> rises to <<his>> feet. "Let it never be said we don't earn our keep."
<br><br>

<<link [[Next|Livestock Field River Rape]]>><<set $molestationstart to 1>><</link>>
<br>