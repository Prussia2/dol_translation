<<effects>>

You shake your head and keep walking.

<<if $rng gte 81>>
	The <<person>> doesn't give up, following and stepping in front of you.
	<br><br>

	"Listen with care, <<girl>>," <<He>> whispers. "You don't want to cross me, or my bosses. You're coming with me."
	<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
	<br><br>

	<<link [[Push past|Street Offer Push]]>><<def 1>><</link>>
	<br>
	<<link [[Nod|Street Offer Nod]]>><<sub 1>><</link>>
	<br>
<<else>>
	The <<person>> doesn't pursue.
	<br><br>

	<<endevent>>
	<<destinationeventend>>
<</if>>