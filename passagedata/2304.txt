<<effects>>
<<if $enemyarousal gte $enemyarousalmax>><<set $forcedprostitutionstat += 1>>
<<ejaculation>>
You're left lying on the ground beneath your stall.
<br><br>
<<tearful>> you climb to your feet.
<br><br>
<<clotheson>>
<<endcombat>>

<<stall_actions>>

<<elseif $enemyhealth lte 0>>
You shove the <<person1>><<person>> to the side, where <<he>> collides with a neighbouring stall. The stall's owner isn't the only one to notice.
<br><br>
<<He>> staggers to <<his>> feet and glares at you, but thinks better than to try anything. <<He>> stomps away as the <<person2>><<person>> pretends <<hes>> not involved.
<br><br>
<<tearful>> you take your place behind your stall.
<br><br>
<<clotheson>>
<<endcombat>>
<<stall_actions>>

<<else>>
<<set $rescued += 1>>
"Did you hear that scream?"
<br>
"What's going on over there?"
<br>
"Someone should take a look."
<br><br>
The <<person2>><<person>> steps away from the table, letting the cover fall and struggling to <<if $pronoun is "m">>zip <<his>> fly<<else>>pull down <<his>> skirt.<</if>>
<br><br>
The <<person1>><<person>> peeks outside. Fear crosses <<his>> face. <<He>> shoots you one last glare, then flees.
<br><br>
<<tearful>> you return to your place behind the display.
<br><br>
<<clotheson>>
<<endcombat>>
<<stall_actions>>

<</if>>