<<set $outside to 0>><<effects>>
You lean forward and kiss <<himstop>> <<His>> eyes close, and <<he>> kisses back. <<His>> cheeks are wet when you pull away. Yours are too.
<br><br>
The rest of the journey is uneventful. Robin stays close to you the whole way.
<br><br>
<<link [[Next|School Front Playground]]>><<endevent>><</link>>
<br>