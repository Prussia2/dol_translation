<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
You sit on the Doctor's lap. <<He>> wraps an arm around your waist.
<<promiscuity1>>
"Today I want to talk about kissing." <<he>> says. "It's a normal and healthy activity. You should be happy kissing anyone at any time." <<He>> leans close. "Kiss me."
<br><br>
<<link [[Kiss cheek|Asylum Sit Kiss]]>><<suspicion -1>><<awareness -1>><</link>><<promiscuous1>><<lsuspicion>><<lawareness>>
<br>
<<if $promiscuity gte 15>>
	<<link [[Kiss mouth|Asylum Sit Kiss Mouth]]>><<suspicion -5>><<awareness -10>><<npcincr Harper love 1>><</link>><<promiscuous2>><<llsuspicion>><<llawareness>>
	<br>
	<<set $skulduggerydifficulty to (700 + ($suspicion * 6))>>
	<<if $leftarm is "bound" and $rightarm is "bound">>
	<<elseif $asylumkeycard isnot 1>>
		<<link [[Steal keycard|Asylum Sit Steal]]>><</link>><<promiscuous2>><<skulduggerydifficulty>> <i>This will be easier with low suspicion.</i>
		<br>
	<</if>>
<</if>>
<<link [[Refuse|Asylum Sit Refuse]]>><<suspicion 1>><<stress 3>><</link>><<gsuspicion>><<gstress>>