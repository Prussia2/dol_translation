<<set $outside to 1>><<effects>>
<<towelup>>
As you rise, they scoop up your clothing, but instead of handing it back, they inform you and the crowd that they will be taking your clothes for forensic investigation.
<br>
You watch in horror as they climb into the car. A window winds down and the <<person2>><<person>> hands you a couple of filthy towels.
<br>
As the officers disappear around a corner, you cover up and hurry away.
<<gtrauma>><<gstress>><<ggarousal>><<trauma 2>><<stress 2>><<arousal 1800>>
<br><br>
<<fameexhibitionism 30>>
<<endevent>>
<<destinationeventend>>