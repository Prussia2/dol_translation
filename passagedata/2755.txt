<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $phase is 0>>
You decide not to make a fuss, and allow the <<person>> to continue groping you while you continue writing on the board. <<He>> is careful not to be too blatant about it, but by the time Leighton lets you out your face is flushed red.
<br><br>
<<link [[Next|Hallways]]>><<endevent>><</link>>
<br>
<<elseif $phase is 1>>
	<<if $delinquency gte 400>>
		You yelp and jump away from the <<personcomma>> clutching your backside and making it clear to Leighton what just happened. <<person1>>Leighton gives you a level stare. "I'm not going to fall for your tricks." <<He>> looks at the <<person2>><<personstop>> "You're done here, leave," the <<person>> wastes no time in vacating the room.
		<br><br>
		<<person1>>Once alone, Leighton grabs you by the arms and bends you over <<his>> knee.
		<br><br>
		<<endevent>>
		<<link [[Next|School Detention Spank]]>><<npc Leighton>><<set $molestationstart to 1>><</link>>
		<br>
	<<else>>
		You yelp and jump away from the <<personcomma>> clutching your backside and making it clear to Leighton what just happened. <<person1>> <<He>> looks at the <<person2>><<personcomma>> then back at you.<<person1>> "You're done here, leave." Something in <<his>> voice precludes argument, and you scurry from the room. You think you hear a muffled cry behind you as you walk down the hall.
		<br><br>
		<<link [[Next|Hallways]]>><<endevent>><</link>>
		<br>
	<</if>>
<</if>>