<<set $outside to 0>><<set $location to "museum">><<effects>>

<<if $phase is 0>>
	<<if $submissive gte 1150>>
	"I-I thought you'd be happy I was showing interest," you say.
	<<elseif $submissive lte 850>>
	You put your hands on your hips. "Is that how you always greet people?" you ask. "I though you'd be happy I was showing interest."
	<<else>>
	"I expected you to be happy I was showing interest," you say.
	<</if>>
	<br><br>
A vein twitches on <<his>> forehead as <<he>> stares at you. "You shouldn't speak to your elders that way," <<he>> says. "But that was rude of me."

<<elseif $phase is 1>>
	<<if $submissive gte 1150>>
	You nod, and keep your hands firm at your sides.
	<<elseif $submissive lte 850>>
	You clench your firsts and look away. "Sure," you say. "Why would I want to touch these dumb things anyway?"
	<<else>>
	"I will," you say, keeping your hands firm at your sides.
	<</if>>
	<br><br>
"Good," <<he>> says. "Forgive my caution, but some of these are very old."

<<else>>
	<<if $submissive gte 1150>>
	"I am very dirty," you purr. "Would you help wash me?"
	<<elseif $submissive lte 850>>
	"That's okay," you purr. "You're the only antique I want to touch."
	<<else>>
	"The only thing I want to touch is you," you purr.
	<</if>>
<<promiscuity1>>

"What insolence," <<he>> sneers. "I suppose I should expect no better. Don't think I won't ban you from the premises."

<</if>>
<br><br>

<<He>> places the pot back on its pedestal. "It's a sorry collection for an area so rich in history. The mayor's office agrees, and has set aside funds to reward anyone who provides local antiques."
<br><br>

<<He>> starts polishing an ornate breastplate. "If you find anything, I'll appraise it. I won't hold my breath."
<br><br>
<<endevent>>
<<link [[Next|Museum]]>><</link>>
<br>