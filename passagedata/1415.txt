<<effects>>

The dog whimpers and flees.
<br><br>

Before the pair can react, you seize the opportunity and bolt away. <<tearful>> you quickly lose them in the dunes.
<br><br>

<<clotheson>>
<<endcombat>>

<<set $stress -= 500>>

<<link [[Next|Beach]]>><</link>>
<<set $eventskip to 1>>