<<effects>>
<<generate3>><<generate4>>
They lead you inside, where a <<person3>><<person>> and <<person4>><<person>> wait. The <<person3>><<person>> shakes hand with the police officers, while the <<person4>><<person>> prowls around you.
<br><br>
"This the chef, then?" the <<person3>><<person>> asks.
<br>
"This the one," The <<person1>><<person>> responds.
<br>
"Dunno what I was expecting. Here's the money." <<He>> nods a the <<person4>><<personcomma>> now stood behind you, and you lurched back as <<he>> ties a collar around your neck.
<br><br>
The police leave as the <<person3>><<person>> takes your leash. "Gonna make a killing from this one," <<he>> whispers as <<he>> tugs. You're pulled forward.
<br><br>
<<link [[Next|Livestock Intro]]>><<endevent>><</link>>
<br>