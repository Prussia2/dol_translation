<<widget "img_tf_doggy_idle">><<nobr>>
<<if $physicalTransform is 1>>
	<<img_tf_wolf_doggy_idle>>
	<<img_tf_cat_doggy_idle>>
	<<img_tf_cow_doggy_idle>>
<</if>>
<<if $specialTransform is 1>>
	<<img_tf_angel_doggy_idle>>
	<<img_tf_fallenangel_doggy_idle>>
	<<img_tf_demon_doggy_idle>>
<</if>>
<</nobr>><</widget>>

<<widget "img_tf_doggy_active">><<nobr>>
<<if $physicalTransform is 1>>
	<<img_tf_wolf_doggy_active>>
	<<img_tf_cat_doggy_active>>
	<<img_tf_cow_doggy_active>>
<</if>>
<<if $specialTransform is 1>>
	<<img_tf_angel_doggy_active>>
	<<img_tf_fallenangel_doggy_active>>
	<<img_tf_demon_doggy_active>>
<</if>>
<</nobr>><</widget>>

<<widget "img_tf_miss_idle">><<nobr>>
<<if $physicalTransform is 1>>
	<<img_tf_wolf_miss_idle>>
	<<img_tf_cat_miss_idle>>
	<<img_tf_cow_miss_idle>>
<</if>>
<<if $specialTransform is 1>>
	<<img_tf_angel_miss_idle>>
	<<img_tf_fallenangel_miss_idle>>
	<<img_tf_demon_miss_idle>>
<</if>>
<</nobr>><</widget>>

<<widget "img_tf_miss_active">><<nobr>>
<<if $physicalTransform is 1>>
	<<img_tf_wolf_miss_active>>
	<<img_tf_cat_miss_active>>
	<<img_tf_cow_miss_active>>
<</if>>
<<if $specialTransform is 1>>
	<<img_tf_angel_miss_active>>
	<<img_tf_fallenangel_miss_active>>
	<<img_tf_demon_miss_active>>
<</if>>
<</nobr>><</widget>>

<<widget "img_tf_wolf_doggy_idle">><<nobr>>
	<<if !_disabled.includes($transformationParts.wolf.ears)>>
		<img class="layer-sexlashes colour-hair anim-idle-2f" @src="'img/sex/doggy/active/transformations/wolf/ears/'+$transformationParts.wolf.ears+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.wolf.pits)>>
		<img class="layer-doggyhirsute colour-hair" @src="'img/sex/doggy/active/transformations/hirsute/pits/'+$transformationParts.wolf.pits+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.wolf.pubes)>>
		<img class="layer-doggyhirsute colour-hair" @src="'img/sex/doggy/active/transformations/hirsute/pubes/'+$transformationParts.wolf.pubes+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.wolf.tail)>>
		<img class="layer-sexlashes colour-hair anim-idle-2f" @src="'img/sex/doggy/active/transformations/wolf/tail/'+$transformationParts.wolf.tail+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_wolf_doggy_active">><<nobr>>
	<<if !_disabled.includes($transformationParts.wolf.ears)>>
		<img @class="'layer-sexlashes colour-hair anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/wolf/ears/'+$transformationParts.wolf.ears+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.wolf.pits)>>
		<img @class="'layer-doggyhirsute colour-hair anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/hirsute/pits/'+$transformationParts.wolf.pits+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.wolf.pubes)>>
		<img @class="'layer-doggyhirsute colour-hair anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/hirsute/pubes/'+$transformationParts.wolf.pubes+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.wolf.tail)>>
		<img @class="'layer-sexlashes colour-hair anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/wolf/tail/'+$transformationParts.wolf.tail+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_wolf_miss_idle">><<nobr>>
	<<if !_disabled.includes($transformationParts.wolf.ears)>>
		<img class="layer-sexlashes colour-hair anim-idle-2f" @src="'img/sex/missionary/active/transformations/wolf/ears/'+$transformationParts.wolf.ears+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.wolf.pits)>>
		<img class="layer-doggyhirsute colour-hair" @src="'img/sex/missionary/active/transformations/hirsute/pits/'+$transformationParts.wolf.pits+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.wolf.pubes)>>
		<img class="layer-doggyhirsute colour-hair" @src="'img/sex/missionary/active/transformations/hirsute/pubes/'+$transformationParts.wolf.pubes+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.wolf.tail)>>
		<img class="layer-sexlashes colour-hair anim-idle-2f" @src="'img/sex/missionary/active/transformations/wolf/tail/'+$transformationParts.wolf.tail+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_wolf_miss_active">><<nobr>>
	<<if !_disabled.includes($transformationParts.wolf.ears)>>
		<img @class="'layer-sexlashes colour-hair anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/wolf/ears/'+$transformationParts.wolf.ears+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.wolf.pits)>>
		<img @class="'layer-doggyhirsute colour-hair anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/hirsute/pits/'+$transformationParts.wolf.pits+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.wolf.pubes)>>
		<img @class="'layer-doggyhirsute colour-hair anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/hirsute/pubes/'+$transformationParts.wolf.pubes+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.wolf.tail)>>
		<img @class="'layer-sexlashes colour-hair anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/wolf/tail/'+$transformationParts.wolf.tail+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_cat_doggy_idle">><<nobr>>
	<<if !_disabled.includes($transformationParts.cat.ears)>>
		<img class="layer-sexlashes colour-hair anim-idle-2f" @src="'img/sex/doggy/active/transformations/cat/ears/'+$transformationParts.cat.ears+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.cat.tail)>>
		<img class="layer-sexlashes colour-hair anim-idle-2f" @src="'img/sex/doggy/active/transformations/cat/tail/'+$transformationParts.cat.tail+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_cat_doggy_active">><<nobr>>
	<<if !_disabled.includes($transformationParts.cat.ears)>>
		<img @class="'layer-sexlashes colour-hair anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/cat/ears/'+$transformationParts.cat.ears+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.cat.tail)>>
		<img @class="'layer-sexlashes colour-hair anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/cat/tail/'+$transformationParts.cat.tail+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_cat_miss_idle">><<nobr>>
	<<if !_disabled.includes($transformationParts.cat.ears)>>
		<img class="layer-sexlashes colour-hair anim-idle-2f" @src="'img/sex/missionary/active/transformations/cat/ears/'+$transformationParts.cat.ears+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.cat.tail)>>
		<img class="layer-sexlashes colour-hair anim-idle-2f" @src="'img/sex/missionary/active/transformations/cat/tail/'+$transformationParts.cat.tail+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_cat_miss_active">><<nobr>>
	<<if !_disabled.includes($transformationParts.cat.ears)>>
		<img @class="'layer-sexlashes colour-hair anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/cat/ears/'+$transformationParts.cat.ears+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.cat.tail)>>
		<img @class="'layer-sexlashes colour-hair anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/cat/tail/'+$transformationParts.cat.tail+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_cow_doggy_idle">><<nobr>>
	<<if !_disabled.includes($transformationParts.cow.horns)>>
		<img class="layer-neck anim-idle-2f" @src="'img/sex/doggy/active/transformations/cow/horns/'+$transformationParts.cow.horns+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.cow.ears)>>
		<img class="layer-neck anim-idle-2f" @src="'img/sex/doggy/active/transformations/cow/ears/'+$transformationParts.cow.ears+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.cow.tail)>>
		<img class="layer-sexlashes anim-idle-2f" @src="'img/sex/doggy/active/transformations/cow/tail/'+$transformationParts.cow.tail+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_cow_doggy_active">><<nobr>>
	<<if !_disabled.includes($transformationParts.cow.horns)>>
		<img @class="'layer-foreground anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/cow/horns/'+$transformationParts.cow.horns+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.cow.ears)>>
		<img @class="'layer-foreground anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/cow/ears/'+$transformationParts.cow.ears+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.cow.tail)>>
		<img @class="'layer-sexlashes anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/cow/tail/'+$transformationParts.cow.tail+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_cow_miss_idle">><<nobr>>
	<<if !_disabled.includes($transformationParts.cow.horns)>>
		<img class="layer-neck anim-idle-2f" @src="'img/sex/missionary/active/transformations/cow/horns/'+$transformationParts.cow.horns+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.cow.ears)>>
		<img class="layer-neck anim-idle-2f" @src="'img/sex/missionary/active/transformations/cow/ears/'+$transformationParts.cow.ears+'.png'">
		<img class="layer-face anim-idle-2f" src="img/sex/missionary/active/transformations/cow/tag.png">
	<</if>>
	<<if !_disabled.includes($transformationParts.cow.tail)>>
		<img class="layer-sexlashes anim-idle-2f" @src="'img/sex/missionary/active/transformations/cow/tail/'+$transformationParts.cow.tail+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_cow_miss_active">><<nobr>>
	<<if !_disabled.includes($transformationParts.cow.horns)>>
		<img @class="'layer-neck anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/cow/horns/'+$transformationParts.cow.horns+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.cow.ears)>>
		<img @class="'layer-neck anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/cow/ears/'+$transformationParts.cow.ears+'.png'">
		<img @class="'layer-face anim-doggy-4f-'+_animspeed" src="img/sex/missionary/active/transformations/cow/tag.png">
	<</if>>
	<<if !_disabled.includes($transformationParts.cow.tail)>>
		<img @class="'layer-sexlashes anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/cow/tail/'+$transformationParts.cow.tail+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_angel_doggy_idle">><<nobr>>
	<<if !_disabled.includes($transformationParts.angel.halo)>>
		<img class="layer-backhair anim-idle-2f" @src="'img/sex/doggy/active/transformations/angel/backhalo/'+$transformationParts.angel.halo+'.png'">
		<img class="layer-parasite anim-idle-2f" @src="'img/sex/doggy/active/transformations/angel/fronthalo/'+$transformationParts.angel.halo+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.angel.wings)>>
		<img class="layer-backhair anim-idle-2f" @src="'img/sex/doggy/active/transformations/angel/backwings/'+$transformationParts.angel.wings+'.png'">
		<img class="layer-parasite anim-idle-2f" @src="'img/sex/doggy/active/transformations/angel/frontwings/'+$transformationParts.angel.wings+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_angel_doggy_active">><<nobr>>
	<<if !_disabled.includes($transformationParts.angel.halo)>>
		<img @class="'layer-backhair anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/angel/backhalo/'+$transformationParts.angel.halo+'.png'">
		<img @class="'layer-parasite anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/angel/fronthalo/'+$transformationParts.angel.halo+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.angel.wings)>>
		<img @class="'layer-backhair anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/angel/backwings/'+$transformationParts.angel.wings+'.png'">
		<img @class="'layer-parasite anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/angel/frontwings/'+$transformationParts.angel.wings+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_angel_miss_idle">><<nobr>>
	<<if !_disabled.includes($transformationParts.angel.halo)>>
		<img class="layer-backhair anim-idle-2f" @src="'img/sex/missionary/active/transformations/angel/backhalo/'+$transformationParts.angel.halo+'.png'">
		<img class="layer-parasite anim-idle-2f" @src="'img/sex/missionary/active/transformations/angel/fronthalo/'+$transformationParts.angel.halo+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.angel.wings)>>
		<img class="layer-sexlashes anim-idle-2f" @src="'img/sex/missionary/active/transformations/angel/wings/'+$transformationParts.angel.wings+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_angel_miss_active">><<nobr>>
	<<if !_disabled.includes($transformationParts.angel.halo)>>
		<img @class="'layer-backhair anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/angel/backhalo/'+$transformationParts.angel.halo+'.png'">
		<img @class="'layer-parasite anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/angel/fronthalo/'+$transformationParts.angel.halo+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.angel.wings)>>
		<img @class="'layer-sexlashes anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/angel/wings/'+$transformationParts.angel.wings+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_fallenangel_doggy_idle">><<nobr>>
	<<if !_disabled.includes($transformationParts.fallenAngel.halo)>>
		<img class="layer-backhair anim-idle-2f" @src="'img/sex/doggy/active/transformations/fallenangel/backhalo/'+$transformationParts.fallenAngel.halo+'.png'">
		<img class="layer-parasite anim-idle-2f" @src="'img/sex/doggy/active/transformations/fallenangel/fronthalo/'+$transformationParts.fallenAngel.halo+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.fallenAngel.wings)>>
		<img class="layer-sexlashes anim-idle-2f" @src="'img/sex/doggy/active/transformations/fallenangel/wings/'+$transformationParts.fallenAngel.wings+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_fallenangel_doggy_active">><<nobr>>
	<<if !_disabled.includes($transformationParts.fallenAngel.halo)>>
		<img @class="'layer-backhair anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/fallenangel/backhalo/'+$transformationParts.fallenAngel.halo+'.png'">
		<img @class="'layer-parasite anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/fallenangel/fronthalo/'+$transformationParts.fallenAngel.halo+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.fallenAngel.wings)>>
		<img @class="'layer-sexlashes anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/fallenangel/wings/'+$transformationParts.fallenAngel.wings+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_fallenangel_miss_idle">><<nobr>>
	<<if !_disabled.includes($transformationParts.fallenAngel.halo)>>
		<img class="layer-backhair anim-idle-2f" @src="'img/sex/missionary/active/transformations/fallenangel/backhalo/'+$transformationParts.fallenAngel.halo+'.png'">
		<img class="layer-parasite anim-idle-2f" @src="'img/sex/missionary/active/transformations/fallenangel/fronthalo/'+$transformationParts.fallenAngel.halo+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.fallenAngel.wings)>>
		<img class="layer-sexlashes anim-idle-2f" @src="'img/sex/missionary/active/transformations/fallenangel/wings/'+$transformationParts.fallenAngel.wings+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_fallenangel_miss_active">><<nobr>>
	<<if !_disabled.includes($transformationParts.fallenAngel.halo)>>
		<img @class="'layer-backhair anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/fallenangel/backhalo/'+$transformationParts.fallenAngel.halo+'.png'">
		<img @class="'layer-parasite anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/fallenangel/fronthalo/'+$transformationParts.fallenAngel.halo+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.fallenAngel.wings)>>
		<img @class="'layer-sexlashes anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/fallenangel/wings/'+$transformationParts.fallenAngel.wings+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_demon_doggy_idle">><<nobr>>
	<<if !_disabled.includes($transformationParts.demon.horns)>>
		<img class="layer-parasite anim-idle-2f" @src="'img/sex/doggy/active/transformations/demon/horns/'+$transformationParts.demon.horns+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.demon.tail)>>
		<img class="layer-sexlashes anim-idle-2f" @src="'img/sex/doggy/active/transformations/demon/tail/'+$transformationParts.demon.tail+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.demon.wings)>>
		<img class="layer-parasite anim-idle-2f" @src="'img/sex/doggy/active/transformations/demon/wings/'+$transformationParts.demon.wings+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_demon_doggy_active">><<nobr>>
	<<if !_disabled.includes($transformationParts.demon.horns)>>
		<img @class="'layer-parasite anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/demon/horns/'+$transformationParts.demon.horns+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.demon.tail)>>
		<<if $enemytype is "man" and ($anusstate is "penetrated" or $vaginastate is "penetrated")>>
			<img @class="'layer-sexback anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/demon/tailsexback/'+$transformationParts.demon.tail+'.png'">
			<img @class="'layer-parasite anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/demon/tailsexfront/'+$transformationParts.demon.tail+'.png'">
		<<else>>
			<img @class="'layer-sexlashes anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/demon/tail/'+$transformationParts.demon.tail+'.png'">
		<</if>>
	<</if>>
	<<if !_disabled.includes($transformationParts.demon.wings)>>
		<img @class="'layer-parasite anim-doggy-4f-'+_animspeed" @src="'img/sex/doggy/active/transformations/demon/wings/'+$transformationParts.demon.wings+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_demon_miss_idle">><<nobr>>
	<<if !_disabled.includes($transformationParts.demon.horns)>>
		<img class="layer-parasite anim-idle-2f" @src="'img/sex/missionary/active/transformations/demon/horns/'+$transformationParts.demon.horns+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.demon.tail)>>
		<img class="layer-sexlashes anim-idle-2f" @src="'img/sex/missionary/active/transformations/demon/tail/'+$transformationParts.demon.tail+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.demon.wings)>>
		<img class="layer-parasite anim-idle-2f" @src="'img/sex/missionary/active/transformations/demon/wings/'+$transformationParts.demon.wings+'.png'">
	<</if>>
<</nobr>><</widget>>

<<widget "img_tf_demon_miss_active">><<nobr>>
	<<if !_disabled.includes($transformationParts.demon.horns)>>
		<img @class="'layer-parasite anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/demon/horns/'+$transformationParts.demon.horns+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.demon.tail)>>
		<img @class="'layer-sexlashes anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/demon/tail/'+$transformationParts.demon.tail+'.png'">
	<</if>>
	<<if !_disabled.includes($transformationParts.demon.wings)>>
		<img @class="'layer-parasite anim-doggy-4f-'+_animspeed" @src="'img/sex/missionary/active/transformations/demon/wings/'+$transformationParts.demon.wings+'.png'">
	<</if>>
<</nobr>><</widget>>