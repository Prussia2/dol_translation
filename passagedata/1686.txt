<<set $outside to 0>><<set $location to "cafe">><<effects>>

You drop the spoon and stand up before <<he>> can take it further.
<br><br>

The <<person>> climbs back out from under the table. "Not hungry?" <<he>> says. "You're so cute."
<br><br>

The rest of the shift passes uneventfully. You earn £5.
<<set $money += 500>>
<br><br>

<<link [[Next|Ocean Breeze]]>><<endevent>><<pass 1 hour>><</link>>