<<effects>>

You swim among the lotus flowers, gathering seeds. <span class="gold">You can now grow lotus.</span> <i>Lotus are valuable, but might be difficulty to find the right environment for.</i>
<br><br>

<<link [[Next|Lake Shallows]]>><</link>>
<br>