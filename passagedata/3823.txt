<<effects>>

Avery tries to pull you in one direction, but you pull <<him>> in another.

<<if $danceskill gte random(1, 600)>>
	<<He>> resists for a moment, <span class="green">but then goes along with it.</span>

	<<if $danceskill gte 600>>
		You're good at this, after all.
	<<elseif $danceskill gte 400>>
		You're a confident dancer, but you're careful not to show <<him>> up.
	<<elseif $danceskill gte 100>>
		It's not your first dance, but you worry you'll show <<him>> up. However, you manage to follow the music without problem.
	<<else>>
		You worry you'll show <<him>> up, until you notice the other dancers are novices themselves.
	<</if>>

	Avery dances well. You've soon attracted quite an audience. <<danceskilluse>><<gglove>><<ggendear>><<npcincr Avery love 3>><<set $endear += 20>>
	<br><br>

	You continue dancing. The other couples stop, more interested in watching you and Avery. All except one other couple, who have attracted an audience of their own.
	<br><br>

	<<link [[Next|Avery Party Dance Competition]]>><</link>>
	<br>

<<else>>
	<span class="red">Your feet get tangled,</span> and you're both left sprawled on the floor. Several people laugh. Avery tries to laugh it off <<himselfcomma>> but you're sure <<hes>> angry. <<danceskilluse>><<garage>><<llove>><<lendear>><<npcincr Avery rage 5>><<npcincr Avery love -1>><<set $endear -= 5>><<gstress>><<stress 6>><<gpain>><<pain 6>>
	<br><br>

	You sit out the rest of the dance, and watch the other couples. Avery doesn't say much, even when the party wraps up and several guests approach to bid <<him>> goodbye. It doesn't help that a few make jabs at the mishap on the dance floor.
	<br><br>

	<<link [[Next|Avery Party End]]>><</link>>
	<br>

<</if>>