<<set $outside to 1>><<set $location to "forest">><<effects>>

<<if $enemyarousal gte $enemyarousalmax>>
	<<beastejaculation>>
	<<if $combatTrain.length gt 0>>
		Satisfied, the beast leaves you be. Another takes its place.
		<<beastNEWinit $combatTrain.numberPerTrain[0] $combatTrain.beastTypes[0]>>
		<<combatTrainAdvance>>
		<br><br>
		[[Next|Forest Wolf2]]
	<<else>>
		Satisfied, the beast leaves you be.
		<br><br>
		[[Next|Forest Wolf2 End]]
	<</if>>
<<elseif $enemyhealth lte 0>>
	<<beastwound>>
	<<if $combatTrain.length gt 0>>
		The beast recoils in pain and fear, but another takes its place.
		<<beastNEWinit $combatTrain.numberPerTrain[0] $combatTrain.beastTypes[0]>>
		<<combatTrainAdvance>>
		<br><br>
		[[Next|Forest Wolf2]]
	<<else>>
		The beast recoils in pain and fear.
		<br><br>
		[[Next|Forest Wolf2 End]]
	<</if>>
<</if>>