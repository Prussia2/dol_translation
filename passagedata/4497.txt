<<effects>>
The <<person1>><<person>> hovers the needle over your pelvis, then gets to work.
<br><br>
It stings.
<<gpain>><<pain 6>>
<br><br>
<<pass 60>>

<<if $skin.left_shoulder.pen isnot "brand" and $skin.left_shoulder.pen isnot "magic" and $skin.left_shoulder.pen isnot "tattoo">>
	The <<person2>><<person>> lifts a needle of <<his>> own and, holding your arm down with <<his>> knees, gets to work on your shoulder.
	<br><br>
<</if>>

You remain held down through the procedure. By the end you've grown accustomed to the pain, and even your anxiety has subsided a little. It flares back up when the <<person1>><<person>> pulls away, wearing an evil smile ear-to-ear.
<br><br>
You look down.

<<if $skin.left_shoulder.pen isnot "brand" and $skin.left_shoulder.pen isnot "magic" and $skin.left_shoulder.pen isnot "tattoo">>
	They've tattooed <span class="pink">"Slave"</span> onto your pelvis, and <span class="pink">"163"</span> onto your shoulder.
	<<add_bodywriting left_shoulder one_six_three tattoo>>
<<else>>
	They've tattooed <span class="pink">"Slave"</span> onto your pelvis.
<</if>>

 "So you don't forget," the <<person1>><<person>> says as the others release you.
<br><br>
<<add_bodywriting pubic slave tattoo>>
They leave the room, taking their machine with them. You're once more plunged into darkness.
<br><br>

<<endevent>>
<<link [[Next|Underground Cell]]>><</link>>
<br>