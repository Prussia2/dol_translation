<<effects>>

<<endevent>><<npc Remy>><<person1>>Harper releases you as the front door opens. Remy enters, wiping sweat from <<his>> forehead. "I must get this one back to the field," <<he>> says. "Fresh air and exercise is important for a healthy yield."
<br><br>

"Of course," Harper responds. "Thank you once again."
<br><br>

You feel the audience stare at your <<bottom>> as you're led outside. Some of them want to do more than look, you're sure.
<br><br>

<<link [[Next|Livestock Job End]]>><<endevent>><</link>>
<br>