<<set $outside to 1>><<effects>>
You follow the dog down the alley. It rounds a corner.
<<if $rng gte 99 and $bestialitydisable is "f">>
	You find it stood in the middle of the alley, looking at you. You hear a growl to your right and left. You're surrounded.
	<br><br>
	<<link [[Next|Street Follow Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<<elseif $rng gte 90>>
	You find it stood beside a brown rug. The rug coughs and rolls over. <<generate1>><<person1>>It's a <<personcomma>> and <<he>> looks sick. The dog looks at you and whimpers.
	<br><br>
	You run from the alley. A <<generate2>><<person2>><<person>> stands nearby, reading something on <<his>> phone. You tug on <<his>> jacket. <<He>> turns. "What?" <<he>> asks, irritated.
	<br><br>
	You explain the situation and ask to borrow <<his>> phone. <<He>> eyes you with suspicion. "Alright. But you're gonna have to give me something of yours to hold so you won't run off." <<He>> steps back and beholds you. "Your <<bottoms>> will do."
	<br><br>
	You feel angry that <<he>> would ask such a thing. You don't know if the <<person1>><<person>> can afford a delay. <<person2>><<He>> waggles the phone in front of you.
	<br><br>
	<<link [[Strip (0:10)|Street Follow Strip]]>><<trauma 6>><<stress 6>><<lowerstrip>><</link>><<gtrauma>><<gstress>>
	<br>
	<<link [[Snatch the phone (0:10)|Street Follow Snatch]]>><<crimeup 30>><<trauma -6>><</link>><<crime>><<ltrauma>>
	<br>
<<else>>
	You find it stood beside a ruined piece of leather. It's a wallet.
	<<set $rng to random(1, 100)>>
	<<if $rng gte 51>>
		There's no money or identification inside, just some faded receipts.
	<<elseif $rng gte 11>>
		There's a £5 note inside.
		<<set $money += 500>>
	<<else>>
		There's £100 inside!
		<<set $money += 10000>>
		You look further but there's no identification.
	<</if>>
	The dog barks happily.
	<br><br>
	<<if $deviancy gte 15 and $bestialitydisable is "f">>
		<<link [[Reward it|Street Follow Sex]]>><<set $sexstart to 1>><</link>><<deviant2>>
		<br>
	<</if>>
	<<link [[Pet it|Street Follow Pet]]>><<trauma -6>><<stress -12>><</link>><<ltrauma>><<lstress>>
	<br>
<</if>>