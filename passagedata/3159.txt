<<effects>>

You're cuffed and dragged behind the police officer. <<He>> pushes you into the back of what you assume is a police car.
<br><br>

<<He>> opens the driver's door, but doesn't get inside. Instead you hear <<him>> speak. <<He>> gives a description of you.
<br><br>
<<if $crime gte 5000>>
	<<He>> laughs, and slaps the car above your seat. "Looks like I've bagged a naughty <<girlstop>> Can't wait to hear what they do to <<phimstop>>" <<He>> climbs in, and you drive away.
	<br><br>

	"I knew you were bad news the moment I saw you," <<he>> says as <<he>> drives.
	<br><br>

	<<link [[Next|Hospital Arrest Sit]]>><</link>>
<<elseif $crime gte 1000>>
	"Alright," <<he>> says. "I'll bring <<phim>> in." <<He>> climbs in, and you drive away.
	<br><br>

	"I knew you were trouble," the <<person>> says as <<he>> drives. "It'll be the pillory for you, I wager."
	<br><br>

	<<link [[Next|Hospital Arrest Sit]]>><</link>>
<<else>>
	"Roger," <<he>> says. "Looks like it's your lucky day." <<His>> hands reach in and remove your blindfold.

	<<if $leftarm is "bound" or $rightarm is "bound" or $feetuse is "bound">>
		<<if $worn.face.type.includes("gag")>>
			<<He>> also removes your gag and unties your <<if $rightarm is "bound" or $leftarm is "bound">>arms<</if>><<if ($rightarm is "bound" or $leftarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>.
		<<else>>
			<<He>> also unties your <<if $rightarm is "bound" or $leftarm is "bound">>arms<</if>><<if ($rightarm is "bound" or $leftarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>.
		<</if>><<unbind>>
	<<else>>
		<<if $worn.face.type.includes("gag")>>
			<<He>> removes your gag.
		<<else>>
		<</if>>
	<</if>>
	<<set $worn.face.type.push("broken")>>
	<<faceruined>>
	You squint against the light.
	<<ltrauma>><<lllstress>><<trauma -6>><<stress -24>>
	<br><br>
	"Someone important is looking out for you," <<he>> says. "Lucky you."
	<br><br>

	<<if $exposed gte 1>><<towelup>>
	<<He>> gives you towels to cover with, and drives you back to the orphanage.
	<<else>>
	<<He>> drives you back to the orphanage.
	<</if>>
	<br><br>
	<<endcombat>>
	<<link [[Next|Orphanage]]>><</link>>
	<br>
<</if>>

<br>