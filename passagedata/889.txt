<<effects>>


<<if $tending gte random(100, 1500)>>
	"Bad dog," you scold, "Bad, bad dog." <<farm_He dog>> hesitates, <span class="green">then comes to a stop.</span>
	<<pass 5>>
	<br><br>
	
	Alex continues discussing with the <<personsimple>> not so far away, so you keep low in the grass while waiting for the knot to deflate. At last, you manage to wiggle free.
	<br><br>
	
	<<clotheson>>
	
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<<else>>
	"B-Bad dog," you scold, but it's hard to sound authoritative while being dragged through a field by a cock. <span class="red">The <<farm_text dog>> ignores you,</span> and pulls you further along.
	<br><br>
	
	
	<<farm_knotted>>
	
<</if>>