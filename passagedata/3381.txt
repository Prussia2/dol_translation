<<effects>>

<<if $submissive gte 1150>>
	"D-do you like it?" you ask. "You can look closer if you want."
<<elseif $submissive lte 850>>
	"If you want to look," you say. "Just ask."
<<else>>
	"Do you like it?" you ask.
<</if>>
You turn so <<he>> can get a clearer look.<<deviancy2>>

<<if $rng gte 81>>
	"There's no way a cute <<girl>> like you knows what that means," <<he>> chuckles. "You should get rid of it before anyone gets the wrong idea."
<<elseif $rng gte 61>>
	"You're a dirty one, aren't ya?" <<He>> laughs as <<he>> continues on.
<<elseif $rng gte 41>>
	<<He>> blushes and looks away.
<<elseif $rng gte 21>>
	"I can point you toward the kennel if you like," <<he>> laughs.
<<else>>
	<<He>> gives you a nervous smile before continuing on.
<</if>>
<br><br>

<<endevent>>
<<destinationeventend>>