<<effects>>

<<set _garden_location to "garden">>
<<for _i to 0; _i lt $plots[_garden_location].length; _i++>>
	<<set _tending_temp to _i>>
	<<set $plots[_garden_location][_tending_temp].water to 1>>
<</for>>

You hand Robin your watering can, and <<he>> gets to work while you find another. Together, you water the flowers around the garden.
<<if $NPCName[$NPCNameList.indexOf("Robin")].trauma gte 40>>
	Robin is quiet, but <<he>> seems happier as <<he>> works.
<<else>>
	Robin hums as <<he>> works.
<</if>>
<br><br>

Job well done, Robin sits on the grass and watches the flowers sway in the breeze.
<br><br>

<<link [[Sit with Robin (0:05)|Garden Robin Sit]]>><<pass 5>><<npcincr Robin love 1>><</link>><<glove>>
<br>
<<link [[Make Robin a flower crown (0:10)|Garden Robin Crown]]>><<pass 10>><</link>><<tendingdifficulty 1 200>>
<br>