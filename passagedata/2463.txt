<<widget "landryoptions">><<nobr>>
	<<if $blackmoney gte 1>>
		<<link [[Sell Stolen Goods|Pub Sell]]>><</link>>
		<br>
	<</if>>
	<<if $spray lt $spraymax and $money gte 250000>>
		<<link [[Buy pepper spray charge (£2500)|Pub Spray]]>><<spray 1>><<set $money -= 250000>><</link>>
		<br>
	<</if>>
	<<if $crime gte 500 and $pubtask isnot 1>>
		<<link [[Ask for help covering your tracks|Pub Task]]>><</link>>
		<br>
	<</if>>
	<<if $police_hack is 1>>
		<<link [[Ask for help breaking into the police computer|Pub Hack]]>><</link>>
		<br>
	<<elseif $pub_hack_job is 3 and $hacker_tasks.length gte 2>>
		<<link [[Pass on the hacker's message|Pub Hack]]>><<set $pub_hack_job to 4>><</link>>
		<br>
	<<elseif $police_hack is 4>>
		<<link [[Ask for updated police password (£300)|Pub Hack]]>><<set $money -= 30000>><</link>>
		<br>
	<</if>>
	<<if $pubtask2 is 1>>
		<<link [[Hand over the black box|Pub Box]]>><</link>>
		<br>
	<</if>>
<</nobr>><</widget>>