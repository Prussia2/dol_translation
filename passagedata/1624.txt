<<set $outside to 0>><<set $location to "cafe">><<effects>>

<<npc Sam>><<person1>>
"That was the mayor!" Sam says, rushing to your side. <<He>> pulls a mirror from <<his>> pocket and checks <<his>> hair. "<<if $NPCName[$NPCNameList.indexOf("Quinn")].pronoun is "m">>He'll<<else>>She'll<</if>> introduce you when we're ready for your speech. That shouldn't be long now." <<He>> looks around the room. People are finding their seats, and the last of the guests shuffle through the doors.
<br><br>
<<endevent>><<npc Quinn>><<person1>>
You look over at Bailey and Quinn. Bailey seems agitated, but Quinn is more jovial. The mayor chuckles, and pulls <<himself>> away from the conversation.
<br><br>
<<endevent>><<npc Sam>><<person1>>
"It's happening," Sam says. "Go sit over there. By the podium." <<He>> looks around the room again. "The cream treats should be here by now..." <<He>> rushes into the kitchen, while you take your seat beside the podium.
<br><br>
<<endevent>>
<<link [[Next|Chef Opening 9]]>><</link>>
<br>