<<set $outside to 0>><<effects>>

"It's okay," you whisper to Robin. "I recognise them. It'll be safer than the streets."
<br><br>

Robin nods, but seems unconvinced. You both climb into the back of Avery's car.
<br><br>

The journey is short. Avery chats and glances at you through the rear-view mirror. "It makes sense I drop you off," <<he>> says at one point. "It's on my normal route."
<<glove>><<npcincr Avery love 1>>
<br><br>

<<He>> ignores Robin, <<endevent>><<npc Robin>><<person1>>who stares at <<his>> hands the whole journey.
<br><br>

Robin seems relieved when it's over.
<br><br>

<<link [[Next|School Front Playground]]>><<endevent>><</link>>
<br>