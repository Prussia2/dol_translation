<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $rng gte 81>>

	<<generatey1>><<generatey2>><<generatey3>>

	<<if $schoolrep.herm gte 5>>
		A <<person1>><<person>> spots you first, and whispers to <<his>> friends. As you pass, three of them grab you and shove you against a locker. The others goad them on, encouraging them to punish the "freak".
		<br><br>
	<<elseif $schoolrep.crossdress gte 5>>
		A <<person1>><<person>> spots you first, and whispers to <<his>> friends. You overhear "crossdresser" and "pervert". As you pass, three of them grab you and shove you against a locker.
		<br><br>
	<<else>>
		A <<person1>><<person>> spots you first, and whispers to <<his>> friends. As you pass, three of them grab you and shove you against a locker.
	<</if>>
	<br><br>
	<<link [[Next|Hallways Low Status Molestation]]>><<set $molestationstart to 1>><</link>>
	<br>

<<else>>

	<<generatey1>>A <<person1>><<person>> spots you first, and whispers to <<his>> friends. They move away from you as you approach, as if you're infectious.<<trauma 6>><<stress 6>>
	<br><br>

	<<endevent>>
	<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
	<br>

<</if>>