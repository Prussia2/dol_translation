<<set $outside to 0>><<effects>>
<<if $athletics gte random(1, 1000)>>
	You snatch the money from the register and run. <<He>> lunges for you, <span class="green">but you dodge <<his>> hand,</span> leaving <<him>> sprawled on the floor.
	<br><br>
	You run from the building, down the street and into an alley. You got away.
	<<set $money += $tip>><<set $crime += ($tip / 100)>>
	<br><br>
	<<endevent>>
	<<destinationeventend>>
<<else>>
	You snatch the money from the register and run. <<He>> lunges for you, <span class="red">and clutches your ankle,</span> leaving you sprawled on the floor.
	<br><br>
	"Now I've really got you," <<he>> says as <<he>> climbs on top of you.
	<br><br>
	<<link [[Next|Skulduggery Shop Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<</if>>