<<effects>>

You turn and face the pair.

<<if $submissive gte 1150>>
"Leave <<person1>><<him>> alone," you say.
<<elseif $submissive lte 850>>
"Fuck off losers," you say.
<<else>>
"Go away," you say. "Be a nuisance somewhere else."
<</if>>
The pair laugh.
<br><br>
"Or what?" the <<person2>><<person>> says. "You gonna fight for your <<person1>><<if $pronoun is "m">>boyfriend<<else>>girlfriend<</if>>?"
<br><br>
<<if $NPCName[$NPCNameList.indexOf("Kylar")].love gte 50>>
"You better listen to <<phimcomma>>" Kylar says from behind. "My <<if $player.gender_appearance is "m">>boyfriend<<else>>girlfriend<</if>> is really strong."
<br><br>
The pair laugh harder, but don't try anything physical. They continue to mock Kylar until someone from the arcade staff shoos them out.
<br><br>
<<endevent>><<npc Kylar>><<person1>>
<<kylaroptions>>
<<else>>
The pair laugh harder, and continue to mock Kylar until someone from the arcade staff shoos them out.
<br><br>
"Th-thank you," Kylar manages once they're gone. <<He>> doesn't make eye contact.
<br><br>
<<endevent>><<npc Kylar>><<person1>>
<<kylaroptions>>
<</if>>