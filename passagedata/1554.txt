<<set $outside to 0>><<effects>>
<<fameexhibitionism 20>>
<<He>> stands up. "Everyone, this <<girl>> isn't wearing anything!" <<he>> bellows, causing heads all over the bus to turn your way. You shrink in your seat, but it only inflames curiosity in your new audience. "Come have a look!"
<br><br>
A <<generatey2>><<person2>><<person>> leans over the seat behind you. <<person1>>"<<Hes>> not lying. Come look." You're fortunate than none have the daring to assault you further, but the rest of the journey is miserable as they crowd and jeer.
<br><br>
<<generate3>><<person3>>The bus comes to a stop at your destination and the driver climbs out of <<his>> seat. "Everyone away from <<phimcomma>> now." The kids start shuffling off the bus with some reluctance, lingering as long as they can. The driver, a <<personcomma>> throws you some towels. "I don't ever want to catch you slutting up my bus again. I'm letting the teachers know."
<br><br>
There's not much to do but cover up and leave the bus.
<br><br>
<<towelup>>
<<endevent>>
<<destination5>>