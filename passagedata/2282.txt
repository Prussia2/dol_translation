<<effects>>

<<if $submissive gte 1150>>
	"<<if $pronoun is "m">>"S-Sir<<else>>"M-Maam<</if>>!" you call. "Would you take a look at my produce?"
<<elseif $submissive lte 850>>
	"Oi," you say. "Loiterer. Buy something or clear off."
<<else>>
	"Can I interest you in my wares?" you say. "They're produced locally."
<</if>>
<br><br>

<<if $rng gte 51>>
	The <<person>> walks away, and finds somewhere else to stand.
	<br><br>

	<<stall_actions>>
<<else>>
	The <<person>> walks over, and examines your <<print setup.plants[$stall_expensive].plural>>. "How much for $stall_amount of these?" <<he>> asks.
	<br><br>

	<<stall_trust>>
	<br><br>

	<<stall_sell_actions>>
<</if>>