<<effects>>

You cover your eyes and hold out the towel. Alex pulls it from your grasp.
<br><br>

You turn to the window. You hear Alex continue to rummage in the basket, then walk back through the kitchen. You hear <<him>> climb the staircase.
<br><br>

<<link [[Next|Farm Stage 6 3]]>><</link>>
<br>