<<set $outside to 0>><<set $location to "home">><<effects>>
You lie on your side and rest your head in Robin's lap. <<He>> shifts <<his>> legs to make <<his>> thighs comfier, and rests one arm on your shoulder. <<He>> continues playing, but massages your scalp and strokes your cheek with <<his>> free fingers.
<br><br>
You wake up. Not long has passed. Robin helps you sit up. "I hope you're sleeping properly," <<he>> says. <<He>> looks concerned at first, but then smiles and laughs. "Are my thighs soft?"
<br><br>
<<robinoptions>>