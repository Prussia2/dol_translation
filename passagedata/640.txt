<<set $location to "wolf_cave">><<effects>>

<<if $wolfpacktrust gte 18 or $wolfpackleader gte 1>>
<<set $dateCount.BlackWolfHunts++>>

You follow the wolves as they depart between the trees. They don't stop you.
<br><br>
<<set $outside to 1>>
Not far from the cave they break into a run. They move fast. It's hard to keep up.
<<gathletics>><<athletics 6>><<physique 6>>
<br><br>

<<wolfhuntevents>>

<<else>>

You try to follow the wolves, but the black wolf turns and growls at you. It wants you to stay with the young wolves.
<br><br>

You watch as they depart between the trees.
<br><br>

<i>If they trusted you more, they might be willing to let you come along.</i>
<br><br>

<<destinationwolfcave>>

<</if>>