<<effects>>
<<set $bartend_info_other to 1>>
<<npcincr Darryl love 1>>
<<famegood 3>>
<<npc Darryl>><<person1>>
You enter Darryl's office. <<He>> looks up from <<his>> desk and smiles. "Is everything okay downstairs?"
<br><br>
You tell <<him>> what you heard, and about how you fear for your colleague's safety. "That's awful," <<he>> says when you're done. "Thank you for telling me. I'll have someone escort them home safely as a precaution. Hopefully they won't be too spooked."
<br><br>
You return to the bar. The rest of the shift passes without event. <<tipreceive>>
<br><br>

<<link [[Next|Strip Club]]>><<endevent>><<pass 30>><</link>>
<br>