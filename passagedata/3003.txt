<<calljanet>>
<i>
Lonely house outside the city.
<br>
Enter Janet.
<br><br>
</i>
<b>JANET:</b>
<br>
"Oh wh're art thee, mine own Belov'd One? Wh're art thee?<span class="black"> (Oh where are you, my Beloved One? Where are you?)</span>
<br>
I've been waiting h're f'r so longeth.<span class="black"> (I've been waiting here for so long.)</span>
<br>
This constant waiting maketh me crazy.<span class="black"> (This constant waiting makes me crazy.)</span>
<br>
What's stopping thee?"<span class="black"> (What's stopping you?)</span>
<br><br>
<i>Enter Raul.</i>
<br><br>
<b>RAUL:</b>
<br>
"Oh mine own ladybird<span class="black"> (Oh my Darling)</span>
<br>
I has't hath returned to thee.<span class="black"> (I have returned to you.)</span>
<br>
Prithee f'rgive me thee hadst to waiteth f'r so longeth<span class="black"> (Please forgive me you had to wait for so long)</span>
<br>
Th're w're things yond i hadst to doth but anon i am all yours."<span class="black"> (There were things I had to do but now I'm all yours.)</span>
<br><br>
<<link [[Continueth|ScarletBook6]]>><<arousal 1200>><</link>>