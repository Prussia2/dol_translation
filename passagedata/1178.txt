<<effects>>

<<generate2>><<generate3>>
<<person2>>
"I've never seen Remy like that," one of the farmhands says on the way back to the cattle field. "Is <<person1>><<he>> losing <<his>> touch?"
<br>
<<if $worn.face.type.includes("gag")>>
	"Nah," a <<person3>><<person>> responds as <<he>> <span class="lblue">unties your $worn.face.name.</span> "This one here's just a fighter. Was impressive, if hard to watch."
	<<set $worn.face.type.push("broken")>>
	<<faceruined>>
<<else>>
	"Nah," a <<person3>><<person>> responds. "This one here's just a fighter. Was impressive, if hard to watch."
<</if>>
<br>
"Never thought I'd see someone-" <<person2>><<he>> catches <<himselfstop>> "Ah, I mean something, that Remy couldn't break."
<br><br>

You arrive in front of the gates. "It's not over for this poor thing," <<person3>>the <<person>> glances around. "Hang on." <<He>> disappears into a small shed, and emerges with a basket covered with a white sheet.
<br><br>
<<He>> passes it to you. "Might need help healing after that." You glimpse beneath the sheet. It's full of shiny, juicy apples. Your mouth waters and your stomach growls.
<br><br>

<<person2>>The <<person>> looks shocked. "Remy's not gonna be happy," <<he>> says. "<<person1>><<Hes>> the only one allowed to give those out."
<br>
"Remy's not gonna find out. Like <<hes>> not gonna find out you said <<he>> lost <<his>> touch."
<br><br>
The <<person2>><<persons>> face pales, and <<he>> drops the topic. The gate shuts behind you.
<br><br>

<<link [[Eat the apples|Livestock Job Eat]]>><<stress -6>><<trauma -6>><<hunger -2000>><</link>><<lllhunger>><<ltrauma>><<lstress>>
<br>
<<link [[Save them for someone else|Livestock Job Save]]>><</link>>
<br>