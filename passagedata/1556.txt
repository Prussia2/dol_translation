<<if $enemyhealth lte 0>>
	You knock the <<person1>><<person>> down, giving you a clear route out. <<tearful>> you run to the front of the bus.
	<<if $submissive lte 850>>
		"Let me off or I'll mess you up too," you say,
	<<elseif $submissive gte 1150>>
		"Please stop the bus," you say,
	<<else>>
		"Let me off!" you say,
	<</if>>
	aware that the <<person>> and <<his>> friends won't take long to recover. The driver doesn't respond.
	<br><br>
	You open the door and look out at the town racing by. Jumping would be dangerous. You glance back at the <<people>> coming for you. You don't have long.
	<br><br>
	<<link [[Jump|Bus Seat Rape Jump]]>><</link>>
	<br>
	<<if $submissive lte 500>>
		<<link [[Punch the driver|Bus Seat Rape Punch]]>><<crimeup 100>><</link>><<crime>>
		<br>
	<</if>>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	The bus stops and the group push you towards the door. They shove you out and drive away, jeering at you through the windows.
	<<if $upperoff is 0 and $loweroff is 0 and $underloweroff is 0 and $underupperoff is 0>>
	<<else>>
		They kept your clothes.
	<</if>>
	<<if $exposed gte 1>>
		<<tearful>> you hide behind a car before you're seen.
		<br><br>
	<<else>>
		<<tearful>> you try to work out where you are.
		<br><br>
	<</if>>
	<<set $stealtextskip to 1>>
	<<stealclothes>>
	<<clotheson>>
	<<endcombat>>
	<<set $rng to random(1, 13)>>
	<<switch $rng>>
	<<case 1>>
		<<set $bus to "nightingale">>
	<<case 2>>
		<<set $bus to "domus">>
	<<case 3>>
		<<set $bus to "elk">>
	<<case 4>>
		<<set $bus to "high">>
	<<case 5>>
		<<set $bus to "starfish">>
	<<case 6>>
		<<set $bus to "barb">>
	<<case 7>>
		<<set $bus to "connudatus">>
	<<case 8>>
		<<set $bus to "wolf">>
	<<case 9>>
		<<set $bus to "harvest">>
	<<case 10>>
		<<set $bus to "oxford">>
	<<case 11>>
		<<set $bus to "danube">>
	<<case 12>>
		<<set $bus to "mer">>
	<<case 13>>
		<<set $bus to "cliff">>
	<</switch>>
	<<destination>>
<</if>>