<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	"Maybe you should find a new line of work," <<he>> says, shoving you to the <<if $weather is "snow">>snow<<elseif $weather is "rain">>mud<<else>>dirt<</if>>.
	<br><br>
	<<tearful>> you stagger to your feet. The <<person>> climbs over a fence.<<gaggro>><<farm_aggro 5>>
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Farm Work]]>><</link>>
	<br>

<<elseif $enemyhealth lte 0>>
	The <<person1>><<person>> trips backwards, and lands in the <<if $weather is "snow">>snow<<elseif $weather is "rain">>mud<<else>>dirt<</if>>.
	<br><br>
	<<tearful>> you step closer. <<He>> throws <<his>> hands up in a protective gesture. "I-I'm just following orders," <<he>> says, scrambling away from you. <<He>> climbs to <<his>> feet as <<he>> crawls away, and runs for the fence.<<llaggro>><<farm_aggro -10>>
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Farm Work]]>><</link>>
	<br>

<<else>>
	<<set $rescued += 1>>
	"I'm coming!" The <<person1>><<person>> pulls you in front of <<him>> as Alex arrives.
	<br><br>
	"It's our farmer <<if $NPCName[$NPCNameList.indexOf("Alex")].gender is "m">>boy<<else>>girl<</if>>," the <<person>> sneers. "The one who thinks they can make it in world alone."
	<br><br>
	
	<<if $exposed gte 1>>
		"Let <<phim>> go," Alex says, doing <<if $NPCName[$NPCNameList.indexOf("Alex")].gender is "m">>his<<else>>her<</if>> best to avoid looking at your <<lewdness>>. "I won't ask again."<<glust>><<npcincr Alex lust 1>>
	<<else>>
		"Let <<phim>> go," Alex says, <<if $NPCName[$NPCNameList.indexOf("Alex")].gender is "m">>his<<else>>her<</if>> fists clenching. "I won't ask again."
	<</if>>
	<br><br>
	The <<person>> tightens <<his>> grip instead. Alex launches forward, <<if $NPCName[$NPCNameList.indexOf("Alex")].gender is "m">>his<<else>>her<</if>> fist sailing over your shoulder. You hear a sickening crunch just behind you.<<llaggro>><<farm_aggro -10>>
	<br><br>
	
	<<link [[Next|Farm Rape Finish Rescue]]>><</link>>
	<br>

<</if>>