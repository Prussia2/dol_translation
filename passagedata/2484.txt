<<set $location to "forest">><<set $outside to 1>><<effects>><<set $forest to 0>>

<<if $drunk is 1>>
	<<if $rng gte 51>><<set $location to "town">>

	You walk with <<himstop>> <<He>> has to grab your shoulders to steady you more than once, but you manage to climb onto the back seat of <<his>> car. The journey doesn't take long. "This is your home right?" <<he>> asks. "It's not my place to say but you should be more careful in the future. There are creeps who prey on cute <<girls>> like you." <<He>> helps you to the front door, only going back to <<his>> car once you're safe inside.
	<br><br>

	<<link [[Next|Orphanage]]>><<endevent>><</link>>
	<br>

	<<else>>
		<<if $cow isnot 6 or random(1, 2) is 2>>
			You disappear with <<him>> into the forest. Shortly after <<he>> turns to you. "I always loved this forest," <<he>> says. "I have something for you." <<He>> holds out <<his>> hand, showing you a pink pill. "You'll like it. I promise."
			<br><br>

			<<link [[Eat the Pill|Pub Pill]]>><<set $drugged += 180>><</link>>
			<br>
			<<link [[Refuse|Pub Pill Refuse]]>><</link>>
			<br>
		<<else>>
			You disappear with <<him>> into the forest. Shortly after <<he>> turns to you. "I always loved this forest," <<he>> says. "I have something for you." <<He>> holds out <<his>> hand, showing you a white pill. "You'll like it. I promise."
			<br><br>

			<<link [[Eat the Pill|Pub White Pill]]>><<set $drugged += 180>><</link>>
			<br>
			<<link [[Refuse|Pub White Pill Refuse]]>><</link>>
			<br>
		<</if>>
	<</if>>
<<else>>

	<<if $cow isnot 6 or random(1, 2) is 2>>
		You disappear with <<him>> into the forest. Shortly after <<he>> turns to you. "I always loved this forest," <<he>> says. "I have something for you." <<He>> holds out <<his>> hand, showing you a pink pill. "You'll like it. I promise."
		<br><br>

		<<link [[Eat the Pill|Pub Pill]]>><<set $drugged += 180>><</link>>
		<br>
		<<link [[Refuse|Pub Pill Refuse]]>><</link>>
		<br>
	<<else>>
		You disappear with <<him>> into the forest. Shortly after <<he>> turns to you. "I always loved this forest," <<he>> says. "I have something for you." <<He>> holds out <<his>> hand, showing you a white pill. "You'll like it. I promise."
		<br><br>

		<<link [[Eat the Pill|Pub White Pill]]>><<set $drugged += 180>><</link>>
		<br>
		<<link [[Refuse|Pub White Pill Refuse]]>><</link>>
		<br>

	<</if>>

<</if>>