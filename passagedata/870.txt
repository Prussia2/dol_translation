<<effects>>

<<set $farm_work.horses_panic to 1>>
<<set $farm_work.chickens_panic to 1>>
<<set $farm_work.pigs_panic to 1>>
<<set $farm_work.dogs_panic to 1>>
<<set $farm_work.cattle_panic to 1>>
<<set $farm_work.fence_damage += random(12, 18)>>
<<pass 10>>

The goons tire, but continue into the farm. You hear them roam around, scaring the animals and smashing the buildings. Alex winces at every snap and shatter.
<br><br>

One gives Alex a final kick, then they leave the farm.
<br><br>

You stagger to your feet. Alex does likewise.
<<clotheson>>

<<if $exposed gte 1 and $farm_naked isnot 1>>
	<<covered>><<He>> blushes, and staggers into the farm house. <<He>> emerges a few moments later, carrying some towels.<<glust>><<npcincr Alex lust 1>><<towelup>>
	<br><br>
<<else>>
<</if>>
"I'm so sorry," <<he>> says, resting <<his>> face in <<his>> palms. "I didn't want you to get hurt."<<npcincr Alex dom -5>><<llldom>>
<br><br>

<<link [[Comfort|Farm Lorry Comfort]]>><<npcincr Alex love 3>><</link>><<gglove>>
<br>
<<link [[Berate|Farm Lorry Berate]]>><<npcincr Alex dom -3>><<npcincr Alex love -1>><</link>><<llove>><<lldom>>
<br>