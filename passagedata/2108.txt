<<effects>>
<<if $submissive gte 1150>>
	"I-I'll do whatever you ask," you say. "How can I serve you?"
<<elseif $submissive lte 850>>
	"Fine," you say. "What do you want me to do?"
<<else>>
	"I understand," you say.
<</if>>
<br><br>
The pair make you keep their drinks full, and fetch snacks from the kitchen on demand. When you're unneeded, they make you stand to the side. <<if $exposed gte 1>>You wish you weren't so exposed.<</if>> They leer at you openly, but there's no helping it. <<if $exposed gte 1>>You can't even cover yourself with your arms bound.<</if>>
<br><br>
The <<person2>><<person>> gestures at <<his>> glass. This time, when you bend over to fill it, <<he>> kicks your bottom, knocking you to your knees. A weight presses against your back before you can stand up. Then another. "I need a new footstool," <<he>> says. "Best stay still. Footstools don't move."
<<gtrauma>><<gstress>><<pain>><<trauma 6>><<stress 6>><<pain 6>>
<br><br>
<<link [[Endure|Sold Work Foot]]>><<pass 60>><<sub 1>><<trauma 6>><<stress 6>><<arousal 4000>><</link>><<gtrauma>><<gstress>><<ggarousal>>
<br>
<<link [[This is too humiliating|Sold Work Foot Refuse]]>><<def 1>><</link>>
<br>