<<set $outside to 1>><<effects>>
You wake up by the side of the road, leaning against a wall. A <<generate1>><<person1>><<person>> stands above you, holding a camera.
<br><br>
<<if $exposed gte 2>>
	You don't remember your predicament until you look down, and feel your face flush. <<covered>> "Oh I wouldn't worry about that," <<he>> says. "I've taken so many pictures already. Can't wait to show the <<if $pronoun is "m">>guys<<else>>girls<</if>>. Anyway, I've places to be."
	<<gtrauma>><<trauma 6>>
	<br><br>
	<<fameexhibitionism 20>>
<<elseif $exposed gte 1>>
	You don't remember your predicament until you look down, and feel your face flush. <<covered>> "Oh I wouldn't worry about that," <<he>> says. "I've taken so many pictures already. Can't wait to show the <<if $pronoun is "m">>guys<<else>>girls<</if>>. Anyway, I've places to be."
	<<gtrauma>><<trauma 6>>
	<br><br>
	<<fameexhibitionism 10>>
<<else>>
	"You alright? I almost called an ambulance, but I thought it might look bad," <<he>> says, giving a sheepish smile.
	<br><br>
	<<if $submissive gte 1150>>
		"I-I'm fine," you say, looking at the pavement.
		<br><br>
	<<elseif $submissive lte 850>>
		"I'm fine. Stop gawking," you say.
		<br><br>
	<<else>>
		"I'm fine," you say, not sure if you're right.
		<br><br>
	<</if>>
	"Splendid. Now if you don't mind, I'll be on my way."
	<br><br>
<</if>>
<<He>> leaves you to dust yourself off.
<br><br>
<<endevent>>
<<destinationeventend>>