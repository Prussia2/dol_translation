<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You feign confusion. "Your locker? I thought this was mine. Silly me."
<br><br>

"Your locker is nowhere near here, I'm not stupid," <<he>> says, looking through <<his>> belongings. Probably checking if anything is missing.
<br><br>

<<link [[Next|School Lockers]]>><<endevent>><</link>>
<br>