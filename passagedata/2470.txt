<<set $outside to 0>><<set $location to "pub">><<effects>><<set $bus to "harvest">>
"That's it! You're a lifesaver." <<He>> examines the box more closely. "I hope it wasn't too much trouble. Don't worry, I'll fulfil my side of things. You'll be clean as can be." <<He>> stands. "Now if you'll excuse me, I've some favours to call in."
<br><br>
<<set $pubtask to 0>>
<<set $pubtask2 to 0>>
<<set $pubtasksetting to 0>>
<<if $crime gte 10000>>
	<<set $crime -= 10000>>
<<else>>
	<<set $crime to 0>>
<</if>>
<<link [[Leave|Pub]]>><<endevent>><</link>>
<br>