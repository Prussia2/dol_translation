<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
"I-It is exciting," you say. "To be exposed in front of people."
<br><br>
<<He>> nods. "Very good," <<he>> says. "You should be happy so many want to look at you." <<He>> glances at the clock. "That's all for today. Be well."
<br><br>
<<endevent>>
<<link [[Next|Asylum]]>><</link>>
<br>