<<set $outside to 0>><<set $location to "home">><<effects>>
<<set _container to $container[$location]>>
<<set _pregnancy to $sexStats.anus.pregnancy>>

Nothing seems to be amiss, until you check in on <<print (_pregnancy.namesChildren is true ? "your children" : "the parasites")>>.
<br><br>
You see a small note near to the <<print _container.name>> with the letter "K" on it.

<<if _container.deadCreatures is 0>>
	It seems someone has been making sure that they are being properly looked after.
	<br><br>
	You should be careful. You might not get the help next time.
	<<set _container.kylarDelay to 7>>
	<<set _container.kylarFed to false>>
	<<set _container.daysSinceFed to 0>>
<<else>>
	It seems someone was looking after them for you. It wasn't enough. They are all dead in the tank.
	<br><br>
	You bury them.
	<<ggtrauma>>
	<<if _pregnancy.namesChildren is true>>
		<<set _traumaMulti to 2>>
	<<else>>
		<<set _traumaMulti to 1>>
	<</if>>
	<<set $trauma += 100 * _traumaMulti * _container.deadCreatures>>
<</if>>
<br><br>
<<endevent>>
<<link [[Next|Containers]]>><<set _container.deadCreatures to 0>><</link>>
<br>