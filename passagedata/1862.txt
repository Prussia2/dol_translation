<<set $outside to 0>><<set $location to "town">><<dockeffects>><<effects>>

You take the <<person>> to the train station on Harvest Street. <<He>> walks close, and keeps bumping into you. The station itself is busy. There's a map of the country on the wall, detailing the train network.
<br><br>

<<He>> points at one town in particular and smiles. You look more closely. It matches the name beneath <<his>> uniform's logo. "Is this your home?" you ask. <<He>> just smiles.
<br><br>

You buy a ticket and walk onto the platform as the train arrives.
<<set $money -= 10000>>
The <<person>> seems anxious.
<br><br>


<<if $money gte 10000>>
	<<link [[Give nothing|Docks Slave Station 2]]>><<famegood 5>><<set $phase to 0>><</link>>
	<br>
	<<link [[Give £100|Docks Slave Station 2]]>><<set $dockslavemoney to 100>><<set $money -= 10000>><<famegood 10>><<set $phase to 1>><</link>>
	<br>
<<else>>
	<<link [[Wave goodbye|Docks Slave Station 2]]>><<famegood 5>><<set $phase to 0>><</link>>
	<br>
<</if>>
<<if $money gte 30000>>
	<<link [[Give £300|Docks Slave Station 2]]>><<set $dockslavemoney to 300>><<set $money -= 30000>><<famegood 30>><<set $phase to 2>><</link>>
	<br>
<</if>>
<<if $money gte 100000>>
	<<link [[Give £1000|Docks Slave Station 2]]>><<set $dockslavemoney to 1000>><<set $money -= 100000>><<famegood 50>><<set $phase to 3>><</link>>
	<br>
<</if>>