<<set $outside to 0>><<set $location to "hospital">><<effects>>

<<generate1>><<person1>>
The receptionist taps something into <<his>> computer and instructs you to take a seat in the waiting room.
<br><br>
<<endevent>>

<<harper_intro>>

"So you have a parasite problem," <<he>> says. "It's probably nothing to worry about, so don't be nervous. Can you explain the symptoms?" Though embarrassed, you manage to stammer a description of your issue.
<br><br>

<<if $parasite.clit.name is "urchin" or $parasite.clit.name is "slime" or
$parasite.penis.name is "urchin" or $parasite.penis.name is "slime" or
$parasite.nipples.name is "urchin" or $parasite.nipples.name is "slime" or
$parasite.bottom.name is "urchin" or $parasite.bottom.name is "slime">>

<<He>> listens intently, nodding along as if there's nothing strange about anything you're saying. When you are finished <<he>> silently turns to <<his>> computer and starts reading something. "Okay," <<he>> says.

	<<if $parasite.left_ear.name is "slime" or $parasite.right_ear.name is "slime">>
	"Firstly, the slime you describe," <<he>> continues. "Entering your ear? Hallucinations are a side effect of many medications. <i>Some people find solace at the temple</i> when they experience similar. Perhaps they could help."
	<br><br>
	<</if>>

"The rest are more real. A rare but relatively benign condition. We should be able to cure you right now." You breathe a sigh of relief. "I'll just be a moment." <<He>> turns back to <<his>> computer.
<br><br>

A minute later <<he>> turns back to you. "We can apply a special form of electrolysis to your unclothed body, at a wavelength that disables this type of parasite. It's completely harmless to people, but you might feel a tingle."
<br><br>

"The procedure will cost you £250." <<He>> pauses before continuing. "As this is a rare illness however, some of my colleagues would like to observe your treatment. If you agree to be observed, you won't be charged." You suddenly don't feel so relieved. "They'll all be medical professionals, so there's no reason to be embarrassed."
<br><br>

<<if $exhibitionism gte 55>>
You try to tell yourself that you don't want to be seen naked by any more people than is necessary, but you can't hide from the lewd feelings stirring at the thought.
<br><br>
<<else>>
You don't like the idea of being seen naked by any more people than is necessary, but the alternative is so expensive.
<br><br>
<</if>>

<<link [[Free procedure (0:20)|Hospital Parasite Public]]>><<pass 10>><</link>>
<br>
<<if $money gte 25000>>
<<link [[Paid Procedure (£250 0:10)|Hospital Parasite Private]]>><<set $money -= 25000>><</link>>
<br>
<</if>>
<<link [[Leave without treatment|Hospital Foyer]]>><<endevent>><</link>>
<br>

<<elseif $parasite.left_ear.name is "slime" or $parasite.right_ear.name is "slime">>

<<He>> listens intently, nodding along as if there's nothing strange about anything you're saying. When you're finished, <<he>> smiles. "A slime inside your ear?" <<he>> says. "Hallucinations are a side effect of many medications. <i>Some people find solace at the temple</i> when they experience similar. Perhaps they could help."
<br><br>

<<He>> starts tapping into <<his>> keyboard. "Do you have any further concerns?" You shake your head.
<br><br>

<<He>> rises and holds open the door for you. You leave the office.
<br><br>

<<link [[Next|Hospital Foyer]]>><<endevent>><</link>>

<<elseif $penilechastityparasite or $vaginalchastityparasite or $analchastityparasite>>

<<He>> listens intently, nodding along as if there's nothing strange about anything you're saying. When you're finished, <<he>> smiles. "As I thought, there's no danger. All you need to do is remove your $worn.genitals.name and the parasites should fall out."
<br><br>

<<He>> starts tapping into <<his>> keyboard. "Do you have any further concerns?" You shake your head, wondering if removing the belt will be an ordeal in of itself.
<br><br>

<<He>> rises and holds open the door for you. You leave the office.
<br><br>

<<link [[Next|Hospital Foyer]]>><<endevent>><</link>>

<<else>>

<<He>> listens intently, nodding along as if there's nothing strange about anything you're saying. When you're finished, <<he>> frowns. "I've not encountered anything like this before," <<he>> says. "But they seem benign. I expect they'll leave on their own time, and there's nothing to worry about."
<br><br>

<<He>> starts tapping into <<his>> keyboard. "Do you have any further concerns?" You shake your head, wondering if removing the belt will be an ordeal in of itself.
<br><br>

<<He>> rises and holds open the door for you. You leave the office.
<br><br>

<<link [[Next|Hospital Foyer]]>><<endevent>><</link>>

<</if>>