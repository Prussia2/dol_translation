<<effects>>

You take <<his>> hand, and <<he>> pulls you into a dance. Other couples are doing likewise, up and down the hall.
<br><br>

<<link [[Follow Avery's Lead|Avery Party Dance Follow]]>><<sub 1>><<npcincr Avery love 1>><<set $endear += 10>><</link>><<glove>><<gendear>>
<br>
<<link [[Take the lead|Avery Party Dance Lead]]>><<def 1>><</link>><<dancedifficulty 1 600>>
<br>