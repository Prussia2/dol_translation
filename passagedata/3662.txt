<<effects>>
<<if $timer lte 1>>
	<span class="red">A <<generate1>><<person1>><<monk>> walks around the pillar.</span> <<His>> eyes are closed in meditative prayer, but <<he>> trips over your leg and almost stumbles to the ground. You cover yourself before <<he>> turns.
	<br><br>
	<<if $temple_rank is "initiate">>
		<<He>> faces you. "You can't just sit where you like initiate!" <<he>> says. "I've half a mind to report you to the bishop."
		<<lgrace monk>><<grace -1 monk>>
	<<elseif $temple_rank is "monk">>
		<<He>> faces you. "Terribly sorry <<if $player.gender_appearance is "m">>brother<<else>>sister<</if>>," <<he>> says. "I'll be more careful in the future."
	<<else>>
		<<He>> faces you. "Forgive me," <<he>> says. "I'll be more careful in the future."
	<</if>>
	<<if $exposed gte 1>>
		<<He>> gasps and shuts <<his>> eyes again when <<he>> realises your state of dress. "W-well I-I..." <<he>> mutters as <<he>> continues on <<his>> way.
		<br><br>
	<<else>>
		<<He>> continues on <<his>> way, now with <<his>> eyes open.
		<br><br>
	<</if>>
<</if>>
<<if $masturbationorgasm gte 1>>
	You look around. No one seems to have noticed your defilement of this sacred ground.
	<<if $angel gte 6>>
	<span class="red">Thinking about it, you feel disgusted with yourself.</span>
	<<llpurity>><<purity -20>><<ggtrauma>><<trauma 50>>
	<<elseif $demon gte 6>>
	<span class="green">Thinking about it, you feel a thrill unlike any other!</span>
	<<llpurity>><<purity -20>><<llstress>><<stress -12>>
	<<else>>
	You feel a cold sense of guilt wash over you, but it quickly fades.
	<<purity -10>><<llpurity>><<gtrauma>><<trauma 6>>
	<</if>>
<<else>>
	You stop before reaching orgasm. You look around. No one seems to have noticed.
<</if>>
<br><br>
<<endmasturbation>>
<<clotheson>>
<<endcombat>>

<<link [[Next|Temple]]>><</link>>
<br>