<<effects>>

<<if $athletics gte random(1, 800)>>
	You kick the ball, <span class="green">and it arcs over the fence.</span> No one sends thanks, but they don't peek over either. <<tearful>> you continue.
	<br><br>
	<<endevent>>
	<<destinationeventend>>
<<else>>
	<<if random(1, 2) gte 2>>
		You kick the ball, <span class="red">but it bounces off the fence with a thud.</span>
	<<else>>
		You try to kick the ball, <span class="red">but miss.</span> You lose your balance and fall to the ground.
		<<gpain>><<pain 6>>
	<</if>>
	<br><br>
	A <<person2>><<persons>> head peeks over before you can try again. <<covered>> <<He>> stares at you, astonished.
	<<gtrauma>><<gstress>><<trauma 6>><<stress 6>><<fameexhibitionism 1>>
	<br><br>

	<<link [[Ask for help|Residential Kick Help]]>><</link>>
	<br>
	<<link [[Run|Residential Kick Run]]>><</link>>
	<br>
<</if>>