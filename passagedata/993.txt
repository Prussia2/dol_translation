<<effects>>

<<npc Alex>><<person1>>
You sneak up to Alex, though you keep yourself hidden behind a bush.

<<if $submissive gte 1150>>
	"A-Alex," you say. "I had an accident. D-do you have something I can dry with?"
<<elseif $submissive lte 850>>
	"Hey," you say. "Alex. Got anything I can dry with?"
<<else>>
	"Alex," you say. "I've had an accident. Do you have anything I could dry with?"
<</if>>
<<exhibitionism1>>

Alex turns, looks perplexed for a moment, then laughs. "You got yourself wet? Don't worry. I'll get you something."
<br><br>

You follow <<him>> to the farmhouse. Alex need only turn to see your <<lewdness>>, but <<he>> doesn't take advantage. "Wait here," <<he>> says as <<he>> climbs the stairs. "I'll chuck something down."
<br><br>

<<link [[Next|Farm Alex Dry 2]]>><</link>>
<br>