<<effects>>
<<endevent>><<npc Sirris>><<person1>>
You are jeered at for a few minutes before Sirris comes over to see what the fuss is about. <<He>> stands dumbfounded for a moment, mouth agape, before coming to <<his>> senses and freeing you. You climb out, more than a little shaken by the ordeal.
<br><br>

<<link [[Next|Science Lesson]]>><<endevent>><</link>>
<br>