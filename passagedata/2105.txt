<<effects>>
<<if $submissive gte 1150>>
	"P-please let me go," you say. "I won't tell anyone. I promise."
<<elseif $submissive lte 850>>
	"You'd best let me go," you say. "People will be looking for me."
<<else>>
	"Please let me go," you say. "I'll forget this ever happened."
<</if>>
<br><br>
The pair look at each other, and laugh. "We've paid for you," the <<person1>><<person>> says. "And we'll get our money's worth."
<br>
The <<person2>><<person>> leans over to <<his>> friend. They whisper back and forth for a moment, until the <<person1>><<person>> turns to you once more. "We've got an idea. We'll let you go, but you're gonna have to work for it."
<br><br>
<<link [[Next|Sold Work]]>><</link>>
<br>