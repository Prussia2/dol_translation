<<effects>><<set $outside to 0>><<set $location to "pool">>
<<set $wardrobe_location to "wardrobe">>
You stand in front of your open locker.
<br><br>
<<wardrobewear>>
<<if $exposed gte 2 and $exhibitionism lt 75>>
You can't go out like this!
<br><br>
<<else>>
<<link [[Leave|School Girl Changing Room]]>><<unset $saveColor>><<unset $wardrobeRepeat>><<unset $tempDisable>><</link>>
<br><br>
<</if>>

<<wardrobe>>