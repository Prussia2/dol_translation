<<set $outside to 0>><<set $location to "cafe">><<effects>>

<<set $chef_state to 1>><<set $chef_sus to 0>><<set $bun_value to 5000>><<set $bun_cut to 0.2>>

<<npc Sam>><<person1>>You approach Sam. "Have you reconsidered?" <<he>> asks, hopeful. You nod. <<He>> claps in excitement. "Fantastic! You can work whenever we're open. I know you'll need to fit it around other commitments. I'll get whatever ingredients you need. This could be the start of something big."
<br><br>

<<endevent>>
<<link [[Next|Ocean Breeze]]>><</link>>
<br>