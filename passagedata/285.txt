<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
You are in the asylum, deep in the forest. The building is over a century old, and little of it has been renovated to modern standards. Security patrol the grounds between the building and surrounding fence.
<<if $asylumstate is "sleep">>
	You're supposed to be in your cell.
	<br><br>
<<else>>
	Other patients are relaxing in the recreation room or exercising outside. Most keep to themselves.
	<br><br>
	<<asylumstats>>
<</if>>
<<if $stress gte 10000>>
	<<passoutasylum>>
<<elseif $asylumstate is "firsttreatment" and $asylumfirsttreatment isnot 1>>
	<<set $asylumfirsttreatment to 1>>
	<<generate1>><<person1>>A <<person>> approaches you. <<Hes>> a nurse.
	<br><br>
	<<asylumoptions>>
	"Doctor says it's time for your treatment," <<he>> says. "Come with me."
	<br><br>
	<<endevent>>
	<<asylumtreatments>>
<<elseif $asylumstate is "secondtreatment" and $asylumsecondtreatment isnot 1>>
	<<set $asylumsecondtreatment to 1>>
	<<generate1>><<person1>>A <<person>> approaches you. <<Hes>> a nurse.
	<br><br>
	<<asylumoptions>>
	"Doctor says it's time for your treatment," <<he>> says. "Come with me."
	<br><br>
	<<endevent>>
	<<asylumtreatments>>
<<elseif $asylumstate is "assessment" and $asylumassessment isnot 1>>
	<<set $asylumassessment to 1>>
	<<generate1>><<person1>>A <<person>> approaches you. <<Hes>> a nurse.
	<br><br>
	<<asylumoptions>>
	"Doctor says it's time for your assessment," <<he>> says. "Come with me."
	<br><br>
	<<endevent>>
	<<asylumassessment>>
<<elseif $asylumstate is "exercise" and $asylumexercise isnot 1>>
	<<set $asylumexercise to 1>>
	<<generate1>><<person1>>A <<person>> approaches you. <<Hes>> an orderly.
	<br><br>
	<<asylumoptions>>
	"Time for exercise," <<he>> says. "Doctor's waiting."
	<br><br>
	<<endevent>>
	<<npc Harper>><<person1>>
	The Doctor is indeed waiting outside. The other patients line up near <<himstop>> <<Hes>> holding a whistle. "Time for your daily exercise," <<he>> says. "When I blow, I want you to run in a circle around the grounds. Don't stop until you hear me blow again."
	<br><br>
	<<He>> whistles, and you run. It's not too harsh a pace, but orderlies follow after and chastise those who fall behind.
	<<gathletics>><<athletics 3>><<physique 3>><<pass 30>>
	<br><br>
	<<He>> whistles again, and the group stops. A few sit on the ground. "We're not done yet," <<he>> says.
	<br><br>
	<<link [[Next|Asylum Shower]]>><</link>>
	<br>
<<elseif $hour is 22>>
	<<generate1>><<person1>>"Bed time," a nurse announces. "Everyone get back in your cells."
	<br><br>
	<<link [[Go to cell|Asylum Cell]]>><<endevent>><</link>>
	<br>
	<<link [[Hide (1:00)|Asylum Hide]]>><<pass 60>><</link>>
	<br>
<<elseif $asylumstate is "sleep">>
	<<link [[Staff door|Asylum Staff]]>><</link>>
	<br>
	<<link [[Cell|Asylum Cell]]>><</link>>
	<br>
<<else>>
	<<if $exposed gte 1>>
		<<link [[Ask for towels|Asylum Towels]]>><<trauma 6>><<stress 3>><<suspicion 1>><</link>><<gsuspicion>><<gtrauma>><<gstress>>
		<br>
	<</if>>
	<<if $exposed gte 2 and $exhibitionism gte 75>>
		<<if $hour is 6>>
			<<link [[Breakfast (1:00)|Asylum Breakfast]]>><<asylumstatus 1>><<awareness -10>><<trauma -3>><<stress -6>><<pass 60>><</link>><<exhibitionist5>><<gcool>><<llawareness>><<ltrauma>><<lstress>>
			<br>
		<</if>>
		<<link [[Exercise outside (0:30)|Asylum Exercise]]>><<tiredness 3>><<stress -6>><<pass 30>><</link>><<exhibitionist5>><<gtiredness>><<lstress>><<gathletics>>
		<br>
		<<link [[Socialise (0:30)|Asylum Socialise]]>><<asylumstatus 1>><<trauma -3>><<stress -6>><<pass 30>><</link>><<exhibitionist5>><<gcool>><<ltrauma>><<lstress>>
		<br>
		<<link [[Study (0:50)|Asylum Study]]>><<pass 50>><<schoolskill>><</link>><<exhibitionist5>><<gschool>>
		<br>
		<<link [[Garden|Asylum Garden]]>><</link>>
		<br>
	<<elseif $exposed gte 1 and $exhibitionism gte 35>>
		<<if $hour is 6>>
			<<link [[Breakfast (1:00)|Asylum Breakfast]]>><<asylumstatus 1>><<awareness -10>><<trauma -3>><<stress -6>><<pass 60>><</link>><<exhibitionist3>><<gcool>><<llawareness>><<ltrauma>><<lstress>>
			<br>
		<</if>>
		<<link [[Exercise outside (0:30)|Asylum Exercise]]>><<tiredness 3>><<stress -6>><<pass 30>><</link>><<exhibitionist3>><<gtiredness>><<lstress>><<gathletics>>
		<br>
		<<link [[Socialise (0:30)|Asylum Socialise]]>><<asylumstatus 1>><<trauma -3>><<stress -6>><<pass 30>><</link>><<exhibitionist3>><<gcool>><<ltrauma>><<lstress>>
		<br>
		<<link [[Study (0:50)|Asylum Study]]>><<pass 50>><<schoolskill>><</link>><<exhibitionist3>><<gschool>>
		<br>
		<<link [[Garden|Asylum Garden]]>><</link>>
		<br>
	<<elseif $exposed is 0>>
		<<if $hour is 6>>
			<<link [[Breakfast (1:00)|Asylum Breakfast]]>><<asylumstatus 1>><<awareness -10>><<trauma -3>><<stress -6>><<pass 60>><</link>><<gcool>><<llawareness>><<ltrauma>><<lstress>>
			<br>
		<</if>>
		<<link [[Exercise outside (0:30)|Asylum Exercise]]>><<tiredness 3>><<stress -6>><<pass 30>><</link>><<gtiredness>><<lstress>><<gathletics>>
		<br>
		<<link [[Socialise (0:30)|Asylum Socialise]]>><<asylumstatus 1>><<trauma -3>><<stress -6>><<pass 30>><</link>><<gcool>><<ltrauma>><<lstress>>
		<br>
		<<link [[Study (0:50)|Asylum Study]]>><<pass 50>><<schoolskill>><</link>><<gschool>>
		<br>
		<<link [[Garden|Asylum Garden]]>><</link>>
		<br>
	<</if>>
	<<if $asylumstatus gte 80 and $asylumdistracted isnot 1>>
		<<link [[Ask the patients to distract the orderlies|Asylum Distract]]>><<set $asylumdistracted to 1>><<asylumstatus -30>><</link>><<lllcool>>
		<br>
	<</if>>
	<br>
	<<link [[Cell|Asylum Cell]]>><</link>>
	<br>
<</if>>