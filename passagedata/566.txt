<<set $location to "lake">><<set $outside to 1>><<effects>>

You follow one of the streams feeding the lake, to the pond Mason likes to relax in. Water bubbles up from a spring beneath it.
<br><br>
<<if $schoolday isnot 1 and $weather is "rain" and $daystate is "dusk">>
	<<npc Mason>><<person1>>Mason sits in it, wearing a swimsuit. <<His>> arms rest on the rocky bank, and <<his>> eyes are shut.
	<br><br>

	<<if $exposed gte 2>>
		<<if $exhibitionism gte 75>>
			<<link [[Sit in the pond|Mason In Pond Naked]]>><</link>><<if $ex_mason isnot 1>><<exhibitionist5>><</if>>
			<br>
			<<link [[Sit beside the pond|Mason Beside Pond Naked]]>><</link>><<if $ex_mason isnot 1>><<exhibitionist5>><</if>>
			<br>
		<<else>>
			<<covered>> You couldn't alert Mason to your presence like this!
			<br><br>
		<</if>>
	<<elseif $exposed gte 1>>
		<<if $exhibitionism gte 35>>
			<<link [[Sit in the pond|Mason In Pond Naked]]>><</link>><<if $ex_mason isnot 1>><<exhibitionist3>><</if>>
			<br>
			<<link [[Sit beside the pond|Mason Beside Pond Naked]]>><</link>><<if $ex_mason isnot 1>><<exhibitionist3>><</if>>
			<br>
		<<else>>
			<<covered>> You couldn't alert Mason to your presence like this!
			<br><br>
		<</if>>
	<<else>>
		<<link [[Sit in the pond|Mason In Pond]]>><</link>>
		<br>
		<<link [[Sit beside the pond|Mason Beside Pond]]>><</link>>
		<br>
	<</if>>
	<<link [[Leave|Lake Waterfall]]>><</link>>
	<br>
<<else>>
	The pond is empty. Mason likes to come here on rainy evenings on weekends and school holidays, after swimming in the lake.
	<br><br>

	<<link [[Leave|Lake Waterfall]]>><</link>>
	<br>
<</if>>