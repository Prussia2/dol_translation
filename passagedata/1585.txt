<<set $outside to 0>><<set $location to "cafe">><<effects>>

<<generate1>><<person1>>
"Absolutely unacceptable," shouts a voice from the main room. "I demand to see the chef." A <<person>> barges in and fixes you with a glare.
<br><br>
"You call this stale turd a cream bun?" <<he>> says, throwing it at you. "I waited long enough. You're gonna make me a fresh one. You better put some actual work into it this time."
<br><br>

<<He>> storms the way <<he>> came.
<br><br>

"Sorry," Sam says, entering a moment later. "<<Hes>> always like that. Just make <<him>> a fresh bun with extra cream. That usually does the trick."
<br><br>

Sam leaves you to get to work.
<br><br>

<<link [[Add your bodily fluids to the cream|Chef Help Masturbation]]>><<set $masturbationstart to 1>><</link>>
<br>

<<link [[Make a bun with regular cream|Chef Regular Bun]]>><</link>>
<br>