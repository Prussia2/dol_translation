<<effects>>

<<earnFeat "Farm Protector">>
The goons struggle to their feet, except the <<person2>><<person>>, who Alex grasps by the ear. "Tell Remy," <<person1>><<he>> pants. "Tell Remy what's coming if dogs like you keep fucking with us."<<lllaggro>><<farm_aggro -20>>
<br><br>

<<tearful>> you watch the goons flee down the lane. Alex leans on your shoulder. "Dunno what I'd do without you," <<he>> says.

<<if $exposed gte 1 and $farm_naked isnot 1>>
	<<covered>> <<He>> glances at your body, and blushes. "L-let me get you some towels."<<towelup>>
	<br><br>
	
	<<He>> disappears into the farmhouse, and emerges with the fabric.
<</if>>

"Take a breather if you like. I'm getting back to work." <<if $weather is "rain" or $weather is "snow">><<He>> wipes off some of the mud,<<else>><<He>> dusts <<himself>> down,<</if>> picks up <<his>> hat, and returns to the farm.

<<if $exposed gte 1>>
	<<glust>><<npcincr Alex lust 1>>
<</if>>
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>