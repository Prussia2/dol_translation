<<effects>>

<<if $submissive gte 1150>>
"I-If it's no trouble, could you untie me please?" you ask.
<<elseif $submissive lte 850>>
"It may have escaped your notice, but I'm tied up. Could you help?" you ask.
<<else>>
"Can you untie me?" you ask.
<</if>>

Without a word, Eden picks up a knife and slashes your bindings.
<br><br>

<<if $bus is "edenclearing">>
<<link [[Next|Eden Clearing]]>><<endevent>><</link>>
<br>
<<else>>
<<link [[Next|Eden Cabin]]>><<endevent>><</link>>
<br>
<</if>>