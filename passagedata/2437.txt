<<set $outside to 0>><<set $location to "police_station">><<effects>>
You ask the <<person>> if you can have your collar removed.
<<if $worn.neck.collaredpolice is 1 and $communityservice is 1>>
<<else>>
	"Sure," <<he>> says. "It'll cost £200. You really shouldn't go putting those things on for fun. They're government property for one thing, and a bitch to get off by design."
	<br><br>
	<<if $promiscuity gte 15 and $policecollarseduceattempt isnot 1>>
		<<link [[Seduce|Police Station Seduce]]>><<set $policecollarseduceattempt to 1>><</link>><<promiscuous2>>
		<br>
	<</if>>
	<<if $money gte 20000>>
		<<link [[Pay up|Police Station Collar Removal]]>><<set $money -= 20000>><</link>>
		<br>
	<</if>>
<</if>>
<<link [[Leave|Police Station]]>><</link>>
<br>