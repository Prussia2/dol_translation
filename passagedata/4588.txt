<<effects>>
<<cream_init>>
You spray yourself with the cream, until everything below your neck is covered. With the exception of your back, which you struggle to reach.
<br><br>
Everyone's going to see you, but maybe they'll assume you're not wearing so little underneath.
<br><br>
<<flaunting>> you swallow your anxiety, and step into the street.
<<if $ex_cream is 1>>
	<br><br>
<<else>>
	<<set $ex_cream to 1>><<exhibitionism3>>
<</if>>
<<link [[Next|Cream Ex Naked Walk 2]]>><</link>>
<br>