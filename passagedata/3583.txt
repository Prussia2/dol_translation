<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
You run left. Nothing impedes you. The sound of footsteps echoes around you. You try to run straight, but occasionally brush against a wall on either side.
<br><br>
<<pass 5>>
The tunnel must lead somewhere, but it's long. Several minutes pass, but <<person1>><<monk>> and <<his>> friend keep up. "You'll be... Sorry," <<he>> shouts between pants.
<<gtiredness>><<tiredness 1>>
<br><br>
You realise the air is getting warmer. Then you smack into yet another wall. You rebound onto a cold floor. You struggle to your feet, but it's all the pair need to catch up.
<<gpain>><<pain 4>>
<br><br>
Someone tears the $worn.face.name from your head. You're in a dim tunnel. Brown stone bricks line the walls. The <<monk>> clutches your neck and leers at you. "I'm gonna enjoy this all the more," <<he>> says. Beside <<him>> stands a <<person2>><<personstop>> The <<person>> doesn't look at you. <<He>> looks up, horror contorting <<his>> face.
<br><br>
<<set $worn.face.type.push("broken")>>
<<faceruined>>
<<link [[Next|Temple Garden Forest 2]]>><</link>>