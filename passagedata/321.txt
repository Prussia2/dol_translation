<<effects>>

<<if $submissive gte 1150>>
	"O-Okay," you say, loud enough for the staff to hear. "You can show me."
<<elseif $submissive lte 850>>
	"Sure," you say, loud enough for the staff to hear. "I'll take a look."
<<else>>
	"Okay," You say, loud enough for the staff to hear. "You can show me."
<</if>>
You close your eyes.
<br><br>

You hear the shuffle of the <<persons>> gown fall to the floor. <<He>> starts counting under <<his>> breath. <<He>> reaches ten, then you hear another shuffle.
<br><br>

"Thank you!" <<he>> says. You open your eyes. The <<person>> smiles, looking relieved. "Thank you." <<He>> hurries away.
<br><br>

<<endevent>>
<<link [[Next|Asylum]]>><</link>>
<br>