<<set $outside to 0>><<set $location to "brothel">><<effects>>
<<unset $corruptionDancing>>
You are in the brothel. Several stages dot the crowded room. The staff look flustered and overworked.
<br><br>
<<if $stress gte 10000>>
	<<passoutshop>>
<<elseif $brothelshowmissed gte 1>>
	<<set $brothelshowmissed to 0>><<npcincr Briar love -1>>
	<<npc Briar>><<generate2>><<generate3>><<person1>>Briar storms towards you as two of <<his>> thugs block the entrance. "Where were you?" <<he>> asks. "The crowd were expecting a show. It cost me, but you'll pay for it," <<he>> holds out <<his>> hand. "£1000. Now."
	<br><br>
	<<if $money gte 100000>>
		<<link [[Pay (£1000)|Brothel Pay]]>><<set $briardom += 1>><<set $money -= 100000>><<set $submissive += 1>><</link>>
		<br>
	<</if>>
	<<link [[Say you can't afford it|Brothel Pay Refuse]]>><</link>>
	<br>
	<<link [[Refuse|Brothel Pay Refuse]]>><<set $briardom -= 1>><<set $submissive -= 1>><</link>>
	<br>

<<elseif $NPCName[$NPCNameList.indexOf("Leighton")].init is 1 and $leightonbrothel is undefined and $brotheljob is 1 and $weekday gte 6 and $pillory_tenant.special.name isnot "Leighton">><<set $leightonbrothel to 1>>
<<npc Leighton>><<person1>>
"You look like one of my students," says an amused voice from your right. It's Leighton. <<He>> sits alone on a sofa, with <<his>> arms outstretched and resting on the back.
<br><br>

"I knew you were the type. Your boss is a friend of mine," <<he>> says. <<He>> pulls a wad of cash from <<his>> pocket and pats the seat beside <<himstop>> "Best keep me happy."
<br><br>

<<link [[Sit (0:05)|Brothel Leighton Sit]]>><<npcincr Leighton love 1>><<npcincr Leighton dom 1>><<pass 5>><</link>>
<br>
<<link [[Get angry|Brothel Leighton Angry]]>><<npcincr Leighton love -1>><<npcincr Leighton dom -1>><<def 1>><<stress -6>><</link>><<lstress>>
<br>
<<link [[Refuse|Brothel Leighton Refuse]]>><</link>>
<br>

<<else>>

<<if $exposed gte 1>>
	You feel exposed, despite not being the only one attired so lewdly.
	<br><br>
<</if>>

<<if $brotheljob is 1 and $fameprostitution gte 30 and $brothelshowintro is undefined>><<set $brothelshowintro to 0>>
	<<generate1>><<person1>>The <<person>> guarding the door rests a hand on your shoulder. <span class="gold">"Boss wants to see you."</span><<endevent>>
	<br><br>
<</if>>

<<if $brotheljob is 1>>

<<link [[Work as a dancer|Brothel Dance]]>><<danceinit>><<set $dancing to 1>><<set $venuemod to 3>><<stress -4>><<tiredness 4>><<set $dancelocation to "brothel">><</link>><<lstress>><<gtiredness>>
<br>
	<<if $brothelshowintro is 1>>
		<<link [[Go behind the stage|Brothel Stage]]>><</link>>
		<br>
	<</if>>
	<<if $leightondancerefused is 1 and $headblackmailed is 1 and $pillory_tenant.special.name isnot "Leighton">>
		<<link [[Dressing Room (0:01)|Leighton Club Ignore]]>><<pass 1>><<set $leightondancerefused to 0>><</link>>
		<br>
	<<else>>
		<<link [[Dressing Room (0:01)|Brothel Dressing Room]]>><<pass 1>><</link>>
		<br>
	<</if>>
	<<link [[Basement (0:01)|Brothel Basement]]>><<pass 1>><</link>>
	<br>
	<<link [[Bathroom and Showers (0:01)|Brothel Bathroom]]>><<pass 1>><</link>>
	<br>
<</if>>

<<link [[Briar's Office (0:03)|Briar's Office]]>><<pass 3>><</link>>
<br>
	<<if $exposed lte 0>>
		<<link [[Leave (0:01)|Harvest Street]]>><<pass 1>><</link>>
		<br>
	<<elseif $exposed gte 2 and $exhibitionism gte 75 and $daystate isnot "night">>
		<<link [[Leave (0:01)|Brothel Leave Naked Day]]>><<pass 1>><</link>><<if $ex_brothel isnot 1>><<exhibitionist5>><</if>>
		<br>
	<<elseif $exposed gte 2 and $exhibitionism gte 55 and $daystate is "night">>
		<<link [[Leave (0:01)|Brothel Leave Naked]]>><<pass 1>><</link>><<if $ex_brothel isnot 1>><<exhibitionist4>><</if>>
		<br>
	<<elseif $exposed is 1 and $exhibitionism gte 35 and $daystate isnot "night">>
		<<link [[Leave (0:01)|Brothel Leave Undies Day]]>><<pass 1>><</link>><<if $ex_brothel isnot 1>><<exhibitionist3>><</if>>
		<br>
	<<elseif $exposed is 1 and $exhibitionism gte 15 and $daystate is "night">>
		<<link [[Leave (0:01)|Brothel Leave Undies]]>><<pass 1>><</link>><<if $ex_brothel isnot 1>><<exhibitionist2>><</if>>
		<br>
	<</if>>

<</if>>