<<effects>>

The couple finish to uproarious applause. It's your turn. You and Avery take each other's arms, and begin.
<br><br>

<<if $danceskill gte random(1, 1000)>>

	<span class="green">You dance well,</span> elegant and sophisticated. Avery is happy to follow your lead. The catch a glimpse of the <<person3>><<person>> frowning as <<he>> watches. <<if $submissive lte 850>>You stick out your tongue.<</if>>
	<<danceskilluse>>
	<br><br>

	<<if $rng gte 51>>
		You twirl away from Avery, close to the audience. When you try to reunite however, you meet resistance. The <<person3>><<person>> reaches from the crowd, <span class="pink">and clutches your $worn.lower.name.</span> <<He>> tugs.
		<<set $worn.lower.integrity -= 50>>
		<br><br>
		<<if $worn.lower.integrity lte 0>>
			<<integritycheck>><<exposure>>
			With a sickening tearing sound, your clothes rip free from your body, remaining in the <<persons>> grasp.
				<<if $worn.under_lower.type.includes("naked")>>
					You're left naked from the waist down, in the middle of the room, your <<genitals>> and <<bottom>> the centre of attention. <<covered>>
					<br><br>
					<<fameexhibitionism 30>>
					<<if $exhibitionism gte 75>>
						<<link [[Keep dancing|Avery Party Dance Naked]]>><</link>><<exhibitionist5>>
						<br>
					<</if>>
					<<link [[Run|Avery Party Dance Run]]>><<trauma 6>><<stress 6>><</link>><<gtrauma>><<gstress>>
				<<else>>
					You're left with just your $worn.under_lower.name covering you from the waist down, in the middle of the room, the centre of attention. <<covered>>
					<br><br>
					<<fameexhibitionism 10>>
					<<if $exhibitionism gte 35>>
						<<link [[Keep dancing|Avery Party Dance Undies]]>><</link>><<exhibitionist3>>
						<br>
					<</if>>
					<<link [[Run|Avery Party Dance Run]]>><<trauma 6>><<stress 6>><</link>><<gtrauma>><<gstress>>
					<br>

				<</if>>
		<<else>>
			You tear your clothes free from <<his>> grip. They're worse for wear, but should hold up. You return and finish the dance.
			<br><br>

			The music comes to a stop, and the guests applaud. You and Avery are the clear winners. You bow.
			<<gglove>><<ggendear>><<npcincr Avery love 3>><<set $endear += 20>><<famesocial 10>>
			<br><br>
			<<earnFeat "Ballroom Show-off">>
			You and Avery are surrounded by admirers until the party winds down. Together you leave the mansion.
			<br><br>

			<<link [[Next|Avery Party End]]>><</link>>
			<br>
		<</if>>
	<<else>>
		The music comes to a stop, and the guests applaud. You and Avery are the clear winners. You bow.
		<<gglove>><<ggendear>><<npcincr Avery love 3>><<set $endear += 20>><<famesocial 10>>
		<br><br>
		<<earnFeat "Ballroom Show-off">>
		You and Avery are surrounded by admirers until the party winds down. Together you leave the mansion.
		<br><br>

		<<link [[Next|Avery Party End]]>><</link>>
		<br>
	<</if>>

<<else>>
	<span class="red">Unfortunately,</span> Avery isn't happy to take your lead with so many watching, and you keep stepping on each other's feet. You catch a glimpse of the <<person3>><<personcomma>> a smug smile on <<his>> face. <<danceskilluse>><<lendear>><<set $endear -= 10>>
	<br><br>
	By the time the music stops, it's clear you've lost. The audience applaud anyway, though it's muted compared to what your competition received.
	<br><br>

	Avery bids <<his>> acquaintances farewell as the party winds down, and together you leave the mansion.
	<br><br>

	<<link [[Next|Avery Party End]]>><</link>>

<</if>>