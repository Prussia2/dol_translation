<<set $outside to 0>><<set $location to "home">><<effects>>

You stop at the first house without any decorations. "I bet these fucks have loads to give," Whitney says. "Just trying to keep it to themselves." Whitney marches you up to the front door. <<His>> friends hang back and rummage around the garden.
<br><br>
There's no answer. Whitney bashes <<his>> bat against the door. <<He>> doesn't stop. A minute passes until a <<person6>><<person>> answers. "I-I don't want any trouble," <<he>> stammers.
<br><br>
"Good evening," Whitney says. "I'm Whitney." <<He>> gestures at you. "This is my babe. And those are my friends back there. Trick or treat."
<br><br>
"I don't have anything," the <<person6>><<person>> replies. "Please leave."
<br><br>
"Alright. Have a nice evening." Whitney takes your hand, and together you walk to the front of the garden.
<br><br>

<<link [[Next|Whitney Trick 4]]>><</link>>
<br>