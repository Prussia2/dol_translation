<<set $outside to 0>><<set $location to "cafe">><<effects>>

The limo approaches the cafe. You look out the window, and see a large crowd gathered beneath a giant picture, mounted above the cafe entrance. It's a picture of you, smiling beneath a chef's hat and pouring <<if $chef_picture is "lewd">>"cream"<<else>>cream<</if>> down your chest from a ladle.
<br><br>

<<if $chef_picture is "lewd">>
	<span class="lewd">You remember exactly what the cream is made of, and blush.</span>
	<<garousal>><<arousal 600>>
	<br><br>
<</if>>
<<if $NPCName[$NPCNameList.indexOf("Avery")].init is 1>>
	The limo halts, and a <<if $rng gte 51>>waiter<<else>>waitress<</if>> opens the door. You're met by applause.
	<<llltrauma>><<trauma -24>>
	<br><br>
	You're walking down the red carpet when a figure appears at your side.<<npc Avery>><<person1>> It's Avery. "I had no idea you were a culinary prodigy," <<he>> says. "I was surprised to see your face up in lights." <<He>> holds out <<his>> hand. "Let me walk you in."
	<br><br>
	<<link [[Take Avery's hand|Chef Opening Avery Accept]]>><<set $chef_avery to 1>><<npcincr Avery love -1>><</link>><<glove>>
	<br>
	<<link [[Refuse Avery's hand|Chef Opening Avery Refuse]]>><<npcincr Avery love -1>><<npcincr Avery rage 5>><</link>><<garage>><<llove>>
	<br>
<<else>>
	The limo halts, and a <<if $rng gte 51>>waiter<<else>>waitress<</if>> opens the door. You're met by applause.
	<<llltrauma>><<trauma -24>>
	<br><br>
	You walk down the red carpet. You recognise Niki among them. <<npc Sam>><<person1>> Sam meets you at the entrance, wearing <<his>> <<if $pronoun is "m">>suit and tie<<else>>red dress<</if>>.
	<br><br>
	"This is everything I've wished for," <<he>> says. "Thank you for making it come true." <<He>> hugs you, eliciting a greater cheer from the audience.
	<<gglove>><<set $NPCName[$NPCNameList.indexOf("Sam")].love += 3>>
	<br><br>
	<<endevent>>
	<<link [[Next|Chef Opening 6]]>><</link>>
	<br>
<</if>>