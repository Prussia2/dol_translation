<<set $outside to 1>><<set $location to "school">><<schooleffects>><<effects>>
<<if $phase is 0>>
	You crouch to clean off the dirt <<he>> pointed to. "That'll do it. You can leave now," <<he>> says, shutting the blinds. You dress and leave the premises.
	<br><br>
<<elseif $phase is 1>>
	You bend over to clean off the dirt, sticking your <<bottom>> out toward Leighton. You feel it wiggle as you rub the car. Leighton's face reddens. "T-that will be all. You can go now." <<He>> stammers, shutting the blinds. You dress and leave the premises.
	<<exhibitionism3>>
	<br><br>
<<elseif $phase is 2>>
	You bend over to clean off the dirt, sticking your bare <<bottom>> out toward Leighton. You feel it wiggle as you rub the car. Leighton's face reddens. "T-that will be all. You can go now." <<He>> stammers, shutting the blinds. You dress and leave the premises.
	<<exhibitionism3>>
	<br><br>
<</if>>
<<link [[Next|Oxford Street]]>><<clotheson>><<endevent>><</link>>
<br>