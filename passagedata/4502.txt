<<set $outside to 0>><<set $location to "underground">><<effects>>
You carefully feel around the edge of the room, looking for anything that might help you get out. You don't find anything. Feeling helpless, you sit back down on the mattress, and feel a cool breeze caress your leg. You put your hands against the floor and feel it again, coming from beneath the mattress.
<br><br>
You shunt it aside and the breeze becomes unmistakable. You can feel small cracks in the ground. It might be your imagination, but you think you can see a tiny amount of light through them too.
<br><br>
<<set $undergroundescape to 1>>
<<link [[Next|Underground Cell]]>><</link>>
<br>