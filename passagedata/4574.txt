<<effects>>

You don't think there's anyone around. You take a deep breath, then run out from behind the tree and into the street. Reaching the top of the staircase can't have taken more than a dozen seconds, but it feels much longer.
<br><br>

You crouch once at the top, hidden from the street by the short barrier on either side. Your heart thunders in your chest.<<if $ex_flyover is undefined>><<exhibitionism3>><<set $ex_flyover to 1>><<else>><br><br><</if>>

<<link [[Next|Flyover Ex Top]]>><</link>>
<br>