<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "danube">>
<<upperruined>><<lowerruined>><<underruined>><<set $leftarm to "bound">><<set $rightarm to "bound">>
<<generate1>><<generate2>><<generate3>><<generate4>><<generate5>><<generate6>>You awaken in darkness, yet you hear muffled voices all around you. You can barely move; your arms and legs have been hogtied behind your back. There's something lodged in your mouth, gagging you. You think you smell roast vegetables.
<br><br>
You hear what sounds like metal clanging on metal, and you are blinded by a sudden light. Polite applause erupts around you before your eyes adjust. Once they do, you find that you're lying exposed and helpless on a platter in the middle of a table, surrounded by <<people>> in fancy dinner wear. You hear the clink of cutlery on glass and the applause tapers off.
<br><br>
<<person1>>A voice behind you speaks. "I hope everyone's enjoyed their evening so far. As you can see, we have something quite special for pudding." <<He>> claps <<his>> hands and you hear doors opening, followed by footsteps. Two pairs of arms grab you by the shoulders and hoist you up, positioning you on your shins and giving you a better view of the room. You see the speaker stood at one end of the table, glass in hand, a proud smile on <<his>> face. <<He>> nods at the pair who lifted you, a <<person2>><<person>> and <<person3>><<person>> each wearing servant's garb.
<br><br>
Without warning, both start pouring a cold white liquid on your chest with large wooden ladles. The sensation makes you gasp, eliciting mischievous whispers from some of the guests. It runs over your nipples, making them feel even more raw and sensitive, and doesn't stop there. When it reaches your <<genitals>> the speaker licks <<his>> lips. They aren't going to eat you, surely!
<br><br>
<<link [[Next|Danube Meal2]]>><</link>>