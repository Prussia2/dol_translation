<<set $outside to 0>><<set $location to "cafe">><<effects>>

<<if $submissive gte 1150>>
"I-it's my fault," you say.
<<elseif $submissive lte 850>>
"Just watch where you're going in the future," you say.
<<else>>
"It's okay," you say.
<</if>>
The <<if $pronoun is "m">>waiter<<else>>waitress<</if>> nods and disappears into the kitchen while you dry off.
<br><br>

"You handled that well," Sam says from behind the till.
<br><br>

The rest of the shift passes uneventfully. You earn £5.
<<set $money += 500>><<pass 1 hour>>
<br><br>

<<link [[Next|Ocean Breeze]]>><<endevent>><</link>>