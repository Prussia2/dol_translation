<<widget "halloweenwhitney">><<nobr>>

<<npc Whitney>><<generatey2>><<generatey3>><<generatey4>><<generatey5>><<generate6>><<person2>>
You hear a screech from your left. A <<person>> steps out from behind a tree, wearing a pair of horns and what look like blood-stained rags. <<His>> face is painted red. <<covered>> <<He>> holds a cricket bat above <<his>> head, as if about to swing.
<br><br>
You back away, but a another screech erupts from behind you. Then from your left and right. More costumed villains emerge and rush around you, each with their own bat. They shriek and hiss, punctuated by laughter.
<br><br>
<<person1>>
You spin around, looking for a way out. You're surrounded. Then the shrieks stop. Each figure turns and stares at something behind you.
<br><br>

<<link [[Next|Whitney Trick 1]]>><</link>>
<br>

<</nobr>><</widget>>