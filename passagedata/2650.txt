<<effects>>

<<if $phase is 0>>
	You write a brief but polite response, wait until you're sure River's back is turned, then throw the paper back at Whitney.
	<br><br>

	<<He>> catches it in one hand and unfurls it. <<His>> friends lean over to read it. One of them whispers something. Whitney chucks the paper over <<his>> shoulder, looking bored.
	<br><br>


<<elseif $phase is 1>>
	You write a brief but suggestive response, wait until you're sure River's back is turned, then throw the paper back at Whitney. <<promiscuity1>>

	<<He>> catches it in one hand and unfurls it. <<His>> friends lean over to read it. One of them whispers something as Whitney grins. <<He>> whispers back to <<his>> friend, who writes something on <<his>> behalf.
	<br><br>

	Whitney throws the paper once more, just as River turns. The teacher snatches it from the air, and drops it in the bin without reading it.
	<<endevent>><<npc River>><<person1>>
	<<He>> points at the door.
	<<endevent>><<npc Whitney>><<person1>>
	With an exasperated sigh, Whitney rises to	<<his>> feet and leaves the room.
	<br><br>

	<<set $whitneymaths to "sent">>
<<else>>
	You write a curt message telling Whitney to leave you alone, wait until you're sure River's back is turned, then throw the paper back.
	<br><br>

	<<He>> catches it in one hand and unfurls it. <<His>> friends lean over to read it. One of them whispers something. Whitney chucks the paper over <<his>> shoulder and sticks <<his>> tongue out at you.
	<br><br>

<</if>>

<<link [[Next|Maths Lesson]]>><<endevent>><</link>>
<br>