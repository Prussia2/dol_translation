<<effects>>

There's enough room in your cell for basic exercise. You work up a sweat, and keep pushing yourself, taking your mind off your imprisonment.
<<physique 6>>
<br><br>

<<if $hour lte 5>>
	<<link [[Keep working out (1:00)|Livestock Workout]]>><<pass 60>><<tiredness 6>><</link>><<gtiredness>>
	<br>
<</if>>
<<link [[Stop|Livestock Cell]]>><</link>>
<br>