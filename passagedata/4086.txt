<<effects>>

You ask Bailey if it were possible to install a spring in the orphanage garden, like Mason suggested. "It's possible," Bailey says. "An underground stream runs right beneath us. I looked into it once. Would have cost £2000."
<br><br>

<<He>> stares at you. "I could have one installed. For <span class="gold">£6000.</span> That's my price. Now piss off."
<br><br>

<<if $money gte 600000>>
	<<link [[Pay for spring (£6000)|Bailey Pond Pay]]>><<set $money -= 600000>><</link>>
	<br>
<</if>>
<<link [[Leave|Orphanage]]>><<endevent>><</link>>
<br>