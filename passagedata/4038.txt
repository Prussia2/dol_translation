<<set $outside to 0>><<set $location to "town">><<effects>>

<<person2>>"<<He>> was asking for it," you say. "Harassing people isn't okay, and Robin didn't like <<himstop>>"
<br><br>
Robin remains quiet as you move to the next house, but stays close to you. You hear the orphans whisper and giggle amongst themselves. You think they'll be talking about your temper for a while.
<br><br>

<<link [[Next|Robin Trick 6]]>><<endevent>><<pass 30>><</link>>
<br>