<<set $outside to 0>><<set $location to "strip_club">><<effects>>

<<if $submissive gte 1150>>
	"Pl-please don't be mad," you say. "But I didn't agree to this."
<<elseif $submissive lte 850>>
	"This isn't what I agreed to," you say. "I'm out."
<<else>>
	"I didn't agree to this," you say.
<</if>>
You try to head back inside, but the <<person1>><<person>> holds you painfully tight. <<His>> friends close in.
<br><br>
You try to break free,

<<if $physique gte random(1, $physiquemax)>><<famescrap 1>>

	<<if $devlevel lte 10>>
		despite the size difference. You punch at <<his>> stomach. You must have connected with a vulnerable spot, as <<he>> keels over,<span class="green"> releasing <<his>> grip.</span>
	<<else>>
		and twist your way out of <<his>> grip. <span class="green">You manage to overpower <<himcomma>></span> sending <<him>> sprawling to the floor.
	<</if>>
	<<His>> friends rush forward, but you manage to throw open the escape door and dart inside the club. You slam it behind you, bashing it against the <<person2>><<persons>> head.
	<br><br>

	Security look amused. One of them walks over, and holds the escape door shut.
	<br><br>

	You return to work. The rest of the shift passes without event. <<tipreceive>>
	<br><br>

<<if !$per_npc.bartend and random(1, 2) is 2>>
	<<saveNPC 0 bartend>>
<</if>>

	<<link [[Next|Strip Club]]>><<endevent>><<pass 30>><</link>>
	<br>
<<else>>

	<<if $devlevel lte 10>>
		despite the size difference. You punch at <<his>> stomach. <span class="red"><<He>> doesn't react.</span>
	<<else>>
		and twist your way out of <<his>> grip. <span class="red"><<Hes>> too strong.</span>
	<</if>>
	<<He>> shoves you to the ground.
	<<pain 6>><<gpain>>
	<br><br>

	<<link [[Next|Bartending VIP Patron Gang Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<</if>>