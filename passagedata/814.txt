<<effects>>

Alex produces another bottle from the alcove as you take a swig. It's strong. "Don't tell my <<if $pronoun is "m">>dad<<else>>mum<</if>>," <<he>> laughs.
<br><br>

<<if $NPCName[$NPCNameList.indexOf("Alex")].lust gte 10 and random(1, 2) is 2>>
	<<He>> wraps an arm around your shoulder as <<he>> reclines again. <<His>> hand wanders down your chest.<<garousal>><<arousal 6>>
	<br><br>
	
	<<link [[Push away|Farm Rest Push]]>><<npcincr Alex love -1>><<npcincr Alex lust -1>><</link>><<llove>><<llust>>
	<br>
	<<if $promiscuity gte 15>>
		<<link [[Allow|Farm Rest Allow]]>><</link>><<promiscuous2>>
	<</if>>
<<else>>
	<<farm_relax_end>>
<</if>>

<<set $farm_relax_drink to "accept">>