<<set $outside to 1>><<set $location to "lake">><<effects>><<lakeeffects>>
<<if $cool gte 160>>
	They're eager to have you along. They form around you and vie for your attention. Everyone knows the forest is dangerous, but you should be safe in a group.
	<<status 1>><<gcool>>
	<br><br>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure)>>
		<<lakejourney>>
	<<else>>
		You arrive at the lake without a problem.
		<br><br>
		<<link [[Next|Lake Shore]]>><</link>>
		<br>
	<</if>>
<<elseif $cool gte 40>>
	They're happy to have you along. You chat as you travel, making sure to stay close. Everyone knows the forest is dangerous, but you should be safe in a group.
	<<status 1>><<gcool>>
	<br><br>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure)>>
		<<lakejourney>>
	<<else>>
		You arrive at the lake without a problem.
		<br><br>
		<<link [[Next|Lake Shore]]>><</link>>
		<br>
	<</if>>
<<else>>
	They look at each other and smirk at your request, but they don't turn you down. Everyone knows the forest is dangerous, but you should be safe in a group.
	<<status 1>><<gcool>>
	<br><br>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure)>>
		<<lakejourney>>
	<<else>>
		You're nearing the lake when someone shoves you from behind. You fall to the ground. The group laughs, and leaves you alone in the forest.
		<<gtrauma>><<gstress>><<trauma 3>><<stress 3>>
		<br><br>
		<<link [[Next|Forest]]>><<set $forest to 30>><</link>>
		<br>
	<</if>>
<</if>>