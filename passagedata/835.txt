<<effects>>

<<if $athletics gte random(1, 600)>>
	You grasp the basket, and run.
	<br><br>
	
	"Oi!" the <<person>> shouts, rushing around the counter in pursuit. You escape onto the road and make for the farm, <span class="green">leaving the <<person>> well behind.</span>
	<br><br>
	
	<<link [[Next|Farm Shopping End]]>><<endevent>><</link>>
	<br>
<<else>>
	You grasp the basket, and run.
	<br><br>
	
	"Oi!" the <<person>> shouts, rushing around the counter in pursuit. You escape onto the road and make for the farm, <span class="red">but the <<person>> catches up and tackles you to the ground.</span>
	<br><br>
	
	"Fucking thief," <<he>> says. "I'll show you what we do to your sort around here."
	<br><br>
	
	<<link [[Next|Farm Shopping Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<</if>>