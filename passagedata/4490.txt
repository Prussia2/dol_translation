<<set $outside to 0>><<set $location to "underground">><<effects>>
<<generate1>><<generate2>>
You are led to another part of the dungeon, and into a well-lit room. A <<person2>><<person>> is already here, holding a camera. Stood beside <<him>> is a <<person1>><<personstop>>
<<if $rng gte 51 and $swarmdisable is "f">>
	There's a bath sitting in the middle of the room. The <<person1>><<person>> pulls you over to it and pushes you inside. Before you get your bearings, <<he>> attaches a clip on the bottom of the bath to your collar, locking you in place. You look up at the <<personcomma>> now holding a bucket, and <<person2>><<person>> and wonder what they intend.
	<br><br>
	The <<person1>><<person>> tips the bucket over you, covering you in thick, long worms. They wriggle all over you, inciting shameful feelings.
	<br><br>
	<<link [[Next|Underground Film Worms]]>><<set $molestationstart to 1>><</link>>
	<br>
<<else>>
	The <<person>> walks right up behind you, uncomfortably close. The <<person2>><<person>> nods, and the <<person1>><<person>> starts to grope you.
	<br><br>
	<<link [[Next|Underground Film Molestation]]>><<set $molestationstart to 1>><</link>>
	<br>
<</if>>