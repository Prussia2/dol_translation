<<effects>>

You wait for a gap in the crowds. <<flaunting>> you clamber over the fence, and run.

<<if $exposed gte 2>>
	<<if $ex_road is undefined>>
		<<set $ex_road to 1>>
		<<exhibitionism5>>
	<<else>>
		<br><br>
	<</if>>
<<else>>
	<<if $ex_road is undefined>>
		<<set $ex_road to 1>>
		<<exhibitionism3>>
	<<else>>
		<br><br>
	<</if>>
<</if>>

The stalls pass in a blur. <<covered>> You don't know how many people can see you.
<br><br>
<<if $rng gte 81>>
	You're halfway across when a cat runs in your path!
	<br><br>

	<<link [[Jump|Road Ex Jump]]>><</link>><<athleticsdifficulty 1 1000>>
	<br>
	<<link [[Stop|Road Ex Cat Stop]]>><</link>>
	<br>
<<elseif $rng gte 61>>
	<<generatey1>><<generatey2>><<generatey3>><<generatey4>><<generatey5>>
	A gaggle of students emerge from a cafe opposite. You recognise them from school. They're distracted by each other. For now.
	<br><br>

	<<link [[Keep running|Road Ex Keep]]>><<stress 6>><<arousal 6>><</link>><<gstress>><<garousal>>
	<br>
	<<set $skulduggerydifficulty to 300>>
	<<link [[Hide|Road Ex Hide]]>><</link>><<skulduggerydifficulty>>
	<br>
<<elseif $rng gte 41>>
	A van pulls up in front of you. You couldn't see it coming behind all the stalls, nor hear it over the din of the market.
	<br><br>

	<<link [[Go under|Road Ex Under]]>><</link>>
	<br>
	<<link [[Go around|Road Ex Around]]>><</link>>
	<br>
	<<link [[Stop|Road Ex Stop]]>><</link>>
	<br>
<<elseif $rng gte 21>>
	In your haste, you didn't realise just how littered the street is.
	<br><br>

	<<link [[Keep up the pace|Road Ex Pace]]>><</link>><<dancedifficulty 1 400>>
	<br>
	<<link [[Go carefully|Road Ex Careful]]>><</link>>
	<br>
<<else>>
	You make it to the other side, round a corner, then lean against a wall. Your whole body shakes.
	<br><br>

	<<link [[Next|Commercial alleyways]]>><</link>>
	<br>
<</if>>