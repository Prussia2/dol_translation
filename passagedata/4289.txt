<<effects>>

"What a shame," Whitney says. "Too bad you forfeited the right to wear clothes when you wore a collar." They leave you stood in the middle of the corridor with your <<lewdness>> on display. There's no one else around.
<<npcincr Whitney lust 5>><<glust>>
<br><br>
<<set $stealtextskip to 1>>
<<stealclothes>>
<<endevent>>

<<link [[Next|Hallways]]>><</link>>
<br>