<<effects>><<set $location to "sewers">><<set $outside to 0>>
<<set $sewerschased to 0>>
<<set $sewersevent to random(5, 12)>>
<<if $rng gte 51>>
	<<npc Morgan>><<person1>>You crawl beneath a pile of algae, just before Morgan enters the tunnel. <<He>> almost steps on you, but doesn't notice. You're safe for now.
	<<lstress>><<stress -6>>
	<br><br>
	<<endevent>>
	<<destinationsewers>>
<<else>>
	<<npc Morgan>><<person1>>You crawl beneath a pile of algae, just before Morgan enters the tunnel. <<He>> steps on your arm, eliciting a muffled cry. Enough for <<him>> to hear. "<<charlescomma>>" <<he>> says. "What are you doing down there?" <<He>> grasps your hair and tugs you from your hiding place.
	<br><br>
	<<morganoptions>>
<</if>>