<<set $outside to 1>><<effects>>
You seize the phone from the <<person2>><<person>> and dash into the alley. "Thief! Someone!" You hear <<him>> chase after you as you dial emergency services. The dog appears in front of you and runs past. Then you hear the <<person>> cry out behind you. You glance back and see the dog holding the <<persons>> ankle in its mouth.
<br><br>
You call an ambulance as <<he>> struggles. You finish as <<he>> kicks the dog away and turns back to you, face red with anger. You throw <<him>> the phone, catching <<him>> off guard. <<He>> almost drops it. <<He>> starts marching towards you, but the dog growls at <<himstop>> <<He>> considers a moment, then spits in your direction and leaves.
<br><br>
You wait with the dog and the <<person1>><<person>> for the ambulance to arrive. You don't have to wait long. Two paramedics rush between the buildings and put <<him>> on a stretcher. They carry <<him>> to the waiting ambulance, and the dog follows. It gives you one last grateful bark then leaps into the vehicle as the doors shut.
<br><br>
<<famegood 10>>
<<endevent>>
<<clotheson>>
<<destinationeventend>>