<<set $outside to 0>><<set $location to "town">><<effects>>

<<if $submissive gte 1150>>
"That's not fair," you say. "I worked really hard on it."
<<elseif $submissive lte 850>>
"Don't," you say. "Or else."
<<else>>
"Give it back," you say. "Or I'll make you regret it."
<</if>>
<br><br>
Whitney laughs. "That mouth is gonna get you in trouble," <<he>> says. <<He>> looks thoughtfully at the paper, then hands it to you. "You'll make it up to me later."
<br><br>

You continue to the front row and find an empty seat. You glance around, worried that Whitney followed you. You see <<him>> sitting with <<his>> friends on the other side of the hall, in the second row.
<br><br>
<<endevent>>
<<link [[Next|Maths Competition Intro]]>><</link>>
<br>