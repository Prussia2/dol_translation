<<set $outside to 1>><<set $location to "forest">><<effects>>

<<if $weather is "rain">>
You walk to the forest, running between shelter to avoid the worst of the rain. "I like the rain," Robin says. "It's fun running between cover like this."
<br><br>

It's less wet under the protective branches of the trees. "The ground is too muddy for a picnic," Robin says. <<He>> looks at the nearby trees. "Look at that one! I bet I can climb it." <<He>> runs over to it and starts hauling <<himself>> up.
<br><br>

	<<if $pronoun is "m">>
	<<His>> shorts are baggy. You'd be able to see right up them if you stood closer.
	<br><br>
	<<else>>
	You'd be able to see right up <<his>> skirt if you stood closer.
	<br><br>
	<</if>>

<<link [[Stand beneath|Robin Forest Beneath]]>><<npcincr Robin lust 1>><</link>><<glust>><<promiscuous1>>
<br>
<<link [[Cheer from a distance|Robin Forest Cheer]]>><<npcincr Robin love 1>><</link>><<glove>>
<br>

<<elseif $weather is "overcast">>

You walk to the forest. "Let's not go too deep," Robin says. "It's scary. I brought a picnic too. We'll need somewhere grassy to set up."
<br><br>

You find a flat patch of grass, surrounded by trees. Bees dance between nearby flowers. Robin lays out a rug. "Sit down before it blows away," <<he>> says.
<br><br>

Before you can sit down, however, the wind picks up. It's strong enough to pick up the rug and scatter the contents of the picnic basket towards the surrounding trees. "No, the food!" <<He>> catches the rug before it flies away, but the food is ruined.
<<gstress>><<stress 1>>
<br><br>

Robin frowns as <<he>> folds up the rug. <<He>> steps toward the spot where the basket disappeared through the branches, then stops. The trees look ominous. "It's okay," <<he>> says. "I can buy a new basket."
<br><br>

	<<if $voredisable is "f">>
	<<link [[Go deeper into the forest to retrieve the basket|Robin Forest Basket]]>><<npcincr Robin love 1>><</link>><<glove>>
	<br>
	<</if>>
<<link [[Leave the basket|Robin Forest Basket Leave]]>><</link>>
<br>
	<<if $money gte 2000>>
	<<link [[Offer to buy the new basket (£20)|Robin Forest New Basket]]>><<set $money -= 2000>><<npcincr Robin love 1>><</link>><<glove>>
	<br>
	<</if>>

<<else>>
You walk to the forest. "Let's not go too deep," Robin says. "It's scary. I brought a picnic too. We'll need somewhere grassy to set up."
<br><br>

You find a flat patch of grass, surrounded by trees. Bees dance between nearby flowers. Robin lays out a rug. "Sit down before it blows away," <<he>> says.
<br><br>

There's not much food in the basket. "I took what I could get away with," <<he>> says. "It's more for the fun of it anyway."
<br><br>

<<if $robinromance is 1>>
<<link [[Kiss|Robin Forest Kiss]]>><<npcincr Robin lust 1>><<npcincr Robin love 1>><</link>><<glove>><<glust>><<promiscuous1>>
<br>
<</if>>
<<link [[Talk|Robin Forest Talk]]>><<npcincr Robin love 1>><<stress -6>><<trauma -3>><</link>><<glove>><<ltrauma>><<lstress>>
<br>

<</if>>