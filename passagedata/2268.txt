<<effects>>

You try to keep up with the <<person1>><<personstop>>
<<if $tending gte random(1, 400)>>
	<span class="green">You know a thing or two yourself.</span> <<gtrust>><<set $enemytrust += 20>>
	<br><br>

	"Not every day I meet someone worth talking to," <<he>> says, looking at the <<print setup.plants[$stall_plant].plural>> in front of <<himstop>> "How much for $stall_amount of these?"
	<br><br>

	<<stall_trust>>
	<br><br>

	<<stall_sell_actions>>

<<else>>
	<span class="red"><<He>> flips between topics too quickly,</span> and you lose track. <<He>> loses interest and moves on without buying anything.
	<br><br>

	<<stall_actions>>
<</if>>