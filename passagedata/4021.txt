<<set $outside to 0>><<set $location to "home">><<effects>>
You reach over and run your fingers over <<his>> tummy. "No fair!" <<he>> shouts, squirming away from you. It's enough to give you the upper hand, and you claim victory shortly after.
<br><br>
"You're lucky you're a guest in my room," <<he>> sulks.
<br><br>
<<link [[Apologise|Robin Play Apologise]]>><<npcincr Robin love 1>><<npcincr Robin dom 1>><</link>><<glove>><<gdom>>
<br>
<<if $robinromance is 1>>
	<<link [[Apologise with lewd|Robin Play Apologise Lewd]]>><<npcincr Robin love 1>><<npcincr Robin lust 1>><</link>><<promiscuous1>><<glove>><<glust>>
	<br>
<</if>>
<<link [[Bask in victory|Robin Options]]>><<npcincr Robin love -1>><<npcincr Robin dom -1>><</link>><<llove>><<ldom>>
<br>