<<set $outside to 0>><<set $location to "underground">><<effects>>

<<if $rng gte 51 and $bestialitydisable is "f">>

You are led once more to the stage, where the crowd awaits. <<covered>> Once there, a metal shutter rattles open behind you, and a gigantic pig steps out. The audience cheers it on.
<br><br>

<<link [[Next|Underground Stage Pig]]>><<set $molestationstart to 1>><</link>>
<br>

<<else>>

You are led once more to the stage, where the crowd awaits. <<covered>> There's a <<generate1>><<person1>><<person>> already waiting for you. <<He>> takes hold of you leash and pulls you closer while looking at the audience. "What shall I do with <<phim>> my friends?"
<br><br>

<<link [[Next|Underground Stage Molestation]]>><<set $molestationstart to 1>><</link>>
<br>

<</if>>