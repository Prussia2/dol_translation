<<set $outside to 1>><<set $location to "forest">><<effects>>
You cheer Robin on as <<he>> climbs the tree. <<He>> makes it to the top. "I knew I could do it," <<he>> shouts. You can barely make <<him>> out in the canopy. "It's pretty wet up here. I should come down."
<br><br>
<<Hes>> panting by the time <<he>> reaches the floor. "That was fun, but I got pretty wet," <<he>> says. "Lets go home or I'll catch a cold."
<br><br>
Together you return to the orphanage.
<br><br>
<<robinoptions>>