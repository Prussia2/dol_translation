<<effects>>

You chat with Mason.

<<if $rng gte 81>>
	<<He>> talks about the lake and the forest surrounding it. It seems they're a big reason <<he>> moved here.
<<elseif $rng gte 61>>
	<<He>> asks about school, and probes you for what the other students think of <<himstop>>
<<elseif $rng gte 41>>
	<<He>> talks about <<his>> successes in competitive swimming.
<<elseif $rng gte 21>>
	<<He>> talks about the computer games <<hes>> played recently.
<<else>>
	<<He>> talks about the other teachers. <<Hes>> careful not to insult any of them.
<</if>>
<br><br>
<<if $swimmingskill lte 999 and random(1, 3) is 3>>
	<<He>> explains some of <<his>> swimming technique to you. <<swimmingskilluse>>
<</if>>

<<mason_actions>>