<<set $outside to 0>><<set $location to "lake_ruin">><<underwater>><<effects>><<lakeeffects>>
You are deep in the submerged ruin beneath the lake. Pots of different sizes fill the room. There's an ancient door opposite the entrance.
<<if $lakeruinkey isnot 2>>
	It's closed.
<<else>>
	It's open.
<</if>>
<br><br>
<<if $stress gte 10000>>
	<<passoutlake>>
<<else>>
	<<if $lakeruinkey is 1>>
		<<link [[Use bronze key|Lake Ruin Door]]>><<wateraction>><</link>><<loxygen>>
		<br>
	<</if>>
	<<link [[Search pots|Lake Pots]]>><<wateraction>><</link>><<loxygen>>
	<br>
	<<if $lakeruinkey is 2>>
		<<link [[Swim inside plinth room|Lake Ruin Plinth]]>><<wateraction>><</link>><<loxygen>>
		<br>
	<</if>>
	<br>
	<<link [[Swim towards exit|Lake Ruin]]>><<wateraction>><</link>><<loxygen>>
	<br>
<</if>>