<<water>><<effects>>

You haul yourself over the railing, and plummet into cool water. The current seizes you, and pulls you downstream.
<br><br>

<<if random(1, 2) is 2>>
	The water carries you through the park, and into more populated parts of town. You don't see anyone, but still keep low in the water to conceal your <<lewdnessstop>>
	<br><br>
	You manage to cling to a dock, and haul yourself from the water.
	<br><br>

	<<link [[Next|Starfish Street]]>><</link>>
	<br>
<<else>>
	Something sucks you below the water, towards a dark crevice in the riverbed. You're pulled into darkness, until you fall through air and land, sprawling, on cold stone.
	<br><br>

	<<link [[Next|Sewers Intro]]>><</link>>
	<br>
<</if>>