<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>

<<if $phase is 0>>
	You place a pound coin on the plate. The <<monk>> gives you a small <<if $pronoun is "m">>bow<<else>>curtsy<</if>>.
	<br><br>

	<<endevent>>
	<<prayoptions>>

<<elseif $phase is 1>>
	You place a five pound note on the plate. "We rely on such kindnesses," <<he>> says. "Thank you."
	<br><br>

	<<endevent>>
	<<prayoptions>>

<<elseif !$soup_kitchen_known>>
	You place £25 on the plate. The <<monk>> beams at you. "Thank you," <<he>> says. "This will go a long way toward maintaining the building." <<He>> pauses before continuing. "Perhaps you would like to assist us further? We run a soup kitchen in the evening, between <span class="gold"><<if $timestyle is "military">>18:00 and 21:00<<else>>6:00 pm and 9:00 pm<</if>></span>. We could use another pair of hands."
	<br><br>

	"If you're interested, you can find the kitchen in the grey building next door."
	<br><br>

	<span class="gold">You've discovered the soup kitchen on Wolf Street.</span>
	<br><br>
	<<set $soup_kitchen_known to 1>>

	<<endevent>>
	<<prayoptions>>

<<else>>
	You place £25 on the plate. The <<monk>> beams at you. "Thank you," <<he>> says. "This will go a long way toward maintaining the building."
	<br><br>

	<<endevent>>
	<<prayoptions>>
<</if>>