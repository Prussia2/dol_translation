<<set $outside to 0>><<set $location to "town">><<effects>>

"Come on," you say. "Bailey's scarier than any movie." Robin laughs and sits down.
<br><br>

The film starts. It's quite frightening. You think you're getting used to it when it catches you off guard. Instinct takes over, and you grope for Robin's hand.
<br><br>

<<He>> stays close beside you on the way home, asking if you think the monster is real. <<He>> almost sounds excited by the time you get back to <<his>> room.
<br><br>

<<link [[Next|Robin Options]]>><</link>>
<br>