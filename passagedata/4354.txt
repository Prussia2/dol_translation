<<effects>>
<<if $tentacles.active lte ($tentacles.max / 2)>>
	The creature loses interest in you, and returns to the depths. <<tearful>> you struggle to your feet.
	<<clotheson>>
	<<endcombat>>
	<<destinationeventend>>
<<else>>
	The walkway beneath you gives way under the strain, and you tumble into the water below.
	<br><br>
	<<if $upperoff isnot 0>>
		<<upperruined>>
	<</if>>
	<<if $loweroff isnot 0>>
		<<lowerruined>>
	<</if>>
	<<if $underloweroff isnot 0>>
		<<underlowerruined>>
	<</if>>
	<<if $underupperoff isnot 0>>
		<<underupperruined>>
	<</if>>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Drain Water]]>><</link>>
	<br>
<</if>>