<<set $outside to 0>><<effects>>

<<if $NPCName[$NPCNameList.indexOf("Kylar")].love gte 50>>
	<<if $rng gte 81>>
	You pat Kylar's head. <<His>> eyes close in bliss.
	<<elseif $rng gte 61>>
	You pull your $worn.lower.name taut against your <<genitalsstop>> "I may need a smaller size," you say. Kylar stands in front of you to shield you from view.<<set $genderknown.pushUnique("Kylar")>>
	<<elseif $rng gte 41>>
	You whisper something naughty into Kylar's ear. "D-don't" <<he>> says, smiling. "Someone might hear you."
	<<elseif $rng gte 21>>
	You lick Kylar's cheek. <<He>> licks you back.
	<<else>>
	You ask to see Kylar's underwear. "Or maybe," you say. "You don't have any on?"
		<<if $pronoun is "m">>
		<<He>> pulls down the hem of <<his>> shorts to reveal white briefs.
		<<else>>
		<<He>> lifts the side of <<his>> skirt to reveal panties with white and pink stripes.
		<</if>>
		<br><br>
		"D-don't tell anyone," <<he>> says. "That's for you only."

	<</if>>
<<else>>
	<<if $rng gte 81>>
	You pat Kylar's head. <<He>> recoils from each touch as if struck. When you stop, <<he>> whispers, "M-more."
	<<elseif $rng gte 61>>
	You pull your $worn.lower.name taut against your <<genitalsstop>> "I may need a smaller size," you say. Kylar's face turns red.
	<<elseif $rng gte 41>>
	You whisper something naughty into Kylar's ear. <<He>> looks away and blushes.
	<<elseif $rng gte 21>>
	You lick Kylar's cheek. <<He>> tenses, but rests <<his>> hand on the spot you touched.
	<<else>>
	You ask Kylar what sort of underwear <<he>> has on. "Or maybe," you say. "You don't have any?" <<He>> trembles.
	<</if>>
<</if>>
<<promiscuity1>>

<<if ($location is "park" or $bus is "starfish") and $NPCName[$NPCNameList.indexOf("Kylar")].love gte 80 and $NPCName[$NPCNameList.indexOf("Kylar")].lust gte 80 and $kylar_invite isnot 1>>
	<<set $kylar_invite to 1>>
	Kylar stares at you, eyes full of intensity. It melts away at once. "D-do you," <<he>> manages before averting <<his>> eyes. "Do you want to go somewhere private, where we can do th-things?"
	<br><br>

	<<link [[Invite Kylar to the orphanage (0:15)|Kylar Invite]]>><<pass 15>><</link>><<promiscuous1>>
	<br>
	<<link [[Decline|Kylar Invite Decline]]>><<npcincr Kylar love -1>><<npcincr Kylar rage 1>><</link>><<llove>><<gksuspicion>>
	<br>
<<else>>
	<<kylaroptions>>
<</if>>