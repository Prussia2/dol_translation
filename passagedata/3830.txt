<<effects>>

You close your eyes first. Maybe it'll make things easier.

<<if $leftarm is "bound" and $rightarm is "bound">>
	You turn to face Avery, giving <<him>> a clear view of your <<undiesstop>> You resist the urge to run and hide.
<<else>>
	You move your hand behind your back, exposing your <<undies>> to Avery. You resist the urge to cover back up, or run and hide.
<</if>>
<br><br>

Avery stares for a moment, then knees in front of you. "Allow me." <<He>> wraps the towel around your waist. <<Hes>> in no hurry.
<br><br>
<<towelup>>

<<link [[Next|Avery Party Dance Return]]>><</link>>
<br>