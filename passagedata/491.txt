<<effects>>


<<set $thighgoo += 1>>
<<set $feetgoo += 1>>
<<set $chestgoo += 1>>
<<set $bottomgoo += 1>>
<<set $tummygoo += 1>>
	
<<if $physique gte random(1, $physiquemax)>>
	You feel around for a solid footing, then punch the wall of the plant. <span class="green">You feel it yield.</span> You punch again, and again. More mucus drips from above, but you continue your assault.
	<br><br>
	
	<<if !$worn.upper.type.includes("naked") or !$worn.lower.type.includes("naked") or !$worn.under_lower.type.includes("naked") or !$worn.under_upper.type.includes("naked")>>
		<span class="pink">Your clothing feels looser as the mucus dissolves it.</span>
		
		<<set $worn.upper.integrity -= 50>>
		<<set $worn.lower.integrity -= 50>>
		<<set $worn.under_upper.integrity -= 50>>
		<<set $worn.under_lower.integrity -= 50>>
		
		<<integritycheck>><<exposure>>
		<br><br>
	<<else>>
		
	<</if>>
	Something wraps around your ankle just as your fist breaks through. You shake yourself free tear open a gap wide enough for your body. More vines spring to life, but they're too slow. You escape into the trees.
	<br><br>
	
	<<link [[Next|Forest]]>><<set $eventskip to 1>><</link>>
	<br>
<<else>>
	You punch the wall of the plant, <span class="red">but it doesn't yield at all.</span> You try again, hoping to find some weak spot, but a thick vine rises from the mucus and snatches your waist, pulling you back into the fluid.
	<br><br>
	
	<<link [[Next|Forest Pitcher Resist]]>><</link>>
	<br>
<</if>>