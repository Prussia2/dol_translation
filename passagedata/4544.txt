<<effects>>

You pull up the grate and slip into the damp darkness beneath, pulling the grate atop you.
<br><br>
<<generate1>><<person1>>
A <<person>> walks closer, pulling a dumpster behind <<himstop>> <<if $pronoun is "f">>You can see up <<his>> skirt.<</if>>
<<garousal>><<arousal 600>>
<br><br>

<<if $rng gte 81>>
	<<He>> pulls the dumpster right above the drain, and leaves it there. You hear <<him>> walk away.
	<<stress>><<stress 6>>
	<br><br>

	Lacking options, you slink deeper into the drains.
	<br><br>

	<<link [[Next|Commercial Drain]]>><<endevent>><</link>>
	<br>
<<elseif $rng gte 61 and $tentacledisable is "f" and $hallucinations gte 2>>
	<<He>> doesn't look down as <<he>> passes. You're about to climb out, when you <span class="pink">feel something slither over your shoulder.</span>
	<<endevent>>
	<br><br>

	<<link [[Next|Commercial Ex Tentacles]]>><<set $molestationstart to 1>><</link>>
	<br>
<<else>>
	<<He>> doesn't look down as <<he>> passes. You climb back out once the sound of the wheels fades.
	<br><br>
	<<endevent>>

	<<destinationeventend>>
<</if>>