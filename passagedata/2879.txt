<<effects>><<set $outside to 0>>

<<set $schooldrainintro to 1>>

There's a shaft hidden beneath the rubber flooring that should lead to the town's storm drain system. You peel back the floor and open the hatch, before climbing down into the dark. The sound of running water grows stronger.
<br><br>

<<link [[Next|Commercial Drain]]>><</link>>
<br>