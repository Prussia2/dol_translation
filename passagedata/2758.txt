<<set $outside to 1>><<set $location to "school">><<schooleffects>><<effects>>

You leave the office and walk to the set of double doors leading outside. You <<nervously>> remove your clothing and leave it lying just inside the doorway. You cover your <<undies>> with a hand before walking over to the car. Leighton watches you as promised, with a blank expression on <<his>> face.
<br><br>

You find a bucket of soapy water containing a sponge and get to work, starting with the side opposite Leighton. You wonder if stripping was really necessary, you manage to avoid getting wet without difficulty.
<br><br>

You keep your <<lewdness>> covered as best you can as you move round the car. You're close to finishing when Leighton opens the window. "Good job, but you missed a spot," <<he>> says, pointing next to your thighs.
<br><br>

<<link [[Crouch|School Detention Car2]]>><<set $phase to 0>><</link>>
<br>
<<if $worn.under_lower.type.includes("naked")>>
	<<if $exhibitionism gte 75>>
		<<link [[Bend over|School Detention Car2]]>><<set $phase to 2>><</link>><<exhibitionist5>>
		<br>
	<</if>>
<<elseif !$worn.under_lower.type.includes("naked")>>
	<<if $exhibitionism gte 35>>
		<<link [[Bend over|School Detention Car2]]>><<set $phase to 1>><</link>><<exhibitionist3>>
		<br>
	<</if>>
<</if>>