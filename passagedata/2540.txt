<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<if $alarm is 1 and $rescue is 1>>
	<<set $rescued += 1>>
	<<endevent>><<npc Mason>><<person1>>Mason rushes in to investigate the scream. The girls back away and pretend they had no part in it.
	<<if $exposed gte 1>>
		<<He>> escorts you from the room.
	<<else>>
		<<He>> wraps towels around you and escorts you from the room.
	<</if>>
	<<if $player.gender is "m" and $genderknown.includes("Mason")>>
		"You shouldn't be in the girl's changing room," <<he>> says once you're alone. "That doesn't make what they did okay, but you need to be more careful in the future."
		<<gdelinquency>><<detention 1>>
	<<elseif $player.gender is "h" and $genderknown.includes("Mason")>>
		<<He>> stops once you're alone, but struggles to look you in the eye. "You need to hide your uniqueness," <<he>> says. "I know that isn't fair, but people can be cruel. Be careful."
	<<elseif $player.gendre_appearance is "m">>
		"You shouldn't be in the girl's changing room," <<he>> says once you're alone. "That doesn't make what they did okay, but you need to be more careful in the future."
		<<gdelinquency>><<detention 1>>
	<<else>>
		"What they were doing isn't okay," <<he>> says once you're alone. "I'll have a word with them."
		<<lcool>><<status -10>>
	<</if>>
	<br><br>
	<<tearful>> you watch the teacher return to <<his>> class.
	<br><br>
	<<clothesontowel>>
	<<endcombat>>
	<<link [[Next|School Pool Entrance]]>><</link>>
<<elseif $enemyhealth lte 0>>
	You knock the <<person>> against a locker. The other girls try to grab you, but you shove them away. <<tearful>> you escape the room.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|School Pool Entrance]]>><</link>>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	You shake free of the <<persons>> weakened grip. The other girls try to grab you, but you shove them away. <<tearful>> you escape the room.
	<br><br>
	<<clothesontowel>>
	<<endcombat>>
	<<link [[Next|School Pool Entrance]]>><</link>>
<<else>>
	<<strip>>
	<<if $player.gender is $player.gender_appearance>>
		The <<person>> backs away from you, your clothes in hand. "If you want them back," <<he>> says. "You better do what we say."
		<br><br>
		The other girls shout suggestions.
		<<if $worn.neck.collared is 1>>
			The <<person>> ignores them, and picks up your leash. "Perfect," <<he>> says. "Time for walkies. Get on your knees like a good dog."
			<br><br>
			<<link [[Get on knees (0:10)|School Girl's Knees]]>><<pass 10>><<status -10>><<trauma 6>><<stress 6>><</link>><<lcool>><<gtrauma>><<gstress>>
			<br>
			<<link [[Refuse|School Girl's Naked Refuse]]>><</link>>
			<br>
		<<else>>
			"Everyone get your phones," <<he>> says to the girls around <<himstop>> "Make sure you get lots of evidence." <<He>> looks back at you. "Time for your punishment. Bend over."
			<br><br>
			<<link [[Bend over (0:10)|School Girl's Bend]]>><<pass 10>><<status -10>><<trauma 6>><<stress 6>><</link>><<lcool>><<gtrauma>><<gstress>>
			<br>
			<<link [[Refuse|School Girl's Naked Refuse]]>><</link>>
			<br>
		<</if>>
	<<elseif $player.gender is "h">>
		The <<person>> backs away from you, your clothes in hand.
		<<if $schoolrep.crossdress gte 5>>
			The girls murmer in excitement. "I knew it," the <<person>> says. "You're the school freak."
			<br><br>
		<<elseif $schoolrep.crossdress gte 3>>
			The girls murmer in excitement. "You're the freak I keep hearing about," the <<person>> says. "I didn't believe it."
			<br><br>
		<<else>>
			The girls gape at your <<genitals>>, too stunned to speak. The <<person>> is first to shake free of <<his>> stupour. "You're a freak," <<he>> says.
			<br><br>
		<</if>>
		<<if $promiscuity gte 35>>
			<<link [[Seduce|School Girl's Crossdress Seduce]]>><</link>><<promiscuous3>>
			<br>
		<</if>>
		<<link [[Say that's just the way you are|School Girl's Crossdress Honest]]>><</link>>
		<br>
		<<link [[Blame a curse (0:05)|School Girl's Herm Explain]]>><<set $phase to 0>><<pass 5>><<status -10>><</link>><<lcool>>
		<br>
		<<link [[Blame strange science (0:05)|School Girl's Herm Explain]]>><<set $phase to 1>><<pass 5>><<status -10>><</link>><<lcool>>
		<br>
		
		<<schoolrep herm 1>>
	<<else>>
		The <<person>> backs away from you, your clothes in hand.
		<<if $schoolrep.crossdress gte 5>>
			"So the rumours are true," the <<person>> says, satisfied. "You're really a girl. Why do you do it? Are you just a pervert?"
		<<elseif $schoolrep.crossdress gte 3>>
			The <<person>> gapes. "You're the crossdresser I keep hearing about," <<he>> says. "Why do you do it? Are you just a pervert?"
		<<else>>
			The <<person>> gapes. "You're not a boy?" <<he>> says. "Why were you dressed as one?"
		<</if>>
		<br><br>
		<<if $promiscuity gte 35>>
			<<link [[Seduce|School Girl's Crossdress Seduce]]>><</link>><<promiscuous3>>
			<br>
		<</if>>
		<<link [[Say you like dressing as a boy|School Girl's Crossdress Honest]]>><</link>>
		<br>
		<<link [[Say you were forced to (0:05)|School Girl's Crossdress Forced]]>><<pass 5>><<status -10>><</link>><<lcool>>
		<br>
		
	<<schoolrep crossdress 1>>
	<</if>>
<</if>>