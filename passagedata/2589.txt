<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<towelup>>

<<if $kylarenglishstate is "active">>
	You stare out the window and daydream. Kylar sketches you.
<<elseif $trauma gte (($traumamax / 10) * 7)>>
	You keep to yourself, afraid that one wrong move will result in the whole class molesting you.
<<elseif $trauma gte (($traumamax / 10) * 2)>>
	You focus on your own thoughts, trying to keep the creeping anxiety at bay.
<<else>>
	You don't really pay attention to the lesson, instead idly staring out the window and thinking about what you'll do after school.
<</if>>
<br><br>

<<pass 5>><<stress -1>>
<<schooleffects>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
	<<pass 5>><<stress -1>>
	<<schooleffects>>
<<else>>
	<<set $phase to 1>>
<</if>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
	<<pass 5>><<stress -1>>
	<<schooleffects>>
<<else>>
	<<set $phase to 1>>
<</if>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
	<<pass 5>><<stress -1>>
	<<schooleffects>>
<<else>>
	<<set $phase to 1>>
<</if>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
<<else>>
	<<set $phase to 1>>
<</if>>

<<if $phase is 1>>
	<<set $phase to 0>>
	The bell rings, signifying the end of the lesson. You
	<<storeon "englishClassroom" "check">>
	<<if _store_check is 1>>
		<<storeon "englishClassroom">>
		grab your coat and
	<</if>>
	leave the classroom.
	<br><br>
	<<if $time is ($hour * 60 + 57)>>
		<<pass 3>>
	<<elseif $time is ($hour * 60 + 58)>>
		<<pass 2>>
	<<elseif $time is ($hour * 60 + 59)>>
		<<pass 1>>
	<</if>>

	<<link [[Next|Hallways]]>><</link>>
<<else>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (8900 - ($allure / 2))>>
		<<eventsenglish>>
	<<else>>
		<<eventsenglishsafe>>
	<</if>>
<</if>>