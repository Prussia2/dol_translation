<<set $outside to 0>><<set $location to "town">><<effects>>

You embrace Robin, and <<he>> squeezes you back. <<He>> rests <<his>> head on your shoulder. You hold <<him>> for a few minutes, listening to <<him>> breathe, until <<he>> pulls away.
<br><br>

"My sweets!" <<he>> says. "I almost forgot." <<He>> lifts <<his>> bag and rifles through the contents. "There are so many," <<he>> says. <<He>> points at your bag. "We can trade the ones we don't like."
<br><br>

You swap sweets with Robin, though <<he>> has trouble making up <<his>> mind, until <<he>> eats one too many and lies back on the bed.
<<stress -18>><<trauma -18>><<lltrauma>><<llstress>>
<br><br>

<<robinoptions>>