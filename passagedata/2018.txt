<<set $outside to 1>><<set $location to "home">><<effects>>

	<<if $submissive gte 1150>>
	You look at your feet and shake your head. You've had quite enough.
	<<elseif $submissive lte 850>>
	You glare at the <<personstop>> "I'm not doing what you want. Give me back my clothes this instant!" you demand, sounding more assured than you feel.
	<<else>>
	"No," you say. "I'm not going to."
	<</if>>
<br><br>

"Worth a try," <<he>> says, shrugging. <<He>> hurls your clothing into the air and giggles as <<he>> runs away.
<br><br>

<<if $rng gte 81>>

Your clothes snag on a tree branch, high above you.
<br><br>

<<link [[Climb|Bask Climb]]>><<endevent>><</link>>
<br>
<<link [[Leave them|Bask Sneak]]>><<set $upperoff to 0>><<set $loweroff to 0>><<endevent>><</link>>
<br>

<<else>>

Your clothes land beside you. You hide behind a bush before dressing, afraid you're still being watched.
<br><br>
<<clotheson>>

<<link [[Next|Garden]]>><<endevent>><</link>>
<br>

<</if>>