<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>

With your arms bound, your mouth gagged, and your eyes blindfolded, you're not sure what you can do. But you have to try.
<br><br>

You jump to your feet and run.
<br><br>

<<link [[Left|Temple Garden Left]]>><</link>>
<br>
<<link [[Right|Temple Garden Right]]>><</link>>
<br>