<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $phase is 0>>
<<npc Whitney>><<person1>>Whitney walks with you back to the canteen, where Robin waits. "Here's your <<if $player.gender_appearance is "f">>girlfriend<<else>>boyfriend<</if>> back, <<pshe>> helped me with a huge load of work", <<he>> chuckles as <<he>> walks away.
<br><br>
<<endevent>>
<<npc Robin>><<person1>>Robin looks worried, "Are you okay? Did<<endevent>>
<<npc Whitney>><<person1>><<he>><<endevent>>
<<npc Robin>><<person1>>hurt you?" <<he>> asks.<<endevent>>
<br><br>

<<link [[Keep it a secret|Robin Kiyoura Secret]]>><</link>>
<br>

<<link [[Tell Robin what happened, but say you didn't want to|Robin Kiyoura Didn't want to]]>><<npcincr Robin dom 1>><<pain -10>><</link>><<gdom>><<lpain>>
<br>

<<link [[Tell Robin what happened, and say you enjoyed it|Robin Kiyoura Enjoyed it]]>><<npcincr Robin love -5>><<npcincr Robin dom -1>><<npcincr Robin lust 1>><</link>><<lllove>><<ldom>><<glust>>
<br>

<<else>>

<<npc Robin>><<person1>>You return to Robin in the canteen. <<He>> looks worried. "Are you okay? Did<<endevent>>
<<npc Whitney>><<person1>><<he>><<endevent>>
<<npc Robin>><<person1>>hurt you?" <<he>> asks.<<endevent>>
<br><br>

<<link [[Keep it a secret|Robin Kiyoura Secret]]>><</link>>
<br>

<<link [[Tell Robin what happened, but say you didn't want to|Robin Kiyoura Didn't want to]]>><<npcincr Robin dom 1>><<pain -10>><</link>><<gdom>><<lpain>>
<br>

<</if>>