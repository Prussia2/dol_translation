<<set $outside to 1>><<set $location to "lake">><<effects>><<lakeeffects>>
<<set $forest to 30>><<set $bus to "lakefirepit">>
<<if $laketeenspresent is 1>>
	You are at the Firepit. There are people sitting on the stone benches that circle the pit. You can see several people splashing in the water down on the shore.
<<elseif $daystate is "night">>
	<<if $laketeensfire is 1>>
		You are at the Firepit. People are sitting around the fire as it burns, talking and drinking. The reflection of the fire on the lake's surface is almost mesmerising.
	<<else>>
		You are at the Firepit. Dark water ripples unseen.
	<</if>>
<<elseif $daystate is "dusk" or $daystate is "dawn">>
	You are at the Firepit. There are stone benches circling the pit.
<<else>>
	You are at the Firepit. There are stone benches circling the pit.
<</if>>
<<if $weather is "rain">>
	The water is alive with motion as rain breaks its surface.
<<else>>
	The water is calm.
<</if>>
<br><br>
<<if $exposed gte 1 and $laketeenspresent is 1 or $laketeensfire is 1>>
	<<if $exhibitionism gte 75>>
	You keep low and stay among the trees. You really wish to show your <<lewdness>>, but you restrain yourself from doing so.
	<<else>>
	You keep low and stay among the trees to keep your <<lewdness>> from being seen.
	<</if>>
	<br><br>
<</if>>
<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $stress gte 10000>>
	<<passoutlake>>
<<elseif $foresthunt gte 10>>
	<<foresthunt>>
<<elseif $danger gte (9900 - $allure) and $eventskip is 0 and $laketeenspresent isnot 1>>
	<<eventlake>>
<<else>>
	<<lakereturnjourney>>
	<<link [[South to shore (0:10)|Lake Shore]]>><<pass 10>><</link>>
	<br><br>
	<<foresticon>><<link [[Forest (0:10)|Forest]]>><<pass 10>><</link>>
	<br>
	<<set $eventskip to 0>>
<</if>>