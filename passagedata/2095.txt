<<effects>>
<<if $writeaction and $writebodyaction>>
	<<add_bodywriting $writebodyaction $writeaction pen>>
	You write <span class="lewd">"<<print $skin[$writebodyaction].writing>>"</span> on your <<bodypart $writebodyaction>>.
	<<unset $writeaction>><<unset $writebodyaction>>
<<elseif $writeaction>>
	<span class="blue">You must choose a body part to write on.</span>
	<<unset $writeaction>>
<<elseif $writebodyaction>>
	<span class="blue">You must choose what to write.</span>
	<<unset $writebodyaction>>
<<else>>
	You hover the pen over your skin.
<</if>>
<br><br>
Location:
<br>
<<for _e to 0; _e lt $bodypart_number; _e++>>
	<<activebodypart>>
	<<if $skin[_active_bodypart].writing>>
	<<else>>
		<<set _bodypart_detected to 1>>
		<label>Write on your <<bodypart _active_bodypart>> <<capture _active_bodypart>><<radiobutton "$writebodyaction" _active_bodypart>><</capture>></label>
		<br>
	<</if>>
<</for>>
<<if _bodypart_detected isnot 1>>
	There's no room on your skin.
	<br>
<</if>>
<br>
Text:
<br>
<label><span class="lewd">£5 whore <<radiobutton "$writeaction" "five_pound_whore">></span></label> |
<label><span class="lewd">£10 a pop <<radiobutton "$writeaction" "ten_pound_a_pop">></span></label> |
<label><span class="lewd">£25 per fuck <<radiobutton "$writeaction" "twenty_five_pound_per_fuck">></span></label> |
<label><span class="lewd">£100 <<radiobutton "$writeaction" "one_hundred_pound">></span></label> |
<label><span class="lewd">It's not rape if you pay me <<radiobutton "$writeaction" "its_not_rape_if_you_pay_me">></span></label> |
<label><span class="lewd">Whore <<radiobutton "$writeaction" "whore">></span></label> |
<label><span class="lewd">Slut for hire <<radiobutton "$writeaction" "slut_for_hire">></span></label> |
<label><span class="lewd">Body for sale <<radiobutton "$writeaction" "body_for_sale">></span></label> |
<br><br>
<<link [[Write|Mirror Bodywriting]]>><</link>>
<br>
<<link [[Stop|$passage_mirror]]>><<unset $writeaction>><<unset $writebodyaction>><</link>>
<br>