<<effects>>
<<if $noise is 2 and $combat is 0>>
	<span class="red">The youths are wondering aloud why the dog is so agitated. They'll find you soon if you don't do something!</span>
	<br><br>
<</if>>
<<if $timer is 1 and $combat is 0>>
	You hear the youths move away from the alley. You take a peek and see that they are indeed gone. The dog looks dejected as you walk away.
	<br><br>
	<<endevent>>
	<<residentialeventend>>
	<br><br>
<<elseif $timer is 1 and $combat is 1>>
	You hear the youths move away from the alley. You try to take a peek but the dog tackles you to the ground.
	<br><br>
	<<set $rescue to 0>>
	<span id="next"><<link [[Next|Residential Dog]]>><</link>></span><<nexttext>>
	<br><br>
<<elseif $noise gte 3>>
	Fed up with the racket, the pair make their way over.
	<br><br>
	<span id="next"><<link [[Next|Residential Dog Alarm]]>><</link>></span><<nexttext>>
<<elseif $phase is 1>>
	It continues barking. You're afraid the youths will investigate if this continues.
	<br><br>
	<<link [[Quietly tell it to shut up|Residential Dog]]>><<set $noise += 1>><</link>>
	<br>
	<<if $bestialitydisable is "f">>
		<<link [[Stroke its head->Residential Dog]]>><<set $phase to 2>><</link>>
		<br><br>
	<</if>>
<<elseif $phase is 2>>
	It quiets at your touch, runs in a small circle, then walks right up to you. You notice its genitals are ready and waiting. It looks at you expectantly.
	<br><br>
	<<link [[Just keep stroking its head->Residential Dog]]>><<set $noise += 1>><<set $phase to 3>><</link>>
	<br>
	<span class="sub"><<link [[Take its genitals in your hand|Residential Dog]]>><<handskilluse>><<handstat>><<set $phase to 4>><</link>></span>
	<br><br>
<<elseif $phase is 3>>
	You continue to stroke its head, but it doesn't seem pleased and barks again.
	<br><br>
	<<link [[Just keep stroking its head->Residential Dog]]>><<set $noise += 1>><<set $phase to 3>><</link>>
	<br>
	<<link [[Take its genitals in your hand|Residential Dog]]>><<set $phase to 4>><<handskilluse>><<handstat>><</link>>
	<br><br>
<<elseif $phase is 4>>
	<<set $molestationstart to 1>><<set $phase to 5>>
	You take its genitals in your hand. It starts humping against your fingers.
	<br><br>
	<span id="next"><<link [[Next|Residential Dog]]>><</link>></span><<nexttext>>
	<br><br>
<<elseif $phase is 5>>
	<<if $molestationstart is 1>>
		<<set $molestationstart to 0>>
		<<controlloss>>
		<<violence 1>>
		<<neutral 1>>
		<<molested>>
		<<beastNOGENinit>>
		<<npchand>>
	<</if>>
	<<if $timer gte 1>><<set $rescue to 1>><</if>>
	<<effectsman>>
	<br>
	<<beast $enemyno>>
	<br><br>
	<<stateman>>
	<br><br>
	<<actionsman>>
	<<if $alarm is 1>>
		<<if $rescue is 1>>
			<span id="next"><<link [[Next->Residential Dog Alarm]]>><</link>></span><<nexttext>>
		<<else>>
			No one comes to your aid.
			<<set $alarm to 0>>
			<<if $drugged gte 1>>Intoxicated as you are, you couldn't cry very convincingly.<</if>>
			<br><br>
			<<if $enemyarousal gte $enemyarousalmax>>
				<span id="next"><<link [[Next->Residential Dog Ejaculation]]>><</link>></span><<nexttext>>
			<<elseif $enemyhealth lte 0>>
				<span id="next"><<link [[Next->Residential Dog Escape]]>><</link>></span><<nexttext>>
			<<else>>
				<span id="next"><<link [[Next->Residential Dog]]>><</link>></span><<nexttext>>
			<</if>>
		<</if>>
	<<elseif $enemyarousal gte $enemyarousalmax>>
		<span id="next"><<link [[Next->Residential Dog Ejaculation]]>><</link>></span><<nexttext>>
	<<elseif $enemyhealth lte 0>>
		<span id="next"><<link [[Next->Residential Dog Escape]]>><</link>></span><<nexttext>>
	<<else>>
		<span id="next"><<link [[Next->Residential Dog]]>><</link>></span><<nexttext>>
	<</if>>
<</if>>