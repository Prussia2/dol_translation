<<effects>>

You kick the <<person3>><<person>> away from you. The others back off.
<br><br>

<<if $exposed gte 2>>

	The <<person1>><<person>> leers at your body, then, in a moment of boldness, grasps you by the shoulders. The other students follow suit, and they pull you from the table. They drag you into a nearby alley, away from sight.
	<br><br>

	You wrestle free of their grasp and shove them away from you, before turning to stare them down.
	<br><br>

	<<link [[Next|Road Ex Examine]]>><</link>>
	<br>

<<else>>

	<<generate6>><<person6>>"What you kids up to?" a <<person>> shouts, peering between the fish hanging from the roof of <<his>> stall.
	<br><br>

	"Shit," the <<person1>><<person>> says, glancing over. <<He>> turns and runs. The other students follow suit.
	<br><br>

	You push yourself to your feet and dart into a nearby alleyway, only stopping once around another corner. You lean against a wall, your whole body shaking.
	<br><br>

	<<link [[Next|Commercial alleyways]]>><<set $eventskip to 1>><</link>>
	<br>

<</if>>