<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>

<<generate1>><<person1>>You find a <<person>> dressed as a <<monk>> walking the halls nearby and ask permission to climb the tower. <<He>> nods.
<br><br>

<<He>> opens the way with a large bronze key, and gestures for you to enter. <<He>> follows close behind as you ascend the spiral stairs, and waits at the top while you reach out the window and peel off the pink lichen.
<br><br>

You turn to find <<him>> stood barring the stairs. "They don't let me near <<girls>> unsupervised," <<he>> pants. "No one will hear you up here. What a treat!" The <<monk>> reaches for you with both hands.
<br><br>

<<link [[Next|Temple Lichen Rape]]>><<set $molestationstart to 1>><</link>>
<br>