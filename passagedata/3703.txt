<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
"Just the two of us in here," you whisper as you walk by. "And all these empty beds."
<<promiscuity1>>
The <<person>> focuses on dusting a single, already clean, spot.
<br><br>
<<if $promiscuity gte 15>>
	<<link [[Seduce|Temple Quarters Seduce]]>><</link>>
	<br>
<</if>>
<<link [[Return to work|Temple Quarters]]>><<endevent>><</link>>
<br>