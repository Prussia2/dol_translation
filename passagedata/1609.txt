<<set $outside to 0>><<set $location to "town">><<effects>>

The <<person2>><<person>> nods. "One moment," <<he>> says, leaving the room. <<He>> returns soon after, carrying a large mirror. The <<person1>><<person>> pushes a chair in front of it, and the <<person2>><<person>> gestures for you to take a seat.
<br><br>

You sit still as the <<person>> and <<person2>><<person>> carefully apply makeup and trim your hair. It takes a while. "Sam wants you looking your best," the <<person2>><<person>> says. "A lot of people will be there."
<br><br>

<<pass 60>>

<<link [[Next|Chef Opening 4]]>><</link>>
<br>