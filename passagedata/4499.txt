<<set $outside to 0>><<set $location to "underground">><<effects>>
You enter another corridor, lined on one side with metal floor hatches. <<He>> opens one of them, and shoves you inside. You land on something soft at least. "Your role here is simple. Do as you're told. You don't want to know what happened to the last <<girl>> who made a fuss." <<He>> slams the hatch shut, plunging you into complete darkness.
<br><br>
You fumble around in the dark, and find you fell on what feels like a mattress. There's also a locked metal door and a tap in the corner, but the room is otherwise bare.
<br><br>
<<endevent>>
<<set $undergroundtime to 0>>
<<set $undergroundwater to 0>>
<<set $undergroundescape to 0>>
<<link [[Next|Underground Cell]]>><</link>>
<br>