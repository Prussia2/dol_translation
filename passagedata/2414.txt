<<set $outside to 0>><<set $location to "town">><<effects>>
<<pass 60>>
<<if $phase is 0>>
You follow Niki's instructions to the letter.
<<else>>
You take Niki's instructions as a guideline, and do your own thing when the urge strikes you. Niki seems neither impressed nor bothered.
<</if>>

"Props," <<he>> says into <<his>> phone after an hour. "I'll be back shortly," <<he>> says. "The cream is stored separately. Regulations. My people will arrive soon with the rest." <<He>> leaves the room, leaving you stood alone in front of the screen.
<br><br>

<<endevent>>

<<link [[Next|Photography Chef 5]]>><</link>>
<br>