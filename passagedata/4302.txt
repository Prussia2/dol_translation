<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
<<ejaculation>>

"F-fuck, yes…" Whitney says, putting a hand on the alley wall. <<tearful>> you duck under <<his>> arm and escape onto Connudatus Street.
<br><br>

<<clotheson>>
<<endcombat>>
<<set $eventskip to 1>>
<<link [[Next|Connudatus Street]]>><</link>>

<<elseif $enemyhealth lte 0>>

<<tearful>> you successfully shove Whitney away from you, and make a break for it onto Connudatus Street. <<His>> friends watch your escape, and seem unwilling to stop you.
<br><br>

<<clotheson>>
<<endcombat>>
<<set $eventskip to 1>>
<<link [[Next|Connudatus Street]]>><</link>>

<<else>>

You double over in pain, collapsing onto the floor. You look up at Whitney, now towering over you. <<He>> nods, and <<he>> and <<his>> friends reach towards you, grabbing and pulling.

<<if $exposed lt 2>><<clothesruined>>
They tear off your clothing and toss it up onto a nearby roof. You're left helpless, with your <<lewdness>> on display.
<br><br>
<</if>>
"Next time," Whitney says. "When I tell you to do something, you do it." <<He>> saunters from the alley. <<His>> friends follow close behind.
<br><br>

<<tearful>> you climb to your feet.
<br><br>

<<clotheson>>
<<endcombat>>
<<set $eventskip to 1>>
<<link [[Next|Connudatus Street]]>><</link>>

<</if>>