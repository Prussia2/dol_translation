<<temple_effects>><<effects>>
<<set $temple_quarters += 10>>
You encourage the initiates to help tidy the quarters. They don't seem motivated, so you get stuck in yourself, sweeping dust and sorting clothes. This works, and together you soon have the quarters looking much more presentable.
<br><br>
<<link [[Next|Temple]]>><</link>>
<br>