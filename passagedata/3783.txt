<<set $outside to 0>><<effects>>
You nod meekly and step away from the register. <<He>> takes the money and pockets it before escorting you outside. "I don't
want to see you around this place at night again, got that?" <<He>> leaves you in front of the building.
<br><br>
<<endevent>>
<<destinationeventend>>