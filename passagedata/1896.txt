<<set $outside to 0>><<set $location to "pub">><<dockeffects>><<effects>>

<<if $phase2 is 0>>

	<<if random(1, 2) is 2>>
	<<set $dockquizhome += 1>>

	You let your colleagues answer the questions. They argue a bit, but write them down, fold the paper and hand it to the pub owner. The sailors do likewise.
	<br><br>

	"That's a point to the dockers," The owner says. You cheer with your colleagues. They make lewd gestures at the sailors, who return the display.
	<<lstress>><<stress -1>>
	<br><br>

	<<else>>

	You let your colleagues answer the questions. They write them down, fold the paper and hand it to the pub owner. The sailors do likewise.
	<br><br>

	The owner reads your paper. "The dockers score zero." You grumble with your colleagues. The sailors laugh. <<if $dockquiz is "body">>"Guess it's up to us to teach you the ropes," the <<person>> teases.<<else>>"Don't worry," the <<person>> teases. "We didn't expect better."<</if>>
	<br><br>

	<</if>>

<<elseif $phase2 is 1>>

	<<if $promiscuity gte random(1, 100)>>
	<<set $dockquizhome += 1>>

	You write down the answers, fold the paper and hand it to the pub owner. The sailors do likewise.
	<br><br>

	"That's a point to the dockers," The owner says. You cheer with your colleagues.
	<<lstress>><<stress -1>>
	<br><br>

	<<else>>

	<<if $promiscuity lte 30>>You're not confident, but you write down the answers, fold the paper and hand it to the pub owner.<<else>>You write down the answers, fold the paper and hand it to the pub owner.<</if>> The sailors do likewise.
	<br><br>

	The owner reads your paper. "The dockers score zero." You grumble with your colleagues. The sailors laugh. <<if $dockquiz is "body">>"Guess it's up to us to teach you the ropes," the <<person>> teases.<<else>>"Don't worry," the <<person>> teases. "We didn't expect better."<</if>>
	<br><br>

	<</if>>

<<else>>

You convince your colleagues that you know your stuff, and write down nonsense answers. You fold the paper and hand it to the pub owner.
<br><br>

The owner reads your paper. "The dockers score zero." You grumble with your colleagues. What a surprise.
<br><br>

<</if>>
The owner reads the sailor's answers.
<<if random(1, 2) is 2>><<set $dockquizaway += 1>>
"That's a point to the sailors." A cheer erupts from their table. "You learn a thing or two sailing between ports," the <<person>> says.
<br><br>
<<else>>
"Sorry guys. No points." They boo. "You lot don't know nothing," the <<person>> says.
<br><br>

<</if>>

<<link [[Next|Docks Pub Crawl Quiz]]>><</link>>
<br>