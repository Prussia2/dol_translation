<<effects>>
You look through the cupboard containing your clothes.
<br><br>
<<set $wardrobe_location to "wardrobe">>
<<wardrobewear>>
<<if $exhibitionism lte 54>>
	<<if $exposed lte 0>>
	<<link [[Done|Asylum Cell]]>><<pass 1>><<unset $saveColor>><<unset $wardrobeRepeat>><<unset $tempDisable>><</link>>
	<br><br>
	<<else>>
	You can't remain undressed like this!
	<br><br>
	<</if>>
<<elseif $exhibitionism gte 75>>
<<link [[Done|Asylum Cell]]>><</link>>
<br><br>
<<else>>
	<<if $exposed lte 1>>
	<<link [[Done|Asylum Cell]]>><<unset $saveColor>><<unset $wardrobeRepeat>><<unset $tempDisable>><</link>>
	<br><br>
	<<else>>
	You can't remain undressed like this!
	<br><br>
	<</if>>
<</if>>
<<wardrobe>>