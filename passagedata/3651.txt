<<effects>>

<<generate1>><<person1>><<generate2>><<generate3>> A <<monk>> emerges from a door marked with a red cross. <<Hes>> flanked by two others. One of them assures Jordan that you'll be treated gently.
<br><br>
Head bowed, Jordan leaves the room as a <<monk>> pulls a gag from <<his>> habit. "To make amends you must perform a service. Stay still."
<br><br>

<<link [[Submit|Temple Arcade Submit]]>><</link>>
<br>
<<link [[Fight|Temple Arcade Fight]]>><<set $fightstart to 1>><</link>>
<br>