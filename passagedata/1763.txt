As the class winds down, you and the other students are led to the corner where a television cart sits. Flipping the television on, Charlie instructs the class to watch a short educational video before leaving.
<br><br>
<<set $rng to random(1, 8)>>
<<if $rng is 1>>
This class's video is on the history of high heels. You watch pictures of 17th century shoes, and how men of royalty used to wear them.
<<elseif $rng is 2>>
A guide to etiquette appears on the screen. The posh narrator explains how to walk and carry oneself when at a fancy restaurant.
<<elseif $rng is 3>>
The video is low-quality and grainy. It shows a woman wearing tall heels, stepping on a banana.
	<<if $awareness gte 200>>
	"This must be someone's fetish," you think.
	<<garousal>><<arousal 100>>
	<</if>>
<<elseif $rng is 4>>
A woman in a leotard appears on screen. You watch her perform a dance routine, paying attention to how she manages such grace in her heels.
	<<garousal>><<arousal 300>>
<<else>>
The video is a guide to walking in high heels. It shows you how to stand, how to sit, and how to walk gracefully.
<</if>>
<br><br>
<<link [[Leave|Dance Studio]]>><<endevent>><</link>>