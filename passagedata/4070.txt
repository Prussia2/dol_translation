<<set $outside to 0>><<set $location to "home">><<effects>>

"You sold the console to pay Bailey," you say. Robin nods. "I'm going to buy you a new one."
<br><br>

<<He>> looks at you, mouth agape. "No, it's too expensive," <<he>> says.
<br><br>

"It's my money," you reply. "Come on, you'll have to pick out the games you want."
<br><br>

<<He>> frowns and smiles at the same time, and jumps to <<his>> feet.
<br><br>

You walk to the High Street together. <<His>> pace picks up when the game shop comes into view. Five minutes later <<hes>> holding a large box in <<his>> arms, and you're £400 poorer. <<His>> face beams.

<<He>> runs ahead of you once back on Domus Street, eager to set it up.
<br><br>
<<endevent>>
<<link [[Next|Domus Street]]>><<set $eventskip to 1>><</link>>
<br>