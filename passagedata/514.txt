<<set $outside to 1>><<set $location to "lake">><<effects>><<lakeeffects>>
<<set $lakecouple to 1>>
<<generate1>><<generatep2>><<person1>><<person2>>
You walk into the light of the fire. You see them more closely now, a <<person1>><<person>> and <<person2>><<personstop>> "Hey," the <<person1>><<person>> says. "What are you doing out here alone? Come join us."
<br><br>
<<link [[Join them (0:10)|Lake Couple 2]]>><<pass 10>><</link>>
<br>
<<link [[Decline and go on your way|Lake Campsite]]>><<endevent>><</link>>
<br>