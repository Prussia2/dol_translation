<<set $outside to 0>><<set $location to "home">><<effects>>
You hide inside a cupboard before being spotted. You dare not touch the creaky door, but the darkness of the small room should conceal you. You crouch behind a trolley stacked full with bottles of cleaning solution.
<br><br>
<<if $rng gte 81>>
	Your heart beats faster as the <<person>> stops outside. You reach around for something, anything to cover with. There's nothing but brooms and bottles. <<He>> blinks at you through the gloom. <<He>> can't see you properly, but <<his>> eyes will adjust.
	<br><br>
	<<He>> grasps the trolley. "I'm on cleaning duty," <<he>> says as <<he>> starts to pull away the only thing protecting you.
	<br><br>
	<<set $skulduggerydifficulty to 400>>
	<<link [[Deceive|Orphanage Hide Skul]]>><</link>><<skulduggerydifficulty>>
	<br>
	<<link [[Throw broom|Orphanage Hide Throw]]>><</link>> <span class="blue">(Medium)</span>
	<br>
<<else>>
	The <<person>> walks by without a glance. Heart thumping, you make it back to the safety of your room.
	<br><br>
	<<link [[Next|Bedroom]]>><<endevent>><</link>>
	<br>
<</if>>