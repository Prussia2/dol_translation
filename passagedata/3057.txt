<<effects>>
<<if $stress gte 10000 or $vorestomach gte 5>>
	It's all just too much for you. You pass out.
	<br><br>
	<<if $upperoff isnot 0>>
		<<upperruined>>
	<</if>>
	<<if $loweroff isnot 0>>
		<<lowerruined>>
	<</if>>
	<<if $underloweroff isnot 0>>
		<<underlowerruined>>
	<</if>>
	<<if $underupperoff isnot 0>>
		<<underupperruined>>
	<</if>>
	<<endcombat>>
	<<link [[Next|Passout Beach]]>><<pass 1 hour>><</link>>
<<else>>
	<<tearful>> you haul yourself out of the $vorecreature's maw. It decides against pursuing you further, and disappears back into the depths.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<destinationeventend>>
<</if>>