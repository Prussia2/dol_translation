<<effects>>

You leap aside, avoiding the speeding vehicle.
<br><br>

You tear yourself free from the tangle. Your clothes snag on the thorns.

<<set $worn.upper.integrity -= 10>><<set $worn.lower.integrity -= 10>><<set $worn.under_upper.integrity -= 10>><<set $worn.under_lower.integrity -= 10>>
<<integritycheck>><<exposure>>
<br><br>

<<destinationfarmroad>>