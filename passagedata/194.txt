<<widget "breastsidle">><<nobr>>
<<set _filters to $skinColor.current>>
<<set _img to setup.tanImg.doggy[$skinColor.tanImgEnabled]>>
<<if $worn.upper.exposed gte 2>>
	<<if $breastsize is 0>>
	<<elseif $breastsize is 1 or $breastsize is 2>>
		<img class="layer-sexmouth" @src="_img.breastsTiny" @style="'filter: ' + _filters.body">
	<<elseif $breastsize is 3 or $breastsize is 4>>
		<img class="layer-sexmouth" @src="_img.breastsSmall" @style="'filter: ' + _filters.body">
	<<elseif $breastsize gte 5 and $breastsize lte 7 >>
		<img class="layer-sexmouth" @src="_img.breastsLarge" @style="'filter: ' + _filters.body">
	<<elseif $breastsize gte 8 and $breastsize lte 12>>
		<img class="layer-sexmouth" @src="_img.breastsHuge" @style="'filter: ' + _filters.body">
	<</if>>
<</if>>
<</nobr>><</widget>>

<<widget "breastsactive">><<nobr>>
<<set _filters to $skinColor.current>>
<<set _img to setup.tanImg.doggy[$skinColor.tanImgEnabled]>>
<<if $worn.upper.exposed gte 2>>
	<<if $breastsize is 0>>
	<<elseif $breastsize is 1 or $breastsize is 2>>
		<img @class="'layer-sexmouth anim-doggy-4f-'+_animspeed" @src="_img.breastsTiny" @style="'filter: ' + _filters.body">
	<<elseif $breastsize is 3 or $breastsize is 4>>
		<img @class="'layer-sexmouth anim-doggy-4f-'+_animspeed" @src="_img.breastsSmall" @style="'filter: ' + _filters.body">
	<<elseif $breastsize gte 5 and $breastsize lte 7 >>
		<img @class="'layer-sexmouth anim-doggy-4f-'+_animspeed" @src="_img.breastsLarge" @style="'filter: ' + _filters.body">
	<<elseif $breastsize gte 8 and $breastsize lte 12>>
		<img @class="'layer-sexmouth anim-doggy-4f-'+_animspeed" @src="_img.breastsHuge" @style="'filter: ' + _filters.body">
	<</if>>
<</if>>
<</nobr>><</widget>>

<<widget "breastsidlemissionary">><<nobr>>
<<set _filters to $skinColor.current>>
<<set _img to setup.tanImg.missionary[$skinColor.tanImgEnabled]>>
<<if $breastsize is 0>>
<<elseif $breastsize is 1 or $breastsize is 2>>
	<img class="layer-sexmouth" @src="_img.breastsTiny" @style="'filter: ' + _filters.body">
<<elseif $breastsize is 3 or $breastsize is 4>>
	<img class="layer-sexmouth" @src="_img.breastsSmall" @style="'filter: ' + _filters.body">
<<elseif $breastsize gte 5 and $breastsize lte 7>>
	<img class="layer-sexmouth" @src="_img.breastsLarge" @style="'filter: ' + _filters.body">
<<elseif $breastsize gte 8 and $breastsize lte 12>>
	<img class="layer-sexmouth" @src="_img.breastsHuge" @style="'filter: ' + _filters.body">
<</if>>

<</nobr>><</widget>>

<<widget "breastsactivemissionary">><<nobr>>
<<set _filters to $skinColor.current>>
<<set _img to setup.tanImg.missionary[$skinColor.tanImgEnabled]>>
<<if $breastsize is 0>>
<<elseif $breastsize is 1 or $breastsize is 2>>
	<img @class="'layer-sexmouth anim-doggy-4f-'+_animspeed" @src="_img.breastsTiny" @style="'filter: ' + _filters.body">
<<elseif $breastsize is 3 or $breastsize is 4>>
	<img @class="'layer-sexmouth anim-doggy-4f-'+_animspeed" @src="_img.breastsSmall" @style="'filter: ' + _filters.body">
<<elseif $breastsize gte 5 and $breastsize lte 7>>
	<img @class="'layer-sexmouth anim-doggy-4f-'+_animspeed" @src="_img.breastsLarge" @style="'filter: ' + _filters.body">
<<elseif $breastsize gte 8 and $breastsize lte 12>>
	<img @class="'layer-sexmouth anim-doggy-4f-'+_animspeed" @src="_img.breastsHuge" @style="'filter: ' + _filters.body">
<</if>>
<</nobr>><</widget>>