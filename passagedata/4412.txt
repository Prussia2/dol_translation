<<effects>><<set $location to "sewers">><<set $outside to 0>>
The wide mouth below you opens and you yell in alarm as you are pulled into it. Though you struggle, it makes no difference. First your thighs disappear. Then your waist. Then your stomach. Then your chest. Before you know it, all but your head is swallowed up and you close your eyes in terror, expecting to feel pain. Slime pushes against every part of you and you just know you are done for.
<br><br>
Your end doesn't come. Instead, air begins rushing past your ears as you and the creature move at a rapid pace down the tunnel, into the darkness. You exhale for a moment, cautiously optimistic.
<<if !$worn.upper.type.includes("naked") or !$worn.lower.type.includes("naked") or !$worn.under_lower.type.includes("naked")>>
	Your optimism only lasts a moment, though, as you shout in alarm while your clothing dissolves.
	<br><br>
	It happens so fast, you almost don't realise it. Within the span of a few heartbeats, every stitch of clothing you were wearing simply breaks apart and the goo comes crashing down onto your nude body.
	Every bit of skin feels the slick warm substance pressed against it and you wonder if you will be dissolved next.
	<<upperruined>><<lowerruined>><<underruined>>
<</if>>
<br><br>
Then the slime begins to tease your body.
<br><br>
<<link [[Next|Sewers Slime 3]]>><</link>>
<br>