<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
You stare down at your hands and try to ignore the laughter. Doren looks at the <<person2>><<person>> "You know I don't like giving detention," <<person1>><<he>> says. "But you're making it hard for me to avoid." <<person2>>The <<person>> looks away, smirking.
<br><br>
<<if $dorenhonest isnot 1>>
	<<person1>>Doren looks back at you. "I'd like to speak to you outside. Come on." You follow <<him>> from the classroom, keeping your head bowed and knowing everyone must be staring at you.
	<br><br>
	<<He>> shuts the door behind you and speaks in an uncharacteristically quiet tone. "Is everything alright <<lasscomma>> or is there something you want to tell me about?"
	<br><br>
	<<link [[Be honest|English Events Honest]]>><<npcincr Doren love 1>><<stress 6>><<trauma -6>><</link>><<gstress>><<ltrauma>>
	<br>
	<<link [[Say you're fine|English Events Fine]]>><</link>>
	<br>
<<else>>
	<<endevent>>
	<<link [[Next|English Lesson]]>><</link>>
	<br>
<</if>>