<<effects>>

<<if $tending gte random(1, 1000)>>

	You crouch and offer your arm. <span class="green">The dog stops and sniffs it.</span> It licks your fingers, content with the remaining cream there.
	<br><br>

	The dog's owner manages to pull it away, and you rise to your feet.
	<br><br>

	<<cream_walk>>

<<else>>

	You crouch and offer your arm. <span class="red">The dog leaps right past it,</span> knocking you to your back and covering your <<if $worn.over_upper.name is "cream">><<breasts>><<else>>thighs<</if>> in licks.
	<<garousal>><<arousal 500>>
	<br><br>

	The dog's owner manages to pull it away from you, an apologetic look on <<generate1>><<person1>><<his>> face. You rise to your feet and assess the damage. Much of the cream is gone.
	<<cream_damage 15>>
	<br><br>
	<<endevent>>
	<<cream_walk>>

<</if>>