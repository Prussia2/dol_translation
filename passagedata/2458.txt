<<set $outside to 1>><<set $location to "town">><<effects>>

<<if $submissive lte 850>>

"Don't you dare. Or I'll find you when I get out," you say, glaring at the <<personstop>> <<He>> looks away from you and puts the fruit away.
<br><br>

<<elseif $submissive gte 1150>>

"P-please don't," you say, hanging your head. The <<person>> takes pity on you and puts the fruit away.
<br><br>

<<else>>

"Please don't," you say. <<He>> hesitates a moment. <<His>> friend pokes <<him>> and <<he>> throws the fruit, but it flies clear of the pillory.
<br><br>

<</if>>

<<endevent>>

<<link [[Next|Pillory]]>><<pass 1 hour>><<set $pillorytime -= 1>><</link>>
<br>