<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You feign shock at River's collapse and join in the laughter. <<npc River>><<person1>><<He>> soon comes to and looks around with bleary eyes. <<He>> fixes you with a gaze, but doesn't say anything. <<He>> climbs to <<his>> feet and continues the lesson as if nothing happened.
<br><br>

<<endevent>>

<<link [[Next|Maths Lesson Daydream]]>><</link>>
<br>