<<set $outside to 1>><<set $location to "town">><<effects>>

You search the <<persons>> <<walletstop>> There's

<<if $rng gte 81>>
£25 inside.
<<set $money += 2500>><<crimeup 25>>
<<elseif $rng gte 61>>
£20 inside.
<<set $money += 2000>><<crimeup 20>>
<<elseif $rng gte 41>>
£15 inside.
<<set $money += 1500>><<crimeup 15>>
<<elseif $rng gte 21>>
£10 inside.
<<set $money += 1000>><<crimeup 10>>
<<else>>
£5 inside.
<<set $money += 500>><<crimeup 5>>
<</if>>
<br><br>

<<endevent>>
<<destinationeventend>>