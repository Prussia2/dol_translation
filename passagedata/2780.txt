<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $schoolstate isnot "early" and $schoolstate isnot "late" and $schoolstate isnot "earlynoschool" and $schoolstate isnot "latenoschool" and $schoolstate isnot "daynoschool">>
	<<if $schoolstate is "morning">>
	You are in the school hallways. Students and faculty already move through the corridors, although school has not officially begun.
	<br><br>
	<<elseif $schoolstate is "afternoon">>
	You are in the school hallways. Some students and faculty remain despite school finishing, as the facilities stay open for a couple of hours.
	<br><br>
	<<elseif $schoolstate is "lunch">>
	You are in the school hallways. It is crowded with students on their lunch break.
	<br><br>
	<<else>>
	You are in the school hallways. You hear speaking behind the various classroom doors.
	<br><br>
	<</if>>
	<<if $exposed gte 1>>
	You dart between storerooms, hide beneath stairwells and avoid popular areas to keep your <<lewdness>> hidden.
	<br><br>
	<</if>>
<<else>>
The school hallways are empty.
<br><br>
<</if>>

<<schoolperiodtext>>
<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $whitneypantiesmaths>>
	<<if $whitneypantiesmaths is "seen">>
		You've barely stepped into the hallway when <<npc Whitney>><<person1>>Whitney and <<his>> friends surround you. "You've been a bad <<girl>>," <<he>> grins. <<His>> friends look excited. "I said no <<if $player.gender_appearance is "f">>panties<<else>>underwear<</if>>, and I meant it. I'll have to take everything now."
		<br><br>

		<<link [[Let it happen|Bully Panties]]>><<trauma 6>><<stress 3>><<set $submissive += 1>><<npcincr Whitney dom 1>><</link>><<gtrauma>><<gstress>><<garousal>><<gdom>>
		<br>
		<<link [[Fight|Bully Panties Fight]]>><<set $fightstart to 1>><<set $submissive -= 1>><<npcincr Whitney dom -1>><</link>><<ldom>>
		<br>
	<<else>>
		You've barely stepped into the hallway when <<npc Whitney>><<person1>>Whitney and <<his>> friends surround you. "I think you have something to hide," <<he>> says, smiling as <<he>> feels beneath your $worn.lower.name.
		<br><br>

		<<if $worn.under_lower.type.includes("naked")>>
			"Hmm," <<he>> continues. "I guess you're a good <<girl>> after all. You should cooperate next time."
			<br><br>

			<<He>> and <<his>> friends saunter away.
			<br><br>

			<<endevent>>
			<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
			<br>
		<<else>>
			"Bad <<girlcomma>>" Whitney chides, smiling. "I'll have to take everything now."
			<br><br>

			<<link [[Let it happen|Bully Panties]]>><<trauma 6>><<stress 3>><<set $submissive += 1>><<npcincr Whitney dom 1>><</link>><<gtrauma>><<gstress>><<garousal>><<gdom>>
			<br>
			<<link [[Fight|Bully Panties Fight]]>><<set $fightstart to 1>><<set $submissive -= 1>><<npcincr Whitney dom -1>><</link>><<ldom>>
			<br>
		<</if>>
	<</if>>
	<<unset $whitneypantiesmaths>>
<<elseif $stress gte 10000>>
<<link [[Everything fades to black...|School Passout]]>><</link>>

<<elseif $schoolstate isnot "early" and $schoolstate isnot "late" and $schoolstate isnot "earlynoschool" and $schoolstate isnot "latenoschool" and $schoolstate isnot "daynoschool" and $danger gte (9900 - $allure) and $eventskip is 0>>
	<<if $exposed gte 1>>
	<<eventschoolhallwaysexposed>>
	<<else>>
	<<eventsschoolhallways>>
	<</if>>
<<else>>

<<schoolperiod>>

	<<if $schoolstate isnot "early" and $schoolstate isnot "late" and $schoolstate isnot "earlynoschool" and $schoolstate isnot "latenoschool" and $schoolstate isnot "daynoschool" and $exposed gte 1>>
	<<link [[Sneak to rear playground (0:05)|School Rear Playground]]>><<pass 5>><</link>>
	<br>
	<<swimmingicon>><<link [[Sneak to pool (0:03)|School Pool Entrance]]>><<pass 3>><</link>>
	<br>
	<<libraryicon>><<link [[Sneak to library (0:05)|School Library]]>><<pass 5>><</link>>
	<br>
	<<toileticon>><<link [[Sneak to toilets (0:05)|School Toilets]]>><<pass 5>><</link>>
	<br>

	<<else>>

		<<if $schoolstate is "afternoon" and $detention gte 1 and $detentionattended isnot 1 and $pillory_tenant.special.name isnot "Leighton">>
		<<link [[Go to detention|School Detention]]>><</link>>
		<br>
			<<if $headdrive gte 1 and $headnodetention isnot 1 and $headmoney isnot 1 and $headphotoshoot isnot 1>>
			<<link [[Blackmail Leighton|Head's Office Blackmail]]>><<npc Leighton>><<person1>><<set $phase to 1>><</link>><<crime>>
			<br>
			<</if>>
		<br>
		<</if>>

	<<lockericon>><<link [[Lockers|School Lockers]]>><</link>>
	<br><br>
Outside
<br>
	<<link [[Front playground (0:02)|School Front Playground]]>><<pass 2>><</link>>
	<br>
	<<link [[Rear playground (0:02)|School Rear Playground]]>><<pass 2>><</link>>
	<br><br>
Facilities
<br>
	<<eaticon>><<link [[Canteen (0:01)|Canteen]]>><<pass 1>><</link>>
	<br>
	<<swimmingicon>><<link [[Pool (0:01)|School Pool Entrance]]>><<pass 1>><</link>>
	<br>
	<<libraryicon>><<link [[Library (0:02)|School Library]]>><<pass 2>><</link>>
	<br>
	<<if $schoolstate is "afternoon" and $detention gte 1 and $detentionattended isnot 1>>
	<<else>>
	<<ind>><<link [[Head's Office (0:02)|Head's Office]]>><<pass 2>><</link>>
	<br>
	<</if>>
	<<toileticon>><<link [[Toilets (0:02)|School Toilets]]>><<pass 2>><</link>>
	<br><br>
Classrooms
<br>
	<<scienceicon>><<link [[Science classroom (0:01)|Science Classroom]]>><<pass 1>><</link>>
	<br>
	<<mathicon>><<link [[Maths classroom (0:01)|Maths Classroom]]>><<pass 1>><</link>>
	<br>
	<<englishicon>><<link [[English classroom (0:01)|English Classroom]]>><<pass 1>><</link>>
	<br>
	<<historyicon>><<link [[History classroom (0:01)|History Classroom]]>><<pass 1>><</link>>
	<br>
	<</if>>

<</if>>

<<set $eventskip to 0>>