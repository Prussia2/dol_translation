<<effects>>
<<pass 5>>

You look over shoulder, and see Alex up ahead. There's a <<person2>><<person>> beside <<person1>><<him>>.
<br><br>

Alex spots you first. <<person1>><<His>> face pales. The <<person2>><<person>> beside <<person1>><<him>> has a different reaction. "I didn't know you ran this sort of farm," <<person2>><<he>> says, pulling a phone from <<his>> pocket. <<He>> points the camera at you.
<br><br>

Alex breaks free from <<person1>><<his>> stupour, and looks away. "W-We aren't-"
<br>
"You've got my business," the <<person2>><<person>> interrupts as you and the <<farm_text dog>> arrive. <<He>> crouches beside you, and moves the camera close to the knot. "I've a few friends who'd love to hear about this."
<br><br>
The <<person2>><<person>> insists on filming until the knot deflates. Stuck on all fours, there' nothing you can do but endure.<<if $phase is 5>>
	<<ggtrauma>><<ggstress>><<lllove>><<gggdom>><<gglust>><<gggfarm>>
<</if>>
<br><br>

<<link [[Next|Farm Knotted Seen 2]]>><</link>>
<br>