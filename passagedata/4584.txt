<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	The pair saunter off, leaving you lying on the mesh floor.
	<br><br>
	<<tearful>> you stagger down the stairs, and duck into the relative safety of a nearby alley.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Industrial alleyways]]>><</link>>
	<br>

<<elseif $enemyhealth lte 0>>
	You shove the <<person1>><<person>> against the barrier. <<He>> almost tumbles off the edge, and requires <<his>> friend's help to regain <<his>> footing. By then, you're gone.
	<br><br>
	<<tearful>> you dart into the relative safety of a nearby alley..
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Industrial alleyways]]>><</link>>

<<else>>
	<<set $rescued += 1>>
	<<generate3>><<person3>>A car pulls up several metres from the staircase. A <<person>> climbs out, and dials a number into <<his>> phone.
	<br><br>
	The <<person1>><<person>> nudges <<his>> colleague, and the two stare at the newcomer. They share a glance, then break into a run.
	<br><br>
	<<tearful>> you stumble down the stairs and dart into the relative safety of a nearby alley. You round another corner and lean against a wall, your heart thundering in your chest.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Industrial alleyways]]>><</link>>

<</if>>