<<effects>>

"The trial of purity is a trial by fire," Jordan continues. "It's not dangerous, despite the name. It's an exercise of will. Let me know when you're ready, and I'll make preparations."
<<set $temple_rank to "prospective">>
<br><br>
<<endevent>>
<<link [[Next|Temple]]>><</link>>
<br>