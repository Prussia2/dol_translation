<<set $outside to 0>><<set $location to "pool">><<schooleffects>><<effects>>
<<if $phase is 1>>The <<person>>
	<<if $rng gte 80>>
		dives elegantly
	<<elseif $rng gte 60>>
		bellyflops painfully
	<<elseif $rng gte 40>>
		dives competently
	<<elseif $rng gte 20>>
		jumps quickly
	<<else>>
		splashes clumsily
	<</if>>
	into the water.
	<<if $rng gte 60>>
		A number of people <<if $rng lt 80>>sarcastically<</if>>
		applaud.
	<</if>>
	<<if $rng % 4 is 0>>
		Unfortunately, <<he>> lost <<his>> swimwear during the dive. A couple of students start to harass <<himstop>>
	<</if>>
	<br><br>

	You continue with your swim.

	<br><br>
	<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
	<br>
<<elseif $phase is 2>>/*secret dive*/
	Mason starts to demonstrate a swimming technique by the poolside.
	<<if $awareness gte 200 or ($rng % 3 is 0)>>
		<<if $awareness gte 300 or $skulduggery gte 400>>
			Detecting that Mason is trying to distract you from something,
		<<else>>
			<<if $rng % 2>>Hearing a noise behind you,<<else>>Noticing Mason's glance behind you,<</if>>
		<</if>>you turn around to see the <<person>> climbing naked onto the diving board.
		<br><br>
		<<link [[Look Away|Events Swimming NPC Nude Dive]]>><<set $phase to 1>><</link>>
		<br>
		<<link [[Watch|Events Swimming NPC Nude Dive]]>><<set $phase to 2>><</link>><<garousal>>
		<br>
		<<link [[Attract Attention|Events Swimming NPC Nude Dive]]>><<set $phase to 3>><</link>><<gcool>>
		<br>
	<<else>>
		It's interesting. A moment later, you hear a splash behind you.
		<br>
		It seems the <<person>> dived while no one was looking. How odd.
		<br><br>

		You continue with your swim.

		<br><br>
		<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
		<br>
	<</if>>
<<elseif $phase is 3>>The <<person>> climbs naked onto the diving board, and then
	<<if $rng gte 80>>
		dives elegantly
	<<elseif $rng gte 60>>
		bellyflops painfully
	<<elseif $rng gte 40>>
		dives competently
	<<elseif $rng gte 20>>
		jumps quickly
	<<else>>
		splashes clumsily
	<</if>>
	into the water. <<if $rng gte 60>>A number of people applaud.<</if>>
	<<if $exhibitionism gte 35>>
		<br><br>
		Imagining yourself exposed like that excites you.
		<<garousal>><<arousal 3>>
	<</if>>
	<br><br>
	<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
	<br>

<<else>>
Gravity briefly inverts sending <<him>> flailing into the roof before dropping <<him>> haplessly into the pool. (This is an error, please report)/*error*/

<br><br>
<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
<br>
<</if>>