<<effects>>
<<npc Mason>><<person1>>
You swim into the middle of the lake. There Mason swims in a wide circle. <<He>> notices you approach, and faces you, treading water.
<br><br>

"It's nice to see you out here," <<he>> says. "If it's okay though, I'd like to continue my laps." <<He>> swims away before you can get close.
<br><br>

<<link [[Try to catch them (0:05)|Lake Mason Swim]]>><<tiredness 1>><</link>><<swimmingdifficulty 1 300>><<gtiredness>>
<br>
<<link [[Leave|Lake Depths]]>><<endevent>><</link>>
<br>