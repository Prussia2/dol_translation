<<widget "voreeffects">><<nobr>>
<<set $rng to random(1, 100)>>
<<effectspain>>
<<effectsorgasm>>
<<effectsdissociation>>
<<if $leftaction is "leftescape" and $rightaction is "rightescape">>
	<<set $leftaction to 0>><<set $rightaction to 0>><<set $leftactiondefault to "leftescape">><<set $rightactiondefault to "rightescape">><<set $attackstat += 2>><<set $leftactiondefault to "leftescape">><<set $rightactiondefault to "rightescape">>
	<<if $rng gte 40>>
		<<if $leftarm is "trapped">><<set $leftarm to 0>><</if>><<if $rightarm is "trapped">><<set $rightarm to 0>><</if>>
		<<if $vorestage is 1>>
			<<set $vorestage -= 1>>
			You hit the $vorecreature's maw with both arms, <span class="green">and make it spit you out.</span>
		<<elseif $vorestage is 2>>
			<<set $vorestage -= 1>>
			You hit the $vorecreature's mouth with both arms. <span class="green">It gags, letting you slide your body out, freeing your <<genitals>> from its maw.</span>
		<<elseif $vorestage is 3>>
			<<set $vorestage -= 1>>
			You hit the $vorecreature's mouth with both arms. <span class="green">It gags, letting you slide your body out, freeing your <<breasts>> from its maw.</span>
		<<elseif $vorestage is 4>>
			<<set $vorestage -= 1>>
			You hit the inside of the $vorecreature's mouth with both arms. <span class="green">It gags, letting you slide your body out, freeing your arms from its maw.</span>
		<<elseif $vorestage is 5>>
			<<set $vorestage -= 1>>
			You hit the inside of the $vorecreature's mouth with both arms. <span class="green">It gags, letting you slide your head back out.</span>
		<<elseif $vorestage is 6>>
			<<set $vorestage -= 1>>
			You hit the walls of the $vorecreature's gullet with both arms. <span class="green">It convulses, violently pushing you up into its mouth.</span>
		<<elseif $vorestage is 7>>
			<<set $vorestage -= 1>>
			You pound the walls of the $vorecreature's stomach with both arms. <span class="green">It convulses, violently pushing you up into its gullet.</span>
		<</if>>
	<<else>>
		<<if $vorestage is 1>>
			You hit the $vorecreature's maw with both arms, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 2>>
			You hit the $vorecreature's mouth with both arms, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 3>>
			You hit the $vorecreature's mouth with both arms, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 4>>
			You hit the inside of the $vorecreature's mouth with both arms, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 5>>
			You hit the inside of the $vorecreature's mouth with both arms, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 6>>
			You hit the walls of the $vorecreature's gullet with both arms, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 7>>
			You pound the walls of the $vorecreature's stomach with both arms, <span class="red">but it doesn't react.</span>
		<</if>>
	<</if>>
<<elseif $leftaction is "leftescape">>
	<<set $leftaction to 0>><<set $leftactiondefault to "leftescape">><<set $attackstat += 1>><<set $leftactiondefault to "leftescape">>
	<<if $rng gte 20>>
		<<if $leftarm is "trapped">><<set $leftarm to 0>><</if>><<if $rightarm is "trapped">><<set $rightarm to 0>><</if>>
		<<if $vorestage is 1>>
			<<set $vorestage -= 1>>
			You hit the $vorecreature's maw with your left arm, <span class="green">and make it spit you out.</span>
		<<elseif $vorestage is 2>>
			<<set $vorestage -= 1>>
			You hit the $vorecreature's mouth with your left arm. <span class="green">It gags, letting you slide your body out, freeing your <<genitals>> from its maw.</span>
		<<elseif $vorestage is 3>>
			<<set $vorestage -= 1>>
			You hit the $vorecreature's mouth with your left arm. <span class="green">It gags, letting you slide your body out, freeing your <<breasts>> from its maw.</span>
		<<elseif $vorestage is 4>>
			<<set $vorestage -= 1>>
			You hit the inside of the $vorecreature's mouth with your left arm. <span class="green">It gags, letting you slide your body out, freeing your arms from its maw.</span>
		<<elseif $vorestage is 5>>
			<<set $vorestage -= 1>>
			You hit the inside of the $vorecreature's mouth with your left arm. <span class="green">It gags, letting you slide your head back out.</span>
		<<elseif $vorestage is 6>>
			<<set $vorestage -= 1>>
			You hit the walls of the $vorecreature's gullet with your left arm. <span class="green">It convulses, violently pushing you up into its mouth.</span>
		<<elseif $vorestage is 7>>
			<<set $vorestage -= 1>>
			You pound the walls of the $vorecreature's stomach with your left arm. <span class="green">It convulses, violently pushing you up into its gullet.</span>
		<</if>>
	<<else>>
		<<if $vorestage is 1>>
			You hit the $vorecreature's maw with your left arm, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 2>>
			You hit the $vorecreature's mouth with your left arm, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 3>>
			You hit the $vorecreature's mouth with your left arm, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 4>>
			You hit the inside of the $vorecreature's mouth with your left arm, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 5>>
			You hit the inside of the $vorecreature's mouth with your left arm, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 6>>
			You hit the walls of the $vorecreature's gullet with your left arm, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 7>>
			You pound the walls of the $vorecreature's stomach with your left arm, <span class="red">but it doesn't react.</span>
		<</if>>
	<</if>>
<<elseif $rightaction is "rightescape">>
	<<set $rightaction to 0>><<set $rightactiondefault to "rightescape">><<set $attackstat += 1>><<set $rightactiondefault to "rightescape">>
	<<if $rng gte 20>>
		<<if $leftarm is "trapped">><<set $leftarm to 0>><</if>><<if $rightarm is "trapped">><<set $rightarm to 0>><</if>>
		<<if $vorestage is 1>>
			<<set $vorestage -= 1>>
			You hit the $vorecreature's maw with your right arm, <span class="green">and make it spit you out.</span>
		<<elseif $vorestage is 2>>
			<<set $vorestage -= 1>>
			You hit the $vorecreature's mouth with your right arm. <span class="green">It gags, letting you slide your body out, freeing your <<genitals>> from its maw.</span>
		<<elseif $vorestage is 3>>
			<<set $vorestage -= 1>>
			You hit the $vorecreature's mouth with your right arm. <span class="green">It gags, letting you slide your body out, freeing your <<breasts>> from its maw.</span>
		<<elseif $vorestage is 4>>
			<<set $vorestage -= 1>>
			You hit the inside of the $vorecreature's mouth with your right arm. <span class="green">It gags, letting you slide your body out, freeing your arms from its maw.</span>
		<<elseif $vorestage is 5>>
			<<set $vorestage -= 1>>
			You hit the inside of the $vorecreature's mouth with your right arm. <span class="green">It gags, letting you slide your head back out.</span>
		<<elseif $vorestage is 6>>
			<<set $vorestage -= 1>>
			You hit the walls of the $vorecreature's gullet with your right arm. <span class="green">It convulses, violently pushing you up into its mouth.</span>
		<<elseif $vorestage is 7>>
			<<set $vorestage -= 1>>
			You pound the walls of the $vorecreature's stomach with your right arm. <span class="green">It convulses, violently pushing you up into its gullet.</span>
		<</if>>
	<<else>>
		<<if $vorestage is 1>>
			You hit the $vorecreature's maw with your right arm, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 2>>
			You hit the $vorecreature's mouth with your right arm, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 3>>
			You hit the $vorecreature's mouth with your right arm, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 4>>
			You hit the inside of the $vorecreature's mouth with your right arm, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 5>>
			You hit the inside of the $vorecreature's mouth with your right arm, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 6>>
			You hit the walls of the $vorecreature's gullet with your right arm, <span class="red">but it doesn't react.</span>
		<<elseif $vorestage is 7>>
			You pound the walls of the $vorecreature's stomach with your right arm, <span class="red">but it doesn't react.</span>
		<</if>>
	<</if>>
<</if>>
<<if $leftaction is "lefthold" and $rightaction is "righthold">>
	<<set $leftaction to 0>><<set $rightaction to 0>><<set $leftactiondefault to "lefthold">><<set $rightactiondefault to "righthold">><<set $vorestruggle to 2>>
	<<if $vorestage is 1>>
		You grab hold of the $vorecreature's maw with both arms.
	<<elseif $vorestage is 2>>
		You grab hold of the $vorecreature's maw with both arms.
	<<elseif $vorestage is 3>>
		You grab hold of the $vorecreature's maw with both arms.
	<<elseif $vorestage is 4>>
		You cling to the side of the $vorecreature's mouth with both arms.
	<<elseif $vorestage is 5>>
		You cling to the side of the $vorecreature's mouth with both arms.
	<<elseif $vorestage is 6>>
		You cling to the side of the $vorecreature's gullet with both arms.
	<</if>>
<<elseif $leftaction is "lefthold">>
	<<set $leftaction to 0>><<set $leftactiondefault to "lefthold">><<set $vorestruggle to 1>>
	<<if $vorestage is 1>>
		You grab hold of the $vorecreature's maw with your left arm.
	<<elseif $vorestage is 2>>
		You grab hold of the $vorecreature's maw with your left arm.
	<<elseif $vorestage is 3>>
		You grab hold of the $vorecreature's maw with your left arm.
	<<elseif $vorestage is 4>>
		You cling to the side of the $vorecreature's mouth with your left arm.
	<<elseif $vorestage is 5>>
		You cling to the side of the $vorecreature's mouth with your left arm.
	<<elseif $vorestage is 6>>
		You cling to the side of the $vorecreature's gullet with your left arm.
	<</if>>
<<elseif $rightaction is "righthold">>
	<<set $rightaction to 0>><<set $rightactiondefault to "righthold">><<set $vorestruggle to 1>>
	<<if $vorestage is 1>>
		You grab hold of the $vorecreature's maw with your right arm.
	<<elseif $vorestage is 2>>
		You grab hold of the $vorecreature's maw with your right arm.
	<<elseif $vorestage is 3>>
		You grab hold of the $vorecreature's maw with your right arm.
	<<elseif $vorestage is 4>>
		You cling to the side of the $vorecreature's mouth with your right arm.
	<<elseif $vorestage is 5>>
		You cling to the side of the $vorecreature's mouth with your right arm.
	<<elseif $vorestage is 6>>
		You cling to the side of the $vorecreature's gullet with your right arm.
	<</if>>
<</if>>
<<if $leftaction is "leftvorefree">>
	<<set $leftaction to 0>>
	<<set $rightarm to 0>>
	<span class="lblue">Using all your strength, you manage to free your right arm from the side of the gullet.</span>
<</if>>
<<if $rightaction is "rightvorefree">>
	<<set $rightaction to 0>>
	<<set $leftarm to 0>>
	<span class="lblue">Using all your strength, you manage to free your left arm from the side of the gullet.</span>
<</if>>
<<if $leftaction is "vorerest">>
	<<set $leftaction to 0>><<set $leftactiondefault to "vorerest">>
<</if>>
<<if $rightaction is "vorerest">>
	<<set $rightaction to 0>><<set $rightactiondefault to "vorerest">>
<</if>>
<br><br>
<</nobr>><</widget>>