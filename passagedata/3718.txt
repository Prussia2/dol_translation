<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	The <<monk>> looks mortified. "What have I done?" <<he>> says. "You've sullied me! Begone!" You walk down the spiral stairs to the bottom of the tower.
	<br><br>
	<<clotheson>>
	<<endcombat>>
<<elseif $enemyhealth lte 0>>
	The <<monk>> recoils in pain and you squirm from <<his>> grip. You escape down the spiral stairs.
	<br><br>
	<<clotheson>>
	<<endcombat>>
<<else>>
	The <<monk>> releases you. "I don't want to catch you again," <<he>> says. "Or I'll punish you like they do in the scriptures." You leave down the spiral stairs.
	<br><br>
	<<clotheson>>
	<<endcombat>>
<</if>>
<<tearful>> you make sure you still have the lichen. It's safe.
<br><br>
<span class="gold">You can add the lichen you found to your project in your room or the school library.</span>
<br><br>
<<link [[Next|Temple]]>><</link>>
<br>