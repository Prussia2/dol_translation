<<effects>>

Alex takes <<his>> <<if $pronoun is "m">>father<<else>>mother<</if>> on a tour of the farm, and insists you come along. The older <<if $pronoun is "m">>man<<else>>woman<</if>> stops to examine each animal, and seems pleased with what <<he>> sees.
<br><br>
Alex beams with pride when you reach the fields. The first three are filled with rows of green potato plants. The last is barren, but clear and ready for planting.
<br><br>

You return to the farmhouse. "You've done a good job," Alex's <<if $pronoun is "m">>father<<else>>mother<</if>> says. "Keep it up. Sorry I can't get out here more often. You know how it is." <<He>> reaches into <<his>> pocket. "Left my keys in the kitchen. Could you grab them?"
<br><br>
Alex enters the farmhouse.
<br><br>

<<link [[Next|Farm Stage 6 7]]>><</link>>
<br>