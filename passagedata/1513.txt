<<set $outside to 0>><<set $location to "brothel">><<effects>>
/* Police raid the place */
The <<person>> rushes out.
<<clotheson>>
<<endcombat>>

You peek through the door. The corridor outside is still quiet, but not far away you hear shouting, crashes and chaos.
<br><br>
The police will soon be here.
<br><br>
<<if $skulduggery gte 200>>
	<<set $skulduggerydifficulty to 800>><<link [[Attempt Escape|Brothel Raid Escape]]>><</link>><<skulduggerydifficulty>>
	<br>
<<else>>
	<span class="blue">You can't think of a way to escape.</span>
	<br>
<</if>>
<<link [[Wait to be Arrested|Brothel Raid Pre Arrest]]>><</link>>
<br>