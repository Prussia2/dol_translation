<<effects>>

<<if $robinromance is 1 and $NPCName[$NPCNameList.indexOf("Robin")].dom gte 40>>
	You sit beside Robin. <<He>> rests <<his>> hand on yours, and gives it a little squeeze.
<<else>>
	You sit beside Robin, and together watch as bees float between the glistening petals.
<</if>>
<br><br>

A few minutes pass, until an argument breaks out inside the orphanage. Robin looks over. "I should investigate," <<he>> says, rising to <<his>> feet. "Thanks for letting me help."
<br><br>

<<link [[Next|Garden Flowers]]>><<endevent>><</link>>
<br>