You shove the officer away and clamber to- <span class="red">Your muscles freeze as you are hit by another jolt.</span>
<br><br>
"Suspect is behaving suspiciously and resists legal search," the <<person2>><<person>> notes loudly. "You're under arrest!"
<br>
"Oh my god!" the <<person1>><<person>> shouts. "<<if $player.gender_appearance is "m">>He<<else>>She<</if>>'s got a concealed weapon!"
<br>
You feel a blinding impact on the back of your neck.
<br><br>
Everything goes dark...
<br><br>
You wake up some time later handcuffed in a holding cell.
<br><br>
<<link [[Wait|Police Cell]]>><<pass 1 hour>><<set $leftarm to "bound">><<set $rightarm to "bound">><<endevent>><</link>>
<br>