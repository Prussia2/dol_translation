<<effects>>

<<set $thighgoo += 1>>
<<set $feetgoo += 1>>

<<if $athletics gte random(1, 1000)>>

	You jump, <span class="green">and your hands gain purchase on the lip of the plant.</span> You haul yourself out, dropping to the vine-covered ground and escaping through the trees before the dust can influence you once more.<<lstress>><<stress -6>>
	<br><br>
	
	<<link [[Next|Forest]]>><<set $eventskip to 1>><</link>>
	<br>

<<else>>

	You jump for the lip of the plant, <span class="red">but your fingers slide off</span>. You try a few more times, until you feel something snake up and around your leg. It tugs, and you fall back into the slime.
	<br><br>
	
	<<link [[Next|Forest Pitcher Resist]]>><</link>>
	<br>

<</if>>