<<bedclotheson>>
<<set $outside to 0>><<set $location to "home">><<home_effects>><<effects>>
You are in your bedroom.
<br><br>
<<if $robinbed is 1>>
	<<set $robinbed to 0>>
	<<npc Robin>><<person1>>Robin yawns and heads to <<his>> own room.<<endevent>>
	<br><br>
<</if>>
<<if $unbind is 1>>
	<<set $unbind to 0>>
	You rub your bindings against your desk. It takes some effort, but eventually the material yields, freeing your limbs.
	<br><br>
<</if>>
<<if $stress gte 10000>>
	<<passouthome>>
<<else>>
	<<if $leftarm is "bound" or $rightarm is "bound" or $feetuse is "bound">>
		<<link [[Undo your bindings (0:10)|Bedroom]]>><<set $unbind to 1>><<pass 10>><<unbind>><</link>>
		<br><br>
	<</if>>
	<<if $robinpaid is 1 and $robinnote isnot 1 and $NPCName[$NPCNameList.indexOf("Robin")].lust gte 10 and $NPCName[$NPCNameList.indexOf("Robin")].love gte 60 and $NPCName[$NPCNameList.indexOf("Robin")].trauma lt 10>>
		<span class="gold">There's a note by the window.</span>
		<br>
		<<link [[Read it|Robin Note]]>><<set $robinnote to 1>><</link>>
		<br><br>
	<</if>>
	<<projectoptions>>
	<<roomoptions>>
	<<if $debug is 1>>
		/*indev radio testing*/
		A radio is on:
		<br>
		<<radio_placehere>>
		<span class="green">More debugging:</span>
		<br>
		<<link [[Add test news|Bedroom]]>><<radio_addnews "meteor_fall" "2">><</link>> radio_addnews "meteor_fall" "2"
		<br>
		<<link [[Add test news|Bedroom]]>><<radio_addnews "godzilla_awaken" "3">><</link>> radio_addnews "godzilla_awaken" "3"
		<br>
		<<link [[Hard reset news|Bedroom]]>><<radio_hardreset>><</link>> radio_hardreset
		<br>
		<<link [[Midnight Importance reduction|Bedroom]]>><<radio_midnight>><</link>> radio_midnight
		<br>
		<<link [[Skip to 23:58|Bedroom]]>><<set $time to 1438>><</link>>
		<br>
		<<link [[Test new features for DoL (0:15)|Bedroom]]>><<pass 15>><</link>>
		<br>
		<span class="green">Debugging end here</span>
		<br><br>
	<</if>>
	Your bed takes up most of the room.
	<<if $slimeEvent is undefined>>
		<br>
		<<link [[Strip and get in bed|Bed]]>><<undress "bed">><</link>>
		<br>
		<<listsleepoutfits>>
	<<else>>
		<br>
		<span class="red">The slime wants you to <<print $slimeEvent>> before you can go to bed.</span>
		<br>
	<</if>>
	<br>
	<<if $arousal gte 2000 and !$worn.genitals.type.includes("chastity") and $homemasturbationtest is 1>>
		<<link [[Masturbate (0:05)|Home Masturbation]]>><<pass 5>><<set $masturbationstart to 1>><</link>>
	<</if>>
	<br>
	Your clothes are kept in the corner.
	<br>
	[[Wardrobe]]
	<br>
	<<if $hour is 3 and $hallucinations gte 2 and $mirrortentacles isnot 1 and $tentacledisable is "f">>
		<br>
		<span class="purple">An eerie light spills from your mirror.</span>
		<br>
		<<link [[Mirror|Eerie Mirror]]>><<set $mirrortentacles to 1>><<set $phase to 0>><</link>>
		<br>
	<<else>>
		[[Mirror]]
		<br>
	<</if>>
	<<if $sexStats.pills.boughtOnce is true>>
		[[Pill Collection|PillCollection]]
		<br>
	<</if>>
	<<if $sexStats.anus.pregnancy.seenDoctor gte 2>>
		<<set $container.lastLocation to $location>>
		<<if $container[$location].kylarFed is true>>
			<<print '<<link [[Check ' + $container[$location].name + ' (0:05)|Kylar Parasites Feed]]>><<pass 5>><</link>>'>>
		<<else>>
			<<print '<<link [[Check ' + $container[$location].name + ' (0:05)|Containers]]>><<pass 5>><</link>>'>>
		<</if>>
		<br>
	<</if>>
	<br>
	The hallway outside connects to the rest of the orphanage.
	<br>
	<<if $exposed gte 2>>
		<<if $daystate isnot "night">>
			<<if $exhibitionism gte 75>>
				The thought of going out with your <<genitals>> on display excites you. You'll be safe if you're careful, right?
				<br><br>
				<<link [[Bathroom (0:01)->Bathroom]]>><<pass 1>><</link>>
				<br>
				<<home_outside>>
				<br><br>
			<<elseif $exhibitionism gte 55>>
				The thought of going out with your <<genitals>> on display excites you. But there's no way you could get away with it at this hour. Maybe after dark.
				<br><br>
			<<else>>
				There's no way you can go out dressed like this!
				<br><br>
			<</if>>
		<<else>>
			<<if $exhibitionism gte 55>>
				The thought of going out with your <<genitals>> on display excites you. It is night after all, what could go wrong?
				<br><br>
				<<link [[Bathroom (0:01)->Bathroom]]>><<pass 1>><</link>>
				<br>
				<<if $NPCName[$NPCNameList.indexOf("Robin")].init is 1 and $exposed lte 0>>
					<<link [[Robin's room (0:01)|Robin's Room Entrance]]>><<pass 1>><</link>>
					<br>
				<</if>>
				<<link [[Main hall (0:01)->Orphanage]]>><<pass 1>><</link>>
				<br>
				<<home_outside>>
				<br><br>
			<<else>>
				There's no way you can go out dressed like this!
				<br><br>
			<</if>>
		<</if>>
	<<elseif $exposed gte 1>>
		<<if $daystate isnot "night">>
			<<if $exhibitionism gte 35>>
				The thought of going out in such a lewd state of dress excites you. You'll be safe if you're careful, right?
				<br><br>
				<<link [[Bathroom (0:01)->Bathroom]]>><<pass 1>><</link>>
				<br>
				<<home_outside>>
				<br><br>
			<<elseif $exhibitionism gte 15>>
				The thought of going out in such a lewd state of dress excites you. But there's no way you could get away with it at this hour. Maybe after dark.
				<br><br>
			<<else>>
				There's no way you can go out dressed like this!
				<br><br>
			<</if>>
		<<else>>
			<<if $exhibitionism gte 15>>
				The thought of going out in such a lewd state of dress excites you. It is night after all, what could go wrong?
				<br><br>
				<<link [[Bathroom (0:01)->Bathroom]]>><<pass 1>><</link>>
				<br>
				<<if $NPCName[$NPCNameList.indexOf("Robin")].init is 1 and $exposed lte 0>>
					<<link [[Robin's room (0:01)|Robin's Room Entrance]]>><<pass 1>><</link>>
					<br>
				<</if>>
				<<link [[Main hall (0:01)->Orphanage]]>><<pass 1>><</link>>
				<br>
				<<home_outside>>
				<br><br>
			<<else>>
				There's no way you can go out dressed like this!
				<br><br>
			<</if>>
		<</if>>
	<<elseif $worn.under_lower.type.includes("naked")>>
		<<if $underwarningskip is 1>>
			<<link [[Bathroom (0:01)->Bathroom]]>><<pass 1>><</link>>
			<br>
			<<if $NPCName[$NPCNameList.indexOf("Robin")].init is 1 and $exposed lte 0>>
				<<link [[Robin's room (0:01)|Robin's Room Entrance]]>><<pass 1>><</link>>
				<br>
			<</if>>
			<<link [[Main hall (0:01)->Orphanage]]>><<pass 1>><</link>>
			<br>
			<<home_outside>>
			<br><br>
		<<else>>
			You are not wearing underwear! The thought of going out like this quickens your pulse.
			<br><br>
			<<link [[It won't hurt|Bedroom]]>><<set $underwarningskip to 1>><</link>>
			<br><br>
		<</if>>
	<<else>>
		<<link [[Bathroom (0:01)->Bathroom]]>><<pass 1>><</link>>
		<br>
		<<if $NPCName[$NPCNameList.indexOf("Robin")].init is 1 and $exposed lte 0>>
			<<link [[Robin's room (0:01)|Robin's Room Entrance]]>><<pass 1>><</link>>
			<br>
		<</if>>
		<<link [[Main hall (0:01)->Orphanage]]>><<pass 1>><</link>>
		<br>
		<<home_outside>>
		<br><br>
	<</if>>
	<<link [[Settings]]>><</link>>
	<br>
	<<link [[Attitudes]]>><<set $attitudesExitPassage to $passage>><</link>>
	<br><br>
<</if>>