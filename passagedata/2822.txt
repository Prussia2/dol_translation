<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<generateyf2>><<generateym3>>

"Everyone form a line at the front of the class," Leighton says. You stand with the other students. Most look worried. "Today is a very important health check," the headteacher says as <<he>> pulls <<his>> camera out. "Unbutton your shirts."
<br><br>

A well-endowed <<person2>><<person>> stood beside <<person1>><<him>> starts sobbing.
<br><br>

<<link [[Comply|Breast Inspection Comply]]>><</link>>
<br>
<<if $player.gender_appearance is "f" or $breastsize gte 2>>
	<<if $exhibitionism gte 55>>
		<<link [[Flaunt|Breast Inspection Flaunt]]>><</link>><<exhibitionist4>>
		<br>
	<</if>>
<<else>>
	<<link [[Flaunt|Breast Inspection Flat Flaunt]]>><</link>><<exhibitionist1>>
	<br>
<</if>>
<<link [[Protest|Breast Inspection Protest]]>><</link>>
<br>