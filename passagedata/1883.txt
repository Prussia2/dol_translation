<<set $outside to 0>><<set $location to "pub">><<dockeffects>><<effects>>

<<if $phase is 0>><<set $phase += 1>>

The first category <<dockpubquiz>>

<<elseif $phase is 1>><<set $phase += 1>>

The second category <<dockpubquiz>>

<<elseif $phase is 2>><<set $phase += 1>>

The third category <<dockpubquiz>>

<<elseif $phase is 3>><<set $phase += 1>>

The fourth category <<dockpubquiz>>

<<elseif $phase is 4>><<set $phase += 1>>

The fifth category <<dockpubquiz>>

<<else>>

	<<if $dockquizhome gt $dockquizaway>>

	"And the winners are," the owner announces. "The dockers!" You cheer and drum the table with your colleagues, until the owner convinces you to settle down.
	<br><br>
	<<earnFeat "Pub Crawl Victors">>
		<<if $dockquiz is "money">>
		The sailors aren't impressed. "Rigged," the <<person>> says, but they hand over the cash. You've gained £60.
		<<set $money += 12000>>
		<br><br>

		You and your colleagues enjoy your free drinks and leave in an elevated mood.
		<<lstress>><<stress -6>>
		<br><br>

		<<dockpuboptions>>
		<<elseif $dockquiz is "body">>
		The sailors aren't impressed. "Rigged," the <<person>> says, but they hand over the cash.
		<br><br>

		<<link [[Take the money for yourself|Docks Pub Crawl Take]]>><<dockstatus -1>><</link>><<lcool>>
		<br>
		<<link [[Share the money with your colleagues|Docks Pub Crawl Share]]>><<dockstatus 1>><</link>><<gcool>>
		<br>
		<<else>>

		The sailors aren't impressed. "Rigged," the <<person>> says.
		<br><br>

		You and your colleagues enjoy your free drinks and leave in an elevated mood.
		<<lstress>><<stress -6>>
		<br><br>

		<<dockpuboptions>>

		<</if>>

	<<elseif $dockquizhome is $dockquizaway>>
		"And the winners are," the owner announces. "No one! It's a draw." The dockers and sailors seem equally upset.
		<br><br>
		You and your colleagues leave soon after.
		<<if $dockquiz is "money">><<set $money += 6000>><</if>>
		<br><br>
		<<dockpuboptions>>
	<<else>>
		"And the winners are," the owner announces. "The sailors!" They cheer and laugh until the owner convinces them to settle down.
		<br><br>
		<<if $dockquiz is "money">>
			They take their money. "Sorry guys," the <<person>> says. "Shoulda known better than to challenge us."
			<<lcool>><<status -1>>
			<br><br>
			You and your colleagues leave in a sour mood.
			<<lstress>><<stress -6>>
			<br><br>
			<<dockpuboptions>>
		<<elseif $dockquiz is "body">>
			The <<person>> looks at you and makes a come-hither motion. <<His>> fellow sailors stare at you with ravenous eyes. "Time to pay up," <<he>> says. "Get that bum over here."
			<br><br>
			<<link [[Walk over|Docks Pub Crawl Gangbang]]>><<set $sexstart to 1>><</link>>
			<br>
			<<if $rng gte 81>>
				<<link [[Refuse|Docks Pub Crawl Fight]]>><<dockstatus -1>><</link>><<lcool>>
				<br>
			<<else>>
				<<link [[Refuse|Docks Pub Crawl Gangbang Refuse]]>><<dockstatus -1>><</link>><<lcool>>
				<br>
			<</if>>
		<<else>>
			You and your colleagues leave in a sour mood.
			<<lstress>><<stress -6>>
			<br><br>
			<<dockpuboptions>>
		<</if>>
	<</if>>
<</if>>