<<effects>>

<<upperstrip>>
<<if $worn.under_upper.type.includes("naked") and ($player.gender is "f" or $player.gender_appearance is "f" or $player.gender is "h")>>
	Whitney tugs your shirt up and off your body, revealing your <<breasts>> to the room.<<exhibitionism3>>
<<elseif $player.gender is "f" or $player.gender_appearance is "f" or $player.gender is "h">>
	Whitney tugs your shirt up and off your body, revealing your $worn.under_upper.name to the room.<<exhibitionism3>>
<<else>>
	Whitney tugs your shirt up and off your body, revealing your <<undertop>> to the room.
	<br><br>
<</if>>

<<endevent>><<npc River>><<person1>>
Whitney's friends cheer, prompting River to turn. <<He>> almost swoons when <<he>> sees Whitney and you, and steadies <<himself>> against <<his>> desk.
<br><br>
"What's wrong <<if $pronoun is "m">>sir<<else>>miss<</if>>?" Whitney asks, wrapping an arm around your waist. "It's hot in here. We're just trying to get comfortable. To help us focus."
<br><br>
River recovers enough to speak. "P-put your shirts on," <<he>> manages. "And you, Whitney, should leave. Now."
<br><br>
<<endevent>><<npc Whitney>><<person1>>
Whitney rises to <<his>> feet, but refuses to wear the shirt, instead slinging it over <<his>> shoulder as <<he>> swaggers from the room.
<br><br>

<<set $whitneymaths to "sent">>
<<link [[Next|Maths Lesson]]>><<clotheson>><<endevent>><</link>>
<br>