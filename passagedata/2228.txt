<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
<<ejaculation>>

The <<person>> adjusts <<his>> clothes and walks away. "Still got it," you hear <<him>> say.
<br><br>

Your knees buckle as the slime's influence gives way. <<tearful>> you steady yourself.
<br><br>

<<clotheson>>
<<endcombat>>

<<else>>

You shove the <<person>> away from you. "Crazy-" you hear <<him>> mutter as <<he>> walks away.
<br><br>

A jolt of pain tears through your body as the slime punishes you for defiance.
<<corruption -1>><<pain 8>><<stress 6>><<trauma 6>><<set $submissive -= 1>><<lcorruption>><<ggpain>><<ggtrauma>><<ggstress>>
<br><br>

<<tearful>> you steady yourself.
<br><br>

<<clotheson>>
<<endcombat>>

<</if>>

<i>Obeying the slime will provide benefits, but increase your corruption. The slime will become more powerful, and harder to resist, as corruption increases.</i>
<br><br>

<<link [[Next|Trash]]>><<set $eventskip to 1>><</link>>