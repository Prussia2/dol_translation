<<temple_effects>><<effects>>

The <<monk>> stands aside to let you pass.
<<if $worn.neck.name is "dark pendant">>
	As you move to enter however, <<he>> thrusts <<his>> arm between you and the door. <<His>> eyes are open. "I don't know how you got that," <<he>> says, pointing at your pendant. "It's forbidden, and you certainly can't enter with it. Hand it over."
	<<gstress>><<stress 6>>
	<br><br>

	<<link [[Give pendant|Temple Prayer Pendant]]>><<neckruined>><</link>>
	<br>
	<<link [[Refuse|Temple Prayer Pendant Refuse]]>><<grace -5>><</link>><<llgrace>>
	<br>
<<else>>
	<<if $worn.neck.name is "holy pendant">>
		"Wise choice of jewellery," <<he>> says as you pass, gesturing at your holy pendant. You continue in.
	<<elseif $worn.neck.name is "stone pendant">>
		<<His>> hand rests on your shoulder as you pass. "That's interesting," <<he>> says, gesturing at your stone pendant. <<He>> considers for a moment, then releases your shoulder. You continue in.
	<<else>>

	<</if>>
	<br><br>

	The door clicks shut behind you. You're in a small hexagonal room, the edges ringed with steps leading nowhere. A single window decorates each wall, far above. A cushion sits in the middle.
	<br><br>

	The air is thick with the smell of a sweet incense. You can't see where it's being burned.
	<br><br>
	<<endevent>>
	<<link [[Next|Temple Prayer]]>><</link>> | <span class="blue">???</span>
	<br>

<</if>>