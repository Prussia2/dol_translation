<<set $outside to 0>><<set $location to "park">><<effects>><<set $bus to "parkmens">>

You are in the men's toilets in the park.
<<storeon "parkmens" "check">>
<<if _store_check is 1>>
Your clothes are stored beneath the sinks.
<</if>>
<br><br>

<<if $stress gte 10000>>
<<passoutpark>>
<<else>>

<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure) and $player.gender_appearance is "f" and $eventskip is 0>>
	<<eventstoilets>>

	<<else>>
	<<storeactions "parkmens">>
	<<link [[Masturbate|Men's Toilets Masturbation]]>><<set $masturbationstart to 1>><</link>>
	<br>
		<<if $facesemen gte 1 or $facegoo gte 1 or $skin.left_cheek.pen is "pen" or $skin.right_cheek.pen is "pen" or $skin.forehead.pen is "pen">>
			<<link [[Wash Face (0:05)|Men's Toilets Wash]]>><<pass 5>><</link>>
			<br>
		<</if>>
		<<if $mouthsemen gte 1 or $mouthgoo gte 1>>
			<<link [[Rinse mouth (0:05)|Men's Toilets Rinse]]>><<pass 5>><</link>>
			<br>
		<</if>>
		<<if _store_check isnot 1>>
		<<link [[Leave|Park]]>><</link>>
		<<elseif $exposed gte 2 and $exhibitionism gte 35>>
		<<link [[Sneak Out|Park Toilets Sneak]]>><<set $phase to 2>><</link>><<exhibitionist3>>
		<<elseif $exposed is 1>>
		<<link [[Sneak Out|Park Toilets Sneak]]>><<set $phase to 1>><</link>><<exhibitionist1>>
		<<elseif $exposed lte 0>>
		<<link [[Leave|Park]]>><</link>>
		<</if>>
	<</if>>
<</if>>

<<set $eventskip to 0>>
<<storecleanup>>