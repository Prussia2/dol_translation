<<widget "baseClothingStrings">><<nobr>>
<<set _class to 'anim-idle-2f' + ($worn[$args[0]].colour ? ' clothes-'+$worn[$args[0]].colour:'')>>
<<set _classAcc to 'anim-idle-2f ' + ($worn[$args[0]].accessory_colour ? ' clothes-'+$worn[$args[0]].accessory_colour :'')>>
<<set _style to ($worn[$args[0]].colourCustom ? $worn[$args[0]].colourCustom :'')>>
<<set _styleAcc to ($worn[$args[0]].accessory_colourCustom ? $worn[$args[0]].accessory_colourCustom :'')>>
<</nobr>><</widget>>

<<widget "baseClothingImg">><<nobr>>
<<if $worn[$args[0]].name isnot "naked">>
	<<baseClothingStrings $args[0]>>
	<div @class="'clothes-div layer-'+$args[0]">
		<<if $worn[$args[0]].mainImage isnot 0>>
			<img @class="_class" @src="'img/clothes/'+[$args[0]]+'/' + $worn[$args[0]].variable + '/' + 'full.png'" @style="_style">
		<</if>>
		<<if $worn[$args[0]].accessory is 1>>
			<img @class="_classAcc" @src="'img/clothes/'+[$args[0]]+'/' + $worn[$args[0]].variable + '/' + 'acc.png'" @style="_styleAcc">
		<</if>>
	</div>
	<<if $worn[$args[0]].back_img is 1>>
		<div @class="'clothes-div layer-'+$args[0]+'-back'">
			<img @class="_classAcc" @src="'img/clothes/'+[$args[0]]+'/' + $worn[$args[0]].variable + '/' + 'back.png'" @style="_styleAcc">
		</div>
	<</if>>
<</if>>
<</nobr>><</widget>>

<<widget "underupperimg">><<nobr>>
<<if $worn.under_upper.name isnot "naked">>
	<<baseClothingStrings "under_upper">>
	<<switch $underupperwetstage>>
		<<case 1>><<set _underUpperWetness to " clothes-damp">>
		<<case 2>><<set _underUpperWetness to " clothes-wet">>
		<<case 3>><<set _underUpperWetness to " clothes-drenshed">>
		<<default>><<set _underUpperWetness to "">>
	<</switch>>

	<<if $worn.under_upper.integrity lte ($worn.under_upper.integrity_max / 10) * 2>>
		<<set _imgName to "tattered">>
	<<elseif $worn.under_upper.integrity lte ($worn.under_upper.integrity_max / 10) * 5>>
		<<set _imgName to "torn">>
	<<elseif $worn.under_upper.integrity lte ($worn.under_upper.integrity_max / 10) * 9>>
		<<set _imgName to "frayed">>
	<<else>>
		<<set _imgName to "full">>
	<</if>>
	<div @class="'clothes-div layer-under_upper' + _underUpperWetness">
		<<if $worn.under_upper.mainImage isnot 0>>
			<img @class="_class" @src="'img/clothes/under_upper/' + $worn.under_upper.variable + '/' + _imgName + '.png'" @style="_style">
		<</if>>
		<<if $worn.under_upper.breast_img is 1>>
			<img @class="_class" @src="'img/clothes/under_upper/' + $worn.under_upper.variable + '/' + (_breastSize - 1) + '.png'" @style="_style">
		<</if>>
		<<if $worn.under_upper.accessory is 1>>
			<img @class="_classAcc" @src="'img/clothes/under_upper/' + $worn.under_upper.variable + '/' + 'acc.png'" @style="_styleAcc">
			<<if $worn.under_upper.breast_img is 1>>
				<img @class="_classAcc" @src="'img/clothes/under_upper/' + $worn.under_upper.variable + '/' + (_breastSize - 1) + '_acc.png'" @style="_styleAcc">
			<</if>>
		<</if>>
	</div>
	<<if _coverRight isnot undefined and $worn.under_upper.sleeve_img is 1>>
		<div @class="'clothes-div layer-rightarmunderclothes' + _underUpperWetness">
		<<if _coverRight is true>>
			<img @class="_class" @src="'img/clothes/under_upper/' + $worn.under_upper.variable + '/' + 'right_cover.png'" @style="_style">
		<<else>>
			<img @class="_class" @src="'img/clothes/under_upper/' + $worn.under_upper.variable + '/' + 'right.png'" @style="_style">
		<</if>>
		</div>
	<</if>>
	<<if _coverLeft isnot undefined and $worn.under_upper.sleeve_img is 1>>
		<div @class="'clothes-div layer-leftarmunderclothes' + _underUpperWetness">
		<<if _coverLeft is true>>
			<img @class="_class" @src="'img/clothes/under_upper/' + $worn.under_upper.variable + '/' + 'left_cover.png'" @style="_style">
		<<else>>
			<img @class="_class" @src="'img/clothes/under_upper/' + $worn.under_upper.variable + '/' + 'left.png'" @style="_style">
		<</if>>
		</div>
	<</if>>
<</if>>

<</nobr>><</widget>>

<<widget "underlowerimg">><<nobr>>
<<if $worn.under_lower.name isnot "naked">>
	<<baseClothingStrings "under_lower">>
	<<switch $underlowerwetstage>>
		<<case 1>><<set _underlowerWetness to " clothes-damp">>
		<<case 2>><<set _underlowerWetness to " clothes-wet">>
		<<case 3>><<set _underlowerWetness to " clothes-drenshed">>
		<<default>><<set _underlowerWetness to "">>
	<</switch>>

	<<if $worn.under_lower.integrity lte ($worn.under_lower.integrity_max / 10) * 2>>
		<<set _imgName to "tattered">>
	<<elseif $worn.under_lower.integrity lte ($worn.under_lower.integrity_max / 10) * 5>>
		<<set _imgName to "torn">>
	<<elseif $worn.under_lower.integrity lte ($worn.under_lower.integrity_max / 10) * 9>>
		<<set _imgName to "frayed">>
	<<else>>
		<<set _imgName to "full">>
	<</if>>
	<<if $worn.under_lower.mainImage isnot 0 and $worn.under_lower.high_img is 1>>
		<div @class="'clothes-div layer-under_lower-high' + _underlowerWetness">
			<img @class="_class" @src="'img/clothes/under_lower/' + $worn.under_lower.variable + '/' + _imgName + '.png'" @style="_style">
		</div>
	<</if>>
	<div @class="'clothes-div layer-under_lower' + _underlowerWetness">
		<<if $worn.under_lower.mainImage isnot 0 and $worn.under_lower.high_img isnot 1>>
			<img @class="_class" @src="'img/clothes/under_lower/' + $worn.under_lower.variable + '/' + _imgName + '.png'" @style="_style">
		<</if>>
		<<if $worn.under_lower.accessory is 1>>
			<img @class="_classAcc" @src="'img/clothes/under_lower/' + $worn.under_lower.variable + '/' + 'acc.png'" @style="_styleAcc">
		<</if>>
	</div>
	<<if $worn.under_lower.penis_img is 1 and $penisexist is 1>>
		<div @class="'clothes-div layer-under_lower-top' + _underlowerWetness">
			<img @class="_class" @src="'img/clothes/under_lower/' + $worn.under_lower.variable + '/' + 'penis.png'" @style="_style">
			<<if $worn.under_lower.accessory is 1>>
				<img @class="_classAcc" @src="'img/clothes/under_lower/' + $worn.under_lower.variable + '/' + 'acc_penis.png'" @style="_styleAcc">
			<</if>>
		</div>
	<</if>>
<</if>>
<</nobr>><</widget>>

<<widget "lowerimg">><<nobr>>
<<if $worn.lower.name isnot "naked">>
	<<baseClothingStrings "lower">>
	<<switch $lowerwetstage>>
		<<case 1>><<set _lowerWetness to " clothes-damp">>
		<<case 2>><<set _lowerWetness to " clothes-wet">>
		<<case 3>><<set _lowerWetness to " clothes-drenshed">>
		<<default>><<set _lowerWetness to "">>
	<</switch>>

	<<if $worn.lower.integrity lte ($worn.lower.integrity_max / 10) * 2>>
		<<set _imgName to "tattered">>
	<<elseif $worn.lower.integrity lte ($worn.lower.integrity_max / 10) * 5>>
		<<set _imgName to "torn">>
	<<elseif $worn.lower.integrity lte ($worn.lower.integrity_max / 10) * 9>>
		<<set _imgName to "frayed">>
	<<else>>
		<<set _imgName to "full">>
	<</if>>

	<<if $worn.lower.mainImage isnot 0 and $worn.lower.high_img is 1>>
		<div @class="'clothes-div layer-lower-high' + _lowerWetness">
			<img @class="_class" @src="'img/clothes/lower/' + $worn.lower.variable + '/' + _imgName + '.png'" @style="_style">
		</div>
	<</if>>
	<div @class="'clothes-div layer-lower' + _lowerWetness">
		<<if $worn.lower.mainImage isnot 0 and $worn.lower.high_img isnot 1>>
			<img @class="_class" @src="'img/clothes/lower/' + $worn.lower.variable + '/' + _imgName + '.png'" @style="_style">
		<</if>>
		<<if $worn.lower.accessory is 1>>
			<img @class="_classAcc" @src="'img/clothes/lower/' + $worn.lower.variable + '/' + 'acc.png'" @style="_styleAcc">
		<</if>>
	</div>
	<<if $worn.lower.back_img is 1>>
		<div @class="'clothes-div layer-back-lower' + _lowerWetness">
			<img @class="_class" @src="'img/clothes/lower/' + $worn.lower.variable + '/' + 'back.png'" @style="_style">
		</div>
	<</if>>
<</if>>
<</nobr>><</widget>>

<<widget "overlowerimg">><<nobr>>
<<if $worn.over_lower.name isnot "naked">>
	<<baseClothingStrings "over_lower">>
	<<if $worn.over_lower.integrity lte ($worn.over_lower.integrity_max / 10) * 2>>
		<<set _imgName to "tattered">>
	<<elseif $worn.over_lower.integrity lte ($worn.over_lower.integrity_max / 10) * 5>>
		<<set _imgName to "torn">>
	<<elseif $worn.over_lower.integrity lte ($worn.over_lower.integrity_max / 10) * 9>>
		<<set _imgName to "frayed">>
	<<else>>
		<<set _imgName to "full">>
	<</if>>
	<div class="layer-layer-over_lower">
		<<if $worn.over_lower.mainImage isnot 0>>
			<img @class="_class" @src="'img/clothes/over_lower/' + $worn.over_lower.variable + '/' + _imgName + '.png'" @style="_style">
		<</if>>
		<<if $worn.over_lower.accessory is 1>>
			<img @class="_classAcc" @src="'img/clothes/over_lower/' + $worn.over_lower.variable + '/' + 'acc.png'" @style="_styleAcc">
		<</if>>
	</div>
	<<if $worn.over_lower.back_img is 1>>
		<div class="layer-back-lower">
			<img @class="_class" @src="'img/clothes/over_lower/' + $worn.over_lower.variable + '/' + 'back.png'" @style="_style">
		</div>
	<</if>>
<</if>>

<</nobr>><</widget>>

<<widget "genitalsimg">><<nobr>>
<<if $worn.genitals.name isnot "naked">>
	<div class="layer-genitals">
		<<if $worn.genitals.mainImage isnot 0 and !$worn.genitals.hideUnderLower.includes($worn.under_lower.name)>>
			<<if $worn.genitals.integrity lte ($worn.genitals.integrity_max / 10) * 2>>
				<img @class="'anim-idle-2f' + ($worn.genitals.colour ? ' clothes-'+$worn.genitals.colour :'')" @src="'img/clothes/genitals/' + $worn.genitals.variable + '/' + 'tattered.png'" @style="$worn.genitals.colourCustom">
			<<elseif $worn.genitals.integrity lte ($worn.genitals.integrity_max / 10) * 5>>
				<img @class="'anim-idle-2f' + ($worn.genitals.colour ? ' clothes-'+$worn.genitals.colour :'')" @src="'img/clothes/genitals/' + $worn.genitals.variable + '/' + 'torn.png'" @style="$worn.genitals.colourCustom">
			<<elseif $worn.genitals.integrity lte ($worn.genitals.integrity_max / 10) * 9>>
				<img @class="'anim-idle-2f' + ($worn.genitals.colour ? ' clothes-'+$worn.genitals.colour :'')" @src="'img/clothes/genitals/' + $worn.genitals.variable + '/' + 'frayed.png'" @style="$worn.genitals.colourCustom">
			<<else>>
				<img @class="'anim-idle-2f' + ($worn.genitals.colour ? ' clothes-'+$worn.genitals.colour :'')" @src="'img/clothes/genitals/' + $worn.genitals.variable + '/' + 'full.png'" @style="$worn.genitals.colourCustom">
			<</if>>
		<</if>>
	</div>
<</if>>

<</nobr>><</widget>>

<<widget "upperimg">><<nobr>>
<<if $worn.upper.name isnot "naked">>
	<<baseClothingStrings "upper">>
	<<switch $upperwetstage>>
		<<case 1>><<set _upperWetness to " clothes-damp">>
		<<case 2>><<set _upperWetness to " clothes-wet">>
		<<case 3>><<set _upperWetness to " clothes-drenshed">>
		<<default>><<set _upperWetness to "">>
	<</switch>>
	<div @class="'clothes-div layer-upper' + _upperWetness">
		<<if $worn.upper.mainImage isnot 0>>
			<<if $worn.upper.integrity lte ($worn.upper.integrity_max / 10) * 2>>
				<<set _imgName to "tattered">>
			<<elseif $worn.upper.integrity lte ($worn.upper.integrity_max / 10) * 5>>
				<<set _imgName to "torn">>
			<<elseif $worn.upper.integrity lte ($worn.upper.integrity_max / 10) * 9>>
				<<set _imgName to "frayed">>
			<<else>>
				<<set _imgName to "full">>
			<</if>>
			<img @class="_class" @src="'img/clothes/upper/' + $worn.upper.variable + '/' + _imgName + '.png'" @style="_style">
		<</if>>
		<<if $worn.upper.breast_img is 1>>
			<img @class="_class" @src="'img/clothes/upper/' + $worn.upper.variable + '/' + (_breastSize - 1) + '.png'" @style="_style">
		<</if>>
		<<if $worn.upper.accessory is 1>>
			<img @class="_classAcc" @src="'img/clothes/upper/' + $worn.upper.variable + '/' + 'acc.png'" @style="_styleAcc">
		<</if>>
	</div>
	<<if _coverRight isnot undefined and $worn.upper.sleeve_img is 1>>
		<div @class="'clothes-div layer-rightarmclothes' + _upperWetness">
			<<if _coverRight is true>>
				<img @class="_class" @src="'img/clothes/upper/' + $worn.upper.variable + '/' + 'right_cover.png'" @style="_style">
			<<else>>
				<img @class="_class" @src="'img/clothes/upper/' + $worn.upper.variable + '/' + 'right.png'" @style="_style">
			<</if>>
		</div>
	<</if>>
	<<if _coverLeft isnot undefined and $worn.upper.sleeve_img is 1>>
		<div @class="'clothes-div layer-leftarmclothes' + _upperWetness">
			<<if _coverLeft is true>>
				<img @class="_class" @src="'img/clothes/upper/' + $worn.upper.variable + '/' + 'left_cover.png'" @style="_style">
			<<else>>
				<img @class="_class" @src="'img/clothes/upper/' + $worn.upper.variable + '/' + 'left.png'" @style="_style">
			<</if>>
		</div>
	<</if>>
<</if>>
<</nobr>><</widget>>

<<widget "overupperimg">><<nobr>>
<<if $worn.over_upper.name isnot "naked">>
	<<baseClothingStrings "over_upper">>
	<<if $worn.over_upper.integrity lte ($worn.over_upper.integrity_max / 10) * 2>>
		<<set _imgName to "tattered">>
	<<elseif $worn.over_upper.integrity lte ($worn.over_upper.integrity_max / 10) * 5>>
		<<set _imgName to "torn">>
	<<elseif $worn.over_upper.integrity lte ($worn.over_upper.integrity_max / 10) * 9>>
		<<set _imgName to "frayed">>
	<<else>>
		<<set _imgName to "full">>
	<</if>>
	<div class="layer-over_upper">
		<<if $worn.over_upper.mainImage isnot 0>>
			<img @class="_class" @src="'img/clothes/over_upper/' + $worn.over_upper.variable + '/' + _imgName + '.png'" @style="_style">
		<</if>>
		<<if $worn.over_upper.breast_img is 1>>
			<img @class="_class" @src="'img/clothes/over_upper/' + $worn.over_upper.variable + '/' + (_breastSize - 1) + '.png'" @style="_style">
		<</if>>
		<<if $worn.over_upper.accessory is 1>>
			<img @class="_classAcc" @src="'img/clothes/over_upper/' + $worn.over_upper.variable + '/' + 'acc.png'" @style="_styleAcc">
		<</if>>
	</div>
	<<if _coverRight isnot undefined and $worn.over_upper.sleeve_img is 1>>
		<div class="layer-rightarmoverclothes">
			<<if _coverRight is true>>
				<img @class="_class" @src="'img/clothes/over_upper/' + $worn.over_upper.variable + '/' + 'right_cover.png'" @style="_style">
			<<else>>
				<img @class="_class" @src="'img/clothes/over_upper/' + $worn.over_upper.variable + '/' + 'right.png'" @style="_style">
			<</if>>
		</div>
	<</if>>
	<<if _coverLeft isnot undefined and $worn.over_upper.sleeve_img is 1>>
		<div class="layer-leftarmoverclothes">
			<<if _coverLeft is true>>
				<img @class="_class" @src="'img/clothes/over_upper/' + $worn.over_upper.variable + '/' + 'left_cover.png'" @style="_style">
			<<else>>
				<img @class="_class" @src="'img/clothes/over_upper/' + $worn.over_upper.variable + '/' + 'left.png'" @style="_style">
			<</if>>
		</div>
	<</if>>
<</if>>

<</nobr>><</widget>>

<<widget "imgOpacity">><<nobr>>
<<if _underUpperOpacity is undefined>>
	<<set _opacity to 1>>
	<<if !$worn.under_upper.type.includes("swim")>>
		<<if $underupperwetstage gte 3>>
			<<set _opacity = Math.clamp(_opacity - 0.5, 0, 1)>>
		<<elseif $underupperwetstage gte 2>>
			<<set _opacity = Math.clamp(_opacity - 0.25, 0, 1)>>
		<</if>>
	<</if>>
	<<set _underUpperOpacity to " opacity("+_opacity+")">>
<</if>>

<<if _underLowerOpacity is undefined>>
	<<set _opacity to 1>>
	<<if !$worn.under_lower.type.includes("swim")>>
		<<if $underlowerwetstage gte 3>>
			<<set _opacity = Math.clamp(_opacity - 0.5, 0, 1)>>
		<<elseif $underlowerwetstage gte 2>>
			<<set _opacity = Math.clamp(_opacity - 0.25, 0, 1)>>
		<</if>>
	<</if>>
	<<set _underLowerOpacity to " opacity("+_opacity+")">>
<</if>>

<<if _lowerOpacity is undefined>>
	<<set _opacity to 1>>
	<<if !$worn.lower.type.includes("swim")>>
		<<if $lowerwetstage gte 3>>
			<<set _opacity = Math.clamp(_opacity - 0.5, 0, 1)>>
		<<elseif $lowerwetstage gte 2>>
			<<set _opacity = Math.clamp(_opacity - 0.25, 0, 1)>>
		<</if>>
	<</if>>
	<<set _lowerOpacity to " opacity("+_opacity+")">>
<</if>>

<<if _upperOpacity is undefined>>
	<<set _opacity to 1>>
	<<if !$worn.upper.type.includes("swim")>>
		<<if $upperwetstage gte 3>>
			<<set _opacity = Math.clamp(_opacity - 0.5, 0, 1)>>
		<<elseif $upperwetstage gte 2>>
			<<set _opacity = Math.clamp(_opacity - 0.25, 0, 1)>>
		<</if>>
	<</if>>
	<<set _upperOpacity to " opacity("+_opacity+")">>
<</if>>
<</nobr>><</widget>>