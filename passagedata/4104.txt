<<effects>>

You bring your hand back up, not wanting to risk getting caught in the act. Robin breathes out, then laughs. "That was close."
<br><br>
You continue eating. Robin’s cheeks are flushed for the remainder of lunch. "I'm going to wait in the classroom," <<he>> says when finished. "I-I," <<He>> interrupts <<himself>>, then hugs you, a bit closer than normal. <<He>> leaves without another word.
<br><br>
	<<if $NPCName[$NPCNameList.indexOf("Kylar")].state is "active">>
	Kylar watches from across the canteen.
	<<ggksuspicion>><<npcincr Kylar rage 3>>
	<br><br>
	<</if>>
<<endevent>>
<<link [[Next|Canteen]]>><</link>>
<br>