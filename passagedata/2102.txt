<<effects>>
Some of the ropes are cut loose, though your arms are still bound. You're led into what looks like a regular kitchen, and into a living room. You're shoved onto the sofa.
<br><br>
"Well now," the <<person1>><<person>> says. "All that trouble getting you, and now that you're here I can't decide what to do."
<br>
"Could just rape <<phercomma>>" the <<person2>><<person>> responds, reclining in a separate seat. "Though is it rape if <<pshe>> doesn't say no?"
<br><br>
They discuss your fate for a few moments. None of it sounds pleasing.
<br><br>
<<link [[Make submissive gestures|Sold Sub]]>><<sub 1>><</link>>
<br>
<<link [[Glare|Sold Glare]]>><<def 1>><</link>>
<br>