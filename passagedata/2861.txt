<<set $outside to 1>><<set $location to "school">><<schooleffects>><<effects>><<set $bus to "schoolrearplayground">>

<<if $schoolstate isnot "early" and $schoolstate isnot "late" and $schoolstate isnot "earlynoschool" and $schoolstate isnot "latenoschool" and $schoolstate isnot "daynoschool">>
	<<if $schoolstate is "morning">>
	You are in the playground behind the school. Younger students are already playing, despite school not yet having officially begun.
	<br><br>
	<<elseif $schoolstate is "afternoon">>
	You are in the playground behind the school. Some students are running around and playing, as the facilities stay open for a couple of hours.
	<br><br>
	<<elseif $schoolstate is "lunch">>
	You are in the playground behind the school. It is crowded with students on their lunch break.
	<br><br>
	<<else>>
	You are in the playground behind the school. It's mostly empty while class is in session.
	<br><br>
	<</if>>
	<<if $exposed gte 1>>
	You hide behind a shed, keeping your <<lewdness>> hidden.
	<br><br>
	<</if>>
<<else>>
	The rear playground is empty.
	<br><br>
<</if>>

<<schoolperiodtext>>

<<if $stress gte 10000>>
	<<link [[Everything fades to black...|School Passout]]>><</link>>
<<else>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure) and $eventskip is 0>>
		<<eventsplayground>>
	<<else>>

		<<if $schoolstate isnot "early" and $schoolstate isnot "late" and $schoolstate isnot "earlynoschool" and $schoolstate isnot "latenoschool" and $schoolstate isnot "daynoschool" and $exposed gte 1>>

			<<link [[Sneak into the school (0:03)|Hallways]]>><<pass 3>><</link>>
			<br>
			<<link [[Sneak in front of the school (0:05)|School Front Playground]]>><<pass 5>><</link>>
			<br><br>
			<<link [[Climb over the fence to the industrial district (0:02)|School Rear Fence Exit]]>><<pass 2>><</link>>
			<br>

		<<else>>

			<<if $exposed lte 0 and $NPCName[$NPCNameList.indexOf("Kylar")].state is "active" and $weather isnot "rain" and $schoolstate is "lunch">>
			Kylar sits on a stump and stares at the ground.
				<br>
				<<link [[Approach|Kylar Playground]]>><</link>>
				<br><br>
			<</if>>
			<<if $exposed lte 0 and $schoolstate is "lunch">>
				<<link [[Relax on a stump (0:10)|School Stump]]>><<stress -1>><<pass 10>><<tiredness -1>><</link>><<ltiredness>><<lstress>>
				<br>
			<</if>>
			<<link [[Enter the school (0:01)|Hallways]]>><<pass 1>><</link>>
			<br>
			<<link [[Front playground (0:02)|School Front Playground]]>><<pass 2>><</link>>
			<br><br>
			<<link [[Climb over the fence to the industrial district (0:02)|School Rear Fence Exit]]>><<pass 2>><</link>>
			<br>

		<</if>>

		<<if $historytrait gte 1>>
			<<if $parktunnelintro gte 1>>
				<<parkicon>><<link [[Secret tunnel to park (0:05)|Park]]>><<pass 5>><</link>>
				<br>
			<<else>>
				<<parkicon>><<link [[Secret tunnel to park (0:05)|Park Tunnel]]>><<pass 5>><</link>>
				<br>
			<</if>>
		<</if>>
	<</if>>
<</if>>

<<set $eventskip to 0>>