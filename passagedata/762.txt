<<effects>>

You push back against the <<farm_text horse>>'s humping. <<farm_He horse>> seems taken aback, and tries to step away, but you grasp <<farm_his horse>> legs and continue grinding.
<<deviancy4>>

You soon have <<farm_him horse>> shuddering in climax. <<farm_His horse>> legs give way, and <<farm_he horse>> falls onto a pile of straw.
<br><br>
<<tearful>> you leave the stall. You shut the door behind you.
<br><br>

<<link [[Next|Farm Work]]>><</link>>
<br>