<<set $outside to 0>><<set $location to "pool">><<schooleffects>><<effects>>

<<if $schoolstate is "fifth" and $time lte ($hour * 60 + 51)>>

	<<if $exposed gte 2 and $swimall is 1>>

	<<npc Mason>><<person1>>
		<<if $swimmingattended is 1>>
			You rejoin the swimming lesson.
			<<if $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
			Mason instantly notices your bound <<if $rightarm is "bound" or $leftarm is "bound">>arms<</if>><<if ($rightarm is "bound" or $leftarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>. "It wouldn't be safe to let you swim like that."
			<br>
			<<He>> moves behind you and <span class="green">you feel your limbs fall free.</span><<unbind>>
		<</if>>
		<br><br>
		<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
		<br>
		<<elseif $time lte ($hour * 60 + 5)>>
			You enter the swimming pool room.
			Mason stands by the pool, covering <<his>> body as <<he>> tries to encourage students to stretch while the rest of the class pours in.
			<<if $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
				<<He>> notices your bound <<if $rightarm is "bound" or $leftarm is "bound">>arms<</if>><<if ($rightarm is "bound" or $leftarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>. "T-that's a little too much."
				<br>
				<<He>> moves behind you and <span class="green">you feel your limbs fall free.</span><<unbind>>
			<</if>>
			<br><br>

			<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
			<br>

		<<else>>
			You enter the swimming pool room.
			<<npc Mason>><<person1>>
			Mason and the class are already in the water. "Glad you could join us!" <<He>> says, beckoning.
			<<gdelinquency>><<detention 1>>
			<br><br>
			<<if $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
				Mason instantly notices your bound <<if $rightarm is "bound" or $leftarm is "bound">>arms<</if>><<if ($rightarm is "bound" or $leftarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>. "Wait! It wouldn't be safe to let you swim like that."
				<br>
				<<He>> moves behind you and <span class="green">you feel your limbs fall free.</span><<unbind>>
			<</if>>
			You climb into the water as Mason continues the lesson.
			<br><br>
			<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
			<br>
		<</if>>

	<<elseif $exposed gte 2>>
		<<schoolpoolexposed>>
		<<if $exhibitionism gte 75 and $swimall isnot 1>>
			<<link [[Ask to swim naked|School Pool Ask Naked]]>><<fameexhibitionism 15>><<endevent>><<set $swimnudecounter += 1>><</link>><<exhibitionist5>>
			<br>
		<</if>>
		<<if $schoolstate is "fifth" and $swimnudecounter gte 1>>
			<<link [[Boy's changing room|School Boy Changing Room]]>><<endevent>><</link>>
			<br>
			<<link [[Girl's changing room|School Girl Changing Room]]>><<endevent>><</link>>
			<br>
		<</if>>

	<<elseif $worn.under_lower.type.includes("swim") and ($worn.under_upper.type.includes("swim") or $worn.under_upper.type.includes("naked"))
	and ($worn.upper.type.includes("swim") or $worn.upper.type.includes("naked")) and ($worn.lower.type.includes("swim") or $worn.lower.type.includes("naked"))>>
		<<npc Mason>><<person1>>
		<<if $swimmingattended is 1>>
			You rejoin the swimming lesson.
			<<if $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
				Mason instantly notices your bound <<if $rightarm is "bound" or $leftarm is "bound">>arms<</if>><<if ($rightarm is "bound" or $leftarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>. "Well... obviously it wouldn't be safe to let you swim like that."
				<br>
				<<He>> moves behind you and <span class="green">you feel your limbs fall free.</span><<unbind>>
			<</if>>
			<br><br>
			<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
			<br>
		<<elseif $time lte ($hour * 60 + 5)>>
			You enter the swimming pool room.
			Mason stands by the pool, encouraging students to stretch while waiting for the rest of the class.
			<<if $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
				<<He>> instantly notices your bound <<if $rightarm is "bound" or $leftarm is "bound">>arms<</if>><<if ($rightarm is "bound" or $leftarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>. "I can't let you swim like that."
				<br>
				<<He>> moves behind you and <span class="green">you feel your limbs fall free.</span><<unbind>>
			<</if>>
			<br><br>

			<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
			<br>

		<<else>>
			You enter the swimming pool room.
			<<npc Mason>><<person1>>
			Mason and the class are already in the water. "Glad you could join us!" <<He>> says, beckoning.
			<<gdelinquency>><<detention 1>>
			<br><br>
			<<if $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
				Mason instantly notices your bound <<if $rightarm is "bound" or $leftarm is "bound">>arms<</if>><<if ($rightarm is "bound" or $leftarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>. "Wait! It wouldn't be safe to let you swim like that."
				<br>
				<<He>> moves behind you and <span class="green">you feel your limbs fall free.</span><<unbind>>
			<</if>>
			You climb into the water as Mason continues the lesson.
			<br><br>

			<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
			<br>

		<</if>>
	<<else>>
		You enter the swimming pool room.
		<<npc Mason>><<person1>>
		Mason looks at your clothing. "You can't swim dressed like that! Go get changed."
		<br><br>

		<<if $exhibitionism gte 75 and $swimall isnot 1>>
			<<link [[Ask to swim naked|School Pool Ask Naked]]>><<fameexhibitionism 15>><<endevent>><<set $swimnudecounter += 1>><</link>><<exhibitionist5>>
			<br>
		<</if>>
		<<if $player.gender is "f" or ($player.gender is "h" and $breastsize gte 3)>>
			<<link [[Say you only have a boy's swimming costume|School Pool Wrong]]>><</link>>
			<br>
			<<link [[Say you don't have anything to change into|School Pool Spare]]>><</link>>
			<br>
		<<else>>
			<<link [[Say you only have a girl's swimming costume|School Pool Wrong]]>><</link>>
			<br>
			<<link [[Say you don't have anything to change into|School Pool Spare]]>><</link>>
			<br>
		<</if>>
		<br>
		<<link [[Boy's changing room|School Boy Changing Room]]>><<endevent>><</link>>
		<br>
		<<link [[Girl's changing room|School Girl Changing Room]]>><<endevent>><</link>>
		<br><br>
	<</if>>

<<elseif $schoollesson is 1>>
	<<if $exposed gte 1>>
		<<schoolpoolexposed>>
	<<else>>
		You step into the pool room.
	<</if>>
	<<if $swimminginterrupted is 1>>
		<<npc Mason>><<person1>>You enter the pool room. Mason ceases <<his>> activity and looks your way. "I've got a class to teach.
		Stop bothering us." <<He>> points to the door.
		<<gdelinquency>>
		<br><br>
		<<detention 1>>

		<<link [[Boy's changing room|School Boy Changing Room]]>><<endevent>><</link>>
		<br>
		<<link [[Girl's changing room|School Girl Changing Room]]>><<endevent>><</link>>
		<br>
	<<else>>
		<<npc Mason>><<person1>>
		You enter the pool room. Mason ceases <<his>> activity and smiles at you. "Are you lost? Or is there something you need to tell me?"
		It seems you've interrupted the lesson.
		<br><br>
		<<set $swimminginterrupted to 1>>

		<<link [[Apologise|Swimming Classroom Apology]]>><<trauma 1>><<stress 1>><</link>><<gstress>><<gtrauma>>
		<br>
		<<if $trauma gte 1>>
			<<link [[Mock|Swimming Classroom Mock]]>><<status 1>><<stress -12>><</link>><<lstress>><<gcool>><<gdelinquency>>
			<br>
		<</if>>
	<</if>>
<<else>>
	You are in the swimming pool room. It's saturated with the smell of chlorine.
	<br><br>

	<<exhibitionclassroom>>

	<<link [[Boy's changing room|School Boy Changing Room]]>><<endevent>><</link>>
	<br>
	<<link [[Girl's changing room|School Girl Changing Room]]>><<endevent>><</link>>
	<br>
<</if>>