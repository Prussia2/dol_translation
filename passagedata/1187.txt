<<effects>>

You pull away from Harper. <<He>> tries to hold you still, but you persist and turn away from <<himstop>>
<br><br>

"Is our beast causing trouble?" says Remy from the door. Harper ceases at once.
<br>
"N-no," the doctor stutters. "Just a little temperamental. Thank you again. I trust my next visit is still scheduled?"
<br>
"Of course." Remy takes your leash. "Until we meet again."
<br><br>

<<if $phase is 1>>
	Remy leads you outside. You spit out the pink fluid. If Remy notices, <<endevent>><<npc Remy>><<person1>><<he>> thinks nothing of it.
<<else>>

<</if>>
<br><br>

<<link [[Next|Livestock Job End]]>><<endevent>><</link>>
<br>