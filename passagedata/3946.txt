<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You lean passed Kylar and kiss Robin's cheek.
<br><br>
<<endevent>><<npc Robin>><<person1>>
Robin looks surprised, and raises a hand to the spot you kissed. <<He>> turns to face you, smiles, and kisses your nose.
<<gglove>><<npcincr Robin love 5>>
<br><br>
<<endevent>><<npc Kylar>><<person1>>
Kylar covers <<his>> face with <<his>> hands and leaves the table.
<<lllove>><<ggksuspicion>><<npcincr Kylar love -5>><<npcincr Kylar rage 5>>
<br><br>
<<endevent>><<npc Robin>><<person1>>
Robin looks concerned. "Is Kylar okay?" <<he>> asks.
<br><br>

You finish the food. "I'm going to wait in the classroom," Robin says when you're done. "I don't like being late." <<He>> hugs you. Kylar glares at <<himcomma>> but <<he>> gives no sign of noticing.
<br><br>

<<endevent>>
<<link [[Next|Canteen]]>><</link>>
<br>