<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "cliff">>
You join the crowd around the pillory on Cliff Street.
<<npc_pillory_detail>>
<<if $stress gte 3000>>
<span class="gold">You are stressed.</span> Things are getting on top of you.
You watch some people throw fruit at <<the_pillory_person_stop>> It looks like fun. <span class="teal">You feel an urge to join in</span> - <<he>> probably deserves it.
<<if $daystate isnot "night">>
<br>
When the decent people are all in bed - you might dare do lewder things.
<</if>>
<br><br>
<</if>>
<<if $daystate is "night">>
	<<if $stress gte 3000>>
		<<if $pillory_tenant.fruitstock gte 1>>
			<<link [[Throw Fruit|Cliff Street Prisoner]]>><<pass 2>><<set $phase to 0>><<stress -6>><<set $pillory_tenant.fruitstock -= 1>><</link>><<lstress>>
			<br>
		<<else>>
			<span class="red">There is no fruit left to throw right now.</span>
			<br>
		<</if>>
	<</if>>
	<<if $cat gte 6 and ($pillory_tenant.ass + $pillory_tenant.genital) gte 2>>
		<<link [[Groom prisoner|Cliff Street Prisoner]]>><<pass 5>><<set $phase to 1>><</link>>
		<br>
	<</if>>
	<<if $pillory_tenant.lowerexposed gte 2 and $stress gte 4000 and $trauma gte (($traumamax / 5) * 1)>>
		<<link [[Spank prisoner|Cliff Street Prisoner]]>><<pass 5>><<set $phase to 2>><</link>><<llstress>><<ltrauma>>
		<br>
	<</if>>
	<<if $pillory_tenant.lowerexposed lte 1 or (($pronoun is "m" and $pillory_tenant.upperexposed lte 1) or ($pronoun is "f" and $pillory_tenant.upperexposed lte 2)) and $stress gte 7000 and $trauma gte (($traumamax / 5) * 2)>>
		<<link [[Expose prisoner|Cliff Street Prisoner]]>><<pass 2>><<set $phase to 3>><</link>><<llstress>>
		<br>
	<</if>>
<<else>>
	<<if $stress gte 4000>>
		<<if $pillory_tenant.fruitstock gte 1>>
			<<link [[Throw Fruit|Cliff Street Prisoner]]>><<pass 2>><<set $phase to 0>><<stress -6>><<set $pillory_tenant.fruitstock -= 1>><</link>><<lstress>>
			<br>
		<<else>>
			<span class="red">There is no fruit left to throw right now.</span>
			<br>
		<</if>>
	<</if>>
		<<if $pillory_tenant.lowerexposed lte 1 and ($stress gte 6000 or $trauma gte (($traumamax / 5) * 2))>>
		<<link [[Wedgie|Cliff Street Prisoner]]>><<pass 5>><<set $phase to 4>><<stress -12>><</link>><<llstress>><<ltrauma>>
		<br>
	<</if>>
<</if>>
<<endevent>>
<<link [[Leave|Cliff Street]]>><</link>>
<br>