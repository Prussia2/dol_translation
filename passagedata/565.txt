<<effects>>

<<if $submissive gte 1150>>
	"I-I don't mean to be rude," you say. "But can I see? No one will know."
<<elseif $submissive lte 850>>
	"Don't be coy," you say. "You want people to see. You've got a good body, it's only natural you'd want to show it off. So show me."
<<else>>
	"Let me see," you say. "No one will know."
<</if>>
<br><br>

<<if $NPCName[$NPCNameList.indexOf("Mason")].lust gte 10>>
	<<earnFeat "Mason's Secret">>
	<<He>> blushes. "F-Fine," <<he>> says, voice quivering with anger as much as embarrassment. "If that's what it'll take to get you to stop bothering me."
	<br><br>

	<<He>> hesitates despite <<his>> words, then slowly moves <<his>> arms aside. You dive under the water for a better look at <<his>> sleek and toned body.

	<<if $pronoun is "f">>
		<<if $NPCList[0].penis isnot "none">>
			Your eyes are drawn to <<his>> <span class="lewd"><<print $NPCList[0].breastsdesc>> and <<print $NPCList[0].penisdesc>>.</span> You're used to seeing their contours beneath <<his>> tight swimsuit, but seeing them exposed in the water is something else.
		<<else>>
			Your eyes are drawn to <<his>> <span class="lewd"><<print $NPCList[0].breastsdesc>>.</span> You're used to seeing their contours beneath <<his>> tight swimsuit, but seeing them exposed in the water is something else.
		<</if>>
	<<else>>
		<<if $NPCList[0].penis isnot "none">>
			Your eyes are drawn to <<his>> <span class="lewd"><<print $NPCList[0].penisdesc>>.</span> You're used to seeing its contours beneath <<his>> tight swimsuit, but seeing it exposed in the water is something else.
		<<else>>
			Your eyes are drawn to <<his>> <span class="lewd">pussy.</span> You're used to seeing its contours beneath <<his>> tight swimsuit, but seeing it exposed in the water is something else.
		<</if>>
	<</if>>
	<br><br>

	<<He>> covers <<himself>> again when you come up for air. "I-I hope you're satisfied," <<he>> says. "Now leave me to finish my laps in peace." <<He>> turn and swims away.
	<br><br>

	<<link [[Next|Lake Depths]]>><<endevent>><</link>>
	<br>

<<else>>
	<<He>> blushes. "Th-that's no request to make of your teacher!" <<he>> says. "Now please, let me get back to my laps." You get a nice look at <<his>> toned ass near the surface of the water as <<he>> swims away.
	<br><br>

	<<link [[Next|Lake Depths]]>><<endevent>><</link>>
	<br>
<</if>>