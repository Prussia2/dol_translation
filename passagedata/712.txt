<<effects>>

<<if $submissive gte 1150>>
	"Y-you should raise your dogs better," you say. "I was almost hurt."
<<elseif $submissive lte 850>>
	"Keep your dogs on a tighter leash," you say. "Next time it could get hurt."
<<else>>
	"Have you not trained your dogs at all?" you say. "I could have been hurt."
<</if>>

<br><br>

"I'm sorry," Alex says, holding up <<his>> hands. "But working with animals has its dangers. Be more careful in the future."
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>