<<effects>>

<<endevent>><<npc River>><<person1>>
River sees Whitney and you, dropping <<his>> pen in shock. "Whitney!" <<He>> shouts, though <<his>> voice trembles a little. "That's assault! Get out, right now!"
<br><br>
<<endevent>><<npc Whitney>><<person1>>
Whitney doesn't argue. <<He>> grins as <<he>> pulls away from you, stands, and swaggers from the room.
<br><br>
<<set $whitneymaths to "sent">>

<<link [[Next|Maths Lesson]]>><<endevent>><</link>>