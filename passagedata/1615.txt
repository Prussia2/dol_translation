<<set $outside to 0>><<set $location to "cafe">><<effects>>

You turn your back on Avery. You hear <<him>> take a step after you, but doesn't follow further. You walk down the red carpet. Several photographers snap pictures. You recognise Niki among them.
<br><br>

<<endevent>><<npc Sam>><<person1>> Sam meets you at the entrance, wearing <<his>> <<if $pronoun is "m">>suit and tie<<else>>red dress<</if>>.
<br><br>

"This is everything I've wished for," <<he>> says. "Thank you for making it come true." <<He>> hugs you, eliciting a greater cheer from the audience.
<<gglove>><<set $NPCName[$NPCNameList.indexOf("Sam")].love += 3>>
<br><br>

<<endevent>>
<<link [[Next|Chef Opening 6]]>><</link>>
<br>