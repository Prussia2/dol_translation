<<effects>>

<<if $submissive gte 1150>>
	"D-do you like it?" you ask. "You can look closer if you want."
<<elseif $submissive lte 850>>
	"If you want to look," you say. "Just ask."
<<else>>
	"Do you like it?" you ask.
<</if>>
You turn so that <<he>> gets a clear view.
<br><br>
<<if $rng gte 81>>
	Caught out, <<he>> blushes and turns away.
<<elseif $rng gte 61>>
	<<He>> seems impressed.
<<elseif $rng gte 41>>
	<<He>> seems interested, though you notice <<him>> stelaing glances at other parts of you.
<<elseif $rng gte 21>>
	<<He>> compliments you before moving on.
<<else>>
	<<He>> smiles and continues on.
<</if>>
<br><br>

<<endevent>>
<<destinationeventend>>