<<set $outside to 0>><<effects>>

You turn to leave. Kylar grasps your wrist. "Wh-where are you going?" <<he>> says. "Stay with me."<<gksuspicion>><<npcincr Kylar rage 1>>
<br><br>

<<kylaroptions>>
<<link [[Shake off|Kylar Shake]]>><<npcincr Kylar rage 5>><</link>><<ggksuspicion>>
<br>