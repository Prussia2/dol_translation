<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
"No," you say. "I didn't do this to myself, other people did."
<br><br>
<<He>> shakes <<his>> head. "It's a cry for help," <<he>> says. "But you're in the right place. We won't let you hurt yourself any longer." <<He>> glances at the clock. "That's all for today. Be well."
<br><br>
<<endevent>>
<<link [[Next|Asylum]]>><</link>>
<br>