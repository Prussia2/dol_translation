<<effects>>

<<if $submissive gte 1150>>
	"Pl-please don't," you say. "You shouldn't do this."
<<elseif $submissive lte 850>>
	"Get off," you say. "Unless you want a beating."
<<else>>
	"Get away from me," you say.
<</if>>
<br><br>


Alex removes <<his>> hand, and you climb to your feet. "I'm sorry," <<he>> says. "I misread the situation." <<He>> avoids eye contact as <<he>> climbs to <<his>> feet and returns to work.
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>