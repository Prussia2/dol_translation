<<set $outside to 0>><<set $location to "home">><<effects>>
Not wanting to repeat the utter shame of even one person seeing you naked, let alone a whole group, you shake your head. You try to close the door again, but a foot blocks you from doing so.
<br><br>
<<if $submissive gte 1150>>
	"P-please leave me alone," you say.
<<elseif $submissive lte 850>>
	"I said no," you say.
<<else>>
	"I can't," you say.
<</if>>
<br><br>
The <<person>> pauses a moment, then moves <<his>> foot. Your heart races as you close the door and sink to the ground. You hear the crowd arguing outside, but they soon head back to their rooms.
<br><br>
<<link [[Next|Bedroom]]>><<endevent>><</link>>
<br>