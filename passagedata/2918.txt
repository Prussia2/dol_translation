<<effects>>
<<if $mathschance is 0>>
	The maths competition booklet lies on the desk in front of you. The task ahead is daunting.
<<elseif $mathschance lte 20>>
	The maths competition booklet and the little progress you've made lie on the desk in front of you.
<<elseif $mathschance lte 40>>
	Your groundwork for the maths competition lies on the desk in front of you.
<<elseif $mathschance lte 60>>
	Your solution for the maths competition lies on the desk in front of you. There's a lot of room for improvement.
<<elseif $mathschance lte 80>>
	Your solution for the maths competition lies on the desk in front of you. It's not complete, but it represents an impressive effort.
<<elseif $mathschancestart lte 99>>
	Your solution for the maths competition lies on the desk in front of you. You know it's wrong, but it's impressive all the same.
<<else>>
	Your solution for the maths competition lies on the desk in front of you. You weren't supposed to solve it. You're confident you have.
<</if>>
<br><br>
You have <span class="gold">$mathsinfo</span> insights.
<br>
<<if $stimdealerknown is 1>>
	You have <span class="gold">$mathsstim</span> stimulants.
	<br>
<</if>>
<br>
<<link [[Work on solution (2:00)|Maths Project Work]]>><<set $mathschance += 1>><<mathsskill 2>><<pass 120>><<stress 12>><</link>><<gstress>><<gmaths>>
<br>
<<if $mathsstim gte 1>>
	<<link [[Use stimulant (2:00)|Maths Project Stim]]>><<set $mathschance += 20>><<set $mathsstim -= 1>><<set $mathsstimused += 1>><<mathsskill 40>><<pass 120>><</link>><<gggmaths>>
	<br>
<</if>>
<<if $mathsinfo gte 1>>
	<<link [[Use a mathematical insight (2:00)|Maths Project Work]]>><<set $mathsinfo -= 1>><<set $mathschance += 10>><<mathsskill 40>><<pass 120>><</link>><<ggmaths>>
	<br>
<</if>>
<br>
<<if $location is "home">>
	<<link [[Stop|Bedroom]]>><</link>>
	<br>
<<else>>
	<<link [[Stop|School Library]]>><</link>>
	<br>
<</if>>