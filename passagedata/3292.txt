<<set $outside to 1>><<effects>>
The <<person2>><<person>> follows you into the alley where you <<nervously>> remove your clothing.
<<if $worn.under_lower.type.includes("naked") and $worn.genitals.type.includes("chastity")>>
	<<His>> eyes widen when <<he>> sees your $worn.genitals.name.
<<elseif $worn.under_lower.type.includes("naked")>>
	<<His>> eyes widen when <<he>> sees your lack of underwear.
<</if>>
<<covered>> You make the exchange.
<br><br>
You phone an ambulance as the <<person>> leers at your body. You pass the phone back and hold your hand out for your clothes. <<He>> hesitates, but the dog barks behind <<him>> and <<he>> reconsiders betraying you.
<br><br>
<<He>> leaves you in the alley with the dog and the <<person1>><<personcomma>> where you wait for the ambulance to arrive. You don't have to wait long. Two paramedics rush between the buildings and put <<him>> on a stretcher. They carry <<him>> to the waiting ambulance, and the dog follows. It gives you one last grateful bark then leaps into the vehicle as the doors shut.
<<ltrauma>><<trauma -6>>
<br><br>
<<famegood 10>>
<<endevent>>
<<clotheson>>
<<destinationeventend>>