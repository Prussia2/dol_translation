<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "domus">>
You flee out the house, the dog snapping at your heels. It doesn't chase you beyond the front door, content to continue barking into the night.
<<beastescape>>
<br><br>
<<link [[Next|Domus Street]]>><</link>>
<br>