<<set $outside to 1>><<set $location to "lake">><<effects>><<lakeeffects>>
You grip <<his>> collar and pull <<him>> close. <<His>> eyes widen in surprise, but then close. You feel <<him>> shiver. The bullies on the rock stare, dumbfounded. One throws an empty bottle to the ground in disgust. They drop from the rock and walk away.
<<promiscuity1>>
You pull away from the <<personstop>> <<Hes>> blushing and tearing up. "Th-thank you." <<He>> turns and runs.
<<famegood 20>>
<br><br>
<<endevent>>
<<link [[Hang out (0:30)|Lake Hang]]>><<pass 30>><<stress -6>><<status 1>><</link>><<gcool>><<lstress>>
<br>
<<link [[Stop|Lake Shore]]>><</link>>
<br>