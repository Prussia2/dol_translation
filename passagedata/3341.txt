<<set $bus to "elk">><<set $outside to 0>><<effects>>
You search the detritus for anything usable. You find strips of cloth, torn from clothes and furniture. You add them to the sheet rope.
<br><br>
You examine your handiwork. You drop one end over the side of the building.
<<set $tower_sheet += 1>>
<<if $tower_sheet gte 12>>
	<span class="green">It falls straight into the mist, and keeps going.</span> You think it's long enough.
<<elseif $tower_sheet gte 9>>
	<span class="teal">It kisses the mist, but you suspect there's still a drop beneath.</span>
<<elseif $tower_sheet gte 6>>
	<span class="lblue">It drops a long way, but it's not enough to reach the mist.</span>
<<elseif $tower_sheet gte 3>>
	<span class="blue">It drops a few stories, but it needs to be much longer.</span>
<<else>>
	<span class="purple">You have a long way to go.</span>
<</if>>
<br><br>
<<link [[Next|Monster Tower]]>><</link>>
<br>