<<effects>>
<<He>> recoils in pain, giving you the chance you need to escape. <<tearful>> you flee to the bathroom and lock the door behind you. There's a window you can use to escape should <<he>> persist.
<br><br>
<<pass 30>>
After thirty minutes pass without event, you quietly head back to your bedroom. You feel relief as you see it empty.
<br><br>
<<clotheson>>
<<endcombat>>
[[Next|Bedroom]]