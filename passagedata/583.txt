<<set $outside to 0>><<set $location to "lake_ruin">><<underwater>><<effects>><<lakeeffects>>
You are deep in the lake ruin, in a small room with a plinth.
<<if $moonstate is "evening" and $hour gte 21 or $moonstate is "morning" and $hour lte 6>>
Red moonlight filters down from cracks in the ceiling. There isn't much, but the plinth seems to glow where the light hits it.
	<<if $moonEvent is true and $parasite.left_ear.name is undefined and $parasite.right_ear.name is undefined and $worn.genitals.name is "gold chastity belt">>
		<<unset $moonEvent>><<set $worn.genitals.integrity *= 0.5>><<control 50>>
		You're drawn to the plinth, and feel an odd sense of peace.
		<<gggcontrol>>
	<</if>>
<</if>>
<<if $moonstate is "evening" and $hour gte 21 and ($parasite.left_ear.name is "slime" or $parasite.right_ear.name is "slime") or $moonstate is "morning" and $hour lte 6 and ($parasite.left_ear.name is "slime" or $parasite.right_ear.name is "slime")>>
	<<unset $moonEvent>>
	You feel it pull to you.
	<br><br>

	<<link [[Sit on the plinth|Lake Ruin Sit]]>><</link>>
	<br>
<<elseif $museumAntiques.antiques.antiqueivorynecklace is "notFound">>
	An ivory necklace sits atop it. It's studded with blue jewels.
	<br><br>
	<<link [[Take it|Lake Ruin Deep]]>><<set $antiquemoney += 2000>><<museumAntiqueStatus "antiqueivorynecklace" "found">><</link>>
	<br>
<<elseif $tentacledisable is "f" and $hallucinations gte 2>>
	You look around the barren room. You don't see anything besides the plinth, but you hear a faint slithering sound. <span class="blue">Something's watching you.</span>
	<br><br>
	<<link [[Search|Lake Ruin Deep Offer]]>><<loxygen>><</link>>
	<br>
<<else>>
	<br><br>
<</if>>
<<link [[Leave|Lake Ruin Deep]]>><</link>>
<br>