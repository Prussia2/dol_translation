<<effects>>

<<if $athletics gte random(1, 1000)>>

	You run around a corner, and then another. You stop at the next, peeking back and looking out for your pursuers. <span class="green">You see no one.</span>
	<br><br>

	You lean against the wall, your whole body shaking.
	<br><br>

	<<link [[Next|Commercial alleyways]]>><<endevent>><</link>>
	<br>

<<else>>

	You run around a corner, and then another. You stop at the next, peeking back and looking out for your pursuers. You think you're in the clear, <span class="red">until a hand taps your shoulder.</span>
	<br><br>

	You turn. It's the students. <<covered>>
	<br><br>

	<<link [[Next|Road Ex Examine]]>><</link>>
	<br>

<</if>>