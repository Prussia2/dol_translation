<<set $outside to 0>><<effects>>

<<if $clothes_choice and $clothes_choice_previous>>
	<<if $clothes_choice is $clothes_choice_previous>>
		<<shopbuy "under_upper">>
	<<else>>
		<<shopbuy "under_upper" "reset">>
	<</if>>
<<else>>
	<<shopbuy "under_upper" "reset">>
<</if>>
<<clothingShop "under_upper" "outfits">>
<br>

<<link [[Back to shop|Clothing Shop]]>><<unset $clothes_choice>><</link>>