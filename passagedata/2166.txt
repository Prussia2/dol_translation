<<set $outside to 0>><<set $location to "hospital">><<effects>>

This small maintenance cupboard contains a variety of cleaning implements and chemicals. You peek through the door. There's no one there.
<br><br>

<<if $stress gte 10000>>
<<passouthospital>>
<<else>>

<<link [[Leave the cupboard (0:01)->Paediatric ward]]>><<pass 1>><</link>>
<br>

<</if>>