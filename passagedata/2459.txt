<<set $outside to 1>><<set $location to "town">><<effects>>

You brace for the impact as the fruit comes flying towards you.

	<<if $rng gte 81>>

	It smashes into your face, hurting more than you expected. Some people applaud.
	<<gtrauma>><<gstress>><<trauma 6>><<stress 6>><<set $pain += 40>>
	<br><br>

	<<else>>

	Their aim is off and it thuds into the pillory beside your head, smashing into pieces but leaving you mostly clean.
	<br><br>

	<</if>>

<<endevent>>

<<link [[Next|Pillory]]>><<pass 1 hour>><<set $pillorytime -= 1>><</link>>
<br>