<<effects>>

You and Robin have a pleasant chat while eating. "I'm going to wait in the classroom," <<he>> says when you're done. "I don't like being late." <<He>> hugs you.
<br><br>

<<if $NPCName[$NPCNameList.indexOf("Kylar")].state is "active">>
Kylar watches from across the canteen.
<<gksuspicion>><<npcincr Kylar rage 1>>
<br><br>
<</if>>
<<endevent>>

<<link [[Next|Canteen]]>><</link>>
<br>