<<effects>>

You remain silent. "Spooky," says the voice.

<<if $rng gte 51>>
	A <<person1>><<person>> appears at the top of the stairwell, not so afraid as to avoid investigating. "It's a ghost! I guess being incorporeal means you can't wear clothes." The others stick their heads over the railing, looking down at you. <<covered>><<fameexhibitionism 4>>
	<br><br>

	You walk up the stairs. At least you'll feel less trapped up there. The <<person>> grasps your arm and pulls you in front of <<himcomma>> exposing you to <<his>> friends.
	<br><br>

	"So you're some kind of pervert, exposing yourself for the thrill?" <<he>> whispers. "Don't worry. We'll help get you off."
	<br><br>

	<<link [[Next|Street Ex Gang Rape]]>><<set $molestationstart to 1>><</link>>
	<br>

<<else>>
	"Let's not investigate any dark stairwells tonight." You hear the group walk away, and breathe a sigh of relief.
	<br><br>
	<<endevent>>
	<<destinationeventend>>
<</if>>