<<set $outside to 1>><<set $location to "forest">><<effects>><<set $bus to "danube">>
<<if $molestationstart is 1>>
	<<set $molestationstart to 0>>
	<<molested>>
	<<controlloss>>
	<<set $combat to 1>>
	<<swarminit "snakes" "swarms" "slithering" "slither" "keep back" 10 0>>
	<<set $timer to 20>>
<</if>>
<<swarmeffects>>
<<swarm>>
<<swarmactions>>
<<if $timer lte 0>>
	<span id="next"><<link [[Next|Forest Snake Swarm Finish]]>><</link>></span><<nexttext>>
<<else>>
	<span id="next"><<link "Next">><<script>>state.display(state.active.title, null)<</script>><</link>></span><<nexttext>>
<</if>>