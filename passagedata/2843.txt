<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $rng gte 81>>

You ignore the pair, and keep studying. The <<person1>><<person>> glances over <<his>> shoulder and spots you. <<He>> grabs the <<person2>><<persons>> arm and they both grin at you through the glass. <<covered>> You feel your face flush.
<<gstress>><<garousal>><<stress 3>><<arousal 600>><<fameexhibitionism 2>>
<br><br>

They move on, still smiling.
<br><br>

<<else>>

You ignore the pair, and keep studying. They don't notice you.
<br><br>

<</if>>

<<endevent>>
<<link [[Next|School Library]]>><</link>>
<br>