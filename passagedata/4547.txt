<<set $outside to 1>><<set $location to "town">><<effects>>
<<if $rng gte 81>>
	You drop to your knees and look through the hole. You see another alley, similar to this one, on the other side. Hearing the voices grow louder, you start crawling through the hole.
	<<set $stripintegrity to 10>><<set $stripobject to "broken brick">><<stripobject>>
	<br><br>
	It goes well at first. You get your top half through, but your hips are a little too wide. No matter how hard you push, you cannot do it. You try to wiggle backwards, but find you can't move in either direction. You're stuck!
	<br><br>
	<<generate1>><<generate2>>You realise the voices have stopped just before something pinches your butt.
	<br><br>
	<<link [[Next|Industrial Ex Hole Rape]]>><<set $molestationstart to 1>><</link>>
<<else>>
	You drop to your knees and look through the hole. You see another alley, similar to this one, on the other side. Hearing the voices grow louder, you start crawling through the hole.
	<<set $stripintegrity to 10>><<set $stripobject to "broken brick">><<stripobject>>
	A few moments later and you're free on the other side.
	<br><br>
	<<endevent>>
	<<industrialeventend>>
<</if>>