<<effects>><<set $location to "sewers">><<set $outside to 0>>

You follow Morgan's recipe as best you can and prepare a fresh batch of "tea" while Morgan prepares the "scones." You're relieved that the tea has actual tea in it.
<br><br>

<<link [[Next|Sewers Morgan]]>><</link>>
<br>