<<effects>>

You hum a gentle tune.
<<if $tending gte random(1, 1200)>>

	<<pass 2 hours>>

	<span class="green">The tendrils looses their grasp,</span> and you pull yourself free.
	<br><br>

	You continue your work. Now wary, you stay far from the denser foliage. After another two hours the <<person>> seems satisfied, and hands you £50.
	<<physique 6>>
	<br><br>
	<<link [[Next|Danube Street]]>><<set $money += 5000>><</link>>
	<br>

<<else>>
	<span class="red">It's no use.</span> The tendrils tighten their grip.
	<br><br>

	<<link [[Next|Danube Tentacles]]>><<set $molestationstart to 1>><</link>>
	<br>

<</if>>