<<set $outside to 0>><<set $location to "town">><<effects>>

<<if $enemyarousal gte $enemyarousalmax>>

<<beastejaculation>>

Satisfied, the dog returns to its kennel.

<<tearful>> you rise to your feet.
<br><br>

<<clotheson>>
<<endcombat>>

No one answered the door.
<br><br>

<<link [[Break in|Domus House Lock]]>><</link>>
<br>
<<link [[Go to the Next House (0:02)|Domus House Knock]]>><<pass 2>><</link>>
<br>
<<link [[Stop|Domus Street]]>><</link>>
<br>

<<elseif $enemyhealth lte 0>>

The dog flees back to its kennel.

<<tearful>> you rise to your feet.
<br><br>

<<clotheson>>
<<endcombat>>

No one answered the door.
<br><br>

<<link [[Break in|Domus House Lock]]>><</link>>
<br>
<<link [[Go to the Next House (0:02)|Domus House Knock]]>><<pass 2>><</link>>
<br>
<<link [[Stop|Domus Street]]>><</link>>
<br>

<<elseif $alarm is 1>><<set $rescued += 1>>
	<<if $rapeavoid is 0 and $NPCList[0].stance is "top">><<famebestiality 1>>

	<<generate1>><<person1>> "I'm coming!" you hear a <<person>> shout as <<he>> runs down the road. <<He>> leans against the gate and stares wide-eyed at you.
	<br><br>

		<<if $submissive lte 850>>
			"Stop staring idiot, get this dog off me," you say.
		<<elseif $submissive gte 1150>>
			"H-help me. I don't wanna be a doggy bitch," you say.
		<<else>>
			"Please help," you say.
		<</if>>
	<br><br>

	<<He>> breaks free from <<his>> stupor and grabs the dog by the collar. <<He>> hauls it away from you and shoos it back to its kennel. <<tearful>> you struggle to your feet.

		<<if $submissive lte 850>>
		"Don't you dare tell anyone about this," you say.
		<<elseif $submissive gte 1150>>
		"Don't let anyone know," you mumble, staring at your feet.
		<<else>>
		"Please don't tell anyone," you say.
		<</if>>
	<<He>> nods, but something tells you <<he>> won't keep <<his>> promise.
	<br><br>

	<<elseif $exposed gte 1>><<fameexhibitionism 1>>

	<<generate1>><<person1>> "I'm coming!" you hear a <<person>> shout as <<he>> runs down the road. <<He>> leans against the gate and stares wide-eyed at you. You feel <<his>> eyes bore into your <<lewdness>> as <<he>> opens the gate.
	<br><br>

		<<if $submissive lte 850>>
		"Stop staring idiot, get this dog off me," you say.
		<<elseif $submissive gte 1150>>
		"H-help me," you say.
		<<else>>
		"Please help," you say.
		<</if>>
	<br><br>

	<<He>> breaks free from <<his>> stupor and grabs the dog by the collar. <<He>> hauls it away from you and shoos it back to its kennel. <<tearful>> you struggle to your feet.

		<<if $submissive lte 850>>
		"I said stop staring!" you say, face reddening.
		<<elseif $submissive gte 1150>>
		"Please don't look," you say.
		<<else>>
		You cover yourself to escape <<his>> gaze.
		<</if>>
	<<He>> looks away from you, but keeps stealing glances as <<he>> leaves the premises.
	<br><br>

	<<else>>

	<<generate1>><<person1>> "I'm coming!" you hear a <<person>> shout as <<he>> runs down the road. <<He>> leans against the gate and stares wide-eyed at you.
	<br><br>

		<<if $submissive lte 850>>
		"Stop staring idiot, get this dog off me," you say.
		<<elseif $submissive gte 1150>>
		"H-help me," you say.
		<<else>>
		"Please help," you say.
		<</if>>
	<br><br>

	<<He>> breaks free from <<his>> stupor and grabs the dog by the collar. <<He>> hauls it away from you and shoos it back to its kennel. <<tearful>> you struggle to your feet. "Are you OK?" <<he>> enquires.
	<br><br>

		<<if $submissive lte 850>>
		"Took you long enough," you say. Satisfied that <<his>> job is done, <<he>> leaves the premises.
		<<elseif $submissive gte 1150>>
		"Yes, thank you," you say. Satisfied that <<his>> job is done, <<he>> leaves the premises.
		<<else>>
		"Yes thanks," you say. Satisfied that <<his>> job is done, <<he>> leaves the premises.
		<</if>>
	<br><br>

	<</if>>
<<clotheson>>
<<endcombat>>

<<domusquick>>
<</if>>