<<set $outside to 0>><<effects>><<set $lock to 200>>

There's a small office building beside the stables. The door is locked. Must be where the money is kept.
<br><br>

	<<if $skulduggery gte $lock>>
	<span class="green">The lock looks easy to pick.</span>
	<br><br>

	<<link [[Pick the door (0:10)|Riding School Register Sneak]]>><<pass 10>><<crimeup 1>><</link>><<crime>>
	<br>
	<<else>>
	<span class="red">The lock looks beyond your ability to pick.</span><<skulduggeryrequired>>
	<br><br>
	<</if>>

<<link [[Leave|Riding School]]>><</link>>
<br>