<<effects>>
<<pass 60>>
<<if $endear gte 30>>
	"I'm glad I brought you," Avery says on your way to the car, already retrieved by a servant. "You left a good impression." You climb in.
<<elseif $endear gte 0>>
	"I'll forgive you this time," Avery says on your way to the car, already retrieved by a servant. "But you need to pretend you aren't trash. Your behaviour reflects on me."
<<else>>
	Avery is silent on the way back to the car, already retrieved by a servant. You climb in. Avery grips the wheel as you drive away.
<</if>>
<br><br>

<<link [[Next|Avery Date 4]]>><</link>>
<br>