<<set $outside to 0>><<set $location to "strip_club">><<effects>>

<<if $submissive gte 1150>>
	"I'm sorry <<sircomma>>" you say. "I have to work the bar."
<<elseif $submissive lte 850>>
	"Are you blind?" you ask. "I'm busy. Go bother one of the dancers."
<<else>>
	"I just work the bar," you say. "One of the dancers might be willing to entertain you."
<</if>>
<br><br>

<<if $rng gte 61>>
You try to turn away from the <<personcomma>> but <<he>> <span class="red">grasps your arm.</span> "Listen here you little <<bitchcomma>>" <<he>> spits. "No one refuses me." <<He>> glances at security. "You think they'll help you? They won't. They know who I am. Get your ass outside."
<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
<br><br>

<<link [[Comply|Bartending VIP Patron Comply]]>><</link>>
<br>
<<link [[Refuse|Bartending VIP Patron Refuse 2]]>><</link>>
<br>

<<else>>
You turn away from the <<personstop>> <<He>> insults you under <<his>> breath, but leaves without further fuss. <<tipreceive>>
<br><br>

<<if !$per_npc.bartend and random(1, 2) is 2>>
	<<saveNPC 0 bartend>>
<</if>>

<<link [[Next|Strip Club]]>><<endevent>><<pass 30>><</link>>
<br>

<</if>>