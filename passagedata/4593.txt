<<effects>>

<<if $athletics gte random(1, 1000)>>

	You <span class="green">jump</span> in the air, over the water, before landing with grace. Someone applauds.
	<br><br>

	The <<person>> lines up another shot, but a <<generate2>><<person2>><<person>> snatches it from <<person1>><<his>> hands, before berating <<him>> for stealing.
	<br><br>
	<<endevent>>
	<<cream_walk>>

<<else>>

	You jump in the air, <span class="red">but not high enough.</span> The water splashes you, <span class="pink">clearing away much of your cream.</span>
	<<cream_damage 15>><<ggarousal>><<arousal 1000>>
	<br><br>

	The <<person>> lines up another shot, but a <<generate2>><<person2>><<person>> snatches it from <<person1>><<his>> hands, before berating <<him>> for stealing.
	<br><br>
	<<endevent>>
	<<cream_walk>>

<</if>>