<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You walk over to Robin.

<<if $submissive gte 1150>>
"S-stop," you say. "That's my friend."
<<elseif $submissive lte 850>>
"Hey, losers." You shout.
<<else>>
You stand between Robin and the delinquents.
<</if>>
<br><br>
One of them, a <<generatey1>><<person1>><<personcomma>> laughs. "Aww, is this your girlfriend? How cute." <<His>> fist clenches. "Looks like I get to show both of you your place."
<br><br>

<<link [[Next|Canteen Lunch Fight]]>><<set $fightstart to 1>><</link>>
<br>