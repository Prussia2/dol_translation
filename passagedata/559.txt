<<effects>>

You smile at <<himcomma>> and try to look confused.

<<if $submissive gte 1150>>
	"In the rain?" you say. "I like it too."
<<elseif $submissive lte 850>>
	"Swimming in the rain isn't that weird," you say. "I like it myself. It's bracing."
<<else>>
	"Don't worry," you say. "I like swimming in the rain too."
<</if>>
<br><br>

<<He>> looks confused for just a moment. "Th-thank you for understanding," <<he>> says, still covering <<himself>> beneath the water. "I need to get back to my laps. Keep practising, you've shown promising skill."
<br><br>

<<link [[Next|Lake Depths]]>><<endevent>><</link>>
<br>