<<effects>>

You shove Eden away from you. <<He>> doesn't give up at first, and continues to paw at you, but your persistence wards <<him>> off before long.
<br><br>

<<He>> stomps back into the cabin.
<br><br>

<<link [[Next|Eden Cabin]]>><<endevent>><</link>>
<br>