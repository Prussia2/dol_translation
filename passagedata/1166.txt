<<effects>>

<<if $rng gte 51 or $bestialitydisable isnot "f">>
	<<if $livestock_harper is undefined>>
		<<set $livestock_harper to 1>>
		You're led down the small lane in the direction of the barn, but take a detour down a smaller path to the side, shielded by hedgerows. You bend around a corner, and come to a small stone building.
		<br><br>

		The interior is dark, but clean. Farm equipment and machinery is neatly tucked to one side, leaving space in the middle for a table and pair of chairs.
		<br><br>

		<<if $hospitalintro>>
			<<endevent>><<npc Harper>><<person1>>A <<if $pronoun is "m">>man<<else>>woman<</if>> wearing a white medical coat sits on one, jotting on a sheet of paper on the table. You recognise <<himstop>>
			<br><br>
			<<He>> stands up as Remy enters. "I'm Doctor Harper," <<he>> says. "Thank you for allowing this. We're not ready for human trials."
			<br>
			"It's my pleasure," Remy responds, taking the doctor's hand. "Good to meet you in person."
			<br>
			"This must be the specimen." Harper says, <<his>> eyes flickering over your body. <<if $uncomfortable.nude is true>><<covered>><</if>> It's as if <<he>> doesn't recognise you.
			<br><br>
			<<set $genderknown.pushUnique("Harper")>>
			Remy turns to you. <<endevent>><<npc Remy>><<person1>>"Do what the good doctor says," <<he>> says as Harper tugs on a pair of gloves. "It won't hurt. It might even be good for you."
			<br><br>

			<<link [[Obey|Livestock Job Harper Obey]]>><<sub 1>><<npcincr Remy dom 1>><<npcincr Harper dom 1>><<livestock_obey 5>><<transform cow 1>><</link>><<ggobey>>
			<br>
			<<link [[Resist|Livestock Job Harper Resist]]>><<def 1>><<npcincr Remy dom -1>><<npcincr Harper dom -1>><<livestock_obey -5>><</link>><<llobey>>
			<br>

		<<else>>
			<<set $hospitalinfo to "livestock">>
			<<endevent>><<npc Harper>><<person1>>A <<if $pronoun is "m">>man<<else>>woman<</if>> wearing a white medical coat sits on one, jotting on a sheet of paper on the table.
			<br><br>
			<<He>> stands up as Remy enters. "I'm Doctor Harper," <<he>> says. <<He>> looks young for a doctor. Can't be older than 25. "Thank you for allowing this. We're not ready for human trials."
			<br>
			"It's my pleasure," Remy responds, taking the doctor's hand. "Good to meet you in person."
			<br>
			"This must be the specimen." The doctor says, <<his>> eyes flickering over your body. <<if $uncomfortable.nude is true>><<covered>><</if>>
			<br><br>

			Remy turns to you. <<endevent>><<npc Remy>><<person1>>"Do what the good doctor says," <<he>> says as Harper tugs on a pair of gloves. "It won't hurt. It might even be good for you."
			<br><br>

			<<link [[Obey|Livestock Job Harper Obey]]>><<sub 1>><<npcincr Remy dom 1>><<npcincr Harper dom 1>><<livestock_obey 5>><<transform cow 1>><</link>><<ggobey>>
			<br>
			<<link [[Resist|Livestock Job Harper Resist]]>><<def 1>><<npcincr Remy dom -1>><<npcincr Harper dom -1>><<livestock_obey -5>><</link>><<llobey>>
			<br>

		<</if>>
	<<elseif $livestock_harper is 1>>
		<<set $livestock_harper to 2>>
		You're led down the small lane in the direction of the barn, but take a detour down the smaller path to the side, shielded by hedgerows. You bend around a corner, and come to the small stone building.
		<br><br>

		The interior is as dark as ever. Farm equipment and machinery is neatly tucked to one side, leaving space in the middle for a table and pair of chairs.
		<br><br>

		<<endevent>><<npc Harper>><<person1>>
		Doctor Harper sits on one. "Ah," <<he>> says, smiling. "Our specimen." Remy hands <<him>> your leash, then stands behind you.
		<br><br>

		"Today we're going to try a little technique that should improve your yield." <<His>> voice has taken on a soothing quality. "Help you be the <<if $player.gender_appearance is "m">>bull<<else>>cow<</if>> you were meant to be."
		<br><br>

		<<He>> lifts a pen from the table beside <<himcomma>> and holds it in front of you. Remy's hands rest on the side of your head, gentle but firm, forcing you to look at the doctor. "Focus on the pen," Harper says. "There's nothing in the universe but this pen, and the sound of my voice."
		<br><br>

		<<link [[This feels nice|Livestock Job Hypnosis]]>><<control 15>><<livestock_obey 5>><<transform cow 2>><<trauma -6>><<stress -6>><<awareness -10>><</link>><<ggobey>><<ggcontrol>><<llawareness>><<ltrauma>><<lstress>>
		<br>
		<<link [[Resist|Livestock Job Hypnosis Resist]]>><<livestock_obey 5>><<stress 6>><</link>><<llobey>><<gstress>>
		<br>

	<<elseif $livestock_harper is 2>>
		<<set $livestock_harper to 3>>
		You're led down the small lane toward the barn. You don't stop there however, instead continuing further until you come to a rustic farmhouse.
		<br><br>

		The interior is cramped, but clean and well-decorated. You're lead into a living room lined by chairs. They're occupied by <<people>>.
		<br><br>

		Doctor Harper rises from the seat closest to the door. <<endevent>><<npc Harper>><<person1>><<He>> holds out <<his>> hand. "Again, thank you," <<he>> says as Remy takes it. "I've brought my colleagues." <<He>> gestures at other figures seated around the room. They look a bit rough to be the doctor's colleagues. "They wish to observe the effects."
		<br>
		Remy nods. "Then I'll leave you to it."
		<br>
		Harper pulls a vial of pink liquid from <<his>> coat pocket. "Could you remain a moment? It needs to drink this, and I have little experience handling beasts."
		<br><br>

		<<endevent>><<npc Remy>><<person1>>
		<<if $worn.face.type.includes("gag")>>
			Remy gropes at the back of your head as the doctor opens the vial. <span class="lblue">Your muzzle comes loose.</span> A sweet smell permeates the room.
			<<set $worn.face.type.push("broken")>>
			<<faceruined>>
			<br><br>

			<<endevent>><<npc Harper>><<person1>>The doctor presses it against your lips.
		<<elseif $livestock_obey gte 30>>
			Remy and <<his>> farmhands stand nearby as the doctor opens the vial. A sweet smell permeats the room. <<endevent>><<npc Harper>><<person1>><<He>> presses it against your lips.
		<<else>>
			Remy has two of <<his>> farmhands restrain you. The doctor opens the vial. A sweet smell permeates the room. <<endevent>><<npc Harper>><<person1>><<He>> presses it against your lips.
		<</if>>
		<br><br>

		<<link [[Obey|Livestock Job Harper 2 Obey]]>><<sub 1>><<npcincr Remy dom 1>><<npcincr Harper dom 1>><<livestock_obey 5>><<transform cow 1>><</link>><<ggobey>><<ggarousal>>
		<br>
		<<link [[Deceive|Livestock Job Harper 2 Deceive]]>><</link>><<skulduggerydifficulty 1 1000>><<garousal>>
		<br>
		<<link [[Struggle|Livestock Job Harper 2 Struggle]]>><<def 1>><<livestock_obey -5>><</link>><<physiquedifficulty 1 $physiquemax>><<llobey>>
		<br>

	<<elseif $livestock_harper is 3>>
		<<set $livestock_harper to 1>>
		You're led down the small lane toward the barn. You walk past it, continuing further until you come to the rustic farmhouse.
		<br><br>

		You're led into the cramped living room. Chairs line the edge. They're occupied by <<people>>.
		<br><br>

		<<endevent>><<npc Harper>><<person1>>Doctor Harper rises from the seat closest to the door. "Again, thank you," <<he>> says. "My colleagues and I are here to observe the long-term effects of the the treatment."
		<br><br>

		Remy hands <<him>> your leash. "Of course. May you find it enlightening. I'll be in the barn should you have trouble."
		<br><br>

		Harper tries to pull you into the middle of the room.
		<br><br>

		<<link [[Obey|Livestock Job Harper 3 Obey]]>><<sub 1>><<livestock_obey 5>><<transform cow 1>><</link>><<ggobey>>
		<br>
		<<link [[Struggle|Livestock Job Harper 3 Struggle]]>><<def 1>><<livestock_obey -5>><</link>><<physiquedifficulty 1 $physiquemax>><<llobey>>
		<br>

	<</if>>
<<else>>
	<<if $livestock_pig is undefined>>
		<<set $livestock_pig to 1>>
		<<endevent>><<npc Niki>><<person1>>
		Remy leads you down the small lane towards the barn. You arrive in front of it, but then turn toward the second, smaller barn opposite.
		<br><br>
		<<set $genderknown.pushUnique("Niki")>>
		The interior is dusky. A mat has been placed in the centre, bathed in light from a couple of studio lamps and surrounded by stools. They're unoccupied. A camera rests on a tripod. A young <<if $pronoun is "m">>man<<else>>woman<</if>> with a stick in <<his>> mouth stares into it.
		<br><br>

		<<if $niki_seen is undefined>>
			<<He>> pulls away from the camera and looks at you. "Niki, I take it?" Remy says beside you.
			<br><br>
			The young <<if $pronoun is "m">>man<<else>>woman<</if>> nods. "We're ready when you are."
		<<else>>
			<<He>> pulls away from the camera and stares at you. You recognise <<himstop>> It's Niki the photographer. <<if $uncomfortable.nude is true>><<covered>><</if>>
			<br><br>
			"Niki, I take it?" Remy says beside you.
			<br><br>
			Niki hesitates, <<his>> eyes lingering on you, then nods. "We're ready when you are."
		<</if>>
		<<set $niki_seen to "farm">>
		<br><br>

		<<endevent>><<npc Remy>><<person1>>
		"Good." Remy turns to you. "We're just doing a practise shoot today, but you need to be good all the same."
		<br><br>

		<<link [[Next|Livestock Job Niki]]>><</link>>
		<br>
	<<elseif $livestock_pig is 1>>
		<<set $livestock_pig to 2>>
		Remy leads you down the small lane towards the barn. You arrive in front of it, but then are turned toward the second, smaller barn opposite.
		<br><br>

		The interior is dusky. The mat, lights and camera from before are set up. Niki operates the camera. A pig already sits before it, waiting beside a hay bale.
		<br><br>

		"Lie on the mat," Remy whispers. "There's a good <<girl>>."
		<br><br>

		<<link [[Obey|Livestock Job Pig Obey]]>><<set $farm_phase to 0>><<livestock_obey 5>><<sub 1>><<transform cow 1>><</link>><<ggobey>>
		<br>
		<<link [[Struggle|Livestock Job Pig Struggle]]>><<set $farm_phase to 0>><<livestock_obey -5>><<def 1>><</link>><<physiquedifficulty 1 $physiquemax>><<llobey>>
		<br>
		<<link [[Plead with Niki|Livestock Job Pig Plead]]>><<set $farm_phase to 0>><<livestock_obey -1>><</link>><<lobey>>
		<br>

	<<elseif $livestock_pig is 2>>
		<<set $livestock_pig to 1>>
		Remy leads you down the small lane towards the barn. You arrive in front of it, but then are turned toward the second, smaller barn opposite.
		<br><br>

		The interior is dusky. The mat, lights and camera from before are set up. A ring of stools sits around the edge. They're occupied by <<people>>. Three pigs sit on the mat, waiting beside a hay bale. Niki operates the camera.
		<br><br>

		"Lie on the mat," Remy Whispers. "There's a good <<girl>>."
		<br><br>

		<<link [[Obey|Livestock Job Pig Obey]]>><<set $farm_phase to 1>><<livestock_obey 5>><<sub 1>><<transform cow 1>><</link>><<ggobey>>
		<br>
		<<link [[Struggle|Livestock Job Pig Struggle]]>><<set $farm_phase to 1>><<livestock_obey -5>><<def 1>><</link>><<physiquedifficulty 1 $physiquemax>><<llobey>>
		<br>
		<<link [[Plead with Niki|Livestock Job Pig Plead]]>><<set $farm_phase to 1>><<livestock_obey -1>><</link>><<lobey>>
		<br>

	<</if>>
<</if>>
<br><br>