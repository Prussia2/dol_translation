<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>

<<if $temple_harassed gte 2>>

You tell Jordan you were attacked. <<He>> listens without a word, only speaking once you're finished. "Thank you child," <<he>> says. "You've handled this appropriately. I'll see the offenders are disciplined."
<br><br>

<<He>> doesn't explain the discipline <<he>> means, but <<he>> sounds sincere.
<<ggstress>><<lltrauma>><<stress 12>><<trauma -12>>
<br><br>

<<else>>

You tell Jordan you were attacked. <<He>> listens without a word, only speaking once you're finished. "Thank you child," <<he>> says. "You've handled this appropriately. I'll see the offender is disciplined."
<br><br>

<<He>> doesn't explain the discipline <<he>> means, but <<he>> sounds sincere.
<<gstress>><<ltrauma>><<stress 6>><<trauma -6>>
<br><br>

<</if>>

<<set $temple_harassed to 0>>
<<endevent>>

<<link [[Next|Temple]]>><</link>>
<br>