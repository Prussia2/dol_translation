<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
<<npc Jordan>><<person1>>You take a seat at the end of a pew as Jordan reads holy scripture. <<His>> melodic voice calms you.
<br><br>
You are called to the front for communion. You kneel in front of Jordan as the other <<if $pronoun is "m">>monks<<else>>nuns<</if>> watch. <<He>> pushes a small piece of bread into your mouth, and holds a cup in front of you. You take a sip of the bitter fluid and make way for the next person.
<br><br>
Jordan finishes with a prayer.
<br><br>
<<endevent>>
<<link [[Next|Temple]]>><</link>>
<br>