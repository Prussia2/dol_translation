<<effects>><<set $location to "sewers">><<set $outside to 0>>
<<set $sewerschased to 0>>
<<set $sewersevent to random(5, 12)>>
<<if $rng gte 51>>
	<<npc Morgan>><<person1>>You hide behind the rubble. Morgan enters the tunnel. <<He>> searches behind some of the rubble, but becomes impatient and walks away, babbling. You're safe for now.
	<<lstress>><<stress -6>>
	<br><br>
	<<endevent>>
	<<destinationsewers>>
<<else>>
	<<npc Morgan>><<person1>>You hide behind the rubble. Morgan enters the tunnel. A piece of stone breaks and falls behind you, drawing <<his>> attention. <<He>> spots you, grasps your hair and tugs you from your hiding place.
	<br><br>
	<<morganoptions>>
<</if>>