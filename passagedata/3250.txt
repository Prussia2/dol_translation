<<effects>>
You stroll to a stop, cross your arms and turn around to face <<himstop>> You hit <<him>> with a solid stare.
<br><br>
The <<person>> looks embarrassed, unable to meet your eye. <<He>> mutters something and shuffles away.
<<lstress>>
<br><br>
<<stress -3>>
<<endevent>>
<<destinationeventend>>