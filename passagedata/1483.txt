<<set $outside to 0>><<set $location to "brothel">><<effects>>

<<if $phase is 0>>

<<He>> eyes up your body as <<he>> speaks.

"Nice. Very nice. Our clientele will love you.

I'd like to offer you work looking after our guests. You saw how it works on the way up." <<He>> pauses before continuing. "Oh right, you probably want details. You can dress however you want, but you'll get more attention if you dress appropriately. Use a stage to show yourself off. You might make some change there, but your real aim should be to get hired for some private time. We provide rooms for that purpose."
<br><br>

"You'll be expected to take care of yourself. If someone tries to rape you just deal with it and don't bother me. I'm not your <<if $pronoun is "m">>daddy<<else>>mummy<</if>>."
<br><br>

"We take a 40% cut of anything you earn, and don't you dare think you can cheat us either. You'll have access to a dressing room, naturally. I think I've covered everything. So, interested?"
<br><br>

<<link [[Yes|Brothel Intro2]]>><<set $phase to 1>><<set $brotheljob to 1>><</link>>
<br>
<<link [[No|Brothel Intro2]]>><<set $phase to 2>><</link>>
<br>
<<link [[I need more information|Brothel Intro2]]>><<set $phase to 3>><</link>>
<br>

<<elseif $phase is 1>>
"Good! Come by whenever you feel like putting your body to work. I'll let the other staff know you're joining them." <<He>> rises to <<his>> feet and guides you to the top of the stairs.

<<if $id is 0>>
<<He>> speaks as you descend. "One more thing. If you're having trouble with your legal status we can fix you a fake ID. For a price, obviously." You turn to look at <<him>> but <<he>> is already gone.
<<else>>
<<He>> doesn't say anything as <<he>> leaves you to descend.
<</if>>
<br><br>

<<link [[Next|Brothel]]>><<endevent>><</link>>
<<elseif $phase is 2>>
"That's a shame. Well, you know where to find us if you change your mind. A body like yours is sure to sell well." <<He>> rises to <<his>> feet and guides you to the top of the stairs.

<<if $id is 0>>
<<He>> speaks as you descend. "One more thing. If you're having trouble with your legal status we can fix you a fake ID. For a price, obviously." You turn to look at <<him>> but <<he>> is already gone.
<<else>>
<<He>> doesn't say anything as <<he>> leaves you to descend.
<</if>>
<br><br>

<<link [[Next|Brothel]]>><<endevent>><</link>>

<<elseif $phase is 3>>

	<<He>> blinks, then laughs. "Don't be coy. You've seen what's going on downstairs, darling. I'm sure you can work out the rest on your own."
	<br><br>

<<link [[I'm interested|Brothel Intro2]]>><<set $phase to 1>><<set $brotheljob to 1>><</link>>
<br>
<<link [[I'm not interested|Brothel Intro2]]>><<set $phase to 2>><</link>>
<br>

<</if>>