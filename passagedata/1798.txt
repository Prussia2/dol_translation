<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "danube">>

You enter the mansion. You hear voices and the clinking of glasses further in. "There's some platters in the kitchen," <<he>> gestures to the door on your right. "Take each in turn and offer them to the guests.
<<if $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
	"
	<br><br>
	Noticing your bound <<if $leftarm is "bound" or $rightarm is "bound">>arms<</if>><<if ($leftarm is "bound" or $rightarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>, <<he>> gives you a strange smile. "It's not that kind of party.<<if $leftarm is "bound" or $rightarm is "bound">> You'll definitely need your hands.<</if>>"
	<br>
	<<He>> moves behind you and a moment later <span class="green">your limbs fall free.</span>
	<<unbind>>
	<br><br>
	"Now if you'll excuse me," <<he>> goes on. "
<<else>>
	Now if you'll excuse me,
<</if>>
I need to get back to being a good <<if $pronoun is "m">>host<<else>>hostess<</if>>."
<br><br>

<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $danger gte (9900 - $allure)>>

You begin the job as instructed. Most guests pay you little attention, few even so much as look at you as they take from the proffered tray or politely decline the offer. An hour into the job however, a <<person1>><<person>> takes an interest in you. You notice <<him>> watching you from across the room.
<br><br>
<<pass 1 hour>>
You offer <<him>> a glass of champagne. "No thanks, darling." As you turn to leave, <<he>> wraps an arm around your waist and grabs your <<bottom>> with <<his>> free hand.
<br><br>

<<link [[Endure it|Danube Party Endure]]>><<stress 6>><<trauma 6>><<set $submissive += 1>><</link>><<gtrauma>><<gstress>>
<br>
<<link [[Struggle free|Danube Party Struggle]]>><<stress -12>><<trauma -6>><<set $submissive -= 1>><</link>><<ltrauma>><<lstress>>
<br>

<<else>>
You begin the job as instructed. No one pays you much attention, few even so much as look at you as they take from the proffered tray or politely decline the offer. After two hours the party seems to be winding down, and the <<person>> approaches you. "Thanks for the help. Here." <<He>> hands you £20.
<br><br>

<<pass 2 hours>>
<<set $money += 2000>>

<<link [[Next|Danube Street]]>><<endevent>><</link>>
<br>

<</if>>