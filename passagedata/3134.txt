<<effects>>
You decide to run and hide.
<<if $athletics gte random(500, 1000)>>
	The security guard attempts to fully grab you, but you're able to avoid <<him>> just enough to get away.
	You run to the rooftop with <<him>> shouting in the distance.
	<br><br>
	<<link [[Roof|Commercial rooftops]]>><<endevent>><</link>>
<<else>>
	However, before being able to get past the security guard, <<he>> pulls on you hard enough for you to fall right on your ass.
	<br><br>
	"You'll regret trying to run from me," <<He>> says with a grin on <<his>> face. "There aren't any cameras here."
	<br><br>
	<<link [[Next|Clothing Shop Night Guard Molestation]]>><<set $molestationstart to 1>><</link>>
<</if>>