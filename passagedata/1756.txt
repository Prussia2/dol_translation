<<set $outside to 0>><<set $location to "dance_studio">><<effects>>
<<if $phase is 0>>
	Your reticence encourages <<him>> to be more bold. <<He>> takes every opportunity to grope and fondle you as you try your best to ignore <<him>> and focus. You breathe a sigh of relief when Charlie announces that it's time for something different.
	<<arousal 600>>
	<br><br>
<<elseif $phase is 1>>
	You push <<him>> away from you, causing <<him>> to trip over <<his>> feet and falls loudly to the floor. Cursing, <<he>> stands up as Charlie announces that it's time to change partners. The <<person2>><<person>> gives you one last angry look before being matched up with an elderly <<if $pronoun is "m">>woman<<else>>man<</if>>;<span class="pink"> the entire class seems irritated with your behaviour.</span>
	<<set $dancestudioanger += 1>>
	<br><br>
<<elseif $phase is 2>>
	<<promiscuity2>>Not wanting to be outdone, you return <<his>> fondling with your own. Hiding the lewd touching under the veneer of dance practise, you grope each other right in the middle of the room, the rest of the class ignorant of the lechery occurring right in front of them. By the time Charlie calls for a stop, both you and the <<person>> are breathing far more heavily than anyone else.
	<br><br>
<</if>>
<<link [[Next|Dancing Lesson]]>><<endevent>><</link>>