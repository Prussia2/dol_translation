<<effects>>

<<if $enemyarousal gte $enemyarousalmax>>

	<<beastejaculation>>

	The <<farm_text pig>> climbs off you, satisfied.
	
	<<if $farm_work.pig.monster is true>>
		The others cheer.
		<br>
		<<if random(1, 2) is 2>>
			"Not so great now, are you?"
		<<else>>
			"I hope <<pshe>> knows <<pher>> place."
		<</if>>
		<br>
		<<if random(1, 2) is 2>>
			"You should've been rougher."
		<<else>>
			"You should be rougher next time."
		<</if>>
		<br>
		<<if random(1, 2) is 2>>
			"We should keep <<phim>> here."
		<<else>>
			"My turn next time."
		<</if>>
	<</if>><<lrespect>><<farm_pigs -1>>
	<br><br>

	<<tearful>> you stagger from the pen. You close the gate behind you.
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<link [[Next|Farm Work]]>><</link>>
	<br>

<<elseif $enemyhealth lte 0>>

	Theh <<farm_text pig>> backs away from you, cowed. The others follow suit.<<grespect>><<farm_pigs 1>>
	<br><br>

	<<tearful>> you leave the pen. You close the gate behind you.
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<link [[Next|Farm Work]]>><</link>>
	<br>

<<else>><<set $rescued += 1>>

	"I'm coming!" It's Alex. The <<farm_text pig>> backs away from you at the sound. The others follow suit.<<lrespect>><<farm_pigs -1>>
	<br><br>
	
	<<clotheson>>
	<<endcombat>>
	
	
	<<npc Alex>><<person1>>
	Alex arrives at the edge of the pen, and laughs. <<He>> hauls <<himself>> over with one arm, and helps you to your feet. "Pigs can be like that," <<he>> says. "Don't be afraid to say down the law. A firm word usually does the trick."
	<br><br>

	<<if $exposed gte 1 and $farm_naked isnot 1>>
		Alex hands you some towels, then returns to <<his>> work.<<towelup>>
	<<else>>
		Alex hands you some towels to wipe of the mud, then returns to <<his>> work.
	<</if>>
	<<if $exposed gte 1>>
		<<glust>><<npcincr Alex lust 1>>
	<</if>>
	<br><br>

	<<link [[Next|Farm Work]]>><</link>>
	<br>

<</if>>