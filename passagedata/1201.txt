<<effects>>

Remy storms over and grasps your leash. <<endevent>><<npc Remy>><<person1>>"Refusing even this simple task," <<he>> says. "Bad <<girl>>." <<He>> yanks your leash.
<br><br>

Niki interrupts. "It's alright. I got what I needed. We're ready for the proper shoots."
<br><br>

"You hear that," Remy says in a low voice. "You're gonna get bred by a pig. Maybe that will teach you to behave."
<<gstress>><<stress 6>>
<br><br>

<<endevent>><<npc Remy>><<person1>>Niki ignores <<himcomma>> instead focusing on packing up <<endevent>><<npc Niki>><<person1>><<his>> things.
<br><br>

<<link [[Next|Livestock Job End]]>><<endevent>><</link>>
<br>