<<effects>>

"Boo!" you say. Alex jumps, and almost trips, at the sound of your voice.
<br><br>

"Fuck," <<he>> says, steadying <<himself>>. You almost gave me a heart attack.
<br><br>

<<if $submissive gte 1150>>
	"You don't need to be sneaky if you want to look," you say. "I'm right here." You walk past <<him>>, and return to work.
<<elseif $submissive lte 850>>
	"That's what you get for being a sneaky perv," you say. "If you want to look, go ahead and look." You march past <<him>>, and return to work.
<<else>>
	"What you looking at?" you say. "Something more interesting than me? I'm surprised." You walk past <<him>>, and return to work.
<</if>>
<br><br>


<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>