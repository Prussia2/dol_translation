<<set $outside to 1>><<set $location to "park">><<effects>><<set $bus to "park">>
<<if $athletics gte random(1, 1000)>>
	You <span class="green">successfully</span> climb onto the plinth. The lichen is attached firm, but you manage to pry it free with some effort. A few heads turn but no one bothers you until you hear a voice shout from across the park. "Oi!" It's a <<npc Avery>><<generate2>><<person2>><<person>> dressed as a police officer. "Get down from there or I'll have your hide."
	<<gstress>><<stress 3>>
	<br><br>
	<<link [[Explain yourself|Park Lichen Explain]]>><</link>>
	<br>
	<<link [[Run|Park Lichen Run]]>><</link>>
	<br>
<<else>>
	You reach up and try to lift your body, but your arms give way and you tumble to the ground.
	<<gstress>><<gpain>><<stress 3>><<pain 6>>
	<br><br>
	<<npc Avery>><<person1>>You hear a <<if $pronoun is "m">>man's<<else>>woman's<</if>> laughter. Before you can recover a pair of arms picks you up by your shoulders. Not stopping there, <<he>> holds you by the hips and helps lift you with surprising strength. It's enough for you to clamber up.
	<<if $worn.lower.skirt is 1>>
		You feel your face flush as you realise <<he>> could have seen up your $worn.lower.name. If <<he>> did <<he>> gives no sign.
	<</if>>
	<<He>> smiles at you. "Why'd you want to go up there anyway?"
	<br><br>
	<<link [[Honest|Park Lichen Honest]]>><</link>>
	<br>
	<<link [[Evasive|Park Lichen Evasive]]>><</link>>
	<br>
<</if>>