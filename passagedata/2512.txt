<<set $outside to 0>><<schooleffects>><<effects>>
<<flaunting>> you <<nervously>> remove your clothing. You make eye contact with as many boys as you can as you undress. Many blush and look away, but a crowd still forms around you. You're soon bare in front of them.
<<covered>><<exhibitionism5>>
<<if $player.gender is $player.gender_appearance>>
	<<strip>>
	The <<person>> is blushing as <<he>> snatches your clothes away. "If you want them back," <<he>> says. "You better do what we say."
	<br><br>
	The other boys shout suggestions.
	<<if $worn.neck.collared is 1>>
		The <<person>> ignores them, and picks up your leash. "Perfect," <<he>> says. "Time for walkies. Get on your knees like a good dog."
		<br><br>

		<<link [[Get on knees (0:10)|School Boy's Knees]]>><<pass 10>><<status -10>><<trauma 6>><<stress 6>><</link>><<lcool>><<gtrauma>><<gstress>>
		<br>
		<<link [[Refuse|School Boy's Naked Refuse]]>><</link>>
		<br>
	<<else>>
		"Everyone get your phones," <<he>> says to the boys around <<himstop>> "Make sure you get lots of evidence." <<He>> looks back at you. "Time for your punishment. Bend over."
		<br><br>
		<<link [[Bend over (0:10)|School Boy's Bend]]>><<pass 10>><<status -10>><<trauma 6>><<stress 6>><</link>><<lcool>><<gtrauma>><<gstress>>
		<br>
		<<link [[Refuse|School Boy's Naked Refuse]]>><</link>>
		<br>
	<</if>>
<<elseif $player.gender is "h">>
	<<if $schoolrep.crossdress gte 5 and !$worn.face.type.includes("mask")>>
		The boys break into excitement as your <<genitals_are>> revealed. "I knew it," the <<person>> says, reaching forward to seize your clothes. "You're the school freak."
		<br><br>
	<<elseif $schoolrep.crossdress gte 3 and !$worn.face.type.includes("mask")>>
		The boys break into excitement as they see your <<genitals>>. "You're the freak I keep hearing about," the <<person>> says, reaching forward to seize your clothes. "I didn't believe it."
		<br><br>
	<<else>>
		The boys gape at your <<genitals>>, too stunned to speak. The <<person>> is first to shake free of <<his>> stupour. "You're a freak," <<he>> says as <<he>> seizes your clothes.
		<br><br>
	<</if>>
	<<if $promiscuity gte 35>>
		<<link [[Seduce|School Boy's Crossdress Seduce]]>><</link>><<promiscuous3>>
		<br>
	<</if>>
	<<link [[Say that's just the way you are|School Boy's Crossdress Honest]]>><</link>>
	<br>
	<<link [[Blame a curse (0:05)|School Boy's Herm Explain]]>><<set $phase to 0>><<pass 5>><<status -10>><</link>><<lcool>>
	<br>
	<<link [[Blame strange science (0:05)|School Boy's Herm Explain]]>><<set $phase to 1>><<pass 5>><<status -10>><</link>><<lcool>>
	<br>
	
	<<schoolrep herm 1>>
<<else>>
	<<strip>>
	<<if $schoolrep.crossdress gte 5 and !$worn.face.type.includes("mask")>>
		"So the rumours are true," the <<person>> says, satisfied. "You're really a boy. Why do you do it? Are you just a pervert?"
	<<elseif $schoolrep.crossdress gte 3 and !$worn.face.type.includes("mask")>>
		The <<person>> gapes. "You're the crossdresser I keep hearing about," <<he>> says. "Why do you do it? Are you just a pervert?"
	<<else>>
		The <<person>> gapes. "You're not a girl?" <<he>> says. "Why were you dressed as one?"
	<</if>>
	<br><br>
	<<if $promiscuity gte 35>>
		<<link [[Seduce|School Boy's Crossdress Seduce]]>><</link>><<promiscuous3>>
		<br>
	<</if>>
	<<link [[Say you like dressing as a girl|School Boy's Crossdress Honest]]>><</link>>
	<br>
	<<link [[Say you were forced to (0:05)|School Boy's Crossdress Forced]]>><<pass 5>><<status -10>><</link>><<lcool>>
	<br>
	<<schoolrep crossdress 1>>
<</if>>