<<set $outside to 1>><<set $location to "sea">><<water>><<effects>><<set $bus to "searocks">>
You are swimming in the sea near some rocks, west of the beach.
<<if $daystate is "night">>
	<<if $weather is "rain">>
		The dark water is hammered by rain all around you.
	<<else>>
		The water is dark and foreboding.
	<</if>>
<<else>>
	<<if $weather is "rain">>
		The surface is agitated by the deluge of rain.
	<<else>>
		The water is calm and clear.
	<</if>>
<</if>>
<br><br>
<<if $seaswim is 1>>
	<<set $seaswim to 0>>
	You spend some time swimming.
	<<physique 3>><<swimmingskilluse>>
<</if>>
<<if $exposed gte 1>>
	You keep your <<lewdness>> hidden beneath the surface of the water.
	<br><br>
<</if>>
<<if $stress gte 10000>>
	<<passoutbeach>>
<<else>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure) and $eventskip is 0>>
		<<eventssea>>
	<<else>>
		<<link [[Climb onto the rocks (0:02)|Rocks]]>><<pass 2>><</link>>
		<br>
		<<link [[Practise swimming (0:30)|Sea Rocks]]>><<pass 30>><<stress -6>><<tiredness 6>><<set $seaswim to 1>><<slimeEventEnd>><</link>><<gswimming>><<lstress>><<gtiredness>>
		<br>
		<<link [[Swim out to sea (0:10)|Sea]]>><<pass 10>><<tiredness 2>><<set $sea to 0>><</link>><<gtiredness>><<swimmingdifficulty 1 100>>
		<br><br>
		<<seabeach>><<swimmingdifficultytext0>>
		<br>
	<</if>>
<</if>>
<<set $eventskip to 0>>