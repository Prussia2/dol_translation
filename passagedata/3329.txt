<<if $athletics gte random(1, 800)>>
	As your body gains speed, you kick off the ground and perform an elegant cartwheel, sticking the landing safely and with a smile.
	<br><br>
	<<endevent>>
	<<destinationeventend>>
<<else>>
	<<pain 18>><<bruise face>>
	Your attempt to avoid the fall only makes matters worse. You land on your face hard, bruising your cheek and twisting your ankle.
	<br><br>
	<<link [[Get up|Street Heel Trip Finish]]>><</link>>
	<br>
<</if>>