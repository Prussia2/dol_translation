<<set $outside to 1>><<set $location to "town">><<effects>>
<<if $submissive gte 1150>>
	"I'm sorry!" you say, placing the <<person1>><<wallet>> on the table and looking between them. "My eyesight isn't very good and I thought it was mine."
<<elseif $submissive lte 850>>
	"Actually," you say, placing the <<person1>><<wallet>> on the table. "I was just returning it to you. Someone else took it and I chased them down. You shouldn't make accusations like that."
<<else>>
	"I work here," you say, placing the <<person1>><<wallet>> on the table. "It's against company policy to let patrons leave their valuables where they can be easily taken. You should be more careful in the future."
<</if>>
<br><br>
<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>
	The <<person>> turns to the <<person2>><<person>> "You always cause a scene."
	<br><br>
	"Cause a scene? I suppose I should keep quiet if I think you're being robbed then?"
	<br><br>
	<span class="green">You leave them to their argument, taking the <<person1>><<wallet>> with you.</span>
	<br><br>
	<<connudatuswallet>>
	<<endevent>>
<<else>>
	<span class="red">The <<person>> picks up the <<wallet>> and tucks it away.</span> "Sure," <<he>> says, looking at you with suspicion. You'll have to leave empty-handed this time.
	<br><br>
	<<endevent>>
	<<link [[Next|Connudatus Street]]>><</link>>
	<br>
<</if>>