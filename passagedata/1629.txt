<<set $outside to 0>><<set $location to "cafe">><<effects>>

Laughter fills the cafe. You're not sure how many knew all along, and how many think you're joking.
<br><br>
<<npc Quinn>><<person1>>The mayor pushes you aside and assumes control of the podium. "Our chef here enjoys a joke," <<he>> says. "How they prepare the cream is their secret, but I assure you it's made from the finest local produce."
<br><br>

The mayor turns to you, away from the microphone. "Good thinking," <<he>> says as <<he>> passes. "You know how to build a legend."
<br><br>

You imagine they'll be talking about this for a while. You join the party. What do you want to do?
<br><br>

<<chefspeechoptions>>