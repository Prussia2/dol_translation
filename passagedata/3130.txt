<<effects>>
<<ShowUnderEquip "over">>
<<ShowUnderEquip "normal">>
<<if $skulduggery lt 100>>
	A security guard watches the entrance. There's no other way out, so you put everything back before leaving.
	<<clothingReset>>
<<else>>
	<<set $rng to random(0,100)>>
	<<if $crime gte 50000 or ($crime gte 40000 and $rng * 20 gte $skulduggery) or ($crime gte 25000 and $rng * 10 gte $skulduggery)
	or ($crime gte 10000 and $rng * 5 gte $skulduggery)>>
		<<generate1>><<person1>>
		A security guard watches the entrance, and as you try to stroll by, <<he>> stops you.
		<br><br>
		"So. I've been keeping an eye on you," <<he>> continues, "I'm letting you go this time, but you're going to be
		banned from the premises for a while. Hopefully this will help you learn why you shouldn't steal clothes."
		<<if $clothingShop.banCount lt 4>>
			<<set $clothingShop.ban to 7>>
		<<elseif $clothingShop.banCount lt 8>>
			<<set $clothingShop.ban to 10>>
		<<else>>
			<<set $clothingShop.ban to 14>>
		<</if>>
		<<crime>>
		<<set $clothingShop.banCount++>>
	<<else>>
		A security guard watches the entrance, but you stroll by without arousing suspicion.
		<<crime>>
	<</if>>
	<<set _crime to ($tryOn.value / 100)>>
	<<crimeup _crime>>
<</if>>
<br><br>
<<link [[Next|Shopping Centre Top]]>><<set $tryOn.autoReset to true>><<endevent>><</link>>