<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<if $phase is 0>>
	You slowly creep up to the outbuilding, and manage to deliver the note and get away without incident. It takes a few minutes however, and when you arrive back at class Doren chides you for lollygagging.
	<br><br>
	<<link [[Next|English Lesson]]>><<endevent>><</link>>
<<elseif $phase is 1>>
	<<if $rng gte 51>>
		You briskly walk up to the outbuilding, and manage to deliver the note and get away without incident.
		<br><br>
		<<link [[Next|English Lesson]]>><<endevent>><</link>>
	<<else>>
		You briskly walk up to the outbuilding and deliver the note, but turn to find the dog advancing on you.
		<br><br>
		<<if $bestialitydisable is "t">>
			You manage to flee back to the classroom, though not unscathed.<<beastescape>>
			<br><br>
			<<integritycheck>><<towelup>>
			<<link [[Next|English Lesson]]>><</link>>
		<<else>>
			<<link [[Next|English Outbuilding Molestation]]>><<set $molestationstart to 1>><</link>>
			<br>
		<</if>>
	<</if>>
<</if>>