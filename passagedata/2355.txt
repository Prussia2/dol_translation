<<set $outside to 0>><<set $location to "museum">><<effects>>

<<npc Winter>><<person1>>"Remarkable," Winter says, turning the compass and observing the spin. "You found it out at sea? We're fortunate it survived so intact."
<br><br>

<<link [[Next|Museum]]>><<endevent>><</link>>
<br>