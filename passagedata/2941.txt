<<effects>><<set $location to "moor">>

You struggle against the bindings wrapped tight around your arms. You're persistent. Something comes loose. You keep struggling until you can pull your arms free.
<br><br>

<<unbind>>You feel the boot of the car, but can find no way to open it.
<br><br>

<<link [[Wait|Maths Boot Wait]]>><</link>>
<br>