<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
<<ejaculation>>

"That weren't so bad, were it?" the <<person>> laughs. <<He>> turns on the engine and continues the task.
<br><br>

<<tearful>> you climb aboard the larger vessel. You help the crew manage the spring lines and swing the vessel around to a successful landing.
<br><br>

<<if $pain gte 40>>
One of the sailors notices your tears and asks what's wrong, but you assure them you're fine.
<br><br>
<</if>>

<<if $phase isnot 1>><<clothesontowel>><</if>>
<<endcombat>>

<<dockoptions>>

<<else>>

You shove them off the boat, turn on the engine, and leave them to flounder. You'll finish the task yourself.
<br><br>

<<if $phase isnot 1>><<clothesontowel>><</if>>
<<endcombat>>

<<tearful>> you climb aboard the larger vessel. Feeling energised by your victory, you guide both the captain and crew to a successful landing.
<br><br>

<<generate1>><<person1>>A <<person>> asks where the other two are. You tell them they fell off the boat. Your colleagues are still laughing when the pair make it back to the docks, soaked and shivering.
<br><br>
<<endevent>>
<<dockoptions>>

<</if>>