<<effects>>

<<if $phase is 0>>
	<<set $drunk += 60>>
	You take the proferred bottle, and take a swig. It's a bitter spirit.
<<else>>
	Alex puts the bottle away.
<</if>>
"It'll be more work," <<he>> continues. "But we'll be making a steady profit. You'll get <span class="gold">£30</span> per hour from now on."
<br><br>
<<He>> points over the hedge beyond the cleared field, at the tangled mess beyond. "That's the next target." <<He>> drops off the fence. "I'm eager to get started."<<gglove>><<npcincr Alex love 3>>
<br><br>

<<set $farm.wage to 3000>>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>