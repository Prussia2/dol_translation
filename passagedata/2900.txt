<<set $outside to 1>><<set $location to "school">><<schooleffects>><<effects>>

You walk over to the <<person1>><<personcomma>> and sit beside <<himstop>>

<<if $phase is 0>>
	<<He>> gulps down <<his>> mouthful. "H-hi," <<he>> says.
<<elseif $phase is 1>>
	<<His>> smile broadens. "Hi," <<he>> says.
<<elseif $phase is 2>>
	<<He>> turns to you. "Y-yes?" <<he>> mumbles.
<<elseif $phase is 3>>
	<<He>> smirks at you. "Hey babe," <<he>> says. "What can I do for you?"
<<else>>
	<<He>> stares at <<his>> lap with great intensity.
<</if>>
<br><br>

<<if $submissive gte 1150>>
"Y-your friend said you like me," you say, leaning closer. <<He>> must be able to feel your breath on <<his>> face. "You're cute too."
<<elseif $submissive lte 850>>
"I saw you looking at me," you say, leaning closer. <<He>> must be able to feel your breath on <<his>> face. "Don't worry, I'm used to it."
<<else>>
"Your friend told me a secret," you say, leaning closer. <<He>> must be able to feel your breath on <<his>> face. "I like you too."
<</if>>
<<promiscuity1>>

<<if $phase is 0>>
	<<He>> opens <<his>> mouth to speak, but burps instead. <<He>> clutches <<his>> face in embarrassment and runs for safety. <<His>> friend follows, laughing.
<<elseif $phase is 1>>
	<<He>> holds out a hand, which you take. You sit together for a little while as <<his>> friend gently pokes fun at you both.
<<elseif $phase is 2>>
	"Th-thank you," <<he>> says. <<He>> gives you a nervous smile. <<His>> friend sits on <<his>> other side, and encourages <<him>> to be more assertive through the use of pokes. <<person2>><<He>> has mild success.
<<elseif $phase is 3>>
	"Of course you like me," <<he>> says, grinning. <<Hes>> leaning at an odd angle. "Who wouldn't like-" <<he>> begins, before losing <<his>> balance. <<He>> tumbles to the ground, <<his>> legs flailing in the air.
	<<if $pronoun is "f">>
	<<His>> skirt tumbles down <<his>> legs and you glimpse <<his>> plain white panties.
	<</if>>
	<br><br>

	<<He>> climbs to <<his>> feet and tries to laugh it off, but is too embarrassed to look at you.
<<else>>
	<<He>> nods at <<his>> lap while turning a deeper shade of red. You try to bring <<him>> out of <<his>> shell, but <<hes>> too shy. <<His>> friend finds <<his>> awkwardness humorous.
<</if>>
<br><br>

<<endevent>>
<<playground>>