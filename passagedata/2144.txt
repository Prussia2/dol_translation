<<effects>>

<<if $enemyhealth lte 0>>
	<<beastwound>>
	<<if $combatTrain.length gt 0>>
		The <<beasttype>> recoils in pain and fear, but another is eager for a go.
		<<beastNEWinit $combatTrain.numberPerTrain[0] $combatTrain.beastTypes[0]>>
		<<combatTrainAdvance>>
		<br><br>
		[[Next|Sea Dolphins]]
	<<else>>
		The <<beasttype>> recoils in pain and fear.
		<br><br>

		The wolves disappear beneath the surface, headed somewhere unknown.
		<br><br>

		<<clotheson>>
		<<endcombat>>

		<<tearful>> you swim down the tunnel, helped along by the flow of water. The tunnel dips and you rush downward, exiting into a storm drain.
		<<set $stress -= 1000>>
		<br><br>

		<<link [[Next|Drain Water]]>><<set $bus to "commercialdrain">><</link>>
	<</if>>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<<beastejaculation>>
	<<if $combatTrain.length gt 0>>
		Satisfied, the <<beasttype>> moves and another takes its turn.
		<<beastNEWinit $combatTrain.numberPerTrain[0] $combatTrain.beastTypes[0]>>
		<<combatTrainAdvance>>
		<br><br>
		[[Next|Sea Dolphins]]
	<<else>>
		Satisfied, the <<beasttype>> moves away from you.
		<br><br>

		The wolves disappear beneath the surface, headed somewhere unknown.
		<br><br>

		<<clotheson>>
		<<endcombat>>

		<<tearful>> you swim down the tunnel, helped along by the flow of water. The tunnel dips and you rush downward, exiting into a storm drain.
		<<set $stress -= 1000>>
		<br><br>

		<<link [[Next|Drain Water]]>><<set $bus to "commercialdrain">><</link>>
	<</if>>
<</if>>