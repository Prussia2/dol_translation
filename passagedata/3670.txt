<<temple_effects>><<pass 60>><<effects>>

<<set $hallucinogen += 60>>
You sit on the cushion, and pray. The incense makes you feel light and dizzy.

<<if $rng gte 81>>
	<<if $NPCName[$NPCNameList.indexOf("Robin")].love gte 60 and random(1, 2) is 2>>
		Your thoughts turn to Robin.
			<<if $NPCName[$NPCNameList.indexOf("Robin")].trauma gte 21>>
				<<ltrauma>><<gstress>><<trauma -6>><<stress 6>>
			<<else>>
				<<ltrauma>><<lstress>><<trauma -6>><<stress -6>>
			<</if>>
	<<elseif $trauma gte 4000>>
		You think about how relentless people have been in their horrible pursuits. You realise you're surviving, despite it all.
		<<gawareness>><<awareness 1>>
	<<elseif $trauma gte 3000>>
		You think about all the horrible things people have done to you. It's oddly cathartic.
		<<lltrauma>><<trauma -12>>
	<<elseif $trauma gte 2000>>
		You think about all the pain you've endured. It's oddly cathartic.
		<<ltrauma>><<trauma -6>>
	<<elseif $trauma gte 1000>>
		You think about how horrible some people can be. You don't feel as vindictive as you expected.
		<<gpurity>><<purity 1>>
	<<else>>
		You think about how, despite everything, you've managed to look after yourself.
		<<lstress>><<stress -6>>
	<</if>>
<<elseif $rng gte 61>>
	<<if $NPCName[$NPCNameList.indexOf("Kylar")].love gte 60 and random(1, 2) is 2>>
		Your thoughts turn to Kylar.
			<<if $NPCName[$NPCNameList.indexOf("Kylar")].rage gte 60>>
				<<ltrauma>><<gstress>><<trauma -6>><<stress 6>>
			<<else>>
				<<ltrauma>><<lstress>><<trauma -6>><<stress -6>>
			<</if>>
	<<elseif $stress gte 8000>>
		You try to shut your eyes, but you find you need them open, watching the door. You realise how afraid you are of an assailant bursting in. You tell yourself the temple wouldn't allow that.
		<<gcontrol>><<control 10>>
	<<elseif $stress gte 6000>>
		You realise how quickly your heart is beating, even at rest. You calm down a little.
		<<llstress>><<stress -12>>
	<<elseif $stress gte 4000>>
		You feel your heart beat. It calms over time.
		<<lstress>><<stress -6>>
	<<elseif $stress gte 2000>>
		You find yourself worrying about being attacked on the way home. You tell yourself there's no sense worrying now.
		<<gcontrol>><<control 5>>
	<<else>>
		You feel at peace.
		<<ltrauma>><<trauma -6>>
	<</if>>
<<elseif $rng gte 41>>
	<<if $NPCName[$NPCNameList.indexOf("Whitney")].love gte 21 and random(1, 2) is 2>>
		Your thoughts turn to Whitney.
			<<if $NPCName[$NPCNameList.indexOf("Whitney")].dom gte 14>>
				<<ltrauma>><<gstress>><<trauma -6>><<stress 6>>
			<<else>>
				<<ltrauma>><<lstress>><<trauma -6>><<stress -6>>
			<</if>>
	<<elseif $arousal gte 8000>>
		Try as you might, you can't keep your thoughts from turning to sex.
		<<gstress>><<larousal>><<stress 6>><<arousal -6>>
	<<elseif $arousal gte 6000>>
		Thoughts of sex intrude. You struggle to keep them at bay.
		<<larousal>><<arousal 600>>
	<<elseif $arousal gte 4000>>
		Your thoughts keep turning to sex, but you manage to keep them at bay.
		<<larousal>><<arousal 600>>
	<<elseif $arousal gte 2000>>
		You struggle to keep your mind clear and focused.
	<<else>>
		You're able to keep your mind clear and focused.
		<<gwillpower>><<willpower 1>>
	<</if>>
<<elseif $rng gte 21>>
	<<if $NPCName[$NPCNameList.indexOf("Eden")].love gte 120 and random(1, 2) is 2>>
		Your thoughts turn to Eden.
		<<if $NPCName[$NPCNameList.indexOf("Eden")].lust gte 60>>
			<<ltrauma>><<gstress>><<trauma -6>><<stress 6>>
		<<else>>
			<<ltrauma>><<lstress>><<trauma -6>><<stress -6>>
		<</if>>
	<<elseif $control gte 800>>
		You feel safe, secure and at peace.
		<<gpurity>><<purity 1>><<ltrauma>><<lstress>><<trauma -6>><<stress -6>>
	<<elseif $control gte 600>>
		The thick stone walls reassure you.
		<<ltrauma>><<lstress>><<trauma -6>><<stress -6>>
	<<elseif $control gte 400>>
		You have trouble keeping your eyes closed in case something sneaks in.
		<<ltrauma>><<trauma -6>>
	<<elseif $control gte 200>>
		Fear dominates your mind, but your heart steadies and you feel less anxious over time.
		<<gcontrol>><<control 10>>
	<<else>>
		You struggle to keep panic at bay. Your heart steadies and you feel less anxious over time.
		<<gcontrol>><<control 10>>
	<</if>>
<<else>>
	<<if $NPCName[$NPCNameList.indexOf("Avery")].love gte 60 and random(1, 2) is 2>>
		Your thoughts turn to Avery.
		<<if $NPCName[$NPCNameList.indexOf("Avery")].rage gte 60>>
			<<ltrauma>><<gstress>><<trauma -6>><<stress 6>>
		<<else>>
			<<ltrauma>><<lstress>><<trauma -6>><<stress -6>>
		<</if>>
	<<elseif $awareness gte 800>>
		You feel some power press against you. Something unseen.
		<<ggwillpower>><<gtrauma>><<willpower 3>><<trauma 6>><<gstress>><<stress 6>>
	<<elseif $awareness gte 600>>
		You know this location isn't arbitrary. They built it here for a reason. You feel uneasy wondering what the priests know, and what they don't.
		<<gwillpower>><<willpower 1>><<gtrauma>><<trauma 6>>
	<<elseif $awareness gte 400>>
		You wonder if this room is as safe as it appears.
		<<gstress>><<stress 6>>
	<<elseif $awareness gte 200>>
		You follow the trail of incense as it swirls up from grates in the floor.
	<<else>>
		The incense swirls around you and makes pretty patterns.
		<<lstress>><<stress -6>>
	<</if>>
<</if>>
<br><br>

<<set $rng to random(1, 100)>>
<<if $rng is 100 or $rng gte 91 and $worn.neck.name is "holy pendant">>
	<span class="green">You feel a presence.</span> The incense takes on a pearly white hue. It brightens and deepens until you're surrounded by a sea of white.
	<br><br>
	You feel profoundly safe, like something unimaginably powerful yet benevolent protects you.
	<br><br>
	A warmth fills your mind.
	<br><br>

	<<link [[Embrace|Temple Prayer Holy Embrace]]>><<control 50>><<trauma -50>><<stress -50>><<awareness 12>><<purity 12>><</link>><<ggpurity>><<gggcontrol>><<llltrauma>><<lllstress>><<ggawareness>>
	<br>
	<<link [[Snap out of it|Temple Prayer Holy Snap]]>><<willpower 5>><<control 20>><<trauma -18>><<stress -18>><</link>><<ggwillpower>><<ggcontrol>><<lltrauma>><<llstress>>
	<br>
<<elseif $rng is 99 or $rng gte 91 and $worn.neck.name is "stone pendant">>
	<span class="blue">You feel a presence.</span> The incense thickens, until the walls around you disappear in the fog. You stop breathing.
	<br><br>

	Something observes you. Something big. You feel no malice from it, just curiosity.
	<br><br>

	The ground around you rises. Or perhaps you're sinking. You can't tell.
	<br><br>

	<<link [[Embrace|Temple Prayer Stone Embrace]]>><<arousal 5000>><<scienceskill 12>><<mathsskill 12>><<historyskill 12>><<purity 6>><<awareness 24>><</link>><<gpurity>><<gggawareness>><<gghistory>><<ggscience>><<ggmaths>><<gggarousal>>
	<br>
	<<link [[Snap out of it|Temple Prayer Stone Snap]]>><<historyskill 3>><<mathsskill 3>><<scienceskill 3>><<set $school += 9>><<willpower 5>><</link>><<ggwillpower>><<ghistory>><<gscience>><<gmaths>>
	<br>
<<elseif $rng is 98 or $rng gte 91 and $worn.neck.name is "dark pendant">>
	<span class="red">You feel a presence.</span> The torches on the walls flicker out, plunging you into pitch darkness. Some light should still be filtering through the high windows, but there's nothing.
	<br><br>

	You can no longer feel the cushion beneath you. You float in a void. You're not alone. Something rushes towards you across an unthinkable distance. A primal fear seizes your mind.
	<<ggstress>><<stress 6>>
	<br><br>

	<<link [[Embrace|Temple Prayer Dark Embrace]]>><<awareness 24>><<purity -36>><<willpower 12>><<stress 36>><</link>><<gggawareness>><<lllpurity>><<ggwillpower>><<gggstress>>
	<br>
	<<link [[Snap out of it|Temple Prayer Dark Snap]]>><<awareness 6>><<purity -6>><<willpower 6>><<stress 6>><</link>><<gawareness>><<lpurity>><<gwillpower>><<gstress>>
	<br>
<<elseif $rng gte 91>>
	You feel something, a presence, watching you. It only lasts a moment. You look around, nervous.
	<<gstress>><<stress 6>>
	<br><br>

	<<prayerend>>
<<elseif $rng gte 81>>
	The incense makes you giddy.
	<<lstress>><<stress -6>>
	<br><br>

	<<prayerend>>
<<elseif $rng gte 71>>
	The incense makes you warm.
	<<garousal>><<arousal 600>>
	<br><br>

	<<prayerend>>
<<elseif $rng gte 61>>
	The incense soothes you.
	<<ltrauma>><<trauma -6>>
	<br><br>

	<<prayerend>>
<<elseif $rng gte 51>>
	You hear birds chirping outside.
	<<lstress>><<stress -6>>
	<br><br>

	<<prayerend>>
<<elseif $rng gte 41>>
	The incense helps you focus.
	<<ggwillpower>><<willpower 3>>
	<br><br>

	<<prayerend>>
<<elseif $rng gte 31>>
	The incense clears your mind.
	<<larousal>><<arousal -6>>
	<br><br>

	<<prayerend>>
<<elseif $rng gte 21>>
	The incense makes you feel safe.
	<<gcontrol>><<control 10>>
	<br><br>

	<<prayerend>>
<<elseif $rng gte 12>>
	The incense tickles your skin, surrounding you in a gentle haze.
	<<set $hallucinogen += 60>>
	<br><br>

	<<prayerend>>
<<elseif $rng gte 9>>
	Your mind wanders to school. You think about your last maths lesson. You see the material in a new light.
	<<gmaths>><<mathsskill 3>>
	<br><br>

	<<prayerend>>
<<elseif $rng gte 6>>
	Your mind wanders to school. You think about your last English lesson. You see the material in a new light.
	<<genglish>><<englishskill 3>>
	<br><br>

	<<prayerend>>
<<elseif $rng gte 3>>
	Your mind wanders to school. You think about your last science lesson. You see the material in a new light.
	<<gscience>><<scienceskill 3>>
	<br><br>

	<<prayerend>>
<<else>>
	Your mind wanders to school. You think about your last history lesson. You see the material in a new light.
	<<ghistory>><<historyskill 3>>
	<br><br>

	<<prayerend>>
<</if>>