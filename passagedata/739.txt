<<effects>>

<<npc Alex>><<person1>>
You call for help. "I'm coming!" Alex shouts in response. <<He>> enters the sty and rushes over, before bonking the <<farm_text pig>> on the head with <<his>> broom. The <<farm_text pig>> releases your $worn.lower.name and retreats into the middle of the pen.
<br><br>

"They'll eat anything," Alex laughs. "It pays to wear something sturdy."
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>