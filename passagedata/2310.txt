<<set $outside to 1>><<set $location to "park">><<effects>>

<<generate2>><<generate3>>

You arrive at the river. Beside it sits a wooden crane. A taut rope connects the top to wooden chair Winter showed you earlier. A <<person2>><<person>> adjusts the rope, while a <<person3>><<person>> kneels beside its base.
<br><br>

"Just a few adjustments," the <<person>> says to Winter. "Then we'll be set."
<br><br>
<<person1>>
Winter claps <<his>> hands together and turns to you. "Your role is to sit in the chair as it is dunked into the river. I'll operate the crane and talk the audience through the history of the device.
<br><br>

"You'll be tied down," <<he>> continues. "But we don't need to submerge you completely. Expect to get wet. I have a suitable garment in mind, so you don't need to worry about that."
<br><br>

You return to the museum with Winter. <<He>> seems excited.
<br><br>

<<link [[Next|Museum]]>><<endevent>><</link>>
<br>