<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<endevent>>

<<npc Leighton>><<person1>>Leighton pushes through the crowd. "There's nothing to see here," <<he>> says. "I'm sure you have better places to be." The crowd disperses. Some students sneak pictures from the wall and hide them in their bags.
<br><br>

Leighton starts tearing them down, but <<he>> puts them in <<his>> own pocket. <<He>> doesn't acknowledge you. <<He>> removes the last of the pictures and leaves you stood alone in the corridor.
<br><br>

<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
<br>