<<effects>>

You follow Avery's lead. <<Hes>> a good dancer, and you soon attract a small audience. You're out of breath when the music concludes. Your audience applauds. Avery holds you close as <<he>> bows, <<his>> hand resting on your thigh. <<danceskilluse>>
<br><br>

You and Avery continue to dance until the party winds down. Avery bids <<his>> acquaintances farewell, and together you leave the mansion.
<br><br>

<<link [[Next|Avery Party End]]>><</link>>