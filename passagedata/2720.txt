<<set $outside to 0>><<set $location to "pool">><<schooleffects>><<effects>>

You decide to just get it over with.

<<if $leftarm is "bound" and $rightarm is "bound">>
Bound and unable to cover yourself, you instead face away from your audience. You jump backwards into the water, which is not what you are supposed to do but Mason won't fault you given the circumstance.
<<else>>
You cover your <<lewdness>> as you approach the platform, intensely aware of all the eyes probing you. You jump into the water, not a spectacular performance but Mason won't fault you given the circumstance.
<</if>>
Once concealed beneath the water, Mason manages to regain some control over the class, and pulls most attention away from you.
<br><br>
<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
<br>