<<effects>>
While you're stopping sooner than <<he>> would like, <<he>> looks relieved as <<he>> returns <<his>> attention back to <<his>> kids. They don't seem to have noticed anything.
<br><br>

<<clotheson>>

<<endcombat>>

[[Next|Beach]]