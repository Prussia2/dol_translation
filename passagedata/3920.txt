<<set $outside to 0>><<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>><<npcincr Kylar lust -20>>
	<<He>> lies on top of you for a few moments, eyes closed in contentment. It's ended by the sound of voices from the corridor outside, making <<him>> tense once more.
	<br><br>

	<<tearful>> you sit upright, your arms around <<him>>.
	<br><br>
	<<clotheson>>
	<<endcombat>>

	<<npc Kylar>><<person1>>

	<<if $weather is "rain">>
		<<link [[Walk to the arcade together (0:15)|Kylar Arcade Walk]]>><<pass 15>><</link>>
		<br>
	<<else>>
		<<link [[Walk to the park together (0:15)|Kylar Park Walk]]>><<pass 15>><</link>>
		<br>
	<</if>>

	<<link [[Bid Kylar goodbye|Kylar Orphanage Leave]]>><</link>>
	<br>
<<elseif $enemyhealth lte 0>>
	Kylar tumbles from the bed. <<He>> climbs to <<his>> feet, <<his>> face a mix of anger and longing. <<His>> nerves get the better of <<him>>, and <<he>> bolts from the room.<<lllove>><<npcincr Kylar love -3>>
	<br><br>
	<<tearful>> you sit upright on the bed.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Bedroom]]>><</link>>
	<br>
<<else>>
	"D-did I do something wrong?" Kylar asks as <<he>> pulls away from you. <<His>> nerves get the better of <<him>>, and <<he>> bolts from the room.
	<br><br>
	<<tearful>> you sit upright on the bed.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Bedroom]]>><</link>>
	<br>
<</if>>