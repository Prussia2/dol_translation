<<set $outside to 0>><<effects>>
<<npc Leighton>><<person1>><<generate2>><<generate3>>
The dressing room is empty, save a <<if $pronoun is "m">>man<<else>>woman<</if>> examining a rack of costumes, <<his>> back turned. It's Leighton.
<br><br>

<<link [[Confront|Leighton Club Confront]]>><</link>>
<br>
<<link [[Hide (0:03)|Leighton Club Hide]]>><</link>>
<br>