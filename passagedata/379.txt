<<set $outside to 0>><<set $location to "cabin">><<effects>>

<<if $forestleashed is 1>>
Eden is asleep, but with your neck leashed tightly to the bed, you can't move very far.
<br><br>

<<link [[Sleep|Cabin Night Sleep]]>><<cabintime>><</link>><<ltiredness>>
<br>
<<link [[Weaken the leash|Cabin Night Leash]]>><<cabintime>><</link>>
<br>
<<else>>
Eden is asleep. You could escape if you wanted.
<br><br>

<<link [[Sleep|Cabin Night Sleep]]>><<cabintime>><</link>><<ltiredness>>
<br>
<<link [[Escape|Cabin Night Escape]]>><<set $stress -= 2000>><</link>>
<br>
<</if>>