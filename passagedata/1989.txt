<<effects>>

The <<person2>><<person>> makes another attempt to get to the <<person3>><<personcomma>> but you stand in <<person2>><<his>> way. <<He>> gives up, spitting more insults as <<he>> leaves.
<br><br>

"Thank you," the <<person3>><<person>> says as <<he>> fixes <<his>> clothing. "I woke up and <<person2>><<he>><<person3>> was on top of me. I was so scared." <<He>> hugs you.
<br><br>

<<endevent>>

<<link [[Next|Orphanage]]>><</link>>
<br>