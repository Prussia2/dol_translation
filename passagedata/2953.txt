<<effects>><<set $location to "moor">>

Bailey glances at you, then turns to the <<person2>><<personstop>> "Five thousand."
<br><br>

"That's right," the <<person>> replies, holding out <<his>> hand. One of Bailey's goons, a <<person3>><<personcomma>> approaches you, unlocks the chain tying you to the bed, and pulls you to your feet.<<if $worn.feet.name is "ankle cuffs">><<set $worn.feet.type.push("broken")>><<feetruined>><</if>>
<br><br>

"You misunderstand," Bailey says. The goons flanking <<person1>><<himcomma>> a <<person4>><<person>> and <<person5>><<personcomma>> pull pistols from their jackets. "You used my property. You're paying for it."
<br><br>

The <<person2>><<person>> gapes. "You're a brave fucker," <<he>> spits. <<Hes>> incensed, but <<he>> looks around the room. <<His>> colleagues stand way back, their hands in the air. <<He>> sighs, then gestures where the table stood.
<br><br>

<<person1>>Bailey walks over, lifts a decaying rug, and opens the trap door <<he>> finds beneath. <<He>> pulls out a rucksack. Inside are rolls of banknotes. <<He>> counts them, then stuffs about half into <<his>> pockets. <<He>> drops the bag back through the hole.
<br><br>

"Five thousand." <<He>> repeats. "Good doing business with you."
<br><br>

<<link [[Next (0:30)|Maths Abduction 6]]>><</link>>
<br>