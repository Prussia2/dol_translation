<<set $outside to 0>><<set $location to "town">><<effects>>
Feeling safe, you cry in <<his>> arms. As the sobs subside you share some of the horrible things people have done to you. It's painful to talk about, but you feel cathartic afterwards.
<<control 25>>
<br><br>
Doren pulls away from you and smiles. "I need to get back to school or I'll be in trouble." You look at the clock on the wall and realise a whole hour has passed. "I'm not home often, but if you visit between four and five in the afternoon I should be here. Visit as often as you like. Every day if you want. I mean it."
<br><br>
<<He>> drives you back to school and waves goodbye in the front playground. You feel guilty for having spent so much of <<his>> time, but <<he>> doesn't seem to mind.
<br><br>
<<endevent>>
<<link [[Return to school (0:05)|School Front Playground]]>><<pass 5>><</link>>
<br>