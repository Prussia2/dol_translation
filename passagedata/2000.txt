<<effects>>

You sit beside the <<person1>><<personstop>>

<<if $submissive gte 1150>>
"I think <<person2>><<he>> likes you," you say. "You should say hello."
<<elseif $submissive lte 850>>
"Don't be a wimp," you say. "Go talk to <<person2>><<himstop>>"
<<else>>
"You should say something," you say.
<</if>>
<br><br>
The <<person1>><<person>> gapes at you. "I couldn't do that," <<he>> says.
<br><br>

<<if $submissive gte 1150>>
"But <<person2>><<he>> might think you don't like <<himstop>>" you add.
<br><br>

The <<person1>><<person>> hesitates a moment. Then <<he>> clenches <<his>> fists, stands, and marches over. The <<person2>><<person>> and <<his>> friends stop talking and face the newcomer. You can't hear what <<person1>><<hes>> saying, but the <<person2>><<person>> blushes, grabs the <<person1>><<persons>> arm, and together they run from the room.
<<elseif $submissive lte 850>>
"Fine," you reply. You stand and march over to the <<person2>><<personstop>>
<br><br>
"Wait!" the <<person1>><<person>> whispers behind you. You ignore <<himstop>>
<br><br>
The <<person2>><<person>> and <<his>> friends cut their conversation short and look at you. "My friend over there's into you," you say. The <<person1>><<persons>> face is mostly hidden by <<his>> book, but you can tell <<hes>> blushing.
<br><br>

The <<person2>><<person>> starts blushing too, but <<he>> walks over and sits beside the <<person1>><<personstop>>
<br><br>

You can't make out what they're saying, but the <<person2>><<person>> grasps the <<person1>><<persons>> arm, and together they run from the room.
<<else>>
"Why not?" you ask. <<person2>>"<<He>> likes you too."
<br><br>

The <<person1>><<person>> hesitates a moment. Then <<he>> clenches <<his>> fists, stands, and marches over. The <<person2>><<person>> and <<his>> friends stop talking and face the newcomer. You can't hear what <<person1>><<hes>> saying, but the <<person2>><<person>> blushes, grabs the <<person1>><<persons>> arm, and together they run from the room.
<</if>>
<br><br>

<<link [[Next|Orphanage]]>><<endevent>><</link>>
<br>