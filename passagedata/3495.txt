<<set $outside to 0>><<set $location to "strip_club">><<effects>>

Security arrives in the form of a <<person6>><<personcomma>> and interposes <<himself>> between you and the onlookers. The crowd tries to shove past, but backup soon arrives.
<br><br>

The crowd disperses, leaving you with the <<person1>><<personstop>> <<Hes>> wearing everyday clothes. "Thought I could return the favour," <<he>> says. "They ate your routine right up. Tipped generously. Was gonna share some with you, but looks like you've outdone me." <<He>> gestures at the money littering the counter. <<He>> leaves without another word.
<br><br>

<<set $tipmod to 1.0>>
<<tipset "serving">>
<<set $tip += 500>>
<<if $drinksservedstat gte 10000>>
<<set $tip += 200>>
<</if>>
<<tipreceive>>
<br><br>

<<clotheson>>

<<link [[Next|Strip Club]]>><<endevent>><<pass 30>><</link>>