<<effects>>
<<pass 15>>
You practically hang off Avery's arm. You laugh at <<his>> jokes, and the jokes of those <<he>> talks to. When someone addresses you directly, you look to Avery as if unsure, and let <<him>> speak for you. You think <<he>> likes it this way.
<br><br>

A servant offers you a drink. Avery nods at you.
<br><br>

<<link [[Drink|Avery Party Drink]]>><<set $drunk += 120>><<set $endear += 10>><</link>><<gendear>>
<br>
<<link [[Refuse|Avery Party Drink Refuse]]>><</link>><<set $endear -= 5>><<lendear>>
<br>