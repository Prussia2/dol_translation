<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<beastejaculation>>
	The wolf returns to the forest. <<tearful>> you hide behind a hedge.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Temple Garden]]>><</link>>
	<br>
<<elseif $enemyhealth lte 0>>
	The wolf flees back into the forest <<tearful>> you hide behind a hedge.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Temple Garden]]>><</link>>
<<else>>
	<<generate1>><<person1>>
	The wolf's ears prick. A moment later you hear it yourself. Footsteps.
	<br><br>
	<<if $current_sex is 1>>
		The wolf tries to flee, but can't pull away from you. The footsteps get closer. You struggle to pull away, but it's no use. A <<monk>> rounds a corner while you're still stuck together.
		<br><br>
		<<He>> stops and stares at you. <<covered>> <<He>> tries to speak, but can't find the words. <<He>> leans again a sundial to steady <<himself>> as the wolf finally pulls free and runs for the forest.
		<br><br>
		"Ab-" <<he>> manages, stopping and steadying <<himself>> again. "Absolutely unacceptable. The bishop will hear of this." <<He>> pulls some towels from <<his>> habit, drops them on top of you, and marches away.
		<br><br>
		<<tearful>> you cover yourself.
		<br><br>
		<<clothesontowel>>
		<<endcombat>>
		<<link [[Next|Temple Garden]]>><</link>>
	<<else>>
		The wolf runs towards the forest, just before a <<monk>> rounds a hedge. "Well I never," <<he>> says, regarding you. <<covered>> <<He>> averts <<his>> eyes and rummages beneath <<his>> habit. <<He>> pulls out some towels and throws them atop you. <<He>> walks away, tutting.
		<<llgrace>><<gtrauma>><<gstress>><<grace -5>><<trauma 6>><<stress 6>>
		<br><br>
		<<tearful>> you cover yourself.
		<br><br>
		<<clothesontowel>>
		<<endcombat>>
		<<link [[Next|Temple Garden]]>><</link>>
	<</if>>
<</if>>