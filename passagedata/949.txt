<<set $outside to 1>><<set $location to "farm">><<effects>><<set $bus to "farmroad6">>
<<if $phase is 1>>
	<<set $phase to 0>>
	You walk deeper into the countryside. The land becomes greener and more organised, split by hedges into separate fields.<<physique 3>>
<<elseif $phase is 2>>
	<<set $phase to 0>>
	You walk towards town. Fields still flank the road on both sides.<<physique 3>>
<<else>>
	You are on the road that connects the town and surrounding farmlands. Fields flank the road on both sides.
<</if>>
<br><br>
<<set $danger to random(1, 10000)>>
<<if $stress gte 10000>>
	<<passoutfarmroad>>
<<elseif $danger gte (9900 - $allure) and $eventskip is 0>>
	<<if $rng gte 51>>
		<<if $monsterchance gte 1 and $hallucinations gte 1 or $monsterchance gte 1 and $monsterhallucinations is "f">>
			<<if $malechance lt random(1, 100)>>
				A naked girl with cow ears, horns and tail stops grazing and walks up to the fence around her field. She moos at you.
				<br><br>
			<<else>>
				A naked boy with bull ears, horns and tail stops grazing and walks up to the fence around his field. He moos at you.
				<br><br>
			<</if>>
		<<else>>
			A cow stops grazing and walks up to the fence around its field. It moos at you.
			<br><br>
		<</if>>
		<<link [[Pet|Farm Road Pet]]>><<stress -6>><</link>><<lstress>>
		<br>
		<<link [[Ignore|Farm Road 6]]>><</link>>
		<br>
	<<else>>
		<<hitchhike>>
	<</if>>
<<else>>
	<<if $exposed lte 0>>
		<<link [[Hitchhike (0:15)|Farm Hitchhike]]>><<pass 15>><</link>>
		<br>
	<</if>>
	<<link [[Enter the farmlands (0:05)|Farmland]]>><<pass 5>><</link>>
	<br>
	<<link [[Walk towards town (0:30)|Farm Road 5]]>><<pass 30>><<set $phase to 2>><<tiredness 3>><</link>><<gtiredness>>
	<br>
<</if>>
<<set $eventskip to 0>>