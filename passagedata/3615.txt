<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
<<npc Jordan>><<person1>>You take a seat at the end of a pew as Jordan reads holy scripture. <<His>> words are violent and accusing.
<br><br>
You are called to the front for communion. You kneel in front of Jordan, but the other <<if $pronoun is "m">>monks<<else>>nuns<</if>> refuse to look at you. The bread tastes like ash. The wine like blood.
<br><br>
Jordan finishes with a prayer.
<br><br>
<<endevent>>
<<link [[Next|Temple]]>><</link>>
<br>