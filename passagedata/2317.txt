<<effects>>

You are plunged once more into the cold water. 
<<if $swimmingskill gte random(600, 1400)>>
	You hold your breath, <span class="green"> and manage to remain calm.</span> You hear muffled but excited voices above the water.
	<br><br>
	<<earnFeat "Submerged">>
	<<if $museumAntiques.antiques.antiquesilvermask isnot "found" and $museumAntiques.antiques.antiquesilvermask isnot "talk" and $museumAntiques.antiques.antiquesilvermask isnot "museum">>
		Something <span class="gold">glints</span> in the riverbed, poking from the silt beside your feet. You nudge it free. <span class="gold">A metal mask stares up at you,</span> its face contorted into a sinister smile. It's tarnished, but silver glimmers through in places.
		<br><br>
		
		You feel the chair lurch into movement once more. You grab the mask between your feet as you rise.
		<br><br>
		
		The applause can be heard even before you break the surface. You seize a sharp breath and blink away the water. The audience look impressed.
		<br><br>
		
		<<link [[Next|Museum Duck Extreme Antique]]>><</link>>
		<br>
	
	<<else>>
		A small fish swims by.
		<br><br>
		Even in your calm state you feel your breath running out, but then you feel the chair lurch into movement once more. The applause can be heard before you even break the surface. You seize a sharp breath and blink away the water. The audience look impressed.
		<br><br>
		
		Winter rushes over once your feet touch the earth. <<He>> steadies the chair and removes the straps binding you. "That ends our demonstration," <<he>> says. "Thank you for attending. We hope you found it informative, and we invite you to visit the museum. The address is on the booklets we handed out. It's right over there." <<He>> points. "Last but not least, let's have a round of applause for our star." <<He>> gestures at you. The applause becomes deafening. Heads turn around the park, perplexed at the celebration.
		<br><br>
		
		Winter wraps a heated towel around you, and then a second, for the journey back to the museum. "You did very well," <<he>> says on the way back. "I'm blessed to have your assistance, truly. Thank you. I was almost afraid I'd gone too far, but I'd like to repeat the demonstration next weekend if it suits you." <<He>> glances back. Most of the audience is following. "I think we've earned some new interest."
		<br><br>

		You arrive at the museum. <<He>> takes you to a small side room, and leaves you to get dressed.
		<br><br>

		That was terrifying, <span class="green">yet you feel a strong catharsis.</span><<trauma -24>>
		<br><br>
		
		<<endevent>>
		<<set $museuminterest += 50>>
		<<link [[Next|Museum]]>><<unbind>><<clotheson>><</link>>
		<br>
	<</if>>
<<else>>
	You hold your breath as best you can, but you soon feel panic build within you. What if something has gone wrong? <span class="red">You press the buzzer.</span>
	<br><br>
	
	Your head breaks the water, and you seize a sharp breath. The chair continues to rise, then move back over the earth, before descending. Despite the premature end, the applause is energetic
	<br><br>

	Winter rushes over to steady the chair and unbind you. "That ends our demonstration," <<he>> says. "Thank you for attending. We hope you found it informative, and we invite you to visit the museum. The address is on the booklets we handed out. It's right over there." <<He>> points. "Last but not least, let's have a round of applause for our star." <<He>> gestures at you. The applause renews, punctuated by the occasional cheer. Heads turn around the park, perplexed at the celebration.
	<br><br>

	Winter wraps a heated towel around you, and then a second, for the journey back to the museum. "You did well," <<he>> says on the way back. "I'm blessed to have your assistance. Thank you. I'd like to repeat the demonstration next weekend if it suits you." <<He>> glances back. Much of the audience is following. "I think we've earned some new interest."
	<br><br>

	You arrive at the museum. <<He>> takes you to a small side room, and leaves you to get dressed.
	<br><br>

	That was terrifying, <span class="green">yet you feel a strong catharsis.</span><<trauma -24>>
	<br><br>
	<<endevent>>
	<<set $museuminterest += 50>>
	<<link [[Next|Museum]]>><<unbind>><<clotheson>><</link>>
	<br>
<</if>>