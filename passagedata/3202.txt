<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "cliff">>
<<get_pillory_npc>><<person1>>
<<if $phase is 0>>
	A box of rotten fruit lies nearby. You grab a piece and weigh it in your hand.
	<br>
	You throw it at <<the_pillory_person>>
	<<if $rng gte 75>>
	<<set $pillory_tenant.fruit += 1>>
		<span class="teal">hitting <<him>> full in the face.</span> <<His>> expression is hilarious.
		<<if $physique gte ($physiquesize / 7) * 4>>You put a lot of strength behind it, it must have hurt.<</if>>
		<<llstress>><<stress -12>>
		<br>
		The crowd cheer and laugh.
	<<else>>
		but miss entirely.<<if $physique lte ($physiquesize / 7) * 3>> Some people nearby mock your feeble throw.<</if>>
	<</if>>
<<elseif $phase is 1>>
	You slurp up the cream from the <<the_pillory_persons>> ass and groin. The crowd watch, awed.
	<<set $famesex +=20>>
<<elseif $phase is 2>>
<<set $pillory_tenant.spank += 1>><<control 10>><<stress -24>><<trauma -12>>
	You <<print either("viciously","resentfully","grimly","self-righteously","maniacally")>> spank <<the_pillory_persons>> ass. Somehow,
	<<print either("it gives you a sense of control and well-being.","it brings a growing sense of calm.","it makes the shadows draw back.")>>
	<br>
	Some of the crowd cheer you on.
<<elseif $phase is 3>><<control 10>><<stress -24>><<trauma -12>>
	Approaching <<the_pillory_person>> from behind, you <<print either("smoothly","awkwardly","quickly")>>
	<<if $pronoun is "m">>
		<<if $pillory_tenant.upperexposed lte 1>>
			<<set $pillory_tenant.upperexposed to 2>>displace <<his>> top, fully exposing <<his>> chest
			<<if $pillory_tenant.lowerexposed lte 1>>and then<</if>>
		<</if>>
		<<if $pillory_tenant.lowerexposed lte 1>>pull <<his>>
			<<if $pillory_tenant.lowerexposed is 0>>trousers and<</if>>
			underpants to the floor, exposing <<his>> ass and <<his>> $pillory_tenant.person.penisdesc
			<<set $pillory_tenant.lowerexposed to 2>>
		<</if>>
	<<elseif $pronoun is "f">>
		<<if $pillory_tenant.upperexposed lte 2>>
			<<if $pillory_tenant.upperexposed lte 0>>displace <<his>> top and<</if>>
			remove <<his>> bra, fully exposing <<his>> $pillory_tenant.person.breastsdesc
			<<if $pillory_tenant.lowerexposed lte 1>>to all. Without hesitation, you then<</if>>
			<<set $pillory_tenant.upperexposed to 3>>
		<</if>>
		<<if $pillory_tenant.lowerexposed lte 1>>
			<<if $pillory_tenant.lowerexposed lte 0>>hitch up <<his>> skirt and pull <<his>> panties to the floor,
			<<else>>pull <<the_pillory_persons>> panties to the floor,<</if>>
			fully exposing <<his>> pussy and ass
			<<set $pillory_tenant.lowerexposed to 2>>
		<</if>>
	<<else>> /* in case the game goes fully non binary */
		tear off all their clothes, fully exposing them
	<</if>>to the watching crowd. <<He>> yells in shock.
	<br>

	Some people <<print either("gasp, but","laugh, and","complain, but","fall silent, but","whistle, and","jeer, and","make lewd threats, and")>> more than a few cheer.
<<elseif $phase is 4>><<control 5>><<stress -20>><<trauma -4>><<set $pillory_tenant.broken += 1>>
	Approaching from behind, you <<print either("cheekily","aggressively","quickly","vindictively","stealthily")>>
	grab <<the_pillory_persons>> <<if $pronoun is "m">>underpants<<else>>panties<</if>> and hoist them up hard.
	<br>
	<<His>> shocked squeal makes you laugh as you run off. As you rejoin the crowd, <<his>> sour expression makes you laugh again -
	stuck in the pillory, <<he>> can't even fix it. <<He>> knows this and so does everyone watching.
	<br>
	Many in the crowd seem to share in your amusement.
<<else>>
	You do something entirely unexpected. Few could explain what you have done. Fewer still could explain why. None will speak of this again. (Error: please report)
<</if>>
<<endevent>>
<br><br>
<<link [[Next|Cliff Street Pillory]]>><</link>>
<br>