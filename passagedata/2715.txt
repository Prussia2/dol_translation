<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
<<ejaculation>>

The pair come to their senses after a moment, and swim away from you, looking a little guilty.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Swimming Lesson]]>><</link>>

<<elseif $enemyhealth lte 0>>

The <<person1>><<person>> recoils in pain and glares at you. <<He>> swims away, taking the <<person2>><<person>><<person1>> with <<himstop>>
<br><br>

<<tearful>> you gather yourself.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Swimming Lesson]]>><</link>>

<<elseif $alarm is 1 and $rescue is 1>>
<<set $rescued += 1>>

	<<if $delinquency gte 200>>
	Your cry attracts Mason's attention. "I knew it would have something to do with you. Stop harassing the other students." The teacher gives your protests no heed.
	<<gdelinquency>><<detention 1>>
	<<else>>
	Your cry attracts Mason's attention. The teacher glares at the pair and herds them away from you.
	<</if>>
	<br><br>

<<tearful>> you gather yourself.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Swimming Lesson]]>><</link>>

<<else>>

Mason finishes breaking up the fight, and the pair leave you alone.
<br><br>

<<tearful>> you gather yourself.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Swimming Lesson]]>><</link>>
<</if>>