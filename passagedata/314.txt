<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
<<set $seductiondifficulty to 6000>>
<<seductioncheck>>
<br><br>
<<if $seductionskill lt 1000>><span class="gold">You feel more confident in your powers of seduction.</span><</if>><<seductionskilluse>>
<br><br>
You lean closer to the <<personstop>> "There are a lot of quiet places here," you whisper. "Who knows what one could get up to."
<<promiscuity2>>
<br><br>
<<if $seductionrating gte $seductionrequired>>
	<<He>> glances around, and takes you by the hand. <<He>> leads you to an empty room, then turns and embraces you.
	<br><br>
	<<link [[Next|Asylum Sex]]>><<set $sexstart to 1>><</link>>
	<br>
<<else>>
	"I-It was nice meeting you," <<he>> says. <<He>> walks away.
	<br><br>
	<<endevent>>
	<<link [[Next|Asylum]]>><</link>>
	<br>
<</if>>