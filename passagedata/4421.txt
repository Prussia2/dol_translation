<<effects>><<set $location to "sewers">><<set $outside to 0>><<set $bus to "sewerscommercial">>
<<set $sewersevent -= 1>>
You are in the old sewers. A canal runs past, disappearing into the dark. Piles of rubbish line the sides. The current looks rough.
<br><br>
<<if $sewerschased is 1>>
	There's a ladder leading up to the town's drain system, but it's retracted and out of reach.
	<br><br>
<<else>>
	There's a ladder leading up to the town's drain system.
	<br><br>
<</if>>
<<if $stress gte 10000>>
	It's too much for you. You pass out.
	<br><br>
	<<sewersend>>
	<<sewerspassout>>
<<elseif $sewerschased is 1 and $sewerschasedstep lte 0>>
	<<morgancaught>>
<<elseif $sewersevent lte 0 and $sewerschased isnot 1>>
	<<eventssewers>>
<<else>>
	<<if $sewerschased is 1 and $sewerschasedstep gte 0>>
		<<morganhunt>>
		<br><br>
	<</if>>
	<<link [[Muddy tunnel (0:05)|Sewers Mud]]>><<pass 5>><</link>>
	<br>
	<<link [[Ruined tunnel (0:05)|Sewers Ruins]]>><<pass 5>><</link>>
	<br>
	<<link [[Swim against the current (0:05)|Sewers Commercial Swim]]>><<pass 5>><<tiredness 1>><</link>><<swimmingdifficulty 1 400>><<gtiredness>>
	<br>
	<<link [[Swim with the current (0:01)|Sewers Residential]]>><<pass 1>><<water>><</link>>
	<br><br>
	<<if $sewerschased isnot 1>>
		<<link [[Climb out of the sewers|Commercial Drain]]>><<sewersend>><</link>>
		<br>
	<</if>>
<</if>>
<<set $eventskip to 0>>