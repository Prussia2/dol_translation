<<effects>>
<<earnFeat "Harper's Bane">>
You spit the pink fluid into Harper's mouth. <<He>> pulls away and wipes <<his>> chin. Almost at once, however, a change comes over <<himstop>> <<He>> was already flushed, but there was a restraint there. Now, there's nothing but a mad thirst. <<He>> pounces on you, tackling you to the floor.
<br><br>

The audience crowd around, glad the show isn't over.
<br><br>

<<link [[Next|Livestock Job Harper 2 Rape]]>><<set $molestationstart to 1>><</link>>
<br>