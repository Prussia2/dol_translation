<<effects>><<set $location to "sewers">><<set $outside to 0>>
<<set $sewerschased to 0>>
<<set $sewersevent to random(5, 12)>>
<<npc Morgan>><<person1>>You climb into the remains on an ancient building and crouch. Morgan walks by, <<his>> head scanning this way and that. "Insolent child," <<he>> says. "<<if $pronoun is "m">>Daddy<<else>>Mummy<</if>> has needs." <<He>> passes through. You're safe for now.
<<lstress>><<stress -6>>
<br><br>
<<endevent>>
<<destinationsewers>>