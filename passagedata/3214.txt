<<set $outside to 1>><<effects>>
<<if $phase is 0>>
	You take cover under a parked car. The footsteps grow louder and you soon see a pair of feet march past. Heart racing, you wait for the footsteps to fade before continuing.
	<<garousal>>
	<br><br>
	<<arousal 50>>
	<<endevent>>
	<<destinationeventend>>
<</if>>
<<if $phase is 1>>
	<<if $rng lte 80>>
		<<arousal 50>>
		You keep to the side of the road opposite the footsteps and soon see a <<person>> step into the glow of a street light. Fortunately they seem in a hurry and don't notice your skulking. Your heart continues to beat rapidly.
		<<garousal>>
		<br><br>
		<<endevent>>
		<<destinationeventend>>
	<<else>>
		<<arousal 50>>
		You keep to the side of the road opposite the footsteps and soon see a <<person>> step into the glow of a street light.
		<br><br>
		Unfortunately the darkness isn't as concealing as you hoped. <<He>> spots you in your embarrassing state of dress. Ashamed, you break into a run, followed by <<his>> whistling.
		<<trauma 2>><<garousal>><<gtrauma>>
		<br><br>
		<<endevent>>
		<<destinationeventend>>
	<</if>>
<</if>>