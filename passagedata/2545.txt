<<set $outside to 0>><<schooleffects>><<effects>>
<<npc Leighton>><<person1>>
Leighton's voice appears behind the group. It's not long before the crowd has dissipated. The headteacher arrives beside you, and stares down. <<He>> found your clothes somewhere, and drops them on top of you. <<He>> walks away, smiling.
<<set $genderknown.pushUnique("Leighton")>>
<br><br>
<<clothesontowel>>
<<endevent>>
<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
<br>