<<set $outside to 0>><<set $location to "cafe">><<effects>>
You make eye contact with the <<personcomma>> but don't stop licking the plate. You slow your pace and exaggerate your movements, letting <<him>> see the cream on your tongue from each lick before you take it into your mouth. You half-close your eyes in feigned rapture and keep licking until your plate is perfectly clean.
<<exhibitionism1>>
You lean back, satisfied. The <<person>> looks away, <<his>> face bright red.
<br><br>
<<endevent>>
<<link [[Next|Ocean Breeze]]>><</link>>
<br>