<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "barb">>

<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $stress gte 10000>>
	<<passoutstreet>>
<<elseif $danger gte (9900 - $allure) and $eventskip is 0>>

<<else>>
	<<if $map.top is true>>
		<<map "barb">>
		<br>
	<</if>>

	<<add_link "Travel<br>">><<hideDisplay>>
	<<domusexposed>>
	<<cliff>>
	<<connudatus>>
	<<if $exposed lte 0>>
		<<buswait>>
	<</if>>
	<<loiter>>

	<<add_link "<br>Alternate Routes<br>">><<hideDisplay>>
	<<residential>>
	<<stormdrain>>
	<br>
	<<displayLinks>>
	<<if $map.top isnot true>>
		<br>
		<<map "barb" "exposed">>
	<</if>>

<</if>>

<<set $eventskip to 0>>