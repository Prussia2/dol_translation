<<effects>>

<<if $submissive gte 1150>>
	"Y-you're wrong," you say. "Please don't imply such awful things."
<<elseif $submissive lte 850>>
	"I don't like what you're implying," you manage. "Buy something or fuck off."
<<else>>
	"You're mistaken," you say. "Can I interest you in some local produce?"
<</if>>
<br><br>

"I know a lewd <<girl>> when I see one," <<he>> says. "I'll see you around."
<<glewdity>><<set $stall_rejected += 1>>
<br><br>

<<stall_actions>>