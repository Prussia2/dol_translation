<<set $outside to 0>><<set $location to "cafe">><<effects>>

You wait as the remaining guests take their seats. It's a large room, and the tables have been packed together quite tightly. Others watch from the second floor, glasses in hand. There are a lot of people here, all dressed for the fanciest of occasions.
<br><br>

<<npc Quinn>><<person1>>Quinn steps up to the podium, and the room falls silent.
<br><br>

"Good evening, ladies and gentlemen," <<he>> says. "It's my privilege as mayor..." <<He>> talks for a while, about investment opportunities in the town, and <<his>> successes in office. Your mind wanders, but snaps back to attention when <<he>> gestures at you.
<br><br>

<<link [[Next|Chef Opening 10]]>><</link>>