<<effects>>

You open your mouth wider, allowing the <<if $pronoun is "m">>man's<<else>>woman's<</if>> fingers to explore as they will. <<He>> grasps your tongue between thumb and forefinger, and gives it a gentle tug. <<He>> seems satisfied as <<he>> steps away.
<br><br>

"I'll process it and add it to the herd," <<he>> says, taking your leash. "It seems docile enough."
<br><br>

<<if $exposed lte 1 or $worn.head.name isnot "naked" or $worn.face.name isnot "naked" or $worn.legs.name isnot "naked" or $worn.feet.name isnot "naked">>
	Hands intrude from behind, grasping your clothes, <span class="red">they tear the fabric from your body.</span>
	<<upperruined>><<lowerruined>><<underupperruined>><<underruined>><<headruined>><<faceruined>><<legsruined>><<feetruined>><<set $genderknown.pushUnique("Remy")>>
	<br><br>
<</if>>

<<link [[Next|Livestock Intro Light]]>><</link>>
<br>