<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "danube">>

You attempt to pick the ancient mechanical lock system.
<br><br>

<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

	Despite the solid, hardwood door, the lock is a very simple 3 lever-tumbler type. Apply torque, poke around a little - 1, 2, 3 - and open it pops. The most difficult part is keeping the noise down as you drag the door open.
	<br><br>
	Inside you find a well-stocked wine-cellar. You won't be able to carry much, so you make a quick assessment. You know little about wine, but <<if $pubintro is 1>>Landry has told you years and labels to look for. Also, <</if>>the way the wine is positioned gives you some clues as to its worth. You identify a couple of the most expensive-looking bottles.
	<br><br>

	<<link [[Take Bottles|Danube House Wine Steal]]>><<crimeup 150>><</link>><<crime>>
	<br>
	<<link [[Leave|Danube Street]]>><</link>>
	<br>

<<else>>

	You've never understood these old locks and can't do anything there. The door is completely solid and doesn't budge at all. You look around a little for any keys lying around, but no luck there either.
	<br><br>
	You leave empty-handed.
	<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>

	<<link [[Next|Danube Street]]>><</link>>
	<br>

<</if>>